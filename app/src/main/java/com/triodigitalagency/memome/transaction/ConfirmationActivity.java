package com.triodigitalagency.memome.transaction;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.triodigitalagency.memome.MainActivity;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.utils.NumberTextWatcher;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ConfirmationActivity extends BaseActivity {

    //<editor-fold desc="Variable">
    private Bundle extras;
    private RelativeLayout tapForReloadLayout;
    private Button buttonConfirm, buttonTapForReload;
    private TextView textViewOrderId;
    private EditText editTextTransferDate, editTextBankName, editTextAccountName, editTextAccountNumber, editTextTotal, editTextNotes;
    private DatePickerDialog transferDateDialog;
    private DateFormat longDateFormatter, dbDateFormatter;
    private String transferDate = "";
    private long orderId = -1;
    private final int BROWSE_CONFIRM_ORDER = 1;
    //</editor-fold>

    //<editor-fold desc="Override">
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);
        setLog("activity_choose_confirm_order");

        firstInit();
        setupActionBar(getResources().getString(R.string.title_confirmation));

        longDateFormatter       = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        dbDateFormatter         = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        buttonConfirm           = (Button)findViewById(R.id.buttonConfirm);
        buttonConfirm.setOnClickListener(buttonConfirmClicked);

        textViewOrderId         = (TextView)findViewById(R.id.textViewOrderId);
        textViewOrderId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(ChooseConfirmOrderActivity.class, BROWSE_CONFIRM_ORDER, new OnSetupIntentExtras() {
                    @Override
                    public void onSetupIntentExtras(Intent intent) {
                        intent.putExtra("selectedId", orderId);
                    }
                });
            }
        });

        editTextBankName        = (EditText)findViewById(R.id.editTextBankName);
        editTextAccountName     = (EditText)findViewById(R.id.editTextAccountName);
        editTextAccountNumber   = (EditText)findViewById(R.id.editTextAccountNumber);
        editTextTotal           = (EditText)findViewById(R.id.editTextTotal);
        editTextTotal.addTextChangedListener(new NumberTextWatcher(editTextTotal));
        editTextNotes           = (EditText)findViewById(R.id.editTextNotes);

        extras                  = getIntent().getExtras();
        if (extras != null){
            orderId             = extras.getLong("orderId");
            textViewOrderId.setText(String.valueOf(extras.getString("orderNumber")));
            editTextTotal.setText(String.valueOf((int)extras.getDouble("grandTotal")));
        }

        initTransferDate();
        initTapForReload();
        loadData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            back();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //Handle the back button
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            back();
            return true;
        }
        else {
            return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == BROWSE_CONFIRM_ORDER) {
            Bundle tempExtras       = data.getExtras();
            orderId                 = tempExtras.getLong("selectedId");
            textViewOrderId.setText(tempExtras.getString("orderNumber"));
            editTextTotal.setText(String.valueOf((int)tempExtras.getDouble("grandTotal")));
        }
    }
    //</editor-fold>

    //<editor-fold desc="Listener">
    protected View.OnClickListener buttonConfirmClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            boolean isGood = true;
            if (!isLoading()) {
                isGood  = validateEditTextStringRequired(isGood);
                isGood  = validateVariableStringRequired(isGood);
                isGood  = validateNumberMustMoreThanZero(isGood);

                if (isGood) {
                    showLoading();
                    new AlertDialog.Builder(context)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(R.string.text_confirmation_title)
                        .setMessage(R.string.text_confirmation_message)
                        .setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                submit();
                            }
                        })
                        .setNegativeButton(R.string.button_no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                hideLoading();
                            }
                        })
                        .show();
                }
            }
        }
    };
    //</editor-fold>

    //<editor-fold desc="Procedure & Function">
    protected void submit() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("clientId", Constant.CLIENT_ID);
        params.put("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
        params.put("orderId", String.valueOf(orderId));
        params.put("transferDate",  String.format("%s", transferDate));
        params.put("bankName", editTextBankName.getText().toString());
        params.put("accountName", editTextAccountName.getText().toString());
        params.put("accountNumber", editTextAccountNumber.getText().toString());
        params.put("notes", editTextNotes.getText().toString());

        long total = Long.parseLong(editTextTotal.getText().toString().replace(".", "").replace(",", ""));
        params.put("total", String.valueOf(total));

        request.getJSONFromUrl(
                Globals.getApiUrl("confirmations/add"),
                params,
                getResources().getString(R.string.text_add_confirmation_error),
                new RequestCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        long cId = 0;
                        try {
                            JSONObject confirmation = response.getJSONObject("confirmation");
                            cId = confirmation.getLong("id");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (cId == 0) {
                            //error
                            Globals.showAlert(
                                    getResources().getString(R.string.text_add_confirmation_error),
                                    getResources().getString(R.string.text_add_confirmation_error_unknown),
                                    activity
                            );
                            hideLoading();
                        } else {
                            Globals.showToast(getResources().getString(R.string.text_add_confirmation_success), context);
                            resetInput();
                            hideLoading();
                        }

                    }

                    @Override
                    public void onError(VolleyError error) {
                        hideLoading();
                    }
                }
        );
    }

    protected void back() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    protected Boolean validateEditTextStringRequired(Boolean isGood) {
        if (isGood) {
            EditText editTextOrderId        = new EditText(activity);
            editTextOrderId.setText(String.valueOf(orderId > 0 ? orderId : ""));
            EditText[] requiredFields       = new EditText[]{editTextOrderId, editTextBankName, editTextAccountName, editTextAccountNumber, editTextTotal};
            String[] requiredFieldNames     = new String[]{
                    getResources().getString(R.string.hint_order_id),
                    getResources().getString(R.string.hint_bank_name),
                    getResources().getString(R.string.hint_account_name),
                    getResources().getString(R.string.hint_account_number),
                    getResources().getString(R.string.hint_total),
            };

            int i = 0;
            for (EditText field : requiredFields) {
                if (field.getText().toString().trim().equals("")) {
                    Globals.showAlert(
                        getResources().getString(R.string.text_add_confirmation_error),
                        String.format(getResources().getString(R.string.text_add_error_required), requiredFieldNames[i]),
                        activity
                    );
                    isGood = false;
                    break;
                }
                i++;
            }
        }
        return isGood;
    }

    protected Boolean validateVariableStringRequired(Boolean isGood) {
        if (isGood) {
            String[] requiredValues         = new String[]{transferDate};
            String[] requiredFieldNames     = new String[]{
                    getResources().getString(R.string.hint_transfer_date),
            };

            int i = 0;
            for (String field : requiredValues) {
                if (field.equals("")) {
                    Globals.showAlert(
                            getResources().getString(R.string.text_add_confirmation_error),
                            String.format(getResources().getString(R.string.text_add_error_required), requiredFieldNames[i]),
                            activity
                    );
                    isGood = false;
                    break;
                }
                i++;
            }
        }
        return isGood;
    }

    protected Boolean validateNumberMustMoreThanZero(Boolean isGood) {
        if (isGood) {
            EditText[] requiredFields       = new EditText[]{editTextTotal};
            String[] requiredFieldNames     = new String[]{
                    getResources().getString(R.string.hint_total),
            };

            int i = 0;
            for (EditText field : requiredFields) {
                if (Double.valueOf(field.getText().toString().replace(".", "").replace(",", "")) <= 0) {
                    Globals.showAlert(
                            getResources().getString(R.string.text_add_confirmation_error),
                            String.format(getResources().getString(R.string.text_add_error_unsigned), requiredFieldNames[i]),
                            activity
                    );
                    isGood = false;
                    break;
                }
                i++;
            }
        }
        return isGood;
    }

    protected void resetInput() {
        orderId = -1;
        textViewOrderId.setText("");
        editTextTransferDate.setText("");
        editTextBankName.setText("");
        editTextAccountName.setText("");
        editTextAccountNumber.setText("");
        editTextTotal.setText("");
        editTextNotes.setText("");
    }

    protected void loadData(){
        if(!isLoading()){
            showLoading();
            Map<String, String> params = new HashMap<String, String>();
            params.put("clientId", Constant.CLIENT_ID);
            request.getJSONFromUrl(Globals.getApiUrl("confirmations/search"),
                    params,
                    getResources().getString(R.string.text_product_error),
                    new RequestCallback() {
                        @Override
                        public void onResponse(JSONObject response) {
                            hideLoading();

                            try {
                                JSONObject data = response.getJSONObject("confirmation");

                                try {
                                    TextView textViewNotes  = (TextView)findViewById(R.id.textViewNotes);
                                    textViewNotes.setText(String.format(getResources().getString(R.string.text_confirm_1), data.getString("bank_name"), data.getString("account_name"), data.getString("account_number")));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(VolleyError error) {
                            tapForReloadLayout.setVisibility(View.VISIBLE);
                            hideLoading();
                        }
                    });
        }
    }

    protected void initTapForReload() {
        buttonTapForReload  = (Button)findViewById(R.id.buttonTapForReload);
        buttonTapForReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tapForReloadLayout.setVisibility(View.GONE);
                loadData();
            }
        });

        tapForReloadLayout  = (RelativeLayout)findViewById(R.id.tapForReload);
        tapForReloadLayout.setVisibility(View.GONE);
    }

    protected void initTransferDate() {
        editTextTransferDate    = (EditText)findViewById(R.id.editTextTransferDate);
        editTextTransferDate.setInputType(InputType.TYPE_NULL);
        editTextTransferDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    hideSoftInputFromWindow();
                    transferDateDialog.show();
                }
            }
        });

        Calendar newCalendar    = Calendar.getInstance();
        transferDateDialog      = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate    = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                editTextTransferDate.setText(longDateFormatter.format(newDate.getTime()));
                transferDate        = dbDateFormatter.format(newDate.getTime());
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }
    //</editor-fold>
}
