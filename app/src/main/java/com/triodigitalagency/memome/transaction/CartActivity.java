package com.triodigitalagency.memome.transaction;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.triodigitalagency.memome.MainActivity;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.adapter.CartArrayAdapter;
import com.triodigitalagency.memome.database.Cart;
import com.triodigitalagency.memome.database.DatabaseHandler;
import com.triodigitalagency.memome.database.Project;
import com.triodigitalagency.memome.database.ProjectDetail;
import com.triodigitalagency.memome.product.ProductReviewActivity;
import com.triodigitalagency.memome.user.LoginActivity;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.utils.NumberTextWatcher;
import com.triodigitalagency.memome.utils.OnclikList;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CartActivity extends BaseActivity {

    protected RelativeLayout tapForReloadLayout;
    protected Button buttonTapForReload;
    protected List<Cart> carts;
    protected int totalItem;
    protected double totalPrice;
    protected DatabaseHandler db;
    protected ArrayList<JSONObject> dataSource;
    protected ListView listView;
    protected CartArrayAdapter cartArrayAdapter;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(MainActivity.class);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        setLog("activity_cart");

        firstInit();
        setupActionBar(getResources().getString(R.string.title_cart));

        db                      = new DatabaseHandler(this);
        listView                = (ListView) findViewById(R.id.listView);
        cartArrayAdapter        = new CartArrayAdapter(context);
        listView.setAdapter(cartArrayAdapter);

        cartArrayAdapter.setOnclikList(new OnclikList() {
            @Override
            public void clik(final int position, int viewId) {
                if (viewId == R.id.buttonMinusCart) {

                    minusQtyCart(carts.get(position).getProjectId());
                    loadData();
                } else if (viewId == R.id.buttonAddCart) {
                    addToCart(carts.get(position).getProjectId());
                    loadData();
                } else if (viewId == R.id.buttonDeleteCart) {
                    new AlertDialog.Builder(context)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle(R.string.text_cart_delete_title)
                            .setMessage(R.string.text_cart_delete_message)
                            .setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    removeCart(carts.get(position).getProjectId());
                                    loadData();
                                }

                            })
                            .setNegativeButton(R.string.button_no, null)
                            .show();
                }else if (viewId == R.id.productCart){
                    editImage(position);
                }else if (viewId == R.id.relativeLayoutTitleArea){
                    editImage(position);
                }
            }
        });

        findViewById(R.id.buttonAddMore).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CartActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        findViewById(R.id.buttonCheckout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLogin() && carts.size() > 0 && !isLoading()) {
                    startActivity(CheckoutActivity.class, new OnSetupIntentExtras() {
                        @Override
                        public void onSetupIntentExtras(Intent intent) {
                            intent.putExtra("totalPrice", totalPrice);
                        }
                    });
                }
                else {
                    startActivity(LoginActivity.class);
                }
            }
        });

        loadData();
        initTapForReload();
    }

    private void editImage(int position) {
        Project project = db.getProject(carts.get(position).getProjectId());

        if (project.getReorderId()==0){
            Intent intent = new Intent(getApplicationContext(), ProductReviewActivity.class);
            intent.putExtra("idp",carts.get(position).getProjectId());
            intent.putExtra("edit","edit");
            startActivity(intent);
        }else {
            Globals.showAlert("Sorry","Image can not be edited because this order from reorder",CartActivity.this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
//            finish();
            startActivity(MainActivity.class);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }
    //</editor-fold>

    //<editor-fold desc="Procedure & Function">
    protected void initTapForReload() {
        buttonTapForReload  = (Button)findViewById(R.id.buttonTapForReload);
        buttonTapForReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tapForReloadLayout.setVisibility(View.GONE);
                loadData();
            }
        });

        tapForReloadLayout  = (RelativeLayout)findViewById(R.id.tapForReload);
        tapForReloadLayout.setVisibility(View.GONE);
    }

    protected void loadData() {

        carts               = db.getAllCarts();
        totalItem           = 0;
        totalPrice          = 0;
        String productIds   = "";

        for (int i = 0; i < carts.size(); i++) {
            totalItem       += carts.get(i).getQty();
            Project project = db.getProject(carts.get(i).getProjectId());
            productIds      += project.getProductId() + (i < carts.size() - 1 ? "," : "");
        }

        if (dataSource == null) {
            dataSource      = new ArrayList<>();
        }
        else {
            dataSource.clear();
        }


        if (!productIds.equals("")) {
            setLog("get data"+productIds);
            loadDataProduct(productIds);
        }
        else {
            setLog("not get data"+productIds);
            cartArrayAdapter.setValues(dataSource);
            cartArrayAdapter.notifyDataSetChanged();

            drawTotal();
        }
    }

    protected void drawTotal() {

        TextView textTotalOfItem   = (TextView)findViewById(R.id.textTotalOfItem);
        textTotalOfItem.setText(String.format(getResources().getString(R.string.text_cart_total_of_item), String.valueOf(totalItem)));

        TextView textTotalPrice = (TextView)findViewById(R.id.textTotalPrice);
        EditText tempEditText   = new EditText(context);
        tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
        tempEditText.setText(String.valueOf((int)totalPrice));
        textTotalPrice.setText(tempEditText.getText().toString());
    }

    protected void loadDataProduct(final String productIds){
        setLog("get Data");
        if(!isLoading()){
            showLoading();
            Map<String, String> params = new HashMap<String, String>();
            params.put("clientId", Constant.CLIENT_ID);
            params.put("productIds", productIds);
            request.getJSONFromUrl(Globals.getApiUrl("products/searchByIds"),
                    params,
                    getResources().getString(R.string.text_product_error),
                    new RequestCallback() {
                        @Override
                        public void onResponse(JSONObject response) {
                            hideLoading();

                            try {
                                JSONArray products  = response.getJSONArray("products");
                                for (int i = 0; i < carts.size(); i++) {
                                    for (int j = 0; j < products.length(); j++) {
                                        JSONObject  product = products.getJSONObject(j);
                                        Project project     = db.getProject(carts.get(i).getProjectId());

                                        if (project.getProductId() == product.getInt("id")) {
                                            product.put("project_id", project.getID());
                                            product.put("path_image_crop", project.getPathImageCrop());
                                            product.put("path_image_original", project.getPathImageOriginal());
                                            product.put("cover_title", project.getTitle());
                                            product.put("cover_subtitle", project.getSubtitle());
                                            product.put("cart_qty", carts.get(i).getQty());

                                            List<ProjectDetail> details = db.getAllProjectDetailsByProjectId(project.getID());
                                            product.put("total_detail", details.size());

                                            product.put("corporate_voucher_id", project.getCorporateVoucherId());
                                            product.put("corporate_voucher_qty", project.getCorporateVoucherQty());

                                            if (product.has("cover_images")  && !product.isNull("cover_images")) {
                                                product.put("images", product.getJSONObject("cover_images"));
                                            }

                                            dataSource.add(new JSONObject(product.toString()));
                                            totalPrice  += carts.get(i).getQty() * (product.getLong("special_price") > 0 ? product.getLong("special_price") : product.getLong("price"));
                                            totalPrice  = totalPrice - (project.getCorporateVoucherQty() > 0 ? project.getCorporateVoucherQty() : 0) * (product.getLong("special_price") > 0 ? product.getLong("special_price") : product.getLong("price"));
                                            break;
                                        }
                                    }
                                }

                                cartArrayAdapter.setValues(dataSource);
                                cartArrayAdapter.notifyDataSetChanged();

                                drawTotal();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(VolleyError error) {
                            tapForReloadLayout.setVisibility(View.VISIBLE);
                            hideLoading();
                        }
                    });
        }
    }

    //</editor-fold>
}
