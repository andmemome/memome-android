package com.triodigitalagency.memome.transaction;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.midtrans.sdk.corekit.callback.CheckoutCallback;
import com.midtrans.sdk.corekit.callback.TransactionFinishedCallback;
import com.midtrans.sdk.corekit.core.LocalDataHandler;
import com.midtrans.sdk.corekit.core.MidtransSDK;
import com.midtrans.sdk.corekit.core.PaymentMethod;
import com.midtrans.sdk.corekit.core.TransactionRequest;
import com.midtrans.sdk.corekit.core.UIKitCustomSetting;
import com.midtrans.sdk.corekit.core.themes.CustomColorTheme;
import com.midtrans.sdk.corekit.models.BillInfoModel;
import com.midtrans.sdk.corekit.models.ExpiryModel;
import com.midtrans.sdk.corekit.models.ItemDetails;
import com.midtrans.sdk.corekit.models.TransactionResponse;
import com.midtrans.sdk.corekit.models.UserAddress;
import com.midtrans.sdk.corekit.models.UserDetail;
import com.midtrans.sdk.corekit.models.snap.CreditCard;
import com.midtrans.sdk.corekit.models.snap.Token;
import com.midtrans.sdk.corekit.models.snap.TransactionResult;
import com.midtrans.sdk.corekit.utilities.Utils;
import com.midtrans.sdk.uikit.SdkUIFlowBuilder;
import com.triodigitalagency.memome.MainActivity;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.adapter.RadioButtonAdapter;
import com.triodigitalagency.memome.adapter.RecyclerAdapterCheckOut;
import com.triodigitalagency.memome.adapter.onClikRadioButton;
import com.triodigitalagency.memome.corporate.BarcodeScannerActivity;
import com.triodigitalagency.memome.database.Cart;
import com.triodigitalagency.memome.database.DatabaseHandler;
import com.triodigitalagency.memome.database.Project;
import com.triodigitalagency.memome.database.ProjectDetail;
import com.triodigitalagency.memome.model.KurirModel;
import com.triodigitalagency.memome.product.ProjectActivity;
import com.triodigitalagency.memome.user.AddressActivity;
import com.triodigitalagency.memome.user.LoginActivity;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.utils.NumberTextWatcher;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class CheckoutActivity extends BaseActivity implements TransactionFinishedCallback,RecyclerAdapterCheckOut.onClickCheckoutAdapter{
    private final String TAG = CheckoutActivity.class.getSimpleName();
    private String firstName, lastName, address, provinceName, cityName, postalCode, phone, email;
    private long voucherId = 0, provinceId = -1, cityId = -1, userAddressId = -1, paymentId = Constant.PAYMENT_TRANSFER_ID;
    long totalSipping = 0;
    private List<Cart> carts;
    private DatabaseHandler db;
    private Bundle extras;
    private double totalSementara;
    private double totalPrice = 0, voucherTotal = 0, voucherPercent = 0;
    private String voucherCode = "", paymentType = Constant.PAYMENT_TRANSFER, paymentName = Constant.PAYMENT_TRANSFER_NAME;
    private Button buttonVoucher, buttonDeleteVoucher, buttonChoosePayment;
    private LinearLayout applyCode;
    private final int BROWSE_VOUCHER = 1;
    private final int BROWSE_USER_ADDRESS = 2;
    private RecyclerAdapterCheckOut recyclerAdapterCheckOut;
    private RecyclerView listView;
    private TextView etEstimatedDelivery,etEstimatedArrival;
    private final int BROWSE_PAYMENT = 3;
    private TextView etPromoCode;
    private ArrayList<JSONObject> obj = new ArrayList<>();
    private ArrayList<KurirModel> kurirList = new ArrayList<>();
    private int id_order;
    private int defaultAddress = 0;

    @BindView(R.id.textViewUserAddress1)TextView textViewUserAddress1;
    @BindView(R.id.textViewUserAddress2)TextView textViewUserAddress2;

    @BindView(R.id.etShipping)TextView etShipping;
    @BindView(R.id.linePromocodeDetail)LinearLayout linePromocodeDetail;
    @BindView(R.id.detailPromocode)TextView detailPromocode;
    @BindView(R.id.rpPromocode)TextView rpPromocode;
    @BindView(R.id.rpGiftCard)TextView rpGiftCard;

    @BindView(R.id.etSubtotal)TextView etSubtotal;
    @BindView(R.id.etSubtotall)TextView etTotal;
    @BindView(R.id.etDiscount)TextView etDiscount;
    @BindView(R.id.buttonChooseFromSavedAddress)Button buttonChooseFromSavedAddress;
    @BindView(R.id.buttonInputManually)Button buttonInputManually;

    @BindView(R.id.text1)TextView text1;
    @BindView(R.id.text2)TextView text2;
    @BindView(R.id.text3)TextView text3;
    @BindView(R.id.text4)TextView text4;
    @BindView(R.id.text5)TextView text5;
    @BindView(R.id.text6)TextView text6;
    @BindView(R.id.txDisCount)TextView txDisCount;
    @BindView(R.id.lineGiftCard)LinearLayout lineGiftCard;
    @BindView(R.id.lineDiscount)LinearLayout lineDiscount;
    @BindView(R.id.lineRpPromocode)LinearLayout lineRpPromocode;
    @BindView(R.id.myrecyclerviewKurir)RecyclerView myrecyclerviewKurir;
    RadioButtonAdapter radioButtonAdapter;
    boolean promocodeIsActivated = false;

    @BindView(R.id.btnAddNewAddress)Button btnAddNewAddress;
    @OnClick(R.id.btnAddNewAddress)void btnAddNewAddress(){

        Intent intent = new Intent(this,AddressActivity.class);
        intent.putExtra("isCreate",true);
        intent.putExtra("userAddressId",Constant.CLIENT_ID);
        startActivityForResult(intent,99);

    }
    @OnClick(R.id.editAddress)void editAddressListener(){
        startActivityForResult(ChooseAddressActivity.class, BROWSE_USER_ADDRESS, new OnSetupIntentExtras() {
            @Override
            public void onSetupIntentExtras(Intent intent) {
                intent.putExtra("selectedId", userAddressId);
            }
        });
    }

    @OnClick(R.id.GiftCard)void applyGift(){
        Intent intent = new Intent(CheckoutActivity.this,BarcodeScannerActivity.class);
        startActivityForResult(intent,77);
    }

    Typeface type;

    private int totalItem;
    private ArrayList<JSONObject> dataSource = new ArrayList<>();
    private String  shippingType,namaShipping;
    private double  shippingTotal = 0, grandTotal = 0, minTransaction = 10000;
    private List<Double> listShippingCosts;
    int percentVoucher,priceVoucher;
    boolean reorder;
    JSONObject jsonObject1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ButterKnife.bind(this);
        setLog("activity_checkout");
        type = Typeface.createFromAsset(getAssets(), "fonts/myriad.otf");

        buttonChooseFromSavedAddress.setTypeface(type);
        buttonInputManually.setTypeface(type);
        buttonChooseFromSavedAddress.setTypeface(type);
        text1.setTypeface(type);
        text2.setTypeface(type);
        text3.setTypeface(type);
        text4.setTypeface(type);
        text5.setTypeface(type);
        text6.setTypeface(type);
        btnAddNewAddress.setTypeface(type);

        initMidtransSdk();

        db = new DatabaseHandler(this);

        firstInit();
        setupActionBar(getResources().getString(R.string.title_checkout));

        /*if (provinceId > 0) {
                    intent.putExtra("provinceId", provinceId);
                    intent.putExtra("cityId", cityId);
                    intent.putExtra("firstName", firstName);
                    intent.putExtra("lastName", lastName);
                    intent.putExtra("address", address);
                    intent.putExtra("provinceName", provinceName);
                    intent.putExtra("cityName", cityName);
                    intent.putExtra("postalCode", postalCode);
                    intent.putExtra("phone", phone);
                    intent.putExtra("email", email);
                }*/

        buttonVoucher       = (Button)findViewById(R.id.buttonVoucher);
        applyCode           = (LinearLayout)findViewById(R.id.applyCode);
        buttonDeleteVoucher = (Button)findViewById(R.id.buttonDeleteVoucher);
        buttonChoosePayment = (Button)findViewById(R.id.buttonChoosePayment);
        etEstimatedDelivery = (TextView)findViewById(R.id.etEstimatedDelivery);
        etEstimatedArrival  = (TextView)findViewById(R.id.etEstimatedArrival);
        etPromoCode         = (TextView)findViewById(R.id.etPromoCode);
        listView            = (RecyclerView)findViewById(R.id.listView);

        recyclerAdapterCheckOut = new RecyclerAdapterCheckOut(this,dataSource,this);

        /* cartArrayAdapter2   = new CartArrayAdapter2(context);
        cartArrayAdapter2.setShowButton(false); */

        buttonChoosePayment.setText(paymentName);
        buttonChoosePayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(ChoosePaymentActivity.class, BROWSE_PAYMENT, new OnSetupIntentExtras() {
                    @Override
                    public void onSetupIntentExtras(Intent intent) {
                        intent.putExtra("selectedId", paymentId);
                    }
                });
            }
        });

        extras              = getIntent().getExtras();
        totalPrice          = extras.getDouble("totalPrice");
        reorder = extras.getBoolean("reorder");

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        listView.setLayoutManager(mLayoutManager);
        listView.setItemAnimator(new DefaultItemAnimator());
        listView.setAdapter(recyclerAdapterCheckOut);

        buttonVoucher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(VoucherActivity.class, BROWSE_VOUCHER, new OnSetupIntentExtras() {
                    @Override
                    public void onSetupIntentExtras(Intent intent) {
                        intent.putExtra("voucherCode", voucherCode);
                    }
                });
            }
        });

        buttonDeleteVoucher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonDeleteVoucher.setVisibility(View.GONE);
                buttonVoucher.setText(getResources().getString(R.string.button_voucher));
                voucherId = -1;
                voucherCode = "";
                voucherTotal = 0;
                voucherPercent = 0;
            }
        });

        findViewById(R.id.buttonChooseFromSavedAddress).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(ChooseAddressActivity.class, BROWSE_USER_ADDRESS, new OnSetupIntentExtras() {
                    @Override
                    public void onSetupIntentExtras(Intent intent) {
                        intent.putExtra("selectedId", userAddressId);
                    }
                });
            }
        });

        findViewById(R.id.buttonInputManually).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (provinceId <= 0) {
                    Globals.showAlert(
                            getResources().getString(R.string.text_checkout_error),
                            String.format(getResources().getString(R.string.text_ship_to)),
                            activity
                    );
                }else {
                    startActivityForResult(ReviewActivity.class, 9, new OnSetupIntentExtras() {
                        @Override
                        public void onSetupIntentExtras(Intent intent) {
                            intent.putExtra("provinceId", provinceId);
                            intent.putExtra("cityId", cityId);
                            intent.putExtra("firstName", firstName);
                            intent.putExtra("lastName", lastName);
                            intent.putExtra("address", address);
                            intent.putExtra("provinceName", provinceName);
                            intent.putExtra("cityName", cityName);
                            intent.putExtra("postalCode", postalCode);
                            intent.putExtra("phone", phone);
                            intent.putExtra("email", email);
                            intent.putExtra("voucherId", voucherId);
                            intent.putExtra("voucherCode", voucherCode);
                            intent.putExtra("voucherTotal", voucherTotal);
                            intent.putExtra("voucherPercent", voucherPercent);
                            intent.putExtra("paymentType", paymentType);
                            intent.putExtra("paymentName", paymentName);
                        }
                    });
                }



            }
        });

        findViewById(R.id.buttonNext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (totalSipping <= 0) {
                    Globals.showAlert(
                            getResources().getString(R.string.text_checkout_error),"Please select shipping method",
                            activity
                    );
                }
                else {
                    showLoading();
                    checkoutMidtrans();



                    /*setLog("===--> "+Integer.valueOf(etTotal.getText().toString().substring(3,etTotal.getText().toString().length()).replace(".","")));
                    Log.d(TAG, "voucherTotal : " + voucherTotal + "; voucherPercent : " + voucherPercent);

                    setLog("token "+Globals.getStringDataPreference(getApplicationContext(), Constant.P_ACCESS_TOKEN));
                    setLog("clientId "+ Constant.CLIENT_ID);*/
                }
            }
        });

        LinearLayout linearLayoutPaymentDetail  = (LinearLayout)findViewById(R.id.linearLayoutPaymentDetail);


        etSubtotal.setTypeface(type);
        etTotal.setTypeface(type);
        etDiscount.setTypeface(type);
        etPromoCode.setTypeface(type);
        etEstimatedDelivery.setTypeface(type);
        etEstimatedArrival.setTypeface(type);


        if (etDiscount.getText().toString().isEmpty()){
            etDiscount.setText("IDR 0");
        }
        if (etShipping.getText().toString().isEmpty()){
            etShipping.setText("IDR 0");
        }

        applyCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etPromoCode.getWindowToken(), 0);
                loadFoucher();
            }
        });



        if (reorder){
            setLog("--"+totalPrice);
            etSubtotal.setText(Globals.currencyFormatIDR(totalPrice));
            etTotal.setText(Globals.currencyFormatIDR(totalPrice));
            loadDataaddress();
            try {
                JSONObject jsonObject = new JSONObject(extras.getString("order"));
                jsonObject1 = new JSONObject(extras.getString("order1"));
                id_order = Integer.parseInt(extras.getString("id_order"));
                setLog("id "+id_order);
                setLog("jsonObject1"+jsonObject1);
                setLog("order ---> "+jsonObject);
                loadDataReorder(jsonObject);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }else {
            setLog("0000000");


            TextView textTotalPrice = (TextView)findViewById(R.id.textTotalPrice);
            textTotalPrice.setTypeface(type);
            EditText tempEditText   = new EditText(context);
            tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
            tempEditText.setText(String.valueOf((int) totalPrice));
            textTotalPrice.setText(String.format(getResources().getString(R.string.text_total_amount), tempEditText.getText().toString()));


            loadData();

            checkCarts();
            if (checkIsJustCorporate()) {
                paymentType = Constant.PAYMENT_CORPORATE;
                paymentName = Constant.PAYMENT_CORPORATE_NAME;
                linearLayoutPaymentDetail.setVisibility(View.GONE);
            }
            else {
                //the old code
                linearLayoutPaymentDetail.setVisibility(View.GONE);
            }

            defaultAddress = Globals.getIntDataPreference(this,"posisi");
            loadDataaddress();
        }


    }

    ArrayList<String> totalShip = new ArrayList<>();

    private void loadDataKurir() {

        radioButtonAdapter = new RadioButtonAdapter(this,kurirList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        myrecyclerviewKurir.setLayoutManager(mLayoutManager);
        myrecyclerviewKurir.setItemAnimator(new DefaultItemAnimator());
        myrecyclerviewKurir.setNestedScrollingEnabled(false);
        myrecyclerviewKurir.setAdapter(radioButtonAdapter);

        radioButtonAdapter.onClikRadioButtonListener(new onClikRadioButton() {
            @Override
            public void onClik(String string) {
                etShipping.setText(string);
                totalSipping = Long.parseLong(string.substring(4,string.length()).replace(".","").trim());
                drawTotal();
            }
        });

        radioButtonAdapter.setEstimated(new RadioButtonAdapter.estimated() {
            @Override
            public void estimated(String string,String jenis,String nama) {
                String strings[] = string.split("-");
                shippingType = jenis;
                namaShipping = nama;

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                calendar.add(Calendar.DAY_OF_YEAR, 3);
                Date date = calendar.getTime();
                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(new Date());
                calendar2.add(Calendar.DAY_OF_YEAR, 4);
                Date date2 = calendar2.getTime();

                loadDataArrival(date,date2,Integer.valueOf(strings[0]),Integer.valueOf(strings[1]));
            }
        });

        setLog("kurir "+Globals.getApiUrl("orders/shipping_cost")+" "+"clientId "+ Constant.CLIENT_ID+" cityId "+ String.valueOf(cityId));
        Map<String, String> params = new HashMap<String, String>();
        params.put("clientId", Constant.CLIENT_ID);
        params.put("cityId", String.valueOf(cityId));

        kurirList.clear();

        request.getJSONFromUrl(Globals.getApiUrl("orders/shipping_cost"), params, getResources().getString(R.string.text_checkout_error), new RequestCallback() {
            @Override
            public void onResponse(JSONObject response) {
                setLog("orders/shipping_cost "+response.toString());

                try {
                    JSONObject codeRest = response.getJSONObject("message");
                    if("200".equals(codeRest.optString("code"))){
                        JSONArray responseKurir = response.getJSONArray("shippingCosts");
                        JSONObject obj = responseKurir.getJSONObject(0);
                        JSONArray costs = obj.getJSONArray("costs");
                        ArrayList<String> etcs = new ArrayList<>();

                        for (int i =0;i<costs.length();i++){
                            JSONObject costObj = costs.getJSONObject(i);
                            String name = costObj.getString("service");
                            setLog(name);
                            JSONArray cost = costObj.getJSONArray("cost");
                            for (int n = 0;n<cost.length();n++) {
                                JSONObject val = cost.getJSONObject(n);
                                String value = val.getString("value");
                                String etd = val.getString("etd");
                                String note = val.getString("note");
                                etcs.add(etd);
                                KurirModel kurirModel = new KurirModel();
                                kurirModel.setEstimasi(etd);
                                kurirModel.setHarga(Globals.currencyFormatIDR(Double.parseDouble(value)));
                                kurirModel.setJenis(name);
                                kurirModel.setCode(obj.optString("code"));

                                kurirList.add(kurirModel);

//                                totalShip.add(value);
                            }
                        }
                        radioButtonAdapter.notifyDataSetChanged();
                        String strings[] = etcs.get(0).split("-");
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(new Date());
                        calendar.add(Calendar.DAY_OF_YEAR, 3);
                        Date date = calendar.getTime();
                        Calendar calendar2 = Calendar.getInstance();
                        calendar2.setTime(new Date());
                        calendar2.add(Calendar.DAY_OF_YEAR, 4);
                        Date date2 = calendar2.getTime();
                        loadDataArrival(date,date2,Integer.valueOf(strings[0]),Integer.valueOf(strings[1]));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(VolleyError error) {
                hideLoading();
            }
        });
    }

    private void loadDataaddress() {
        setLog("detail user "+Globals.getApiUrl("users/view")+" "+"clientId "+ Constant.CLIENT_ID+" accessToken "+ Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
        Map<String, String> params = new HashMap<String, String>();
        params.put("clientId", Constant.CLIENT_ID);
        params.put("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
        request.getJSONFromUrl(Globals.getApiUrl("users/view"), params, getResources().getString(R.string.text_checkout_error), new RequestCallback() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    setLog("users/view" +response.toString());
                    JSONArray userAddresses = response.getJSONArray("userAddresses");
                    if (userAddresses.length()>0){
                        for (int i =0 ;i<userAddresses.length();i++){
                            JSONObject obj1 = userAddresses.getJSONObject(i);
                            obj.add(obj1);
                            findViewById(R.id.linearLayoutUserAddress).setVisibility(View.VISIBLE);
                            findViewById(R.id.tittleShippingInformation).setVisibility(View.VISIBLE);

                        }
                        findViewById(R.id.shippingEmpety).setVisibility(View.GONE);
                        setvalueAddress();
                    }else {
                        //hide address
                        findViewById(R.id.shippingEmpety).setVisibility(View.VISIBLE);
                        findViewById(R.id.linearLayoutUserAddress).setVisibility(View.GONE);
                    }

                    loadDataKurir();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                hideLoading();
            }

            @Override
            public void onError(VolleyError error) {
                hideLoading();
            }
        });
    }

    private void setvalueAddress() throws JSONException {


        firstName       = obj.get(defaultAddress).getString("first_name");
        lastName        = obj.get(defaultAddress).getString("last_name");
        address         = obj.get(defaultAddress).getString("address");
        provinceName    = obj.get(defaultAddress).getString("provinceName");
        cityName        = obj.get(defaultAddress).getString("cityName");
        postalCode      = obj.get(defaultAddress).getString("postal_code");
        phone           = obj.get(defaultAddress).getString("phone");
        email           = obj.get(defaultAddress).getString("email");
        cityId          = Integer.valueOf(obj.get(defaultAddress).getString("city_id"));
        provinceId      = Integer.valueOf(obj.get(defaultAddress).getString("province_id"));


        String name         = firstName;
        if (!lastName.equals("")) {
            name            += " " + lastName;
        }

        textViewUserAddress1.setTypeface(type);
        textViewUserAddress2.setTypeface(type);
        textViewUserAddress1.setText(firstName + " " + lastName);
        textViewUserAddress2.setText(address + "\n" + cityName + ", " + provinceName + " - " + postalCode + "\n" + phone + " / " + email);

    }



    private void checkoutMidtrans() {

        MidtransSDK.getInstance().setTransactionRequest(initTransactionRequest());
        MidtransSDK.getInstance().checkout(new CheckoutCallback() {
            @Override
            public void onSuccess(Token token) {
                if (token != null) {
                    hideLoading();
                    try {
                        MidtransSDK.getInstance().startPaymentUiFlow(CheckoutActivity.this,PaymentMethod.CREDIT_CARD);
//                        MidtransSDK.getInstance().startPaymentUiFlow(getApplicationContext(), PaymentMethod.CREDIT_CARD);
                    }catch (Exception e){
                        setLog(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Token token, String s) {
                hideLoading();
                System.out.println(token);
                System.out.println(s);
            }

            @Override
            public void onError(Throwable throwable) {
                System.out.println(throwable.getMessage());
            }
        });



    }

    private void initMidtransSdk() {
        String client_key = "VT-client-LQ5AL1YEpSoH7DdJ";
        String base_url = "http://memome.co.id/transactions/index.php";

        SdkUIFlowBuilder.init()
                .setClientKey(client_key) // client_key is mandatory
                .setContext(this) // context is mandatory
                .setTransactionFinishedCallback(this) // set transaction finish callback (sdk callback)
                .setMerchantBaseUrl(base_url) //set merchant url
                .enableLog(true) // enable sdk log
                .setColorTheme(new CustomColorTheme("#65c6bd", "#5C82C2", "#65c6bd")) // will replace theme on snap theme on MAP
                .buildSDK();
    }

    private TransactionRequest initTransactionRequest() {
        // Create new Transaction Request
        TransactionRequest transactionRequestNew = new
                TransactionRequest(String.valueOf(Calendar.getInstance().getTimeInMillis()), Integer.valueOf(etTotal.getText().toString().substring(3,etTotal.getText().toString().length()).replace(".","").trim()));

        ArrayList<ItemDetails> itemDetails = new ArrayList<>();

        for (int i =0;i<dataSource.size();i++){
            try {

                ItemDetails itemDetail = new ItemDetails();
                itemDetail.setId(String.valueOf(id_order));
//                itemDetail.setPrice((double)Integer.valueOf(String.valueOf((int) dataSource.get(i).getLong("special_price") > 0 ? dataSource.get(i).getLong("special_price") : dataSource.get(i).getLong("price")).replace(".","")));
                itemDetail.setPrice(Integer.valueOf(String.valueOf((int) dataSource.get(i).getLong("special_price") > 0 ? dataSource.get(i).getLong("special_price") : dataSource.get(i).getLong("price")).replace(".","")));
                itemDetail.setName(!dataSource.get(i).getString("cover_title").equals("") ? "\"" +  dataSource.get(i).getString("cover_title").trim() + "\"" :  dataSource.get(i).getString("title"));
                itemDetail.setQuantity(dataSource.get(i).getInt("cart_qty"));
                itemDetails.add(itemDetail);
//                setLog("===+++___+++ "+ String.valueOf((int) dataSource.get(i).getLong("special_price") > 0 ? dataSource.get(i).getLong("special_price") : dataSource.get(i).getLong("price")));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        ItemDetails itemDetail = new ItemDetails();
        itemDetail.setId(String.valueOf(voucherId));
//        itemDetail.setPrice((double)-Integer.parseInt(etDiscount.getText().toString().replace("IDR ","").replace(".","")));
        itemDetail.setPrice(-Integer.parseInt(etDiscount.getText().toString().replace("IDR ","").replace(".","")));
        itemDetail.setName("discount");
        itemDetail.setQuantity(1);
        itemDetails.add(itemDetail);

        ItemDetails itemDetail2 = new ItemDetails();
        itemDetail2.setId(namaShipping);
        itemDetail2.setPrice(Integer.parseInt(String.valueOf(totalSipping)));
//        itemDetail2.setPrice((double)Integer.parseInt(String.valueOf(totalSipping)));
        itemDetail2.setName(namaShipping+"-"+shippingType);
        itemDetail2.setQuantity(1);
        itemDetails.add(itemDetail2);


        transactionRequestNew.setItemDetails(itemDetails);

        BillInfoModel billInfoModel = new BillInfoModel("demo_label", "demo_value");
        transactionRequestNew.setBillInfoModel(billInfoModel);

        CreditCard creditCardOptions = new CreditCard();
        creditCardOptions.setSaveCard(true);
        creditCardOptions.setSecure(false);
        creditCardOptions.setAuthentication(CreditCard.AUTHENTICATION_TYPE_3DS);
        transactionRequestNew.setCreditCard(creditCardOptions);

//        CreditCard creditCardOptions = new CreditCard();
//        creditCardOptions.setSaveCard(false);
//        creditCardOptions.setSecure(false);
//        creditCardOptions.setBank(BankType.BCA);
//        creditCardOptions.setChannel(CreditCard.MIGS);
//        transactionRequestNew.setCreditCard(creditCardOptions);

        /*

        ArrayList<String> enabledPayments = new ArrayList<>();
        enabledPayments.add(PaymentType.CREDIT_CARD);
        enabledPayments.add(BCA);
        enabledPayments.add(PaymentType.BCA_VA);
        enabledPayments.add(PaymentType.GOPAY);
        enabledPayments.add(PaymentType.PERMATA_VA);
        enabledPayments.add(PaymentType.BCA_VA);
        enabledPayments.add(PaymentType.BNI_VA);
        enabledPayments.add(PaymentType.GCI);
        enabledPayments.add(PaymentType.ALL_VA);
        enabledPayments.add(PaymentType.E_CHANNEL);
        enabledPayments.add(PaymentType.INDOMARET);
        enabledPayments.add(PaymentType.KLIK_BCA);
        enabledPayments.add(PaymentType.BBM_MONEY);
        enabledPayments.add(PaymentType.MANDIRI_ECASH);
        enabledPayments.add(PaymentType.BRI_EPAY);
        enabledPayments.add(PaymentType.DANAMON_ONLINE);
        enabledPayments.add(PaymentType.KIOSON);
        transactionRequestNew.setEnabledPayments(enabledPayments);

        */

        UIKitCustomSetting uiKit = MidtransSDK.getInstance().getUIKitCustomSetting();
        uiKit.setShowPaymentStatus(true);
        uiKit.setSaveCardChecked(true);
        uiKit.setEnableAutoReadSms(true);
        MidtransSDK.getInstance().setUIKitCustomSetting(uiKit);


        ExpiryModel expiryModel = new ExpiryModel();
        expiryModel.setStartTime(Utils.getFormattedTime(System.currentTimeMillis()));
        expiryModel.setDuration(3);
        expiryModel.setUnit(ExpiryModel.UNIT_HOUR);
        transactionRequestNew.setExpiry(expiryModel);
        UserDetail userDetail = initCustomerDetails();
        LocalDataHandler.saveObject(getString(R.string.user_details), userDetail);

        return transactionRequestNew;
    }

    private UserDetail initCustomerDetails() {

        UserDetail mCustomerDetails = new UserDetail();
        mCustomerDetails.setUserId(String.valueOf(Globals.getLongDataPreference(this,Constant.P_USER_ID)));
        mCustomerDetails.setPhoneNumber(phone+"");
        mCustomerDetails.setUserFullName(firstName+"");
        mCustomerDetails.setEmail(email+"");

        ArrayList<UserAddress> userAddresses = new ArrayList<>();
        UserAddress userAddress = new UserAddress();
        userAddress.setAddress(address);
        userAddress.setCity(cityName);
        userAddress.setAddressType(com.midtrans.sdk.corekit.core.Constants.ADDRESS_TYPE_BOTH);
        userAddress.setZipcode(postalCode);
        userAddress.setCountry("IDN");
        userAddresses.add(userAddress);
        mCustomerDetails.setUserAddresses(userAddresses);

        return mCustomerDetails;
    }

    public void loadFoucher(){

        Map<String, String> params = new HashMap<String, String>();
        params.put("clientId", Constant.CLIENT_ID);
        params.put("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
        params.put("voucherCode", etPromoCode.getText().toString());

        String url  = "vouchers/search";
        showLoading();
        request.getJSONFromUrl(
                Globals.getApiUrl(url), params, "I am sorry",
                new RequestCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideLoading();
                        setLog("vouchers/search "+response.toString());
                        try {
                            JSONObject jsonObject1 = response.getJSONObject("message");
                            if ("200".equals(jsonObject1.getString("code"))){

                                if (promocodeIsActivated){
                                    Globals.showAlert("Sorry..","You can not apply for both PROMO CODE and GIFT CARD",CheckoutActivity.this);
                                }else {
                                    promocodeIsActivated = true;
                                    JSONObject jsonObject = response.getJSONObject("voucher");
                                    voucherId = Long.valueOf(jsonObject.getString("id"));
                                    double percentage = jsonObject.getDouble("percent");
                                    double price = jsonObject.getDouble("price");

                                    percentVoucher = (int)percentage;
                                    priceVoucher = (int) price;
                                    if (percentVoucher>0||priceVoucher>0){
                                        linePromocodeDetail.setVisibility(View.VISIBLE);
                                        lineDiscount.setVisibility(View.VISIBLE);
                                        txDisCount.setText("Discount Promo Code");
                                        detailPromocode.setText("PROMOCODE Discount "+percentVoucher+"% Applied");
                                    }else {

                                    }
                                    drawTotal();
                                }

                            }else {
                                percentVoucher = 0;
                                priceVoucher = 0;
                                linePromocodeDetail.setVisibility(View.GONE);
                                drawTotal();
                                Globals.showAlert("I am sorry",jsonObject1.getString("content"),activity);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(VolleyError error) {
                        hideLoading();
                        promocodeIsActivated = false;
                        lineDiscount.setVisibility(View.GONE);
                        etDiscount.setText("IDR 0");
                        percentVoucher = 0;
                        priceVoucher = 0;
                        linePromocodeDetail.setVisibility(View.GONE);
                        drawTotal();
                    }
                }
        );
    }

    /*private void loadDataArrival() {
        etEstimatedDelivery.setText(Globals.calculateShipping(new Date(), 3, 4));
        etEstimatedArrival.setText(Globals.calculateShipping(new Date(), 4, 7));
    }*/

    private void loadDataArrival(Date date,Date date2,int from,int to) {
        etEstimatedDelivery.setText(Globals.calculateShipping(new Date(), 12, 16));
        etEstimatedArrival.setText(Globals.calculateShipping(date,date2, (from+13), (to+17)));
    }

    protected void loadData() {
        carts               = db.getAllCarts();
        totalItem           = 0;
        totalPrice          = 0;
        String productIds   = "";

        for (int i = 0; i < carts.size(); i++) {
            totalItem       += carts.get(i).getQty();
            Project project = db.getProject(carts.get(i).getProjectId());
            productIds      += project.getProductId() + (i < carts.size() - 1 ? "," : "");
        }

        if (dataSource == null) {
            dataSource      = new ArrayList<JSONObject>();
        }
        else {
            dataSource.clear();
        }

        setLog("=== total "+totalItem);
        setLog("=== totalPrice "+totalPrice);

        if (!productIds.equals("")) {
            loadData(productIds);
        }else {

            recyclerAdapterCheckOut.notifyDataSetChanged();
            /*cartArrayAdapter2.setValues(dataSource);
            cartArrayAdapter2.notifyDataSetChanged();*/
            drawTotal();
        }
    }

    protected void loadData(final String productIds) {
        if(!isLoading()){
            showLoading();
            setLog("clientId"+ Constant.CLIENT_ID);
            setLog("productIds"+ productIds);
            setLog("cityId"+ String.valueOf(cityId));

            Map<String, String> params = new HashMap<>();
            params.put("clientId", Constant.CLIENT_ID);
            params.put("productIds", productIds);
            params.put("cityId", String.valueOf(cityId));
            request.getJSONFromUrl(Globals.getApiUrl("orders/review"),
                    params,
                    getResources().getString(R.string.text_product_error),
                    new RequestCallback() {
                        @Override
                        public void onResponse(JSONObject response) {
                            hideLoading();
                            setLog("orders/review "+response.toString());

                            try {
                                JSONArray products = response.getJSONArray("products");
                                for (int i = 0; i < carts.size(); i++) {
                                    for (int j = 0; j < products.length(); j++) {
                                        JSONObject product = products.getJSONObject(j);
                                        Project project = db.getProject(carts.get(i).getProjectId());

                                        if (project.getProductId() == product.getInt("id")) {
                                            product.put("project_id", project.getID());
                                            product.put("product_id", project.getProductId());
                                            product.put("reorder_id", project.getReorderId());
                                            product.put("path_image_crop", project.getPathImageCrop());
                                            product.put("path_image_original", project.getPathImageOriginal());
                                            product.put("cover_title", project.getTitle());
                                            product.put("cover_subtitle", project.getSubtitle());
                                            product.put("cart_qty", carts.get(i).getQty());

                                            List<ProjectDetail> details = db.getAllProjectDetailsByProjectId(project.getID());
                                            product.put("total_detail", details.size());

                                            product.put("corporate_voucher_id", project.getCorporateVoucherId());
                                            product.put("corporate_voucher_qty", project.getCorporateVoucherQty());
                                            product.put("corporate_voucher_code", project.getCorporateVoucherCode());

                                            if (product.has("cover_images") && !product.isNull("cover_images")) {
                                                product.put("images", product.getJSONObject("cover_images"));
                                            }

                                            dataSource.add(new JSONObject(product.toString()));
                                            totalPrice += carts.get(i).getQty() * (product.getLong("special_price") > 0 ? product.getLong("special_price") : product.getLong("price"));
                                            totalPrice  = totalPrice - (project.getCorporateVoucherQty() > 0 ? project.getCorporateVoucherQty() : 0) * (product.getLong("special_price") > 0 ? product.getLong("special_price") : product.getLong("price"));
                                            break;
                                        }
                                    }
                                }
                                voucherTotal = voucherTotal > 0 ? voucherTotal : (totalPrice * voucherPercent / 100);


                                recyclerAdapterCheckOut.notifyDataSetChanged();

/*
                                cartArrayAdapter2.setShowButton(true);
                                cartArrayAdapter2.setValues(dataSource);
                                cartArrayAdapter2.notifyDataSetChanged();
*/
                                setLog("data source "+ dataSource.toArray().length);

                                drawTotal();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(VolleyError error) {
                            hideLoading();
                        }
                    });
        }
    }


    protected void loadDataReorder(final JSONObject jsonObject) {
        JSONObject product = new JSONObject();

        String imageString ="";
        try {
            for (int i =0;i<jsonObject.optJSONArray("values").length();i++){
                product.put("product_id",jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optString("product_id"));
                product.put("project_id", jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optString("id"));
                try {
                    product.put("path_image_crop", jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optJSONObject("cover_image").optJSONArray("values").optJSONObject(0).optJSONObject("nameValuePairs").optJSONObject("image").optJSONObject("nameValuePairs").optString("large"));
                    product.put("path_image_original", jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optJSONObject("cover_image").optJSONArray("values").optJSONObject(0).optJSONObject("nameValuePairs").optJSONObject("image").optJSONObject("nameValuePairs").optString("large"));

                }catch (Exception e){
                    e.printStackTrace();

                    imageString = jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optJSONObject("order_product_detail").optJSONArray("values").optJSONObject(0).optJSONObject("nameValuePairs").optJSONObject("image").optJSONObject("nameValuePairs").optString("large");
                    product.put("path_image_crop", imageString);
                    product.put("path_image_original", imageString);
                    setLog("pleace"+imageString);
                }
                product.put("cover_title", jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optString("title"));
                product.put("cover_subtitle", jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optString("subtitle"));
                product.put("price", (long)(double)Double.valueOf(jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optString("price")));
                countMint.add((long)(double)Double.valueOf(jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optString("price")));
                product.put("cart_qty", jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optJSONObject("product").optJSONArray("values").length());

                String prdId = jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optString("product_id");
                if ("14".equals(prdId)||"9".equals(prdId)||"3".equals(prdId)){
                    product.put("total_detail", String.valueOf(jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optInt("count_photo")-1));
                }else {
                    product.put("total_detail", jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optInt("count_photo"));
                }

                /*"cover_width":"2303",
                        "cover_height":"2303",
                        "cover_border_top":"118",
                        "cover_border_left":"118",
                        "cover_content_width":"2067",
                        "cover_content_height":"2067",
                        "cover_title_area_width":"1051",
                        "cover_title_area_height":"42k5",
                        "cover_title_area_top":"709",
                        "cover_title_area_left":"614",
                        "cover_title_font_size":"83",
                        "cover_subtitle_font_size":"47",
                        "cover_title_top":"142",
                        "cover_subtitle_top":"280",
                        "size":"21 x 21",
                        "price":"159000.00",
                        "special_price":"0.00",
                        "is_require_title":"1",
                        "is_corporate":"0",
                        "position":"0",
                        "print_layout":"[{\"pages\":\"34\",\"orientation\":\"portrait\",\"horz\":0,\"vert\":0,\"top\":0,\"bottom\":0,\"left\":0,\"right\":0}]",
                        "min_image":"25",
                        "max_image":"0"*/

                product.put("corporate_voucher_qty", jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optString("discount_qty"));

                for (int n=0;n<jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optJSONObject("product").optJSONArray("values").length();n++){
                    product.put("corporate_voucher_id", (long)(double)Double.valueOf(jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optJSONObject("product").optJSONArray("values").optJSONObject(n).optJSONObject("nameValuePairs").optString("voucher_id")));
                    product.put("cover_width",(long)(double)Double.valueOf(jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optJSONObject("product").optJSONArray("values").optJSONObject(n).optJSONObject("nameValuePairs").optString("cover_width")));
                    product.put("cover_height",(long)(double)Double.valueOf(jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optJSONObject("product").optJSONArray("values").optJSONObject(n).optJSONObject("nameValuePairs").optString("cover_height")));

                    product.put("cover_content_width",(long)(double)Double.valueOf(jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optJSONObject("product").optJSONArray("values").optJSONObject(n).optJSONObject("nameValuePairs").optString("cover_content_width")));
                    product.put("cover_content_height",(long)(double)Double.valueOf(jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optJSONObject("product").optJSONArray("values").optJSONObject(n).optJSONObject("nameValuePairs").optString("cover_content_height")));
                    product.put("cover_border_left",(long)(double)Double.valueOf(jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optJSONObject("product").optJSONArray("values").optJSONObject(n).optJSONObject("nameValuePairs").optString("cover_border_left")));
                    product.put("cover_border_top",(long)(double)Double.valueOf(jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optJSONObject("product").optJSONArray("values").optJSONObject(n).optJSONObject("nameValuePairs").optString("cover_border_top")));
                    product.put("cover_title_area_width",(long)(double)Double.valueOf(jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optJSONObject("product").optJSONArray("values").optJSONObject(n).optJSONObject("nameValuePairs").optString("cover_title_area_width")));
                    product.put("cover_title_area_height",(long)(double)Double.valueOf(jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optJSONObject("product").optJSONArray("values").optJSONObject(n).optJSONObject("nameValuePairs").optString("cover_title_area_height")));
                    product.put("cover_title_area_top",(long)(double)Double.valueOf(jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optJSONObject("product").optJSONArray("values").optJSONObject(n).optJSONObject("nameValuePairs").optString("cover_title_area_top")));
                    product.put("cover_title_area_left",(long)(double)Double.valueOf(jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optJSONObject("product").optJSONArray("values").optJSONObject(n).optJSONObject("nameValuePairs").optString("cover_title_area_left")));
                    product.put("cover_title_font_size",(long)(double)Double.valueOf(jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optJSONObject("product").optJSONArray("values").optJSONObject(n).optJSONObject("nameValuePairs").optString("cover_title_font_size")));
                    product.put("cover_subtitle_font_size",(long)(double)Double.valueOf(jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optJSONObject("product").optJSONArray("values").optJSONObject(n).optJSONObject("nameValuePairs").optString("cover_subtitle_font_size")));
                    product.put("cover_title_top",(long)(double)Double.valueOf(jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optJSONObject("product").optJSONArray("values").optJSONObject(n).optJSONObject("nameValuePairs").optString("cover_title_top")));
                    product.put("cover_subtitle_top",(long)(double)Double.valueOf(jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optJSONObject("product").optJSONArray("values").optJSONObject(n).optJSONObject("nameValuePairs").optString("cover_subtitle_top")));
                    product.put("special_price",(long)(double)Double.valueOf(jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optJSONObject("product").optJSONArray("values").optJSONObject(n).optJSONObject("nameValuePairs").optString("special_price")));
                    product.put("title",jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optJSONObject("product").optJSONArray("values").optJSONObject(n).optJSONObject("nameValuePairs").optString("title"));

                }



                JSONObject jsonObject1 = new JSONObject();
                try {
                   jsonObject1.put("thumb",imageString);
//                    jsonObject1.put("thumb",jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optJSONObject("image").optJSONObject("nameValuePairs").optString("large"));
                }catch (Exception e){
                    e.printStackTrace();
                    jsonObject1.put("thumb","");
                }

                product.put("images", jsonObject1);



                dataSource.add(new JSONObject(product.toString()));
                recyclerAdapterCheckOut.notifyDataSetChanged();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    protected void drawTotal() {

        if(percentVoucher !=0){
            if ( priceVoucher != 0){
                totalSementara = totalPrice + (int)totalSipping;
                totalSementara = totalSementara - priceVoucher;
                etDiscount.setText(Globals.currencyFormatIDR((double) priceVoucher));
                rpPromocode.setText(Globals.currencyFormatIDR((double) priceVoucher));
                totalAmount();

            }else {
                totalSementara = totalPrice + (int)totalSipping;
                int discount = (int)totalPrice * percentVoucher /100;
                totalSementara = totalSementara - discount;
                etDiscount.setText(Globals.currencyFormatIDR((double) discount));
                rpPromocode.setText(Globals.currencyFormatIDR((double)discount));
                totalAmount();
            }
        }else {
            totalSementara = totalPrice + (int)totalSipping;
            totalAmount();
            /*if(totalSipping!=0){

            }else {
                totalSementara = totalPrice+
                totalAmount();
            }*/
        }
    }

    private void totalAmount() {
//        etSubtotal.setText(String.format(getResources().getString(R.string.text_cart_total_of_item), String.valueOf(totalItem)));
        etSubtotal.setText(Globals.currencyFormatIDR(totalPrice));
        EditText tempEditText   = new EditText(context);
        tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
        tempEditText.setText(String.valueOf((int)totalSementara));
        etTotal.setText("IDR "+tempEditText.getText().toString());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            back();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            back();
            return true;
        }
        else {
            return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!reorder){
            checkCarts();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

      /*  Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();

        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();*/

        if (resultCode == RESULT_OK && requestCode == BROWSE_VOUCHER) {
            Bundle tempExtras       = data.getExtras();
            voucherId               = tempExtras.getLong("voucherId");
            voucherCode             = tempExtras.getString("voucherCode");
            voucherTotal            = tempExtras.getDouble("voucherTotal");
            voucherPercent          = tempExtras.getDouble("voucherPercent");

            EditText tempEditText   = new EditText(context);
            tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
            if(voucherTotal > 0) {
                tempEditText.setText(String.valueOf((int) voucherTotal));
                buttonVoucher.setText(voucherCode + " (IDR " + tempEditText.getText().toString() + ")");
            }else{
                tempEditText.setText(String.valueOf((int) voucherPercent));
                buttonVoucher.setText(voucherCode + " (" + tempEditText.getText().toString() + "%)");
            }
            buttonDeleteVoucher.setVisibility(View.VISIBLE);
        }
        else if (resultCode == RESULT_OK && requestCode == BROWSE_USER_ADDRESS) {
            Bundle tempExtras       = data.getExtras();
            userAddressId           = tempExtras.getLong("selectedId");
            provinceId              = tempExtras.getLong("provinceId");
            cityId                  = tempExtras.getLong("cityId");
            firstName               = tempExtras.getString("firstName");
            lastName                = tempExtras.getString("lastName");
            address                 = tempExtras.getString("address");
            provinceName            = tempExtras.getString("provinceName");
            cityName                = tempExtras.getString("cityName");
            postalCode              = tempExtras.getString("postalCode");
            phone                   = tempExtras.getString("phone");
            email                   = tempExtras.getString("email");
            Globals.setDataPreference(this,"posisi",Integer.valueOf(tempExtras.getInt("posisi")));

            textViewUserAddress1.setTypeface(type);
            textViewUserAddress2.setTypeface(type);
            textViewUserAddress1.setText(firstName + " " + lastName);
            textViewUserAddress2.setText(address + "\n" + cityName + ", " + provinceName + " - " + postalCode + "\n" + phone + " / " + email);
            findViewById(R.id.linearLayoutUserAddress).setVisibility(View.VISIBLE);

            loadDataKurir();
            totalSipping = 0;
            etShipping.setText(Globals.currencyIDRFormat(0));
            drawTotal();
        }
        else if (resultCode == RESULT_OK && requestCode == BROWSE_PAYMENT) {
            Bundle tempExtras       = data.getExtras();
            paymentId               = tempExtras.getLong("selectedId");
            paymentName             = tempExtras.getString("paymentName");
            paymentType             = tempExtras.getString("paymentType");
            buttonChoosePayment.setText(paymentName);
        }

        if (requestCode == 9) {
            if(resultCode == Activity.RESULT_OK){
                double result= data.getDoubleExtra("result",0);
                etShipping.setTypeface(type);
                totalSipping = (long) result;

                DecimalFormat myFormatter = new DecimalFormat("#,###.###");
                String output = myFormatter.format(totalSipping).replaceAll(",", ".");
                etShipping.setText("Rp "+output);
                drawTotal();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }

        if (requestCode == 77) {
            if(resultCode == Activity.RESULT_OK){

                if (data != null){
                    if (promocodeIsActivated){
                        Globals.showAlert("Sorry..","You can not apply for both PROMO CODE and GIFT CARD",this);
                    }else {
                        lineDiscount.setVisibility(View.VISIBLE);
                        txDisCount.setText("Discount Gift Card");
                        data.getLongExtra("corporateVoucherId",0);//long
                        data.getStringExtra("corporateVoucherName");//String
                        data.getIntExtra("corporateVoucherQty",0);//int
                        data.getStringExtra("corporateVoucherCode");//String

                        percentVoucher = 90;
                        priceVoucher = data.getIntExtra("corporateVoucherQty",0);
                        promocodeIsActivated = true;
                        if (priceVoucher>0){
                            linePromocodeDetail.setVisibility(View.VISIBLE);
                            lineGiftCard.setVisibility(View.VISIBLE);
                            lineRpPromocode.setVisibility(View.GONE);
                            rpGiftCard.setText(Globals.currencyFormatIDR((double)data.getIntExtra("corporateVoucherQty",0)));
                        }
                        drawTotal();
                    }
                }else {
                    priceVoucher = 0;
                    percentVoucher = 0;
                    promocodeIsActivated = false;
                    lineDiscount.setVisibility(View.GONE);
                    linePromocodeDetail.setVisibility(View.GONE);
                    lineGiftCard.setVisibility(View.GONE);
                    lineRpPromocode.setVisibility(View.GONE);

                    drawTotal();
                }


            }
            if (resultCode == Activity.RESULT_CANCELED) {

            }
        }

        if (requestCode == 99) {
            if(resultCode == Activity.RESULT_OK){
                loadDataaddress();
            }if (resultCode == Activity.RESULT_CANCELED) {

            }
        }

    }


    protected boolean checkIsJustCorporate() {
        boolean flag = true;

        for(Cart cart : carts) {
            Project project = db.getProject(cart.getProjectId());
            if (project.getCorporateVoucherId() <= 0 || (project.getCorporateVoucherId() > 0 && cart.getQty() > project.getCorporateVoucherQty())) {
                flag = false;
            }
        }

        return flag;
    }


    protected void checkCarts() {
        carts               = db.getAllCarts();
        if (carts.size() <= 0 ) {
            Intent intent = new Intent(this, ProjectActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    protected void back() {
        new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.text_back_title)
                .setMessage(R.string.text_back_message)
                .setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton(R.string.button_no, null)
                .show();
    }

    @Override
    public void onTransactionFinished(TransactionResult result) {
        if (result.getResponse() != null) {
            switch (result.getStatus()) {
                case TransactionResult.STATUS_SUCCESS:
                    setLog(gson.toJson(result.getResponse()));
                    setSendData();
                    if (reorder){
                        submitReorder(result.getResponse());
                    }else {
                        submit(result.getResponse());
                    }
//                    Toast.makeText(this, "Transaction Succes: " + result.getResponse().getTransactionId(), Toast.LENGTH_LONG).show();
                    break;
                case TransactionResult.STATUS_PENDING:
                    finish();
                    Toast.makeText(this, "Transaction telah berhasil silahkan melakukan pembayaran : ",Toast.LENGTH_LONG).show();
                    break;
                case TransactionResult.STATUS_FAILED:
                    Toast.makeText(this, "Transaction Failed. ID: " + result.getResponse().getTransactionId() + ". Message: " + result.getResponse().getStatusMessage(), Toast.LENGTH_LONG).show();
                    break;
            }
            result.getResponse().getValidationMessages();
        } else if (result.isTransactionCanceled()) {
            Toast.makeText(this, "Transaction Canceled", Toast.LENGTH_LONG).show();
        } else {
            if (result.getStatus().equalsIgnoreCase(TransactionResult.STATUS_INVALID)) {
                Toast.makeText(this, "Transaction Invalid", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Transaction Finished with failure.", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onClick(int holder, final int position) {

        int viewId = holder;
        if (viewId == R.id.buttonMinusCart) {
            if (reorder){
                minusReorder(position);
            }else {
                minusQtyCart(carts.get(position).getProjectId());
                loadData();
            }
        } else if (viewId == R.id.buttonAddCart) {
            if (reorder){
                plusReorder(position);
            }else {
                addToCart(carts.get(position).getProjectId());
                loadData();
                setLog("add cart");
            }
        } else if (viewId == R.id.buttonDeleteCart) {
            if (reorder){
                deletreorder(position);
            }else {
                new AlertDialog.Builder(context)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(R.string.text_cart_delete_title)
                        .setMessage(R.string.text_cart_delete_message)
                        .setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                removeCart(carts.get(position).getProjectId());
                                loadData();
                            }

                        })
                        .setNegativeButton(R.string.button_no, null)
                        .show();
            }
        }
    }

    private void plusReorder(int position) {
        try {
            dataSource.get(position).put("cart_qty",dataSource.get(position).getInt("cart_qty")+1);
            dataSource.get(position).put("price",dataSource.get(position).getLong("price")+countMint.get(position));
            /*for (int a =0;a<dataSource.get(position).optJSONArray("values").length();a++){
                dataSource.get(position).put("cart_qty",dataSource.get(position).optJSONArray("values").optJSONObject(a).optJSONObject("nameValuePairs").optJSONObject("product").optJSONArray("values").length()+1);
                dataSource.get(position).put("price", (long)(double)Double.valueOf(dataSource.get(position).optJSONArray("values").optJSONObject(a).optJSONObject("nameValuePairs").optString("price"))*dataSource.get(position).getInt("cart_qty"));
            }
            */
            setTotal();
            drawTotal();
            recyclerAdapterCheckOut.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //            cart_qty totalPrice (int) item.getLong("special_price") > 0 ? item.getLong("special_price") : item.getLong("price"))
    }

    private void setTotal() {
        long total = 0;
        for (int i=0;i<dataSource.size();i++){
            try {
                total = total+dataSource.get(i).getLong("price");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        totalPrice = total;
    }

    ArrayList<Long> countMint = new ArrayList<>();

    private void minusReorder(int position) {
        try {
            dataSource.get(position).put("cart_qty",dataSource.get(position).getInt("cart_qty")-1);
            dataSource.get(position).put("price",dataSource.get(position).getLong("price")-countMint.get(position));
            if (dataSource.get(position).getInt("cart_qty")==0){
                dataSource.remove(position);
                countMint.remove(position);
            }
            drawTotal();
            recyclerAdapterCheckOut.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void deletreorder(int position) {
        dataSource.remove(position);
        drawTotal();
        recyclerAdapterCheckOut.notifyDataSetChanged();
    }

    public void setSendData(){
        setLog("clientId "+Constant.CLIENT_ID);
        setLog("accessToken "+ Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
        setLog("provinceId "+String.valueOf(provinceId));
        setLog("cityId "+ String.valueOf(cityId));
        setLog("firstName "+ firstName);
        setLog("lastName "+ lastName);
        setLog("address "+ address);
        setLog("province "+ provinceName);
        setLog("city "+ cityName);
        setLog("postalCode "+ postalCode);

        setLog("phone "+ phone);
        setLog("email "+ email);
        setLog("paymentType "+ paymentType);
        setLog("shippingPrice "+ String.valueOf(totalSipping));
        setLog("shippingType "+ namaShipping+"-"+shippingType);
        setLog("voucherCode "+ voucherId);
        setLog("voucherId " +String.valueOf(voucherId));
        setLog("voucherTotal "+ String.valueOf(etDiscount.getText().toString().substring(3,etDiscount.getText().toString().length()).trim()));
        setLog("orderid_reorder "+id_order);

        JSONArray orderProduct          = new JSONArray();
        JSONArray orderProductDetail    = new JSONArray();

        try {
            for (int i = 0; i < dataSource.size(); i++) {

                JSONObject tempOrderProduct = new JSONObject();
                tempOrderProduct.put("project_id", dataSource.get(i).optString("project_id"));
                tempOrderProduct.put("product_id", dataSource.get(i).optString("product_id"));
                tempOrderProduct.put("reorder_id", String.valueOf(dataSource.get(i).optInt("reorder_id")));
                tempOrderProduct.put("title", dataSource.get(i).optString("cover_title"));
                tempOrderProduct.put("subtitle", dataSource.get(i).optString("cover_subtitle"));
                tempOrderProduct.put("qty", dataSource.get(i).optString("cart_qty"));
                tempOrderProduct.put("corporate_voucher_id", dataSource.get(i).optString("corporate_voucher_id"));
                tempOrderProduct.put("corporate_voucher_code", dataSource.get(i).optString("corporate_voucher_code") != null || dataSource.get(i).optString("corporate_voucher_code").equalsIgnoreCase("null") ? "" : dataSource.get(i).optString("corporate_voucher_code"));

                for (int j = 0; j < dataSource.size(); j++) {
                    JSONObject tempOrderProductDetail = new JSONObject();
                    tempOrderProductDetail.put("project_id", dataSource.get(j).optString("project_id"));
                    tempOrderProductDetail.put("project_detail_id", "0");
                    tempOrderProductDetail.put("product_detail_id", "0");
                    orderProductDetail.put(new JSONObject(tempOrderProductDetail.toString()));
                }

                orderProduct.put(new JSONObject(tempOrderProduct.toString()));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        setLog("orderProduct "+ orderProduct.toString());
        setLog("orderProductDetail"+ orderProductDetail.toString());



    }

    protected void submitReorder(final TransactionResponse responses) {
        showLoading();
        Map<String, String> params = new HashMap<String, String>();
        params.put("clientId", Constant.CLIENT_ID);
        params.put("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
        params.put("provinceId", String.valueOf(provinceId));
        params.put("cityId", String.valueOf(cityId));
        params.put("firstName", firstName);
        params.put("lastName", lastName);
        params.put("address", address);
        params.put("province", provinceName);
        params.put("city", cityName);
        params.put("postalCode", postalCode);

        params.put("shippingEtd",etEstimatedDelivery.getText().toString());
        params.put("phone", phone);
        params.put("email", email);
        params.put("paymentType", paymentType);
        params.put("shippingPrice", String.valueOf(totalSipping));
        params.put("shippingType", namaShipping+"-"+shippingType);
        params.put("voucherCode", voucherCode);
        params.put("voucherId", String.valueOf(voucherId));
        params.put("orderid_reorder",String.valueOf(id_order));
        params.put("voucherTotal", String.valueOf(etDiscount.getText().toString().substring(3,etDiscount.getText().toString().length()).trim()));

        final JSONArray orderProduct      = new JSONArray();
        JSONArray orderProductDetail      = new JSONArray();

        try {
            for (int i = 0; i < dataSource.size(); i++) {

                JSONObject tempOrderProduct = new JSONObject();
                tempOrderProduct.put("project_id", dataSource.get(i).optString("project_id"));
                tempOrderProduct.put("product_id", dataSource.get(i).optString("product_id"));
                tempOrderProduct.put("reorder_id", String.valueOf(dataSource.get(i).optInt("reorder_id")));
                tempOrderProduct.put("title", dataSource.get(i).optString("cover_title"));
                tempOrderProduct.put("subtitle", dataSource.get(i).optString("cover_subtitle"));
                tempOrderProduct.put("qty", dataSource.get(i).optString("cart_qty"));
                tempOrderProduct.put("corporate_voucher_id", dataSource.get(i).optString("corporate_voucher_id"));
                tempOrderProduct.put("corporate_voucher_code", dataSource.get(i).opt("corporate_voucher_code") == null || dataSource.get(i).optString("corporate_voucher_code").equals("null") ? "" : dataSource.get(i).optString("corporate_voucher_code"));

                for (int j = 0; j < dataSource.size(); j++) {
                    JSONObject tempOrderProductDetail = new JSONObject();
                    tempOrderProductDetail.put("project_id", dataSource.get(j).optString("project_id"));
                    tempOrderProductDetail.put("project_detail_id", "0");
                    tempOrderProductDetail.put("product_detail_id", "0");
                    orderProductDetail.put(new JSONObject(tempOrderProductDetail.toString()));
                }

                orderProduct.put(new JSONObject(tempOrderProduct.toString()));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        params.put("orderProduct", orderProduct.toString());
        params.put("orderProductDetail", orderProductDetail.toString());

        setLog("---- test 2" +gson.toJson(orderProduct));
        setLog("---- test 3" +gson.toJson(orderProductDetail));

        setLog("----- test 1 "+voucherId);
        request.getJSONFromUrl(
                Globals.getApiUrl("orders/add"), params,getResources().getString(R.string.text_checkout_error),
                new RequestCallback() {
                    @Override
                    public void onResponse(final JSONObject response) {
                        setLog("sukses reorder = "+id_order+" "+response.toString());
                        hideLoading();
                        startActivity(MainActivity.class);
                    }

                    @Override
                    public void onError(VolleyError error) {
                        hideLoading();
                    }
                }
        );
    }

    protected void submit(final TransactionResponse responses) {
        showLoading();
        Map<String, String> params = new HashMap<String, String>();
        params.put("clientId", Constant.CLIENT_ID);
        params.put("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
        params.put("provinceId", String.valueOf(provinceId));
        params.put("cityId", String.valueOf(cityId));
        params.put("firstName", firstName);
        params.put("lastName", lastName);
        params.put("address", address);
        params.put("province", provinceName);
        params.put("city", cityName);
        params.put("postalCode", postalCode);

        params.put("shippingEtd",etEstimatedDelivery.getText().toString());
        params.put("phone", phone);
        params.put("email", email);
        params.put("paymentType", paymentType);
        params.put("shippingPrice", String.valueOf(totalSipping));
        params.put("shippingType", namaShipping+"-"+shippingType);
        params.put("voucherCode", voucherCode);
        params.put("voucherId", String.valueOf(voucherId));
        params.put("orderid_reorder","0");
        params.put("voucherTotal", String.valueOf(etDiscount.getText().toString().substring(3,etDiscount.getText().toString().length()).trim()).replace(".",""));

        JSONArray orderProduct          = new JSONArray();
        JSONArray orderProductDetail    = new JSONArray();
        setLog("----- test 1"+voucherId);

        try {
            for (int i = 0; i < carts.size(); i++) {
                Project project = db.getProject(carts.get(i).getProjectId());
                List<ProjectDetail> projectDetail = db.getAllProjectDetailsByProjectId(project.getID());

                JSONObject tempOrderProduct = new JSONObject();
                tempOrderProduct.put("project_id", project.getID());
                tempOrderProduct.put("product_id", project.getProductId());
                tempOrderProduct.put("reorder_id", project.getReorderId());
                tempOrderProduct.put("title", project.getTitle());
                tempOrderProduct.put("subtitle", project.getSubtitle());
                tempOrderProduct.put("qty", carts.get(i).getQty());
                tempOrderProduct.put("corporate_voucher_id", project.getCorporateVoucherId());
                tempOrderProduct.put("corporate_voucher_code", project.getCorporateVoucherCode() == null || project.getCorporateVoucherCode().equals("null") ? "" : project.getCorporateVoucherCode());

                for (int j = 0; j < projectDetail.size(); j++) {
                    JSONObject tempOrderProductDetail = new JSONObject();
                    tempOrderProductDetail.put("project_id", project.getID());
                    tempOrderProductDetail.put("project_detail_id", projectDetail.get(j).getID());
                    tempOrderProductDetail.put("product_detail_id", projectDetail.get(j).getProductDetailId());
                    orderProductDetail.put(new JSONObject(tempOrderProductDetail.toString()));
                }

                orderProduct.put(new JSONObject(tempOrderProduct.toString()));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        params.put("orderProduct", orderProduct.toString());
        params.put("orderProductDetail", orderProductDetail.toString());

        setLog("---- test 2" +gson.toJson(orderProduct));
        setLog("---- test 3" +gson.toJson(orderProductDetail));
        request.getJSONFromUrl(
                Globals.getApiUrl("orders/add"), params,getResources().getString(R.string.text_checkout_error),
                new RequestCallback() {
                    @Override
                    public void onResponse(final JSONObject response) {
                        setLog("sukses add"+response.toString());
                        hideLoading();
                        long id = 0;
                        String number = "";
                        try {
                            JSONObject order    = response.getJSONObject("order");
                            id                  = order.getLong("id");
                            number              = order.getString("number");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (id == 0) {
                            //error
                            Globals.showAlert(
                                    getResources().getString(R.string.text_checkout_error),
                                    getResources().getString(R.string.text_checkout_error_unknown),
                                    activity
                            );
                            hideLoading();
                        } else {
                            for (Cart cart : carts) {
                                Project project = db.getProject(cart.getProjectId());
                                project.setCorporateVoucherId(-1);
                                project.setCorporateVoucherCode("");
                                project.setCorporateVoucherName("");
                                project.setCorporateVoucherQty(-1);
                                db.updateProject(project);
                            }

                            final long orderId    = id;
                            final String orderNumber = number;
                            startActivity(ProgressActivity.class, new OnSetupIntentExtras() {
                                @Override
                                public void onSetupIntentExtras(Intent intent)  {
                                    intent.putExtra("orderId", orderId);
                                    intent.putExtra("orderNumber", orderNumber);
                                    try {
                                        intent.putExtra("grandTotal", Double.valueOf(response.getJSONObject("order").optString("grand_total")));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    intent.putExtra("paymentType", responses.getPaymentType());
                                }
                            });
                        }
                    }

                    @Override
                    public void onError(VolleyError error) {
                        hideLoading();
                    }
                }
        );
    }

}
