package com.triodigitalagency.memome.transaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.android.volley.VolleyError;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.adapter.NotPaidArrayAdapter;
import com.triodigitalagency.memome.adapter.UserAddressArrayAdapter;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;
import com.triodigitalagency.memome.webview.WebViewActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ListNotPaidActivity extends BaseActivity {

    //<editor-fold desc="Variable">
    protected NotPaidArrayAdapter arrayAdapter;
    protected ListView listView;
    protected ArrayList<JSONObject> dataSource;
    protected Bundle extras;

    protected RelativeLayout tapForReloadLayout;
    protected Button buttonTapForReload;
    //</editor-fold>

    //<editor-fold desc="Override">
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.showCartIcon   = true;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_not_paid);
        setLog("activity_choose_confirm_order");

        firstInit();
        setupActionBar(getResources().getString(R.string.title_payment));

        dataSource       = new ArrayList<JSONObject>();
        listView         = (ListView)findViewById(R.id.listView);
        arrayAdapter     = new NotPaidArrayAdapter(context);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long l) {
                //arrayAdapter.setSelectedId(arrayAdapter.getItemId(position));
                //arrayAdapter.notifyDataSetChanged();

                if (arrayAdapter.getPaymentType(position).equals(Constant.PAYMENT_CREDIT_CARD)) {
                    startActivity(WebViewActivity.class, new OnSetupIntentExtras() {
                        @Override
                        public void onSetupIntentExtras(Intent intent) {
                            intent.putExtra("title", getResources().getString(R.string.title_payment));
                            intent.putExtra("url",  String.format("%1$s?id=%2$d&grand_total=%3$d", Constant.PAYMENT_URL, arrayAdapter.getItemId(position), (long)arrayAdapter.getItemGrandTotal(position)));
                        }
                    });
                }
                else {
                    startActivity(ConfirmationActivity.class, new OnSetupIntentExtras() {
                        @Override
                        public void onSetupIntentExtras(Intent intent) {
                            intent.putExtra("orderId", arrayAdapter.getItemId(position));
                            intent.putExtra("orderNumber", arrayAdapter.getItemNumber(position));
                            intent.putExtra("grandTotal", arrayAdapter.getItemGrandTotal(position));
                        }
                    });
                }
            }
        });

        loadData();
        initTapForReload();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_default, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        else if (id == R.id.actionCart) {
            startActivity(CartActivity.class);
        }

        return super.onOptionsItemSelected(item);
    }
    //</editor-fold>

    //<editor-fold desc="Procedure & Function">
    protected void loadData() {
        if (!isLoading()) {
            showLoading();
        }

        Map<String, String> params = new HashMap<String, String>();
        params.put("clientId", Constant.CLIENT_ID);
        params.put("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
        request.getJSONFromUrl(Globals.getApiUrl("orders/getOrderNotYetPaid"), params, getResources().getString(R.string.text_checkout_error), new RequestCallback() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray orders    = response.getJSONArray("orders");
                    for (int i = 0; i < orders.length(); i++) {
                        dataSource.add(orders.getJSONObject(i));
                    }

                    findViewById(R.id.textViewDataNotFound).setVisibility(orders.length() <= 0 ? View.VISIBLE : View.GONE);

                    arrayAdapter.setValues(dataSource);
                    arrayAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                hideLoading();
            }

            @Override
            public void onError(VolleyError error) {
                tapForReloadLayout.setVisibility(View.VISIBLE);
                hideLoading();
            }
        });
    }

    protected void initTapForReload() {
        buttonTapForReload  = (Button)findViewById(R.id.buttonTapForReload);
        buttonTapForReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tapForReloadLayout.setVisibility(View.GONE);
                loadData();
            }
        });

        tapForReloadLayout  = (RelativeLayout)findViewById(R.id.tapForReload);
        tapForReloadLayout.setVisibility(View.GONE);
    }
    //</editor-fold>
}
