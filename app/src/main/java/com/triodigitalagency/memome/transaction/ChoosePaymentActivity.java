package com.triodigitalagency.memome.transaction;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.adapter.PaymentArrayAdapter;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ChoosePaymentActivity extends BaseActivity {

    //<editor-fold desc="Variable">
    protected PaymentArrayAdapter arrayAdapter;
    protected ListView listView;
    protected ArrayList<JSONObject> dataSource;
    protected Bundle extras;

    protected RelativeLayout tapForReloadLayout;
    protected Button buttonTapForReload;

    private long selectedId = -1;
    //</editor-fold>

    //<editor-fold desc="Override">
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_payment);
        setLog("activity_choose_confirm_order");

        DisplayMetrics metrics  = getResources().getDisplayMetrics();
        int screenHeight        = (int) (metrics.heightPixels * 0.40);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, screenHeight);

        firstInit();

        extras = getIntent().getExtras();
        if (extras != null){
            if (extras.get("selectedId") != null && extras.getLong("selectedId") > 0) {
                selectedId = extras.getLong("selectedId");
            }
        }

        dataSource       = new ArrayList<JSONObject>();
        listView         = (ListView)findViewById(R.id.listView);
        arrayAdapter     = new PaymentArrayAdapter(context);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                selectedId = arrayAdapter.getItemId(position);
                arrayAdapter.setSelectedId(selectedId);
                arrayAdapter.notifyDataSetChanged();

                Intent data = new Intent();
                data.putExtra("selectedId", selectedId);
                data.putExtra("paymentType", arrayAdapter.getItemType(position));
                data.putExtra("paymentName", arrayAdapter.getItemName(position));

                setResult(RESULT_OK, data);
                finish();
            }
        });

        loadData();
    }
    //</editor-fold>

    //<editor-fold desc="Procedure & Function">
    protected void loadData() {
        try {
            dataSource.add(new JSONObject(String.format("{name:'%1$s', type:'%2$s', id: %3$d}", Constant.PAYMENT_TRANSFER_NAME, Constant.PAYMENT_TRANSFER, Constant.PAYMENT_TRANSFER_ID)));
            dataSource.add(new JSONObject(String.format("{name:'%1$s', type:'%2$s', id: %3$d}", Constant.PAYMENT_CREDIT_CARD_NAME, Constant.PAYMENT_CREDIT_CARD, Constant.PAYMENT_CREDIT_CARD_ID)));
        }
        catch (JSONException e) {}

        if (selectedId > 0) {
            arrayAdapter.setSelectedId(selectedId);
        }

        arrayAdapter.setValues(dataSource);
        arrayAdapter.notifyDataSetChanged();
    }

    //</editor-fold>
}
