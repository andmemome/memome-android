package com.triodigitalagency.memome.transaction;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.region.CityActivity;
import com.triodigitalagency.memome.region.ProvinceActivity;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class InputAddressActivity extends BaseActivity {

    //<editor-fold desc="Variable">
    protected Bundle extras;

    private Long provinceId, cityId;
    private EditText editTextFirstName, editTextLastName, editTextAddress, editTextPostalCode, editTextPhone, editTextEmail;
    private TextView textViewProvince, textViewCity;

    private final int BROWSE_PROVINCE = 1;
    private final int BROWSE_CITY = 2;
    //</editor-fold>

    //<editor-fold desc="Override">
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_address);
        setLog("activity_choose_confirm_order");

        firstInit();
        setupActionBar(getResources().getString(R.string.title_user_address));

        findViewById(R.id.buttonSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });
        editTextFirstName   = (EditText)findViewById(R.id.editTextFirstName);
        editTextLastName    = (EditText)findViewById(R.id.editTextLastName);
        editTextAddress     = (EditText)findViewById(R.id.editTextAddress);
        editTextPostalCode  = (EditText)findViewById(R.id.editTextPostalCode);
        editTextPhone       = (EditText)findViewById(R.id.editTextPhone);
        editTextEmail       = (EditText)findViewById(R.id.editTextEmail);
        textViewProvince    = (TextView)findViewById(R.id.textViewProvince);
        textViewProvince.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(ProvinceActivity.class, BROWSE_PROVINCE, new OnSetupIntentExtras() {
                    @Override
                    public void onSetupIntentExtras(Intent intent) {
                        if (provinceId != null && provinceId > 0) {
                            intent.putExtra("selectedId", provinceId);
                        }
                    }
                });
            }
        });
        textViewCity        = (TextView)findViewById(R.id.textViewCity);
        textViewCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (provinceId != null && provinceId > 0) {
                    startActivityForResult(CityActivity.class, BROWSE_CITY, new OnSetupIntentExtras() {
                        @Override
                        public void onSetupIntentExtras(Intent intent) {
                            intent.putExtra("provinceId", provinceId);
                            if (cityId != null && cityId > 0) {
                                intent.putExtra("selectedId", cityId);
                            }
                        }
                    });
                } else {
                    Globals.showAlert(getResources().getString(R.string.text_city_error), getResources().getString(R.string.text_province_required), activity);
                }
            }
        });

        extras              = getIntent().getExtras();
        if (extras != null) {
            provinceId          = extras.getLong("provinceId");
            cityId              = extras.getLong("cityId");
            editTextFirstName.setText(extras.getString("firstName"));
            editTextLastName.setText(extras.getString("lastName"));
            editTextAddress.setText(extras.getString("address"));
            editTextPostalCode.setText(extras.getString("postalCode"));
            editTextPhone.setText(extras.getString("phone"));
            editTextEmail.setText(extras.getString("email"));
            textViewProvince.setText(extras.getString("provinceName"));
            textViewCity.setText(extras.getString("cityName"));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == BROWSE_PROVINCE && resultCode == RESULT_OK){
            Bundle temp     = data.getExtras();
            provinceId      = temp.getLong("selectedId");
            cityId          = Long.valueOf(0);
            textViewProvince.setText(temp.getString("selectedName"));
            textViewCity.setText("");
        }
        else if (requestCode == BROWSE_CITY && resultCode == RESULT_OK) {
            Bundle temp     = data.getExtras();
            cityId          = temp.getLong("selectedId");
            textViewCity.setText(temp.getString("selectedName"));
        }
    }
    //</editor-

    //<editor-fold desc="Procedure & Function">
    protected void submit() {
        Boolean isGood  = true;
        isGood          = validateEditTextStringRequired(isGood);
        isGood          = validateSelectRequired(isGood);
        if (isGood && !Globals.isValidEmail(editTextEmail.getText().toString())){
            Globals.showAlert(getResources().getString(R.string.text_address_error),
                    getResources().getString(R.string.text_invalid_email),
                    activity);
            isGood = false;
        }

        CheckBox checkBox   = (CheckBox)findViewById(R.id.checkBoxSaveAddress);
        if (isGood && checkBox.isChecked()) {
            if (!isLoading()) {
                showLoading();
            }

            Map<String, String> params = new HashMap<String, String>();
            params.put("clientId", Constant.CLIENT_ID);
            params.put("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
            params.put("firstName", editTextFirstName.getText().toString());
            params.put("lastName", editTextLastName.getText().toString());
            params.put("address", editTextAddress.getText().toString());
            params.put("provinceId", String.valueOf(provinceId));
            params.put("cityId", String.valueOf(cityId));
            params.put("postalCode", editTextPostalCode.getText().toString());
            params.put("phone", editTextPhone.getText().toString());
            params.put("email", editTextEmail.getText().toString());

            request.getJSONFromUrl(
                Globals.getApiUrl("users/addAddress"), params, getResources().getString(R.string.text_address_error),
                new RequestCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        long id = 0;
                        try {
                            JSONObject userAddress  = response.getJSONObject("userAddress");
                            id                      = userAddress.getLong("id");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (id == 0) {
                            //error
                            Globals.showAlert(
                                    getResources().getString(R.string.text_address_error),
                                    getResources().getString(R.string.text_add_user_address_error_unknown),
                                    activity
                            );
                            hideLoading();
                        } else {
                            hideLoading();
                            Intent data = setUserAddressIntent();
                            setResult(RESULT_OK, data);
                            finish();
                        }

                    }

                    @Override
                    public void onError(VolleyError error) {
                        hideLoading();
                    }
                }
            );
        }
        else if (isGood && !checkBox.isChecked()) {
            Intent data = setUserAddressIntent();
            setResult(RESULT_OK, data);
            finish();
        }
    }

    protected Intent setUserAddressIntent() {
        Intent data   = new Intent();
        data.putExtra("selectedId", -1);
        data.putExtra("provinceId", provinceId);
        data.putExtra("cityId", cityId);
        data.putExtra("firstName", editTextFirstName.getText().toString());
        data.putExtra("lastName", editTextLastName.getText().toString());
        data.putExtra("address", editTextAddress.getText().toString());
        data.putExtra("provinceName", textViewProvince.getText().toString());
        data.putExtra("cityName", textViewCity.getText().toString());
        data.putExtra("postalCode", editTextPostalCode.getText().toString());
        data.putExtra("phone", editTextPhone.getText().toString());
        data.putExtra("email", editTextEmail.getText().toString());
        return data;
    }

    protected Boolean validateEditTextStringRequired(Boolean isGood) {
        if (isGood) {
            EditText[] requiredFields       = new EditText[]{editTextFirstName, editTextLastName, editTextAddress, editTextPostalCode, editTextPhone, editTextEmail};
            String[] requiredFieldNames     = new String[]{
                    getResources().getString(R.string.hint_first_name),
                    getResources().getString(R.string.hint_last_name),
                    getResources().getString(R.string.hint_address),
                    getResources().getString(R.string.hint_postal_code),
                    getResources().getString(R.string.hint_phone),
                    getResources().getString(R.string.hint_email),
            };

            int i = 0;
            for (EditText field : requiredFields) {
                if (field.getText().toString().trim().equals("")) {
                    Globals.showAlert(
                            getResources().getString(R.string.text_address_error),
                            String.format(getResources().getString(R.string.text_add_error_required), requiredFieldNames[i]),
                            activity
                    );
                    isGood = false;
                    break;
                }
                i++;
            }
        }
        return isGood;
    }

    protected Boolean validateSelectRequired(Boolean isGood) {
        if (isGood) {
            Long[] requiredFields           = new Long[]{provinceId, cityId};
            String[] requiredFieldNames     = new String[]{
                    getResources().getString(R.string.hint_province),
                    getResources().getString(R.string.hint_city),
            };

            int i = 0;
            for (Long field : requiredFields) {
                if (field == null || field <= 0) {
                    Globals.showAlert(
                            getResources().getString(R.string.text_address_error),
                            String.format(getResources().getString(R.string.text_add_error_required), requiredFieldNames[i]),
                            activity
                    );
                    isGood = false;
                    break;
                }
                i++;
            }
        }
        return isGood;
    }
    //</editor-fold>
}
