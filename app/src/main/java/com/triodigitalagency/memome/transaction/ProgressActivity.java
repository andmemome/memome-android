package com.triodigitalagency.memome.transaction;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alexbbb.uploadservice.AbstractUploadServiceReceiver;
import com.alexbbb.uploadservice.ContentType;
import com.alexbbb.uploadservice.MultipartUploadRequest;
import com.android.volley.VolleyError;
import com.triodigitalagency.memome.MainActivity;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.database.Cart;
import com.triodigitalagency.memome.database.DatabaseHandler;
import com.triodigitalagency.memome.database.Project;
import com.triodigitalagency.memome.database.ProjectDetail;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.utils.NumberTextWatcher;
import com.triodigitalagency.memome.volley.callback.RequestCallback;
import com.triodigitalagency.memome.webview.WebViewActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.microedition.khronos.opengles.GL;

public class ProgressActivity extends BaseActivity {

    private Bundle extras;
    private List<Cart> carts;
    private DatabaseHandler db;
    private long orderId;
    private String orderNumber;
    private double grandTotal;
    private String paymentType;
    private TextView textViewProgress, textViewOrderId, textViewGrandTotal;
    boolean uploadImagesStatus = false;

    private RelativeLayout tapForReloadLayout;
    private Button buttonTapForReload;
    private boolean isRevisit = false, isStillUploading = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress);
        setLog("activity_progress");

        Globals.setDataPreference(getApplicationContext(),"uploadfile",true);

        db                  = new DatabaseHandler(this);

        firstInit();
        setupActionBar(getResources().getString(R.string.title_progress));

        carts               = db.getAllCarts();
        try{
            extras              = getIntent().getExtras();
            orderId             = extras.getLong("orderId");
            orderNumber         = extras.getString("orderNumber");
            grandTotal          = extras.getDouble("grandTotal");
            paymentType         = extras.getString("paymentType");
            isRevisit           = extras.getBoolean("reVisit", false);

            Globals.setDataPreference(getApplicationContext(),extras.getLong("orderId"),extras.getString("orderNumber"),extras.getDouble("grandTotal"),extras.getString("paymentType"),extras.getBoolean("reVisit", false));

        }catch (Exception e){
            orderId             = Globals.getorderIdSession(getApplicationContext(),"orderId");
            orderNumber         = Globals.getorderNumberSession(getApplicationContext(),"orderNumber");
            grandTotal          = Double.parseDouble(Globals.getgrandTotalSession(getApplicationContext(),"grandTotal"));
            paymentType         = Globals.getpaymentTypeSession(getApplicationContext(),"paymentType");
            isRevisit           = Globals.getreVisitSession(getApplicationContext(),"reVisit");

            e.printStackTrace();
        }

        textViewProgress    = (TextView)findViewById(R.id.textViewProgress);
        textViewOrderId     = (TextView)findViewById(R.id.textViewOrderId);
        textViewGrandTotal  = (TextView)findViewById(R.id.textViewGrandTotal);

        if (paymentType.equals(Constant.PAYMENT_TRANSFER)) {
            LinearLayout linearLayoutPaymentTransfer = (LinearLayout)findViewById(R.id.linearLayoutPaymentTransfer);
            linearLayoutPaymentTransfer.setVisibility(View.VISIBLE);
        }

        textViewProgress.setText("0%");
        textViewOrderId.setText(String.format(getResources().getString(R.string.text_order_id), orderNumber));
        EditText tempEditText   = new EditText(context);
        tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
        tempEditText.setText(String.valueOf((int) grandTotal));
        textViewGrandTotal.setText(String.format(getResources().getString(R.string.text_total_amount), tempEditText.getText().toString()));

        findViewById(R.id.buttonClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                back();
            }
        });

        loadData();
        initTapForReload();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            back();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //Handle the back button
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            back();
            return true;
        }
        else {
            return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        uploadReceiver.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        uploadReceiver.unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        uploadReceiver.unregister(this);
    }

    //</editor-fold>

    //<editor-fold desc="Procedure & Function">
    protected final AbstractUploadServiceReceiver uploadReceiver = new AbstractUploadServiceReceiver() {
        @Override
        public void onProgress(String uploadId, int progress) {
            //Log.i(TAG, "The progress of the upload with ID " + uploadId + " is: " + progress);
            isStillUploading    = true;
            textViewProgress.setText(progress + "%");
        }

        @Override
        public void onError(String uploadId, Exception exception) {
            //Log.e(TAG, "Error in upload with ID: " + uploadId + ". " + exception.getLocalizedMessage(), exception);
            isStillUploading    = false;
            tapForReloadLayout.setVisibility(View.VISIBLE);
        }

        @Override
        public void onCompleted(String uploadId, int serverResponseCode, String serverResponseMessage) {
            hideLoading();
            uploadImagesCompleted();
        }
    };

    protected void uploadImagesCompleted() {
        isStillUploading    = false;
        Globals.setDataPreference(getApplicationContext(),"uploadfile",false);
        textViewProgress.setText("100%");
        uploadImagesStatus = true;

        for (int i = 0; i< carts.size(); i++) {
            db.deleteCart(carts.get(i));
        }

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (paymentType.equals(Constant.PAYMENT_TRANSFER)) {
                    startActivity(ConfirmationActivity.class, new OnSetupIntentExtras() {
                        @Override
                        public void onSetupIntentExtras(Intent intent) {
                            intent.putExtra("orderId", orderId);
                            intent.putExtra("orderNumber", orderNumber);
                            intent.putExtra("grandTotal", grandTotal);
                        }
                    });
                }
                else if (paymentType.equals(Constant.PAYMENT_CREDIT_CARD)) {
                    startActivity(WebViewActivity.class, new OnSetupIntentExtras() {
                        @Override
                        public void onSetupIntentExtras(Intent intent) {
                            intent.putExtra("title", getResources().getString(R.string.title_payment));
                            intent.putExtra("url",  String.format("%1$s?id=%2$d&grand_total=%3$d", Constant.PAYMENT_URL, orderId, (long)grandTotal));
                        }
                    });
                }
                else if (paymentType.equals(Constant.PAYMENT_CORPORATE)) {
                    Globals.showToast(getResources().getString(R.string.text_corporate_order_success), context);
                    Intent intent = new Intent(ProgressActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }else {
                    startActivity(new Intent(ProgressActivity.this,MainActivity.class));
                }


            }
        }, 500);


    }

    protected void uploadImages() {
        String serverUrlString  = Globals.getApiUrl("orders/uploadImages");

        final MultipartUploadRequest request =
                new MultipartUploadRequest(this, UUID.randomUUID().toString(), serverUrlString);

        request.addParameter("clientId", Constant.CLIENT_ID);
        request.addParameter("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
        request.addParameter("orderId", String.valueOf(orderId));

        for (int i = 0; i< carts.size(); i++) {
            Project project                     = db.getProject(carts.get(i).getProjectId());
            String paramNameString              = "order_product_images[]";
            if (project.getReorderId() == 0) {
                if (project.getPathImageCrop() != null && !project.getPathImageCrop().isEmpty() && !project.getPathImageCrop().equals("")) {
                    request.addFileToUpload(project.getPathImageCrop(), paramNameString,
                            "memome", ContentType.APPLICATION_OCTET_STREAM);
                }

                List<ProjectDetail> projectDetails = db.getAllProjectDetailsByProjectId(project.getID());
                for (ProjectDetail pd : projectDetails) {
                    String paramNameStringDetail = "order_product_detail_images_" + i + "[]";
                    request.addFileToUpload(pd.getPathImageCrop(), paramNameStringDetail,
                            "memome", ContentType.APPLICATION_OCTET_STREAM);
                }

            }else {
                boolean empty = false;
                List<ProjectDetail> projectDetails = db.getAllProjectDetailsByProjectId(project.getID());
                for (ProjectDetail pd : projectDetails){
                    setLog("---->>>"+project.getReorderId());
                    if (pd.getPathImageCrop().equals("")){
                        empty = true;
                    }
                }
            }
            /*if (projectDetails.size()<2){
            }*/

        }

        request.setNotificationConfig(R.mipmap.ic_launch,
                getString(R.string.app_name),
                getString(R.string.uploading),
                getString(R.string.upload_success),
                getString(R.string.upload_error),
                false, true);

        request.setCustomUserAgent("UploadServiceDemo/1.0");

        // set the intent to perform when the user taps on the upload notification.
        // currently tested only with intents that launches an activity
        // if you comment this line, no action will be performed when the user taps
        // on the notification
        Intent intent = new Intent(context, ProgressActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("orderId", orderId);
        intent.putExtra("orderNumber", orderNumber);
        intent.putExtra("grandTotal", grandTotal);
        intent.putExtra("paymentType", paymentType);
        intent.putExtra("reVisit", true);
        request.setNotificationClickIntent(intent);

        // set the maximum number of automatic upload retries on error
        request.setMaxRetries(2);

        try {
            request.startUpload();
        } catch (Exception exc) {
            if("You have to add at least one file to upload".equals(exc.getLocalizedMessage())){
                uploadImagesCompleted();
            }else {
                Globals.showToast("Malformed upload request. " + exc.getLocalizedMessage(), context);
            }
        }
    }

    protected void back() {

        if (uploadImagesStatus){
            Intent exit = new Intent(ProgressActivity.this,MainActivity.class);
            exit.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(exit);
            finish();
        }else {
            Intent exit = new Intent(ProgressActivity.this,MainActivity.class);
            startActivity(exit);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        back();
    }

    protected void initTapForReload() {
        buttonTapForReload  = (Button)findViewById(R.id.buttonTapForReload);
        buttonTapForReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tapForReloadLayout.setVisibility(View.GONE);
                textViewProgress.setText("0%");
                loadData();
            }
        });

        tapForReloadLayout  = (RelativeLayout)findViewById(R.id.tapForReload);
        tapForReloadLayout.setVisibility(View.GONE);

    }

    protected void loadData() {
        if(!isLoading()) {
            showLoading();
        }

        Map<String, String> params = new HashMap<String, String>();
        params.put("clientId", Constant.CLIENT_ID);
        params.put("orderId", String.valueOf(orderId));
        request.getJSONFromUrl(Globals.getApiUrl("orders/getById"),
                params,
                getResources().getString(R.string.text_product_error),
                new RequestCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        setLog("--------- "+response.toString());
                        hideLoading();

                        try {
                            JSONObject orders    = response.getJSONObject("order");
                            JSONObject data     = orders.getJSONObject("bank_account");
                            if (data.has("bank_image")) {
                                ImageView imageView = (ImageView)findViewById(R.id.imageView);
                                request.getImageFromUrl(data.getString("bank_image"), imageView, 0);
                            }

                            if (data.has("account_name")) {
                                TextView textView   = (TextView)findViewById(R.id.textAccountName);
                                textView.setText(data.getString("account_name"));
                            }

                            if (data.has("account_number")) {
                                TextView textView   = (TextView)findViewById(R.id.textAccountNumber);
                                textView.setText(data.getString("account_number"));
                            }

                            if (!isRevisit) {
                                uploadImages();
                            }else {
                                setLog("Up load Image");
                                showLoading();

                                JSONObject order = response.getJSONObject("order");
                                final boolean imagesIsUploaded = order.has("images_is_uploaded") && order.getInt("images_is_uploaded") == 1 ? true : false;
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (!isStillUploading && !imagesIsUploaded) {
                                            isRevisit   = false;
                                            tapForReloadLayout.setVisibility(View.VISIBLE);
                                            hideLoading();
                                        } else if (!isStillUploading && imagesIsUploaded) {
                                            hideLoading();
                                            uploadImagesCompleted();
                                        }
                                    }
                                }, 2000);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(VolleyError error) {
                        tapForReloadLayout.setVisibility(View.VISIBLE);
                        hideLoading();
                    }
                });
    }

}
