package com.triodigitalagency.memome.transaction;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.android.volley.VolleyError;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VoucherActivity extends BaseActivity {
    private final String TAG = VoucherActivity.class.getSimpleName();
    //<editor-fold desc="Variable">
    private EditText editTextVoucherCode;
    private Bundle extras;
    //</editor-fold>

    //<editor-fold desc="Override">
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voucher);
        setLog("activity_voucher");

        firstInit();
        setupActionBar(getResources().getString(R.string.title_voucher));

        extras              = getIntent().getExtras();
        editTextVoucherCode = (EditText)findViewById(R.id.editTextVoucherCode);
        if (extras != null && !extras.getString("voucherCode").equals("")) {
            editTextVoucherCode.setText(extras.getString("voucherCode"));
        }

        findViewById(R.id.buttonSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //Handle the back button
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            return true;
        }
        else {
            return super.onKeyDown(keyCode, event);
        }
    }
    //</editor-fold>

    //<editor-fold desc="Procedure & Function">
    protected void submit() {
        if (!isLoading()) {
            showLoading();
        }

        Map<String, String> params = new HashMap<String, String>();
        params.put("clientId", Constant.CLIENT_ID);
        params.put("voucherCode", editTextVoucherCode.getText().toString());
        params.put("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
        request.getJSONFromUrl(Globals.getApiUrl("vouchers/search"), params, getResources().getString(R.string.text_voucher_error), new RequestCallback() {
            @Override
            public void onResponse(JSONObject response) {
                hideLoading();
                try {
                    JSONObject voucher = response.getJSONObject("voucher");

                    Intent data = new Intent();
                    data.putExtra("voucherId", voucher.getLong("id"));
                    data.putExtra("voucherCode", editTextVoucherCode.getText().toString());
                    data.putExtra("voucherTotal", voucher.getDouble("price"));
                    data.putExtra("voucherPercent", voucher.has("percent") ? voucher.getDouble("percent") : 0.0);
                    setResult(RESULT_OK, data);
                    finish();

                    hideLoading();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(VolleyError error) {
                hideLoading();
            }
        });
    }
    //</editor-fold>
}
