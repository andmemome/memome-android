package com.triodigitalagency.memome.transaction;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.adapter.CartArrayAdapter;
import com.triodigitalagency.memome.database.Cart;
import com.triodigitalagency.memome.database.DatabaseHandler;
import com.triodigitalagency.memome.database.Project;
import com.triodigitalagency.memome.database.ProjectDetail;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.utils.NumberTextWatcher;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReviewActivity extends BaseActivity {
    private final String TAG = ReviewActivity.class.getSimpleName();
    //<editor-fold desc="Variable">
    private RelativeLayout tapForReloadLayout;
    private Button buttonTapForReload, buttonSubmit;
    private List<Cart> carts;
    private int totalItem;
    private double totalPrice, voucherTotal, voucherPercent, shippingTotal = 0, grandTotal = 0, minTransaction = 10000;
    private DatabaseHandler db;
    private ArrayList<JSONObject> dataSource;
    private ListView listView;
    private CartArrayAdapter cartArrayAdapter;
    private Bundle extras;
    private String firstName, lastName, address, provinceName, cityName, postalCode, phone, email;
    private long voucherId, provinceId, cityId;
    private String voucherCode, paymentType, paymentName, shippingType;
    private RelativeLayout listHeaderView, listFooterView;
    private List<Double> listShippingCosts;
    private RadioGroup radioGroup;
    private Button next;
    private RadioButton radioButton;
    private LinearLayout linearLayoutPaymentDetail;
    //</editor-fold>

    //<editor-fold desc="Override">
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        setLog("activity_review3");

        firstInit();
        setupActionBar(getResources().getString(R.string.title_review));

        extras                          = getIntent().getExtras();
        provinceId                      = extras.getLong("provinceId");
        cityId                          = extras.getLong("cityId");
        firstName                       = extras.getString("firstName");
        lastName                        = extras.getString("lastName");
        address                         = extras.getString("address");
        provinceName                    = extras.getString("provinceName");
        cityName                        = extras.getString("cityName");
        postalCode                      = extras.getString("postalCode");
        phone                           = extras.getString("phone");
        email                           = extras.getString("email");
        voucherId                       = extras.getLong("voucherId");
        voucherCode                     = extras.getString("voucherCode");
        voucherTotal                    = extras.getDouble("voucherTotal");
        voucherPercent                  = extras.getDouble("voucherPercent");
        paymentType                     = extras.getString("paymentType");
        paymentName                     = extras.getString("paymentName");

        db                              = new DatabaseHandler(this);
        cartArrayAdapter                = new CartArrayAdapter(context);
        LayoutInflater inflater         = activity.getLayoutInflater();
        next                            = (Button)findViewById(R.id.buttonId);
        listHeaderView                  = (RelativeLayout)inflater.inflate(R.layout.layout_review_header, null);
        listFooterView                  = (RelativeLayout)inflater.inflate(R.layout.layout_review_footer, null);
        listView                        = (ListView) findViewById(R.id.listView);
        listView.addHeaderView(listHeaderView);
//        listView.addFooterView(listFooterView);
        listView.setAdapter(cartArrayAdapter);

        listShippingCosts               = new ArrayList<Double>();
        radioGroup                      = (RadioGroup)listHeaderView.findViewById(R.id.radioGroupShipping);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                int checkedRadioButtonId    = radioGroup.getCheckedRadioButtonId();
//                radioButton = (RadioButton)findViewById(checkedRadioButtonId);
                shippingTotal               = listShippingCosts.get(checkedRadioButtonId).doubleValue();
                setLog("shipping Total ="+shippingTotal);
                drawTotal();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result",shippingTotal);
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
            }
        });

        buttonSubmit                    = (Button)listFooterView.findViewById(R.id.buttonSubmit);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isGood  = true;

                if (isGood && grandTotal < minTransaction && !checkIsJustCorporate()) {
                    isGood                      = false;
                    EditText tempEditText       = new EditText(context);
                    tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
                    tempEditText.setText(String.valueOf((int)minTransaction));
                    Globals.showAlert(getResources().getString(R.string.text_checkout_error), String.format(getResources().getString(R.string.text_min_transaction), tempEditText.getText().toString()), activity);
                }

                if (isGood && radioGroup.getCheckedRadioButtonId() == -1) {
                    isGood                      = false;
                    Globals.showAlert(getResources().getString(R.string.text_checkout_error), getResources().getString(R.string.text_shipping_method_error), activity);
                }

                if (isGood) {
                    showLoading();
                    new AlertDialog.Builder(context)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle(R.string.text_checkout_title)
                            .setMessage(R.string.text_confirmation_message)
                            .setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    submit();
                                }
                            })
                            .setNegativeButton(R.string.button_no, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    hideLoading();
                                }
                            })
                            .show();
                }
            }
        });

        TextView textViewPayment        = (TextView)listHeaderView.findViewById(R.id.textViewPayment);
        textViewPayment.setText(paymentName);

        TextView textViewUserAddress1   = (TextView)listHeaderView.findViewById(R.id.textViewUserAddress1);
        TextView textViewUserAddress2   = (TextView)listHeaderView.findViewById(R.id.textViewUserAddress2);
        textViewUserAddress1.setText(firstName + " " + lastName);
        textViewUserAddress2.setText(address + "\n" + cityName + ", " + provinceName + " - " + postalCode + "\n" + phone + " / " + email);

        loadData();
        initTapForReload();

        linearLayoutPaymentDetail       = (LinearLayout)findViewById(R.id.linearLayoutPaymentDetail);
        linearLayoutPaymentDetail.setVisibility(checkIsJustCorporate() ? View.GONE : View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //</editor-fold>

    //<editor-fold desc="Procedure & Function">
    protected boolean checkIsJustCorporate() {
        boolean flag = true;

        for(Cart cart : carts) {
            Project project = db.getProject(cart.getProjectId());
            if (project.getCorporateVoucherId() <= 0 || (project.getCorporateVoucherId() > 0 && cart.getQty() > project.getCorporateVoucherQty())) {
                flag = false;
            }
        }

        return flag;
    }

    protected void initTapForReload() {
        buttonTapForReload  = (Button)findViewById(R.id.buttonTapForReload);
        buttonTapForReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tapForReloadLayout.setVisibility(View.GONE);
                loadData();
            }
        });

        tapForReloadLayout  = (RelativeLayout)findViewById(R.id.tapForReload);
        tapForReloadLayout.setVisibility(View.GONE);
    }

    protected void loadData() {
        carts               = db.getAllCarts();
        totalItem           = 0;
        totalPrice          = 0;
        String productIds   = "";

        for (int i = 0; i < carts.size(); i++) {
            totalItem       += carts.get(i).getQty();
            Project project = db.getProject(carts.get(i).getProjectId());
            productIds      += project.getProductId() + (i < carts.size() - 1 ? "," : "");
        }

        if (dataSource == null) {
            dataSource      = new ArrayList<JSONObject>();
        }
        else {
            dataSource.clear();
        }

        if (!productIds.equals("")) {
            loadData(productIds);
        }
        else {
            /*cartArrayAdapter.setValues(dataSource);
            cartArrayAdapter.notifyDataSetChanged();*/

            drawTotal();
        }
    }

    protected void loadData(final String productIds) {
        if(!isLoading()){
            showLoading();
            Map<String, String> params = new HashMap<String, String>();
            params.put("clientId", Constant.CLIENT_ID);
            params.put("productIds", productIds);
            params.put("cityId", String.valueOf(cityId));

            setLog("clientId ="+ Constant.CLIENT_ID);
            setLog("productIds ="+ productIds);
            setLog("cityId ="+ String.valueOf(cityId));

            request.getJSONFromUrl(Globals.getApiUrl("orders/review"),
                    params,
                    getResources().getString(R.string.text_product_error),
                    new RequestCallback() {
                        @Override
                        public void onResponse(JSONObject response) {
                            hideLoading();

                            try {
                                JSONArray products = response.getJSONArray("products");
                                setLog("<=="+products);
                                for (int i = 0; i < carts.size(); i++) {
                                    for (int j = 0; j < products.length(); j++) {
                                        JSONObject product = products.getJSONObject(j);
                                        Project project = db.getProject(carts.get(i).getProjectId());

                                        if (project.getProductId() == product.getInt("id")) {
                                            product.put("project_id", project.getID());
                                            product.put("path_image_crop", project.getPathImageCrop());
                                            product.put("path_image_original", project.getPathImageOriginal());
                                            product.put("cover_title", project.getTitle());
                                            product.put("cover_subtitle", project.getSubtitle());
                                            product.put("cart_qty", carts.get(i).getQty());

                                            List<ProjectDetail> details = db.getAllProjectDetailsByProjectId(project.getID());
                                            product.put("total_detail", details.size());

                                            product.put("corporate_voucher_id", project.getCorporateVoucherId());
                                            product.put("corporate_voucher_qty", project.getCorporateVoucherQty());

                                            if (product.has("cover_images") && !product.isNull("cover_images")) {
                                                product.put("images", product.getJSONObject("cover_images"));
                                            }

                                            dataSource.add(new JSONObject(product.toString()));
                                            totalPrice += carts.get(i).getQty() * (product.getLong("special_price") > 0 ? product.getLong("special_price") : product.getLong("price"));
                                            totalPrice  = totalPrice - (project.getCorporateVoucherQty() > 0 ? project.getCorporateVoucherQty() : 0) * (product.getLong("special_price") > 0 ? product.getLong("special_price") : product.getLong("price"));
                                            break;
                                        }
                                    }
                                }
                                voucherTotal = voucherTotal > 0 ? voucherTotal : (totalPrice * voucherPercent / 100);

                                cartArrayAdapter.setShowButton(false);
                                /*cartArrayAdapter.setValues(dataSource);
                                cartArrayAdapter.notifyDataSetChanged();*/
                                setLog(" <=="+dataSource.toArray().length);

                                int ctr                 = 0;
                                if (!checkIsJustCorporate()) {
                                    JSONArray shippingCosts = response.getJSONArray("shippingCosts");
                                    setLog("<--- "+shippingCosts);
                                    JSONObject courier      = shippingCosts.getJSONObject(0);
                                    shippingType            = courier.getString("code");

                                    for (int i = 0; i < courier.getJSONArray("costs").length(); i++) {
                                        JSONObject service  = courier.getJSONArray("costs").getJSONObject(i);
                                        Double shippingCost = service.getJSONArray("cost").getJSONObject(0).getDouble("value");

                                        if(totalPrice + shippingCost.doubleValue() - voucherTotal < minTransaction){
                                            shippingCost = minTransaction - (totalPrice - voucherTotal);
                                        }
                                        listShippingCosts.add(ctr, shippingCost);

                                        EditText tempEditText   = new EditText(context);
                                        tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
                                        tempEditText.setText(String.valueOf((int) shippingCost.doubleValue()));

                                        RadioButton radioButton = new RadioButton(activity);
                                        radioButton.setText(service.getString("service") + " (" + service.getString("description") + ") - " + "IDR " + tempEditText.getText().toString());
                                        radioButton.setId(ctr);
                                        if (ctr == 0) {
                                            radioButton.setChecked(true);
                                            shippingTotal = shippingCost.doubleValue();
                                        }
                                        radioGroup.addView(radioButton);

                                        ctr++;
                                    }
                                }
                                else {
                                    shippingType            = Constant.SHIPPING_FREE;
                                    listShippingCosts.add(ctr, Double.valueOf(0));
                                    RadioButton radioButton = new RadioButton(activity);
                                    radioButton.setText("Free");
                                    radioButton.setId(ctr);
                                    radioButton.setChecked(true);
                                    shippingTotal           = 0;
                                    radioGroup.addView(radioButton);
                                }

                                drawTotal();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(VolleyError error) {
                            tapForReloadLayout.setVisibility(View.VISIBLE);
                            hideLoading();
                        }
                    });
        }
    }

    protected void drawTotal() {
        TextView textTotalOfItem    = (TextView)listFooterView.findViewById(R.id.textTotalOfItem);
        textTotalOfItem.setText(String.format(getResources().getString(R.string.text_cart_total_of_item), String.valueOf(totalItem)));

        TextView textTotalPrice     = (TextView)listFooterView.findViewById(R.id.textTotalPrice);
        EditText tempEditText       = new EditText(context);
        tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
        tempEditText.setText(String.valueOf((int)totalPrice));
        textTotalPrice.setText(String.format(getResources().getString(R.string.text_total_amount), tempEditText.getText().toString()));

        if (!voucherCode.equals("")) {
            listFooterView.findViewById(R.id.linearLayoutVoucher).setVisibility(View.VISIBLE);
            TextView textVoucherTotal  = (TextView)listFooterView.findViewById(R.id.textVoucherTotal);
            if(voucherTotal > 0.0) {
                tempEditText.setText(String.valueOf((int) voucherTotal));
                textVoucherTotal.setText("- " + String.format(getResources().getString(R.string.text_total_amount), tempEditText.getText().toString()));
                if(voucherPercent > 0) {
                    tempEditText.setText(String.valueOf((int) voucherPercent));
                    textVoucherTotal.setText(textVoucherTotal.getText().toString() + " (" + tempEditText.getText().toString() + "%)");
                }
            }else{
                tempEditText.setText(String.valueOf((int) voucherPercent));
                textVoucherTotal.setText("- " + tempEditText.getText().toString() + "%");
            }
        }

        TextView textShippingTotal  = (TextView)listFooterView.findViewById(R.id.textShippingTotal);
        tempEditText.setText(String.valueOf((int)shippingTotal));
        textShippingTotal.setText(String.format(getResources().getString(R.string.text_total_amount), tempEditText.getText().toString()));

        TextView textGrandTotal     = (TextView)listFooterView.findViewById(R.id.textGrandTotal);
//        grandTotal                  = totalPrice + shippingTotal - voucherTotal;
        voucherTotal                    = voucherTotal > 0 ? voucherTotal : (totalPrice * voucherPercent / 100);
        grandTotal                  = totalPrice + shippingTotal;

        grandTotal                  = grandTotal - voucherTotal;
        if (grandTotal < 0) {
            grandTotal  = 0;
        }
        tempEditText.setText(String.valueOf((int) grandTotal));
        textGrandTotal.setText(String.format(getResources().getString(R.string.text_total_amount), tempEditText.getText().toString()));

        TextView textError      = (TextView)listFooterView.findViewById(R.id.textViewError);
        if (grandTotal < minTransaction && !checkIsJustCorporate()) {
            tempEditText.setText(String.valueOf((int)minTransaction));
            textError.setText(String.format(getResources().getString(R.string.text_min_transaction), tempEditText.getText().toString()));
            textError.setVisibility(View.VISIBLE);
        }
        else {
            textError.setVisibility(View.GONE);
        }
    }

    protected void submit() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("clientId", Constant.CLIENT_ID);
        params.put("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
        params.put("provinceId", String.valueOf(provinceId));
        params.put("cityId", String.valueOf(cityId));
        params.put("firstName", firstName);
        params.put("lastName", lastName);
        params.put("address", address);
        params.put("province", provinceName);
        params.put("city", cityName);
        params.put("postalCode", postalCode);
        params.put("phone", phone);
        params.put("email", email);
        params.put("paymentType", paymentType);
        params.put("shippingPrice", String.valueOf(shippingTotal));
        params.put("shippingType", shippingType);
        params.put("voucherCode", voucherCode);
        params.put("voucherId", String.valueOf(voucherId));
        params.put("voucherTotal", String.valueOf(voucherTotal));

        JSONArray orderProduct          = new JSONArray();
        JSONArray orderProductDetail    = new JSONArray();

        try {
            for (int i = 0; i < carts.size(); i++) {
                Project project = db.getProject(carts.get(i).getProjectId());
                List<ProjectDetail> projectDetail = db.getAllProjectDetailsByProjectId(project.getID());

                JSONObject tempOrderProduct = new JSONObject();
                tempOrderProduct.put("project_id", project.getID());
                tempOrderProduct.put("product_id", project.getProductId());
                tempOrderProduct.put("title", project.getTitle());
                tempOrderProduct.put("subtitle", project.getSubtitle());
                tempOrderProduct.put("qty", carts.get(i).getQty());
                tempOrderProduct.put("corporate_voucher_id", project.getCorporateVoucherId());
                tempOrderProduct.put("corporate_voucher_code", project.getCorporateVoucherCode());

                for (int j = 0; j < projectDetail.size(); j++) {
                    JSONObject tempOrderProductDetail = new JSONObject();
                    tempOrderProductDetail.put("project_id", project.getID());
                    tempOrderProductDetail.put("project_detail_id", projectDetail.get(j).getID());
                    tempOrderProductDetail.put("product_detail_id", projectDetail.get(j).getProductDetailId());
                    orderProductDetail.put(new JSONObject(tempOrderProductDetail.toString()));
                }

                orderProduct.put(new JSONObject(tempOrderProduct.toString()));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        params.put("orderProduct", orderProduct.toString());
        params.put("orderProductDetail", orderProductDetail.toString());

        request.getJSONFromUrl(
                Globals.getApiUrl("orders/add"),
                params,
                getResources().getString(R.string.text_checkout_error),
                new RequestCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        long id = 0;
                        String number = "";
                        try {
                            JSONObject order    = response.getJSONObject("order");
                            id                  = order.getLong("id");
                            number         = order.getString("number");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (id == 0) {
                            //error
                            Globals.showAlert(
                                    getResources().getString(R.string.text_checkout_error),
                                    getResources().getString(R.string.text_checkout_error_unknown),
                                    activity
                            );
                            hideLoading();
                        } else {
                            for (Cart cart : carts) {
                                Project project = db.getProject(cart.getProjectId());
                                project.setCorporateVoucherId(-1);
                                project.setCorporateVoucherCode("");
                                project.setCorporateVoucherName("");
                                project.setCorporateVoucherQty(-1);
                                db.updateProject(project);
                            }

                            final long orderId    = id;
                            final String orderNumber = number;
                            startActivity(ProgressActivity.class, new OnSetupIntentExtras() {
                                @Override
                                public void onSetupIntentExtras(Intent intent) {
                                    intent.putExtra("orderId", orderId);
                                    intent.putExtra("orderNumber", orderNumber);
                                    intent.putExtra("grandTotal", grandTotal);
                                    intent.putExtra("paymentType", paymentType);
                                }
                            });
                        }

                    }

                    @Override
                    public void onError(VolleyError error) {
                        hideLoading();
                    }
                }
        );
    }
    //</editor-fold>
}
