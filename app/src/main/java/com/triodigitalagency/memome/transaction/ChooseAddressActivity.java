package com.triodigitalagency.memome.transaction;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.adapter.RegionArrayAdapter;
import com.triodigitalagency.memome.adapter.UserAddressArrayAdapter;
import com.triodigitalagency.memome.user.AddressActivity;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChooseAddressActivity extends BaseActivity {

    //<editor-fold desc="Variable">
    protected UserAddressArrayAdapter arrayAdapter;
    LayoutInflater inflater;
    protected ListView listView;
    protected ArrayList<JSONObject> dataSource;
    protected Bundle extras;
    protected RelativeLayout tapForReloadLayout;

    protected Button buttonTapForReload;
    private long selectedId = -1;
    @BindView(R.id.back)TextView back;

    @OnClick(R.id.btnAddAddress)void buttonAddAddressListner(){
        Intent intent = new Intent(this,AddressActivity.class);
        intent.putExtra("isCreate",true);
        intent.putExtra("userAddressId",Constant.CLIENT_ID);
        startActivityForResult(intent,99);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_address);
        setLog("activity_choose_address");
        ButterKnife.bind(this);

        firstInit();

        extras = getIntent().getExtras();
        if (extras != null){
            if (extras.get("selectedId") != null && extras.getLong("selectedId") > 0) {
                selectedId = extras.getLong("selectedId");
            }
        }

        dataSource       = new ArrayList<JSONObject>();
        listView         = (ListView)findViewById(R.id.listView);
        inflater         = activity.getLayoutInflater();
//        footerView       = inflater.inflate(R.layout.space, null);
        arrayAdapter     = new UserAddressArrayAdapter(context);
        listView.setAdapter(arrayAdapter);
//        listView.addFooterView(footerView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                selectedId = arrayAdapter.getItemId(position);
                arrayAdapter.setSelectedId(selectedId);
                arrayAdapter.notifyDataSetChanged();

                if(view.getId()==R.id.textViewEdit){
                    getEditAdrress(position);
                }else {

                    Intent data = new Intent();
                    data.putExtra("selectedId", selectedId);
                    data.putExtra("provinceId", arrayAdapter.getItemProvinceId(position));
                    data.putExtra("cityId", arrayAdapter.getItemCityId(position));
                    data.putExtra("firstName", arrayAdapter.getItemFirstName(position));
                    data.putExtra("lastName", arrayAdapter.getItemLastName(position));
                    data.putExtra("address", arrayAdapter.getItemAddress(position));
                    data.putExtra("provinceName", arrayAdapter.getItemProvinceName(position));
                    data.putExtra("cityName", arrayAdapter.getItemCityName(position));
                    data.putExtra("postalCode", arrayAdapter.getItemPostalCode(position));
                    data.putExtra("phone", arrayAdapter.getItemPhone(position));
                    data.putExtra("email", arrayAdapter.getItemEmail(position));
                    data.putExtra("posisi",position);

                    setResult(RESULT_OK, data);
                    finish();
                }

            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        loadData();
        initTapForReload();
    }

    private void getEditAdrress(int position) {
        Intent intent = new Intent(this,AddressActivity.class);
        intent.putExtra("isCreate",false);
        intent.putExtra("userAddressId",Constant.CLIENT_ID);
        intent.putExtra("userAddressId",arrayAdapter.getItemId(position));
        setLog(""+arrayAdapter.getItemId(position));
        /*
        intent.putExtra("edit","oke");
        intent.putExtra("selectedId", selectedId);
        intent.putExtra("provinceId", arrayAdapter.getItemProvinceId(position));
        intent.putExtra("cityId", arrayAdapter.getItemCityId(position));
        intent.putExtra("firstName", arrayAdapter.getItemFirstName(position));
        intent.putExtra("lastName", arrayAdapter.getItemLastName(position));
        intent.putExtra("address", arrayAdapter.getItemAddress(position));
        intent.putExtra("provinceName", arrayAdapter.getItemProvinceName(position));
        intent.putExtra("cityName", arrayAdapter.getItemCityName(position));
        intent.putExtra("postalCode", String.valueOf(arrayAdapter.getItemPostalCode(position)));
        intent.putExtra("phone", String.valueOf(arrayAdapter.getItemPhone(position)));
        intent.putExtra("email", arrayAdapter.getItemEmail(position));*/
        startActivityForResult(intent,99);


    }

    protected void loadData() {
        dataSource.clear();
        if (!isLoading()) {
            showLoading();
        }

        Map<String, String> params = new HashMap<String, String>();
        params.put("clientId", Constant.CLIENT_ID);
        params.put("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
        request.getJSONFromUrl(Globals.getApiUrl("users/view"), params, getResources().getString(R.string.text_checkout_error), new RequestCallback() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray userAddresses = response.getJSONArray("userAddresses");

                    for (int i = 0; i < userAddresses.length(); i++) {
                        dataSource.add(userAddresses.getJSONObject(i));
                    }

                    if (selectedId > 0) {
                        arrayAdapter.setSelectedId(selectedId);
                    }

                    findViewById(R.id.textViewDataNotFound).setVisibility(userAddresses.length() <= 0 ? View.VISIBLE : View.GONE);

                    arrayAdapter.setCanEdit(true);
                    arrayAdapter.setValues(dataSource);
                    arrayAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                hideLoading();
            }

            @Override
            public void onError(VolleyError error) {
                tapForReloadLayout.setVisibility(View.VISIBLE);
                hideLoading();
            }
        });
    }

    protected void initTapForReload() {
        buttonTapForReload  = (Button)findViewById(R.id.buttonTapForReload);
        buttonTapForReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tapForReloadLayout.setVisibility(View.GONE);
                loadData();
            }
        });

        tapForReloadLayout  = (RelativeLayout)findViewById(R.id.tapForReload);
        tapForReloadLayout.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 99) {
            if(resultCode == Activity.RESULT_OK){
                if (data != null){
                    loadData(data);
                }else {
                    loadData();
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
    }

    protected void loadData(final Intent dataa) {

        dataSource.clear();
        if (!isLoading()) {
            showLoading();
        }

        Map<String, String> params = new HashMap<>();
        params.put("clientId", Constant.CLIENT_ID);
        params.put("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
        request.getJSONFromUrl(Globals.getApiUrl("users/view"), params, getResources().getString(R.string.text_checkout_error), new RequestCallback() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray userAddresses = response.getJSONArray("userAddresses");

                    for (int i = 0; i < userAddresses.length(); i++) {
                        dataSource.add(userAddresses.getJSONObject(i));
                    }

                    if (selectedId > 0) {
                        arrayAdapter.setSelectedId(selectedId);
                    }

                    findViewById(R.id.textViewDataNotFound).setVisibility(userAddresses.length() <= 0 ? View.VISIBLE : View.GONE);

                    arrayAdapter.setCanEdit(true);
                    arrayAdapter.setValues(dataSource);
                    arrayAdapter.notifyDataSetChanged();

                    Intent data = new Intent();
                    data.putExtra("selectedId", selectedId);
                    data.putExtra("provinceId", dataa.getStringExtra("provinceId"));
                    data.putExtra("cityId", arrayAdapter.getItemCityId(dataSource.size()-1));
                    data.putExtra("firstName", dataa.getStringExtra("firstName"));
                    data.putExtra("lastName", dataa.getStringExtra("lastName"));
                    data.putExtra("address", dataa.getStringExtra("address"));
                    data.putExtra("provinceName", dataa.getStringExtra("provinceName"));
                    data.putExtra("cityName", dataa.getStringExtra("cityName"));
                    data.putExtra("postalCode", dataa.getStringExtra("postalCode"));
                    data.putExtra("phone", dataa.getStringExtra("phone"));
                    data.putExtra("email", dataa.getStringExtra("email"));
                    data.putExtra("posisi", 0);
                    setResult(RESULT_OK, data);
                    finish();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                hideLoading();
            }

            @Override
            public void onError(VolleyError error) {
                tapForReloadLayout.setVisibility(View.VISIBLE);
                hideLoading();
            }
        });
    }

}
