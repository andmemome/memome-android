package com.triodigitalagency.memome.region;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.android.volley.VolleyError;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.adapter.RegionArrayAdapter;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CityActivity extends BaseActivity {

    //<editor-fold desc="Variable">
    protected RegionArrayAdapter arrayAdapter;
    protected ListView listView;
    protected ArrayList<JSONObject> dataSource;
    protected Bundle extras;

    protected RelativeLayout tapForReloadLayout;
    protected Button buttonTapForReload;

    private long provinceId = -1, selectedId = -1;
    //</editor-fold>

    //<editor-fold desc="Override">
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);
        setLog("activity_city");

        firstInit();

        extras = getIntent().getExtras();
        if (extras != null){
            provinceId  = extras.getLong("provinceId");
            if (extras.get("selectedId") != null && extras.getLong("selectedId") > 0) {
                selectedId  = extras.getLong("selectedId");
            }
        }

        dataSource       = new ArrayList<JSONObject>();
        listView         = (ListView) findViewById(R.id.listView);
        arrayAdapter     = new RegionArrayAdapter(context);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String selectedName = arrayAdapter.getItemName(position);
                selectedId = arrayAdapter.getItemId(position);
                arrayAdapter.setSelectedId(selectedId);
                arrayAdapter.notifyDataSetChanged();

                Intent data = new Intent();
                data.putExtra("selectedId", selectedId);
                data.putExtra("selectedName", selectedName);
                setResult(RESULT_OK, data);
                finish();
            }
        });

        loadData();
        initTapForReload();
    }
    //</editor-fold>

    //<editor-fold desc="Procedure & Function">
    protected void loadData() {
        if (!isLoading()) {
            showLoading();

            Map<String, String> params = new HashMap<String, String>();
            params.put("clientId", Constant.CLIENT_ID);
            params.put("provinceId", String.valueOf(provinceId));

            request.getJSONFromUrl(Globals.getApiUrl("regions/getCities"), params, getResources().getString(R.string.text_city_error), new RequestCallback() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONArray cities = response.getJSONArray("cities");

                        for (int i = 0; i < cities.length(); i++) {
                            dataSource.add(cities.getJSONObject(i));
                        }

                        if (selectedId > 0) {
                            arrayAdapter.setSelectedId(selectedId);
                        }

                        arrayAdapter.setValues(dataSource);
                        arrayAdapter.notifyDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    hideLoading();
                }

                @Override
                public void onError(VolleyError error) {
                    tapForReloadLayout.setVisibility(View.VISIBLE);
                    hideLoading();
                }
            });
        }
    }

    protected void initTapForReload() {
        buttonTapForReload  = (Button)findViewById(R.id.buttonTapForReload);
        buttonTapForReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tapForReloadLayout.setVisibility(View.GONE);
                loadData();
            }
        });

        tapForReloadLayout  = (RelativeLayout)findViewById(R.id.tapForReload);
        tapForReloadLayout.setVisibility(View.GONE);
    }
    //</editor-fold>
}
