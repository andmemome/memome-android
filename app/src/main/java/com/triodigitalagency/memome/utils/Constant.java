package com.triodigitalagency.memome.utils;

public class Constant {
//    public static final String WEB_URL                  = "http://192.168.1.26/memome/";
//    public static final String API_URL                  = "http://192.168.1.26/memome/api/";

    /*  voucers/search
    post : clientId,token,voucercode
*/

    public static final String WEB_URL                  = "http://memome.co.id/";
//  public static final String WEB_URL                  = "http://10.0.2.2/memome/";
    public static final String API_URL                  = WEB_URL + "api/";
    public static final String PAYMENT_URL              = WEB_URL + "veritrans/index";

    public static final String CLIENT_ID                = "797c5c13f30bdf906683925ee85c9736";
    public static final int SEARCH_LIMIT                = 10;
    public static final String P_USER_ID                = "userId";
    public static final String P_ACCESS_TOKEN           = "accessToken";
    public static final String P_USER_NAME              = "userName";
    public static final String P_USER_IMAGE_PATH        = "userImagePath";

    public static final String DATABASE_NAME            = "memome_db";
    public static final int DATABASE_VERSION            = 2;

    public static final String SHIPPING_FREE            = "free";
    public static final long PAYMENT_TRANSFER_ID        = 1;
    public static final String PAYMENT_TRANSFER         = "bank transfer";
    public static final String PAYMENT_TRANSFER_NAME    = "Bank Transfer";
    public static final String PAYMENT_CORPORATE        = "corporate";
    public static final String PAYMENT_CORPORATE_NAME   = "Corporate";
    public static final long PAYMENT_CREDIT_CARD_ID     = 2;
    public static final String PAYMENT_CREDIT_CARD      = "credit card";
    public static final String PAYMENT_CREDIT_CARD_NAME = "Credit Card";

    // push notification
    public static final String KEY = "key";
    public static final String KEY_NOTIFICATIONS = "notifications";
    public static final String KEY_REGISTER_GCM = "register";

    // flag to identify whether to show single line
    // or multi line test push notification tray
    public static boolean appendNotificationMessages = true;

    // id to handle the notification in the notification try
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    // broadcast receiver intent filters
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    public static final String TOPIC = "topic";
    public static final String TOPIC_GLOBAL = "memomeGlobal";
    public static final String SUBSCRIBE = "subscribe";
    public static final String UNSUBSCRIBE = "unsubscribe";

    // type of push messages
    public static final int PUSH_TYPE_CHATROOM = 1;
    public static final int PUSH_TYPE_USER = 2;


    public static final String CONTACT_US_PHONE = "031 545 1169";
    public static final String CONTACT_US_EMAIL = "info@memome.co.id";

    public static final int MIN_PIXEL = 1000;
}
