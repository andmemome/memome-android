package com.triodigitalagency.memome.utils;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.adapter.MyPurchasesArrayAdapter;
import com.triodigitalagency.memome.user.PurchaseDetailActivity;
import com.triodigitalagency.memome.volley.utils.VolleyRequest;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by khoiron on 07/03/18.
 */

abstract public class BaseFragment extends Fragment{


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View layout = inflater.inflate(getLayout(), container, false);
        return layout;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        onMain(view,savedInstanceState);
    }

    protected abstract void onMain(View view, Bundle savedInstanceState);
    protected abstract int getLayout();
    
    protected void setLog(String message){
        Log.e("Test Aplikasi ==> ",message);
    }
    
    protected void setToast(String message){
        Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }
}
