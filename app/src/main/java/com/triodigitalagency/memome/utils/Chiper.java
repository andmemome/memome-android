package com.triodigitalagency.memome.utils;

/**
 * Created by staff on 11/27/15.
 */
public class Chiper {
    static String key = "abcdefgh";
    static String allowedChars = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"; //random order
    static String keyChars = "0123456789abcdefghijklmnopqrstuvwxyz"; // a-z 0-9 random order

    private static String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    public static String encrypt(String source){
        return Chiper.encrypt(source, "");
    }

    public static String encrypt(String source, String key){
        key = Chiper.MD5( key + Chiper.key );

        String result = "";
        int mod = Chiper.allowedChars.length();

        for(int i=0; i<source.length(); i++) {
            int shift = Chiper.keyChars.indexOf(key.charAt(i));
            int index = Chiper.allowedChars.indexOf(source.charAt(i));
            if(shift > -1 && index > -1) {
                result += Chiper.allowedChars.charAt((index + shift) % mod);
            }
            else {
                result += source.charAt(i);
            }

        }

        return result;
    }

    public static String decrypt(String source){
        return Chiper.decrypt(source, "");
    }

    public static String decrypt(String source, String key){
        key = Chiper.MD5(key + Chiper.key);

        String result = "";
        int mod = Chiper.allowedChars.length();

        for(int i=0; i<source.length(); i++) {
            int shift = Chiper.keyChars.indexOf(key.charAt(i));
            int index = Chiper.allowedChars.indexOf(source.charAt(i));
            if(shift > -1 && index > -1) {
                index -= shift;
                if(index < 0) {
                    index += mod;
                }
                result += Chiper.allowedChars.charAt(index);
            }
            else {
                result += source.charAt(i);
            }

        }

        return result;
    }
}
