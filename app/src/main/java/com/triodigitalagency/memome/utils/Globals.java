package com.triodigitalagency.memome.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.triodigitalagency.memome.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

public class Globals {

    private static Map<String, String> lastSearchParams;
    private static Map<String, String> lastSearchOrder;

    public static final String CLIENT_ID = "134dca16622f4d58932d038320844706";
    public static final String REDIRECT_URI = "http://memome.co.id";

    public static final int REQUEST_CODE_INSTAGRAM_PICKER = 997;

    private static String imageTempPath;
    public static String voucher = "";

    public static String getApiUrl(String url) {
        return Constant.API_URL + url;
    }

    public static String getClientId() {
        return Constant.CLIENT_ID;
    }

    public static void setLog(String message) {
        Log.e("--> path ",message);
    }

    public static void showAlert(String title, String message, Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(activity.getResources().getString(R.string.button_positive), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    public static void showToast(String message, Context context) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void setDataPreference(Context context, String key, String value) {
        SharedPreferences sharedPref    = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void setDataPreference(Context context, String key, Boolean value) {
        SharedPreferences sharedPref    = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static Boolean getDataPreference(Context context, String key) {
        SharedPreferences sharedPref    = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getBoolean(key,false);
    }


    public static void setDataPreference(Context context, String key, Integer value) {
        SharedPreferences sharedPref    = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static void setDataPreference(Context context, Long orderId,String orderNumber,Double grandTotal,String paymentType,Boolean reVisit) {
        SharedPreferences sharedPref    = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong("orderId", orderId);
        editor.putString("orderNumber",orderNumber);
        editor.putString("grandTotal",String.valueOf(grandTotal));
        editor.putString("paymentType",paymentType);
        editor.putBoolean("reVisit",reVisit);

        editor.apply();
    }

    public static Long getorderIdSession(Context context,String key){
        SharedPreferences sharedPref    = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getLong(key,0);
    }

    public static String getorderNumberSession(Context context,String key){
        SharedPreferences sharedPref    = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getString(key,"");
    }
    public static String getgrandTotalSession(Context context,String key){
        SharedPreferences sharedPref    = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getString(key,"");
    }
    public static String getpaymentTypeSession(Context context,String key){
        SharedPreferences sharedPref    = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getString(key,"");
    }
    public static Boolean getreVisitSession(Context context,String key){
        SharedPreferences sharedPref    = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getBoolean(key,false);
    }

    public static void setDataPreference(Context context, String key, Long value) {
        SharedPreferences sharedPref    = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public static void setDataPreference(Context context, String key, Float value) {
        SharedPreferences sharedPref    = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    public static String getStringDataPreference(Context context, String key) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getString(key, "");
    }

    public static Integer getIntDataPreference(Context context, String key) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getInt(key, 0);
    }

    public static Long getLongDataPreference(Context context, String key) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getLong(key, 0);
    }

    public static Float getFloatDataPreference(Context context, String key) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPref.getFloat(key, 0);
    }

    public static String currencyIDRFormat(double price) {
        DecimalFormat decimalFormat         = new DecimalFormat();
        DecimalFormatSymbols formatSymbols  = new DecimalFormatSymbols();
        formatSymbols.setDecimalSeparator(',');
        formatSymbols.setGroupingSeparator('.');
        decimalFormat.setGroupingSize(3);
        decimalFormat.setGroupingUsed(true);
        decimalFormat.setDecimalFormatSymbols(formatSymbols);
        return "Rp " + decimalFormat.format(price);
    }

    public static String currencyFormatIDR(double price) {
        DecimalFormat decimalFormat         = new DecimalFormat();
        DecimalFormatSymbols formatSymbols  = new DecimalFormatSymbols();
        formatSymbols.setDecimalSeparator(',');
        formatSymbols.setGroupingSeparator('.');
        decimalFormat.setGroupingSize(3);
        decimalFormat.setGroupingUsed(true);
        decimalFormat.setDecimalFormatSymbols(formatSymbols);
        return "IDR " + decimalFormat.format(price);
    }

    public static String doubleFormat(double d) {
        if(d == (long) d) {
            return String.format("%d", (long) d);
        }
        else {
            return String.format("%s", d);
        }
    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static Map<String, String> getLastSearchParams() {
        return lastSearchParams;
    }

    public static void setLastSearchParams(Map<String, String> lastSearchParams) {
        Globals.lastSearchParams = lastSearchParams;
    }

    public static Map<String, String> getLastSearchOrder() {
        return lastSearchOrder;
    }

    public static void setLastSearchOrder(Map<String, String> lastSearchOrder) {
        Globals.lastSearchOrder = lastSearchOrder;
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url                         = new URL(src);
            HttpURLConnection connection    = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();

            InputStream input   = connection.getInputStream();
            Bitmap myBitmap     = BitmapFactory.decodeStream(input);
            return myBitmap;
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getImageTempPath() {
        return imageTempPath;
    }

    public static void setImageTempPath(String imageTempPath) {
        Globals.imageTempPath = imageTempPath;
    }

    public static String BitmapToString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream  = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);

        byte [] b   = byteArrayOutputStream.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context){
        Resources resources     = context.getResources();
        DisplayMetrics metrics  = resources.getDisplayMetrics();
        float px                = dp * (metrics.densityDpi / 160f);
        return px;
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context){
        Resources resources     = context.getResources();
        DisplayMetrics metrics  = resources.getDisplayMetrics();
        float dp                = px / (metrics.densityDpi / 160f);
        return dp;
    }

    /**
     * This method converts device specific pixels to scaled pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent sp equivalent to px value
     */
    public static float convertPixelsToSp(float px, Context context){
        Resources resources     = context.getResources();
        DisplayMetrics metrics  = resources.getDisplayMetrics();
        float sp                = px / metrics.scaledDensity;
        return sp;
    }

    public static void drawProductLayout(
            Long productCoverWidth,
            Long productCoverHeight,
            ImageView imageView,
            ImageView imageViewContent,
            Long productCoverContentWidth,
            Long productCoverContentHeight,
            Long productCoverBorderLeft,
            Long productCoverBorderTop
    ) {
        float ratio                         = (float)productCoverWidth / (float)imageView.getLayoutParams().width;
        Log.e("Ratio - "," "+ratio);
        imageView.getLayoutParams().height  = (int)((float)productCoverHeight / ratio);

        RelativeLayout.LayoutParams layoutParams    = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.width                  = (int)((float)productCoverContentWidth / ratio);
        layoutParams.height                 = (int)((float)productCoverContentHeight / ratio);
        Log.e("width - ",productCoverContentWidth+" "+(int)((float)productCoverContentWidth / ratio));
        Log.e("height - ",productCoverContentHeight+" "+(int)((float)productCoverContentHeight / ratio));
        layoutParams.setMargins((int) ((float) productCoverBorderLeft / ratio), (int) ((float) productCoverBorderTop / ratio), 0, 0);
        Log.e("left - ",productCoverBorderLeft+" "+(int) ((float) productCoverBorderLeft / ratio));
        Log.e("top - ",productCoverBorderTop+" "+(int) ((float) productCoverBorderTop / ratio));
        imageViewContent.setLayoutParams(layoutParams);

        imageView.requestLayout();
        imageViewContent.requestLayout();
    }

    public static void drawProductLayout2(
            float lat,
            Long productCoverWidth,
            Long productCoverHeight,
            ImageView imageView,
            ImageView imageViewContent,
            Long productCoverContentWidth,
            Long productCoverContentHeight,
            Long productCoverBorderLeft,
            Long productCoverBorderTop
    ) {
        float ratio                         = (float)productCoverWidth / lat;
        Log.e("Ratio - "," "+ratio);
        imageView.getLayoutParams().height  = (int)((float)productCoverHeight / ratio);

        RelativeLayout.LayoutParams layoutParams    = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.width                  = (int)((float)productCoverContentWidth / ratio);
        layoutParams.height                 = (int)((float)productCoverContentHeight / ratio);
        Log.e("width - ",productCoverContentWidth+" "+(int)((float)productCoverContentWidth / ratio));
        Log.e("height - ",productCoverContentHeight+" "+(int)((float)productCoverContentHeight / ratio));
        layoutParams.setMargins((int) ((float) productCoverBorderLeft / ratio), (int) ((float) productCoverBorderTop / ratio), 0, 0);
        Log.e("left - ",productCoverBorderLeft+" "+(int) ((float) productCoverBorderLeft / ratio));
        Log.e("top - ",productCoverBorderTop+" "+(int) ((float) productCoverBorderTop / ratio));
        imageViewContent.setLayoutParams(layoutParams);

        imageView.requestLayout();
        imageViewContent.requestLayout();
    }

    public static void drawProductWithTitleLayout(
            Context context,
            Long productCoverWidth,
            Long productCoverHeight,
            ImageView imageView,
            ImageView imageViewContent,
            Long productCoverContentWidth,
            Long productCoverContentHeight,
            Long productCoverBorderLeft,
            Long productCoverBorderTop,
            RelativeLayout relativeLayoutTitleArea,
            Long productCoverTitleAreaWidth,
            Long productCoverTitleAreaHeight,
            Long productCoverTitleAreaTop,
            Long productCoverTitleAreaLeft,
            Long productCoverTitleFontSize,
            Long productCoverSubtitleFontSize,
            Long productCoverTitleTop,
            Long productCoverSubtitleTop,
            EditText title,
            EditText subtitle
    ) {
        Globals.drawProductLayout(productCoverWidth, productCoverHeight, imageView, imageViewContent, productCoverContentWidth, productCoverContentHeight, productCoverBorderLeft, productCoverBorderTop);

        float ratio                                 = (float)productCoverWidth / (float)imageView.getLayoutParams().width;
        RelativeLayout.LayoutParams layoutParams    = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.width                          = (int)((float)productCoverTitleAreaWidth / ratio);
        layoutParams.height                         = (int)((float)productCoverTitleAreaHeight / ratio);
        layoutParams.setMargins((int) ((float) productCoverTitleAreaLeft / ratio), (int) ((float) productCoverTitleAreaTop / ratio), 0, 0);
        relativeLayoutTitleArea.setLayoutParams(layoutParams);

        title.setTextSize(TypedValue.COMPLEX_UNIT_SP, Globals.convertPixelsToSp((float) productCoverTitleFontSize / ratio, context));
        layoutParams                        = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.width = (int) ((float) productCoverTitleAreaWidth / ratio);
        layoutParams.setMargins(0, (int) ((float) productCoverTitleTop / ratio), 0, 0);
        title.setLayoutParams(layoutParams);

        subtitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, Globals.convertPixelsToSp((float) productCoverSubtitleFontSize / ratio, context));
        layoutParams                        = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.width                  = (int)((float)productCoverTitleAreaWidth / ratio);
        layoutParams.setMargins(0, (int) ((float) productCoverSubtitleTop / ratio), 0, 0);
        subtitle.setLayoutParams(layoutParams);

        imageView.requestLayout();
        imageViewContent.requestLayout();
    }

    public static Bitmap decodeScaledBitmapFromFilePath(String filePath, int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds          = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize
        options.inSampleSize                = Globals.calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filePath, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height    = options.outHeight;
        final int width     = options.outWidth;
        int inSampleSize    = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio   = Math.round((float) height / (float) reqHeight);
            final int widthRatio    = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize            = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

    public static void addNotification(Context context, String notification) {

        // get old notifications
        String oldNotifications = getNotifications(context);

        if (oldNotifications != null) {
            oldNotifications += "|" + notification;
        } else {
            oldNotifications = notification;
        }

        setDataPreference(context, Constant.KEY_NOTIFICATIONS, oldNotifications);
    }

    public static String getNotifications(Context context) {
        return getStringDataPreference(context, Constant.KEY_NOTIFICATIONS);
    }

    public static long getFolderOrFileSize(File f) {
        long size = 0;
        if (f.isDirectory()) {
            for (File file : f.listFiles()) {
                size += getFolderOrFileSize(file);
            }
        } else {
            size=f.length();
        }
        return size;
    }

    public static Bitmap codec(Bitmap src, Bitmap.CompressFormat format,
                                int quality) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        src.compress(format, quality, os);

        byte[] array = os.toByteArray();
        return BitmapFactory.decodeByteArray(array, 0, array.length);
    }

    public static String calculateShipping(Date date, Integer from, Integer to) {
        Date today = date;

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.add(Calendar.DAY_OF_YEAR, from);
        Date minDate = calendar.getTime();

        Integer minMonth = calendar.get(Calendar.MONTH);
        Integer minYear = calendar.get(Calendar.YEAR);

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(today);
        calendar2.add(Calendar.DAY_OF_YEAR, to);
        Date maxDate = calendar2.getTime();

        Integer maxMonth = calendar.get(Calendar.MONTH);
        Integer maxYear = calendar.get(Calendar.YEAR);

        SimpleDateFormat minDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        SimpleDateFormat maxDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.US);

        if (minYear.equals(maxYear)) {
            if (minMonth.equals(maxMonth)) {
                minDateFormat = new SimpleDateFormat("dd", Locale.US);
            }
            else {
                minDateFormat = new SimpleDateFormat("dd MMM", Locale.US);
            }
        }

        return minDateFormat.format(minDate) + " - " + maxDateFormat.format(maxDate);
    }

    public static String calculateShipping(Date date,Date date2, Integer from, Integer to) {
        Date today = date;
        Date today2 = date2;

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.add(Calendar.DAY_OF_YEAR, from);
        Date minDate = calendar.getTime();

        Integer minMonth = calendar.get(Calendar.MONTH);
        Integer minYear = calendar.get(Calendar.YEAR);

        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(today2);
        calendar2.add(Calendar.DAY_OF_YEAR, to);
        Date maxDate = calendar2.getTime();

        Integer maxMonth = calendar.get(Calendar.MONTH);
        Integer maxYear = calendar.get(Calendar.YEAR);

        SimpleDateFormat minDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        SimpleDateFormat maxDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.US);

        if (minYear.equals(maxYear)) {
            if (minMonth.equals(maxMonth)) {
                minDateFormat = new SimpleDateFormat("dd", Locale.US);
            }
            else {
                minDateFormat = new SimpleDateFormat("dd MMM", Locale.US);
            }
        }

        return minDateFormat.format(minDate) + " - " + maxDateFormat.format(maxDate);


    }
}
