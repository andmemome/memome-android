package com.triodigitalagency.memome.utils;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.SearchManager;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.google.gson.Gson;
import com.triodigitalagency.memome.MainActivity;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.app.AppController;
import com.triodigitalagency.memome.database.Cart;
import com.triodigitalagency.memome.database.DatabaseHandler;
import com.triodigitalagency.memome.database.Project;
import com.triodigitalagency.memome.volley.utils.VolleyRequest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BaseActivity extends AppCompatActivity {

    protected ProgressBar progressBar;
    protected VolleyRequest request;
    protected Activity activity;
    protected Context context;

    protected SearchView searchView;
    protected String quickSearchString;

    protected ActionBar actionBar;
    protected Toolbar toolbar;
    protected MenuItem searchItem;
    public Gson gson =  new Gson();

    protected int cartTotal = 0, myProjectTotal = 0;
    protected boolean showCartIcon = false;

    protected void setupActionBar() {
        toolbar         = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        actionBar       = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.toolbar_navigation_back_button);
    }

    protected void setupActionBar(String pageTitle) {
        this.setupActionBar();
        actionBar.setTitle(pageTitle);
    }

    protected void setupActionBar(String pageTitle, int navigationIcon) {
        this.setupActionBar(pageTitle);
        if (navigationIcon > 0) {
            toolbar.setNavigationIcon(navigationIcon);
        }
    }
    protected void setupActionBarImage() {
        this.setupActionBar();
        actionBar.setTitle("");
        ImageView imgLogo = (ImageView)toolbar.findViewById(R.id.imgLogo);
        imgLogo.setVisibility(View.VISIBLE);
    }
    protected void setupActionBarImage(int navigationIcon) {
        this.setupActionBarImage();
        if (navigationIcon > 0) {
            toolbar.setNavigationIcon(navigationIcon);
        }
    }

    public interface OnSetupIntentExtras{
        void onSetupIntentExtras(Intent intent);
    }

    protected void firstInit() {
        request     = new VolleyRequest(this);
        progressBar = findViewById(R.id.ctrlActivityIndicator);
        activity    = this;
        context     = this;
    }

    protected void initQuickSearch(Menu menu) {
        SearchManager searchManager = (SearchManager)context.getSystemService(Context.SEARCH_SERVICE);
        searchView                  = null;

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }

        if (searchView != null) {
            searchView.setQueryHint(getResources().getString(R.string.action_search_hint));
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            searchView.setOnQueryTextListener(quickSearchQueryListener);
            searchView.setOnSearchClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            searchView.setOnCloseListener(new SearchView.OnCloseListener() {
                @Override
                public boolean onClose() {
                    return false;
                }
            });
        }

    }

    protected SearchView.OnQueryTextListener quickSearchQueryListener = new SearchView.OnQueryTextListener() {
        public boolean onQueryTextChange(String newText) {
            return true;
        }

        public boolean onQueryTextSubmit(String query) {
            quickSearchString                   = query;
            Map<String, String> searchParams    = new HashMap<String, String>();
            searchParams.put("keyword", quickSearchString);
            Globals.setLastSearchParams(searchParams);

            startActivity(MainActivity.class);
            finish();
            return true;
        }
    };

    protected void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    protected void hideLoading() {
        request.setAlreadyShowErrorAlert(false);
        progressBar.setVisibility(View.GONE);
    }

    protected boolean isLoading() {
        return progressBar.getVisibility() == View.VISIBLE;
    }

    protected void startActivity(Class<?> cls) {
        AppController.getInstance().getRequestQueue().cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return true;
            }
        });

        this.startActivity(cls, new OnSetupIntentExtras() {
            @Override
            public void onSetupIntentExtras(Intent intent) {

            }
        });
    }

    protected void startActivity(Class<?> cls, OnSetupIntentExtras onSetupIntentExtras) {
        AppController.getInstance().getRequestQueue().cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return true;
            }
        });

        Intent intent               = new Intent(this, cls);
        PendingIntent pendingIntent = TaskStackBuilder.create(this)
                                        .addNextIntentWithParentStack(intent)
                                        .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder  = new NotificationCompat.Builder(this);
        builder.setContentIntent(pendingIntent);
        onSetupIntentExtras.onSetupIntentExtras(intent);
        this.startActivity(intent);
    }

    protected void startActivityForResult(Class<?> cls, int requestCode) {
        AppController.getInstance().getRequestQueue().cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return true;
            }
        });

        this.startActivityForResult(cls, requestCode, new OnSetupIntentExtras() {
            @Override
            public void onSetupIntentExtras(Intent intent) {

            }
        });
    }

    protected void startActivityForResult(Class<?> cls, int requestCode, OnSetupIntentExtras onSetupIntentExtras) {
        AppController.getInstance().getRequestQueue().cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return true;
            }
        });

        Intent intent               = new Intent(this, cls);
        PendingIntent pendingIntent = TaskStackBuilder.create(this)
                                            .addNextIntentWithParentStack(intent)
                                            .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder  = new NotificationCompat.Builder(this);
        builder.setContentIntent(pendingIntent);
        onSetupIntentExtras.onSetupIntentExtras(intent);
        this.startActivityForResult(intent, requestCode);
    }

    protected Boolean isLogin() {
        return Globals.getLongDataPreference(context, Constant.P_USER_ID) > 0 && !Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN).equalsIgnoreCase("");
    }

    protected void hideSoftInputFromWindow() {
        View view = activity.getCurrentFocus();
        if (view != null) {
            ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    protected void addToCart(int projectId) {
        DatabaseHandler db  = new DatabaseHandler(this);
        List<Cart> carts    = db.getAllCarts();
        Cart cart           = null;
        for (int i = 0; i < carts.size(); i++) {
            if (carts.get(i).getProjectId() == projectId) {
                cart    = carts.get(i);
                break;
            }
        }

        if (cart != null) {
            cart.setQty(cart.getQty() + 1);
            db.updateCart(cart);
        }
        else {
            Project project = db.getProject(projectId);
            db.addCart(new Cart(projectId, project.getCorporateVoucherId() > 0 ? project.getCorporateVoucherQty() : 1));
        }
    }

    protected void minusQtyCart(int projectId) {
        DatabaseHandler db  = new DatabaseHandler(this);
        List<Cart> carts    = db.getAllCarts();
        Cart cart           = null;
        for (int i = 0; i < carts.size(); i++) {
            if (carts.get(i).getProjectId() == projectId) {
                cart    = carts.get(i);
                break;
            }
        }

        Project project = db.getProject(projectId);
        if (cart != null && cart.getQty() > (project.getCorporateVoucherId() > 0 ? project.getCorporateVoucherQty() : 1)) {
            cart.setQty(cart.getQty() - 1);
            db.updateCart(cart);
        }
    }

    protected void removeCart(int projectId) {
        DatabaseHandler db  = new DatabaseHandler(this);
        List<Cart> carts    = db.getAllCarts();
        Cart cart           = null;
        for (int i = 0; i < carts.size(); i++) {
            if (carts.get(i).getProjectId() == projectId) {
                cart    = carts.get(i);
                break;
            }
        }

        if (cart != null) {
            db.deleteCart(cart);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        DatabaseHandler db          = new DatabaseHandler(this);
        List<Cart> carts            = db.getAllCarts();
        this.cartTotal              = carts.size();

        List<Project> projects      = db.getAllProjects();
        this.myProjectTotal         = projects.size();

        if (this.showCartIcon) {
            TextView textViewCartTotal = (TextView) findViewById(R.id.textViewCartTotal);
            if (this.cartTotal > 0) {
                textViewCartTotal.setText(String.valueOf(this.cartTotal));
                textViewCartTotal.setVisibility(View.VISIBLE);
            } else {
                textViewCartTotal.setVisibility(View.GONE);
            }
        }
    }

    public void setLog(String message){
        Log.e("Test Aplikasi ==> ",message);
    }

    public void setToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
