package com.triodigitalagency.memome.utils;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

/**
 * Created by ampersanda on 21/03/18.
 */

public class Helper {
    public static void getListViewSize(ListView listView) {
        Log.e("===> ","tes");
        ListAdapter myListAdapter = listView.getAdapter();
        if (myListAdapter == null) return;

        int totalHeight = 0;
        for (int i = 0; i < myListAdapter.getCount(); i++) {
            View listItem = myListAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (myListAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
}
