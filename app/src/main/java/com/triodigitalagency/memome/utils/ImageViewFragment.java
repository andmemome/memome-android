package com.triodigitalagency.memome.utils;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.triodigitalagency.memome.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ImageViewFragment extends Fragment {

    private ImageView imageView;

    public static ImageViewFragment getInstance(int position, String imagePath) {
        ImageViewFragment myFragment    = new ImageViewFragment();
        Bundle args                     = new Bundle();

        args.putInt("position", position);
        args.putString("imagePath", imagePath);
        myFragment.setArguments(args);

        return myFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View layout     = inflater.inflate(R.layout.fragment_image_view, container, false);
        imageView       = (ImageView)layout.findViewById(R.id.imageView);
        Bundle bundle   = getArguments();

        Glide.with(layout.getContext())
            .load(bundle.getString("imagePath"))
            .placeholder(R.mipmap.loading_img)
            .into(imageView);

//        Picasso.with(layout.getContext())
//                .load(bundle.getString("imagePath"))
//                .placeholder(R.mipmap.loading_img)
//                .fit()
//                .into(imageView);

        return layout;
    }


}
