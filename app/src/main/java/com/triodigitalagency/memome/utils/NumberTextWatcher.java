package com.triodigitalagency.memome.utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;

public class NumberTextWatcher implements TextWatcher {

    private DecimalFormat df;
    private DecimalFormat dfnd;
    private boolean hasFractionalPart;
    private EditText et;

    @SuppressWarnings("unused")
    private static final String TAG = "NumberTextWatcher";

    public NumberTextWatcher(EditText et) {
        df      = new DecimalFormat("#,###.##");
        df.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.ITALIAN));
        df.setDecimalSeparatorAlwaysShown(true);
        
        dfnd    = new DecimalFormat("#,###");
        dfnd.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.ITALIAN));
        
        this.et             = et;
        hasFractionalPart   = false;
    }
    
    public void afterTextChanged(Editable s) {
        et.removeTextChangedListener(this);

        try {
            int iniLen, endLen;

            iniLen      = et.getText().length();
            String v    = s.toString().replace(String.valueOf(df.getDecimalFormatSymbols().getGroupingSeparator()), "");
            Number n    = df.parse(v);
            int cp      = et.getSelectionStart();

            if (hasFractionalPart) {
                et.setText(df.format(n));
            }
            else {
                et.setText(dfnd.format(n));
            }

            endLen      = et.getText().length();
            int sel     = (cp + (endLen - iniLen));

            if (sel > 0 && sel <= et.getText().length()) {
                et.setSelection(sel);
            }
            else {
                // place cursor at the end?
                et.setSelection(et.getText().length() - 1);
            }
        }
        catch (NumberFormatException nfe) {
            // do nothing?
        }
        catch (ParseException e) {
            // do nothing?
        }

        et.addTextChangedListener(this);
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.toString().contains(String.valueOf(df.getDecimalFormatSymbols().getDecimalSeparator()))) {
            hasFractionalPart = true;
        } 
        else {
            hasFractionalPart = false;
        }
    }
}