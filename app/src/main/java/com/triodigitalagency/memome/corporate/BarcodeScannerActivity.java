package com.triodigitalagency.memome.corporate;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.database.Cart;
import com.triodigitalagency.memome.database.DatabaseHandler;
import com.triodigitalagency.memome.database.Project;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class BarcodeScannerActivity extends BaseActivity implements ZBarScannerView.ResultHandler {


    private final String TAG = BarcodeScannerActivity.class.getSimpleName();
    private ZBarScannerView scannerView;
    private List<Cart> carts;
    private DatabaseHandler db;
    private EditText editCode;



    public static final int PERMISSION_REQUEST_CAMERA = 1;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_scanner);
        setLog("activity_barcode_scanner");

        firstInit();
        setupActionBar( getResources().getString(R.string.title_redeem_voucher));

        db                      = new DatabaseHandler(this);
        carts                   = db.getAllCarts();
        ViewGroup contentFrame  = (ViewGroup) findViewById(R.id.content_frame);
        scannerView             = new ZBarScannerView(this);

        Button btnAppl = (Button) findViewById(R.id.buttonApply);
        btnAppl.setOnClickListener(btnApplClick);
        editCode = (EditText) findViewById(R.id.editBarcode);

        contentFrame.addView(scannerView);

        editCode.setText(Globals.voucher);
        // Request permission. This does it asynchronously so we have to wait for onRequestPermissionResult before trying to open the camera.
        if (!haveCameraPermission())
            requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CAMERA);
    }


    private boolean haveCameraPermission()
    {
        if (Build.VERSION.SDK_INT < 23)
            return true;
        return checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        // This is because the dialog was cancelled when we recreated the activity.
        if (permissions.length == 0 || grantResults.length == 0)
            return;

        switch (requestCode)
        {
            case PERMISSION_REQUEST_CAMERA:
            {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    startCamera();
                }
                else
                {
                    finish();
                }
            }
            break;
        }
    }

    public void startCamera()
    {
        scannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        scannerView.startCamera();          // Start camera on resume
    }

    public void stopCamera()
    {
        scannerView.stopCamera();
    }


    @Override
    public void onResume() {
        super.onResume();
        scannerView.setResultHandler(this);
        scannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        scannerView.stopCamera();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void handleResult(Result rawResult) {
//        Toast.makeText(this, "Contents = " + rawResult.getContents() +
//                ", Format = " + rawResult.getBarcodeFormat().getName(), Toast.LENGTH_SHORT).show();

        String content  = rawResult.getContents();
        checkCode(content);
//        if (!content.substring(0, 6).equals(getResources().getString(R.string.app_name).toUpperCase())) {
//            Globals.showAlert(getResources().getString(R.string.text_barcode_error), getResources().getString(R.string.text_barcode_not_found), activity);
//            resumeCamera();
//        }
//        else {
//            checkCode(content.substring(7));
//        }
    }


    private View.OnClickListener btnApplClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String str = editCode.getText().toString();
            if(!str.equals("")) {
                if(str.length()<=6){
                    Globals.showAlert("I am sorry","coupon should be more than six",activity);
                }else {
                    if (!str.substring(0, 6).equals(getResources().getString(R.string.app_name).toUpperCase())) {
                        str = "MEMOME-" + str;
                    }
                    checkCode(str);
                }

            }
        }
    };

    private void resumeCamera() {
        // Note:
        // * Wait 2 seconds to resume the preview.
        // * On older devices continuously stopping and resuming camera preview can result in freezing the app.
        // * I don't know why this is the case but I don't have the time to figure out.
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                scannerView.resumeCameraPreview(BarcodeScannerActivity.this);
            }
        }, 2000);
    }

    private void checkCode(final String code) {
        if (!code.substring(0, 6).equals(getResources().getString(R.string.app_name).toUpperCase())) {
            Globals.showAlert(getResources().getString(R.string.text_barcode_error), getResources().getString(R.string.text_barcode_not_found), activity);
            resumeCamera();
        }
        else {
            boolean flag = true;
            final String voucherCode = code.substring(7);

            for (Cart cart : carts) {
                Project project = db.getProject(cart.getProjectId());
                if (project.getCorporateVoucherCode().equals(voucherCode)) {
                    flag = false;
                }
            }

            if (flag) {
                if (!isLoading()) {
                    showLoading();
                }

                Map<String, String> params = new HashMap<String, String>();
                params.put("clientId", Constant.CLIENT_ID);
                params.put("code", voucherCode);
                params.put("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
                request.getJSONFromUrl(Globals.getApiUrl("corporates/search"), params, getResources().getString(R.string.text_barcode_error), new RequestCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        setLog("Giftcard response "+response.toString());
                        hideLoading();
                        Globals.voucher = voucherCode;
                        try {
                            final JSONObject corporateVoucher = response.getJSONObject("corporateVoucher");
                            final long corporateVoucherId = corporateVoucher.getLong("id");
                            final String corporateVoucherName = corporateVoucher.getString("name");
                            final int corporateVoucherQty = corporateVoucher.getInt("qty");

                            Intent returnIntent = new Intent();
                            returnIntent.putExtra("corporateVoucherId", corporateVoucherId);//long
                            returnIntent.putExtra("corporateVoucherName", corporateVoucherName);//String
                            returnIntent.putExtra("corporateVoucherQty", corporateVoucherQty);//int
                            returnIntent.putExtra("corporateVoucherCode", voucherCode);//String
                            setResult(Activity.RESULT_OK,returnIntent);
                            finish();

                            hideLoading();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(VolleyError error) {
                        setResult(Activity.RESULT_OK);
                        finish();
//                        resumeCamera();
                        hideLoading();
                    }
                });
            } else {
                Globals.showAlert(getResources().getString(R.string.text_barcode_error), getResources().getString(R.string.text_barcode_used), activity);
                resumeCamera();
            }
        }
    }
}
