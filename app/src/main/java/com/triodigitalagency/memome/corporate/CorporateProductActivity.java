package com.triodigitalagency.memome.corporate;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.android.volley.VolleyError;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.adapter.ProductArrayAdapter;
import com.triodigitalagency.memome.product.ProductViewActivity;
import com.triodigitalagency.memome.transaction.CartActivity;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.EndlessScrollListener;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CorporateProductActivity extends BaseActivity {

    //<editor-fold desc="Variable">
    private ProductArrayAdapter productArrayAdapter;
    private ListView productListView;
    private ArrayList<JSONObject> productDataSource;
    private Boolean isLastProduct;
    private long corporateVoucherId, lastProductIndex, limitProduct;
    private RelativeLayout tapForReloadLayout;
    private Button buttonTapForReload;
    private RelativeLayout listHeaderView;
    private Bundle extras;
    private String corporateVoucherName, corporateVoucherCode;
    private int corporateVoucherQty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.showCartIcon   = true;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_corporate_product);
        setLog("activity_corporate_product");

        firstInit();
        setupActionBar(getResources().getString(R.string.title_redeem_voucher));

        extras                      = getIntent().getExtras();
        if (extras != null && extras.getLong("corporateVoucherId", 0) != 0) {
            corporateVoucherId      = extras.getLong("corporateVoucherId");
            corporateVoucherName    = extras.getString("corporateVoucherName");
            corporateVoucherQty     = extras.getInt("corporateVoucherQty");
            corporateVoucherCode    = extras.getString("corporateVoucherCode");
        }
        else {
            finish();
        }

        isLastProduct               = false;
        lastProductIndex            = 0;
        limitProduct                = Constant.SEARCH_LIMIT;
        productDataSource           = new ArrayList<JSONObject>();
        productListView             = (ListView) findViewById(R.id.listView);
        productArrayAdapter         = new ProductArrayAdapter(context);

        LayoutInflater inflater     = activity.getLayoutInflater();
        listHeaderView              = (RelativeLayout)inflater.inflate(R.layout.layout_corporate_product_header, null);
        productListView.addHeaderView(listHeaderView);
        productListView.setAdapter(productArrayAdapter);
        productListView.setOnItemClickListener(onProductClickListener);
        productListView.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                super.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
            }

            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadDataProduct();
            }
        });

        initTapForReload();
        resetAndLoadDataProduct();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_default, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        else if (id == R.id.actionCart) {
            startActivity(CartActivity.class);
        }

        return super.onOptionsItemSelected(item);
    }
    //</editor-fold>

    //<editor-fold desc="Listener">
    protected AdapterView.OnItemClickListener onProductClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
            if (position >= 1 && position <= productArrayAdapter.getCount()) {
                startActivity(ProductViewActivity.class, new BaseActivity.OnSetupIntentExtras() {
                    @Override
                    public void onSetupIntentExtras(Intent intent) {
                        intent.putExtra("productId", productArrayAdapter.getItemId(position - 1));
                        intent.putExtra("corporateVoucherId", corporateVoucherId);
                        intent.putExtra("corporateVoucherName", corporateVoucherName);
                        intent.putExtra("corporateVoucherQty", corporateVoucherQty);
                        intent.putExtra("corporateVoucherCode", corporateVoucherCode);
                    }
                });
            }
        }
    };
    //</editor-fold>

    //<editor-fold desc="Procedure & Function">
    protected void resetAndLoadDataProduct() {
        isLastProduct      = false;
        lastProductIndex   = 0;
        if (productDataSource == null) {
            productDataSource = new ArrayList<JSONObject>();
        }
        else {
            productDataSource.clear();
        }
        productArrayAdapter.setValues(productDataSource);
        productArrayAdapter.notifyDataSetChanged();
        loadDataProduct();
    }

    protected void loadDataProduct() {
        if (!isLoading() && !isLastProduct) {
            showLoading();

            Map<String, String> params = new HashMap<String, String>();
            params.put("clientId", Constant.CLIENT_ID);
            params.put("corporateVoucherId", String.valueOf(corporateVoucherId));
            params.put("limit", String.format("%d", limitProduct));
            params.put("start", String.format("%d", lastProductIndex));

            request.getJSONFromUrl(Globals.getApiUrl("corporates/searchProduct"), params, getResources().getString(R.string.text_corporate_product_error), new RequestCallback() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONArray products = response.getJSONArray("products");

                        lastProductIndex += products.length();
                        if (products.length() < limitProduct) {
                            isLastProduct = true;
                        }

                        for (int i = 0; i < products.length(); i++) {
                            productDataSource.add(products.getJSONObject(i));
                        }

                        productArrayAdapter.setValues(productDataSource);
                        productArrayAdapter.notifyDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    hideLoading();
                }

                @Override
                public void onError(VolleyError error) {
                    tapForReloadLayout.setVisibility(View.VISIBLE);
                    hideLoading();
                }
            });
        }
    }

    protected void initTapForReload() {
        buttonTapForReload  = (Button)findViewById(R.id.buttonTapForReload);
        buttonTapForReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tapForReloadLayout.setVisibility(View.GONE);
                resetAndLoadDataProduct();
            }
        });

        tapForReloadLayout  = (RelativeLayout)findViewById(R.id.tapForReload);
        tapForReloadLayout.setVisibility(View.GONE);
    }
    //</editor-fold>
}
