package com.triodigitalagency.memome;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.triodigitalagency.memome.adapter.CategoryArrayAdapter;
import com.triodigitalagency.memome.adapter.ImageViewAdapter;
import com.triodigitalagency.memome.adapter.NavDrawerListAdapter;
import com.triodigitalagency.memome.company.AboutActivity;
import com.triodigitalagency.memome.company.TermsActivity;
import com.triodigitalagency.memome.corporate.BarcodeScannerActivity;
import com.triodigitalagency.memome.gcm.GcmIntentService;
import com.triodigitalagency.memome.gcm.NotificationUtils;
import com.triodigitalagency.memome.model.Message;
import com.triodigitalagency.memome.model.NavDrawerItem;
import com.triodigitalagency.memome.product.CategoryDetailActivity;
import com.triodigitalagency.memome.transaction.CartActivity;
import com.triodigitalagency.memome.transaction.ProgressActivity;
import com.triodigitalagency.memome.tutorial.TutorialActivity;
import com.triodigitalagency.memome.user.LoginActivity;
import com.triodigitalagency.memome.user.MyProfileActivity;
import com.triodigitalagency.memome.user.MyPurchasesActivity;
import com.triodigitalagency.memome.user.ResetPasswordActivity;
import com.triodigitalagency.memome.utils.AppRater;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.EndlessScrollListener;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;
import com.triodigitalagency.memome.webview.PrivacyPolice;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends BaseActivity {

    private DrawerLayout drawerLayout;
    private ListView listViewSliderMenu;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter navDrawerListAdapter;
    private boolean doubleBackToExitPressedOnce = false;

    private Button btnContactUsPhone, btnContactUsEmail;

    private CategoryArrayAdapter categoryArrayAdapter;
    private ListView productListView;
    private ArrayList<JSONObject> productDataSource ;
    private Boolean isLastProduct;
    private long lastProductIndex, limitProduct;
    private RelativeLayout tapForReloadLayout;
    private Button buttonTapForReload;
    private RelativeLayout listHeaderView;
    private LinearLayout line1;

    private GoogleApiClient mClient;
    private String mUrl;
    private String mTitle;
    private String mDescription;

    private int confirmTotal = 0;

    private String TAG = MainActivity.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.showCartIcon = true;

        mClient = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        mUrl = String.format("%s://%s/", getResources().getString(R.string.deep_link_schema1), getResources().getString(R.string.deep_link_host1));
        mTitle = getResources().getString(R.string.app_name);
        mDescription = getResources().getString(R.string.app_description);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setLog("activity_main");

        firstInit();
//      setupActionBar(getResources().getString(R.string.title_make_prints), R.drawable.toolbar_navigation_button);
        this.setupActionBarImage(R.drawable.toolbar_navigation_button);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.setScrimColor(ContextCompat.getColor(context, R.color.blackTransparent));
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLogin()) {
                    drawerLayout.openDrawer(GravityCompat.START);
                } else {
                    startActivity(LoginActivity.class);
                }
            }
        });
        toggle.syncState();

        btnContactUsPhone = (Button) findViewById(R.id.btnContactUsPhone);
        btnContactUsPhone.setText(Constant.CONTACT_US_PHONE);
        btnContactUsPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + Constant.CONTACT_US_PHONE));
                if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
            }
        });

        btnContactUsEmail           = (Button)findViewById(R.id.btnContactUsEmail);
        btnContactUsEmail.setText(Constant.CONTACT_US_EMAIL);
        btnContactUsEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

                emailIntent.setType("message/rfc822");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{Constant.CONTACT_US_EMAIL});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Contact Us");
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");

                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            }
        });

        Button buttonTerms          = (Button)findViewById(R.id.buttonTerms);
        Button privacyPolice          = (Button)findViewById(R.id.buttonPrivacy);
        privacyPolice.setOnClickListener(buttonPrivacyClicked);
        buttonTerms.setOnClickListener(buttonTermsClicked);

        navDrawerItems              = new ArrayList<NavDrawerItem>();
        navDrawerListAdapter        = new NavDrawerListAdapter(getApplicationContext());
        navDrawerListAdapter.setValue(navDrawerItems);
        listViewSliderMenu          = (ListView) findViewById(R.id.list_slider_menu);
        listViewSliderMenu.setOnItemClickListener(new SlideMenuClickListener());
        listViewSliderMenu.setAdapter(navDrawerListAdapter);

        isLastProduct               = false;
        lastProductIndex            = 0;
        limitProduct                = Constant.SEARCH_LIMIT;
        productDataSource           = new ArrayList<JSONObject>();
        line1                       = findViewById(R.id.line1);
        productListView             = (ListView) findViewById(R.id.listView);
        categoryArrayAdapter         = new CategoryArrayAdapter(context);


//      productListView.setAdapter(categoryArrayAdapter);
        productListView.setOnItemClickListener(onProductClickListener);
        productListView.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                super.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
            }

            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadDataProduct();
            }
        });

        if (isLogin()) {
            loadDataOrdersNotYetPaid();
        }

        if (!isLogin()){
            TextView textViewProfileName = findViewById(R.id.textViewProfileName);
            textViewProfileName.setText(getResources().getString(R.string.text_profile_name2));
        }

        AppRater.appLaunched(context);

        setupMenu();
        checkDeepLink();
        initTapForReload();
        resetAndLoadDataProduct();


        /**
         * Broadcast receiver calls in two scenarios
         * 1. gcm registration is completed
         * 2. when new push notification is received
         * */
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Constant.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    subscribeToGlobalTopic();

                } else if (intent.getAction().equals(Constant.SENT_TOKEN_TO_SERVER)) {
                    // gcm registration id is stored in our server's MySQL
                    //Log.e(TAG, "GCM registration id is sent to our server");

                } else if (intent.getAction().equals(Constant.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    handlePushNotification(intent);
                    //Log.e(TAG, "GCM push");
                }
            }
        };

        /**
         * Always check for google play services availability before
         * proceeding further with GCM
         * */
        if (checkPlayServices()) {
            registerGCM();
        }
        /*
        if(Globals.getDataPreference(getApplicationContext(),"uploadfile")){
            startActivity(new Intent(MainActivity.this,ProgressActivity.class));
        }*/
    }

    @Override
    public void onStart() {
        super.onStart();
        mClient.connect();
        AppIndex.AppIndexApi.start(mClient, getAction());
    }

    @Override
    public void onStop() {
        AppIndex.AppIndexApi.end(mClient, getAction());
        mClient.disconnect();
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();

                return;
            }

            this.doubleBackToExitPressedOnce = true;

            Globals.showToast(getResources().getString(R.string.text_double_back), context);

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (isLogin()) {
            TextView textViewProfileName        = findViewById(R.id.textViewProfileName);
            ImageView imageViewProfilePicture   = findViewById(R.id.imageViewProfilePicture);
            String name                         = Globals.getStringDataPreference(context, Constant.P_USER_NAME);
            String imagePath                    = Globals.getStringDataPreference(context, Constant.P_USER_IMAGE_PATH);
            textViewProfileName.setText(String.format(getResources().getString(R.string.text_profile_name), name));



            if (!imagePath.equals("")) {
                request.getImageFromUrl(imagePath, imageViewProfilePicture, 0);
            }
            else {
                imageViewProfilePicture.setImageResource(0);
            }

            imageViewProfilePicture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(MyProfileActivity.class);
                }
            });

            loadDataOrdersNotYetPaid();
        }

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Constant.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Constant.PUSH_NOTIFICATION));

        // clearing the notification tray
        NotificationUtils.clearNotifications();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_default, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.actionCart) {
            startActivity(CartActivity.class);
        }

        return super.onOptionsItemSelected(item);
    }
    //</editor-fold>

    //<editor-fold desc="Listener">
    public void checkDeepLink() {
        Intent intent   = getIntent();
        Uri data        = intent.getData();

        if (data != null) {
            Boolean forgetPassword      = false;
            List<String> pathSegments   = data.getPathSegments();
            for (int i = 0; i < pathSegments.size(); i++) {
                if (pathSegments.get(i).toLowerCase().trim().equals("forgetpassword")) {
                    forgetPassword = true;
                }
            }

            if (forgetPassword) {
                final String activationKey  = data.getLastPathSegment();
                startActivity(ResetPasswordActivity.class, new OnSetupIntentExtras() {
                    @Override
                    public void onSetupIntentExtras(Intent intent) {
                        intent.putExtra("activationKey", activationKey);
                    }
                });
            }
        }
    }

    public Action getAction() {
        Thing object = new Thing.Builder()
            .setName(mTitle)
            .setDescription(mDescription)
            .setUrl(Uri.parse(mUrl))
            .build();

        return new Action.Builder(Action.TYPE_VIEW)
            .setObject(object)
            .setActionStatus(Action.STATUS_TYPE_COMPLETED)
            .build();
    }

    protected class SlideMenuClickListener implements  ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            switch (position) {
//                case 0:
//                    startActivity(ProjectActivity.class);
//                    break;
//                case 1:
//                    startActivity(ListNotPaidActivity.class);
//                    break;
                case 0:
                    startActivity(MyPurchasesActivity.class);
                    break;
                case 1:
                    startActivity(MyProfileActivity.class);
                    break;
                case 2:
                    startActivity(TutorialActivity.class);
                    break;
                case 3:
                    startActivity(AboutActivity.class);
                    break;
                case 4:
                    startActivity(BarcodeScannerActivity.class);
                    break;
                default:
                    break;
            }
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    protected View.OnClickListener buttonPrivacyClicked = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(new Intent(MainActivity.this,PrivacyPolice.class));
        }
    };

    protected View.OnClickListener buttonTermsClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(TermsActivity.class);
        }
    };

    protected AdapterView.OnItemClickListener onProductClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
            if (position >= 1 && position <= categoryArrayAdapter.getCount()) {
                startActivity(CategoryDetailActivity.class, new BaseActivity.OnSetupIntentExtras() {
                    @Override
                    public void onSetupIntentExtras(Intent intent) {
                        String title = "";
                        try {
                            title = ((JSONObject) categoryArrayAdapter.getItem(position - 1)).getString("name");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        intent.putExtra("categoryTitle", title);
                        intent.putExtra("categoryId", categoryArrayAdapter.getItemId(position - 1));
                    }
                });
            }
        }
    };

    protected void reSetUpMenu() {
        setupMenu();

        TextView textViewHomeTotal   = (TextView)findViewById(R.id.textViewHomeTotal);
        if (this.confirmTotal > 0) {
            textViewHomeTotal.setText(String.valueOf(this.confirmTotal));
            textViewHomeTotal.setVisibility(View.GONE);
        }
        else {
            textViewHomeTotal.setVisibility(View.GONE);
        }
    }

    protected void setupMenu() {
        navDrawerItems.clear();
//        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.menu_my_projects), R.drawable.icon_my_projects, true, String.valueOf(myProjectTotal)));
//        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.menu_confirm_payment), R.drawable.icon_confirm_payment, true, String.valueOf(confirmTotal)));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.menu_my_purchase), R.drawable.icon_purchase));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.menu_my_profile), R.drawable.icon_profile));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.menu_how_to_use), R.drawable.icon_how_to_use));
        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.menu_about_us), R.drawable.icon_about));
//        navDrawerItems.add(new NavDrawerItem(getResources().getString(R.string.menu_redeem), R.drawable.icon_redeem));
        navDrawerListAdapter.setValue(navDrawerItems);
        navDrawerListAdapter.notifyDataSetChanged();
    }

    protected void resetAndLoadDataProduct() {
        isLastProduct      = false;
        lastProductIndex   = 0;
        if (productDataSource == null) {
            productDataSource = new ArrayList<JSONObject>();
        }
        else {
            productDataSource.clear();
        }
        categoryArrayAdapter.setValues(productDataSource);
        categoryArrayAdapter.notifyDataSetChanged();
        loadDataProduct(true);
        loadBanners(true);
    }

    protected void loadDataOrdersNotYetPaid() {
        if (!isLoading()) {
            showLoading();
        }

        Map<String, String> params = new HashMap<String, String>();
        params.put("clientId", Constant.CLIENT_ID);
        params.put("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
        request.getJSONFromUrl(Globals.getApiUrl("orders/getOrderNotYetPaid"), params, getResources().getString(R.string.text_order_error), new RequestCallback() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray orders = response.getJSONArray("orders");
                    confirmTotal = orders.length();

                    reSetUpMenu();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                hideLoading();
            }

            @Override
            public void onError(VolleyError error) {
                hideLoading();
            }
        });
    }

    protected void loadDataProduct() {
        this.loadDataProduct(false);
    }

    protected void loadDataProduct(boolean isForce) {
        if ((!isLoading() || isForce) && !isLastProduct) {
            showLoading();

            Map<String, String> params = new HashMap<String, String>();
            params.put("clientId", Constant.CLIENT_ID);
            params.put("limit", String.format("%d", limitProduct));
            params.put("start", String.format("%d", lastProductIndex));

            request.getJSONFromUrl(Globals.getApiUrl("category/search"), params, getResources().getString(R.string.text_product_error), new RequestCallback() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONArray products = response.getJSONArray("category");

                        lastProductIndex += products.length();
                        if (products.length() < limitProduct) {
                            isLastProduct = true;
                        }

                        productDataSource.clear();
                        for (int i = 0; i < products.length(); i++) {
                            productDataSource.add(products.getJSONObject(i));
                        }

                        categoryArrayAdapter.setValues(productDataSource);
                        categoryArrayAdapter.notifyDataSetChanged();


//                        JSONArray banners = response.getJSONArray("banners");
//                        List<String> tempImagePaths = new ArrayList<String>();
//
//                        for (int i = 0; i < banners.length(); i++) {
//                            if (banners.getJSONObject(i).has("images") && !banners.getJSONObject(i).isNull("images")) {
//                                JSONObject images = banners.getJSONObject(i).getJSONObject("images");
//                                tempImagePaths.add(images.getString("original"));
//                            }
//                        }
//
//                        if(tempImagePaths.size() > 0) {
//                            if(listHeaderView == null){
//                                LayoutInflater inflater         = activity.getLayoutInflater();
//                                listHeaderView                  = (RelativeLayout)inflater.inflate(R.layout.layout_product_banner, null);
//                                if(productListView.getAdapter() == null) {
//                                    productListView.addHeaderView(listHeaderView);
//
//                                    productListView.setAdapter(categoryArrayAdapter);
//                                }
//                            }
//
//                            ViewPager pager = (ViewPager) listHeaderView.findViewById(R.id.viewPagerProductBanner);
//                            pager.setAdapter(new ImageViewAdapter(getSupportFragmentManager(), MainActivity.this, tempImagePaths));
//
//                            CirclePageIndicator indicator = (CirclePageIndicator) listHeaderView.findViewById(R.id.viewIndicatorProductBanner);
//                            indicator.setViewPager(pager);
//                        }
//                        else if(productListView.getAdapter() == null) {
                            productListView.setAdapter(categoryArrayAdapter);
//                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    hideLoading();
                }

                @Override
                public void onError(VolleyError error) {
                    tapForReloadLayout.setVisibility(View.VISIBLE);
                    hideLoading();
                }
            });
        }
    }

    protected void loadBanners(boolean isForce) {
//        if ((!isLoading() || isForce) && !isLastProduct) {
//            showLoading();

            Map<String, String> params = new HashMap<String, String>();
            params.put("clientId", Constant.CLIENT_ID);
//            params.put("limit", String.format("%d", limitProduct));
//            params.put("start", String.format("%d", lastProductIndex));

            request.getJSONFromUrl(Globals.getApiUrl("products/banner"), params, getResources().getString(R.string.text_banner_error), new RequestCallback() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
//                        JSONArray products = response.getJSONArray("category");
//
//                        lastProductIndex += products.length();
//                        if (products.length() < limitProduct) {
//                            isLastProduct = true;
//                        }
//
//                        for (int i = 0; i < products.length(); i++) {
//                            productDataSource.add(products.getJSONObject(i));
//                        }
//
//                        categoryArrayAdapter.setValues(productDataSource);
//                        categoryArrayAdapter.notifyDataSetChanged();


                        JSONArray banners = response.getJSONArray("banners");
                        List<String> tempImagePaths = new ArrayList<String>();

                        for (int i = 0; i < banners.length(); i++) {
                            if (banners.getJSONObject(i).has("images") && !banners.getJSONObject(i).isNull("images")) {
                                JSONObject images = banners.getJSONObject(i).getJSONObject("images");
                                tempImagePaths.add(images.getString("original"));
                            }
                        }

                        if(tempImagePaths.size() > 0) {
                            if(listHeaderView == null){
                                LayoutInflater inflater         = activity.getLayoutInflater();
                                listHeaderView                  = (RelativeLayout)inflater.inflate(R.layout.layout_product_banner, null);

                                    productListView.addHeaderView(listHeaderView);

                                    productListView.setAdapter(categoryArrayAdapter);

                            }

                            ViewPager pager = (ViewPager) listHeaderView.findViewById(R.id.viewPagerProductBanner);
                            pager.setAdapter(new ImageViewAdapter(getSupportFragmentManager(), MainActivity.this, tempImagePaths));

                            CirclePageIndicator indicator = (CirclePageIndicator) listHeaderView.findViewById(R.id.viewIndicatorProductBanner);
                            indicator.setViewPager(pager);
                        }
                        else if(productListView.getAdapter() == null) {
                            productListView.setAdapter(categoryArrayAdapter);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    hideLoading();
                }

                @Override
                public void onError(VolleyError error) {
                    tapForReloadLayout.setVisibility(View.VISIBLE);
                    hideLoading();
                }
            });
//        }
    }

    protected void initTapForReload() {
        buttonTapForReload  = (Button)findViewById(R.id.buttonTapForReload);
        buttonTapForReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tapForReloadLayout.setVisibility(View.GONE);
                resetAndLoadDataProduct();
            }
        });

        tapForReloadLayout  = (RelativeLayout)findViewById(R.id.tapForReload);
        tapForReloadLayout.setVisibility(View.GONE);
    }

    // subscribing to global topic
    private void subscribeToGlobalTopic() {
        Intent intent = new Intent(this, GcmIntentService.class);
        intent.putExtra(Constant.KEY, Constant.SUBSCRIBE);
        intent.putExtra(Constant.TOPIC, Constant.TOPIC_GLOBAL);
        startService(intent);
    }

    /**
     * Handles new push notification
     */
    private void handlePushNotification(Intent intent) {
        int type = intent.getIntExtra("type", -1);

        // if the push is of chat room message
        // simply update the UI unread messages count
        if (type == Constant.PUSH_TYPE_USER) {
            // push belongs to user alone
            // just showing the message in a toast
            Message message = (Message) intent.getSerializableExtra("message");
            Globals.showToast(message.getMessage(), context);
        }

    }

    // starting the service to register with GCM
    private void registerGCM() {
        Intent intent = new Intent(this, GcmIntentService.class);
        intent.putExtra("key", "register");
        startService(intent);
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported. Google Play Services not installed!");
                Globals.showToast("This device is not supported. Google Play Services not installed!", context);
                finish();
            }
            return false;
        }
        return true;
    }
}
