package com.triodigitalagency.memome.volley.callback;

import android.graphics.Bitmap;

import com.android.volley.VolleyError;

public interface ImageRequestCallback {

    void onResponse(Bitmap bitmap);
    void onError(VolleyError error);

}
