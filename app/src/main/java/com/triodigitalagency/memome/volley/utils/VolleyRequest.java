package com.triodigitalagency.memome.volley.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.app.AppController;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.ImageRequestCallback;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import static com.android.volley.Request.Method.POST;

public class VolleyRequest {

    private Boolean alreadyShowErrorAlert = false;

    private Activity activity;

    public VolleyRequest(Activity _activity){
        activity = _activity;
    }

    public void setAlreadyShowErrorAlert(Boolean flag) {
        alreadyShowErrorAlert    = flag;
    }

    public String urlToTag(String url){
        String result = url.replace(Globals.getApiUrl(""), "");
        if(result.indexOf("?") > 0) {
            result = result.substring(0, result.indexOf("?") - 1);
        }
        result = result.replaceAll("/", "_");
        return result;
    }

    public void getJSONFromUrl(String url, final Map<String, String> params, final String errorTitle, final RequestCallback requestCallback){
        // Tag used to cancel the request
        String tag_json_obj = urlToTag(url);

        StringRequest jsonObjReq = new StringRequest(POST,
                url,
//                (String)null,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String resp) {
                        JSONObject response = null;
                        try {
                            response = new JSONObject(resp);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("Error", "String Response : " + resp);
                        }

                        Boolean isError = false;
                        VolleyError error = null;
                        try{
                            if (response.getJSONObject("message").getInt("code") != 200) {
                                Globals.showAlert(
                                        errorTitle,
                                        response.getJSONObject("message").getString("content"),
                                        activity
                                );
//                                Globals.showToast(response.getJSONObject("message").getString("content"), activity);
                                error = new VolleyError( response.getJSONObject("message").getString("content") );
                                isError = true;
                            }
                        }
                        catch (Exception e){
                            if (!alreadyShowErrorAlert) {
                                Log.d("Error", "Error: " + e.getMessage());
//                                Globals.showAlert(
//                                        errorTitle,
//                                        activity.getResources().getString(R.string.text_unknown_error),
//                                        activity
//                                );
                                Globals.showToast(activity.getResources().getString(R.string.text_unknown_error), activity);
                                error               = new VolleyError(activity.getResources().getString(R.string.text_unknown_error));
                                isError             = true;
                                alreadyShowErrorAlert    = true;
                            }
                        }

                        if (isError){
                            requestCallback.onError(error);
                        }
                        else{
                            requestCallback.onResponse(response);
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (!alreadyShowErrorAlert) {
                    Log.d("Error", error.getMessage() == null ? activity.getResources().getString(R.string.text_connection_error) : error.getMessage());
//                    Globals.showAlert(
//                            errorTitle,
//                            activity.getResources().getString(R.string.text_connection_error),
//                            activity
//                    );
                    Globals.showToast(activity.getResources().getString(R.string.text_connection_error), activity);
                    alreadyShowErrorAlert    = true;
                }

                requestCallback.onError(error);
            }
        }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjReq.setRetryPolicy(policy);

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public void requestImage(String url, final String errorTitle, final MarkerOptions marker){
        ImageRequest request = new ImageRequest(url,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        marker.icon(BitmapDescriptorFactory.fromBitmap(bitmap));
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        AppController.getInstance().addToRequestQueue(request);
    }

    public void requestImage(String url, final String errorTitle, final ImageRequestCallback imageRequestCallback){
        ImageRequest request = new ImageRequest(url,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        imageRequestCallback.onResponse(bitmap);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        imageRequestCallback.onError(error);
                    }
                });
        AppController.getInstance().addToRequestQueue(request);
    }

    public void getImageFromUrl(String url, ImageView imageView, int placeholderImage){
        ImageLoader imageLoader = AppController.getInstance().getImageLoader();
        // Loading image with placeholder and error image
        imageLoader.get(url, ImageLoader.getImageListener( imageView, placeholderImage, placeholderImage ));
    }

    public void getImageFromUrl(Context context, JSONObject images, ImageView imageView, int placeholderImage){
        int density= context.getResources().getDisplayMetrics().densityDpi;
        try {
            switch(density)
            {
                case DisplayMetrics.DENSITY_LOW:
                    getImageFromUrl(images.getString("mdpi"), imageView, placeholderImage);
                    break;
                case DisplayMetrics.DENSITY_MEDIUM:
                    getImageFromUrl(images.getString("mdpi"), imageView, placeholderImage);
                    break;
                case DisplayMetrics.DENSITY_HIGH:
                    getImageFromUrl(images.getString("hdpi"), imageView, placeholderImage);
                    break;
                case DisplayMetrics.DENSITY_XHIGH:
                    getImageFromUrl(images.getString("xhdpi"), imageView, placeholderImage);
                    break;
                case DisplayMetrics.DENSITY_XXHIGH:
                    getImageFromUrl(images.getString("xxhdpi"), imageView, placeholderImage);
                    break;
                case DisplayMetrics.DENSITY_XXXHIGH:
                    getImageFromUrl(images.getString("xxhdpi"), imageView, placeholderImage);
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
