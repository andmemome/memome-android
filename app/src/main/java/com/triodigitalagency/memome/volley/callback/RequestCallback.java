package com.triodigitalagency.memome.volley.callback;

import com.android.volley.VolleyError;

import org.json.JSONObject;

public interface RequestCallback {

    void onResponse(JSONObject response);
    void onError(VolleyError error);

}
