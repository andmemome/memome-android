package com.triodigitalagency.memome.company;

import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TermsActivity extends BaseActivity {

    protected RelativeLayout tapForReloadLayout;
    protected Button buttonTapForReload;
    @BindView(R.id.myWebview)WebView myWebview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
        ButterKnife.bind(this);
        setLog("activity_terms");

        firstInit();
        setupActionBar(getResources().getString(R.string.title_terms));


        myWebview.loadUrl("file:///android_asset/termsconditon.html");


        /*initTapForReload();
        loadData();*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //</editor-fold>

    //<editor-fold desc="Procedure & Function">
    protected void loadData(){
        if(!isLoading()){
            showLoading();
            Map<String, String> params = new HashMap<String, String>();
            params.put("clientId", Constant.CLIENT_ID);
            request.getJSONFromUrl(Globals.getApiUrl("about/search"),
                    params,
                    getResources().getString(R.string.text_product_error),
                    new RequestCallback() {
                        @Override
                        public void onResponse(JSONObject response) {
                            setLog(response.toString());
                            hideLoading();

                            try {
                                drawData(response.getJSONObject("about"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(VolleyError error) {
                            tapForReloadLayout.setVisibility(View.VISIBLE);
                            hideLoading();
                        }
                    });
        }
    }

    protected void drawData(JSONObject data){
        try {
            if (data.has("terms_content")) {
                TextView textView = (TextView)findViewById(R.id.textContent);
                textView.setText(Html.fromHtml(data.getString("terms_content")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void initTapForReload() {
        buttonTapForReload  = (Button)findViewById(R.id.buttonTapForReload);
        buttonTapForReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tapForReloadLayout.setVisibility(View.GONE);
                loadData();
            }
        });

        tapForReloadLayout  = (RelativeLayout)findViewById(R.id.tapForReload);
        tapForReloadLayout.setVisibility(View.GONE);
    }
    //</editor-fold>
}
