package com.triodigitalagency.memome.company;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.adapter.ImageViewAdapter;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AboutActivity extends BaseActivity {

    //<editor-fold desc="Variable">
    protected RelativeLayout tapForReloadLayout;
    protected Button buttonTapForReload;
    //</editor-fold>

    //<editor-fold desc="Override">
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        setLog("activity_about");

        firstInit();
        setupActionBar(getResources().getString(R.string.title_about));

        initTapForReload();
        loadData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //</editor-fold>

    //<editor-fold desc="Procedure & Function">
    protected void loadData(){
        if(!isLoading()){
            showLoading();
            Map<String, String> params = new HashMap<String, String>();
            params.put("clientId", Constant.CLIENT_ID);
            request.getJSONFromUrl(Globals.getApiUrl("about/search"),
                    params,
                    getResources().getString(R.string.text_product_error),
                    new RequestCallback() {
                        @Override
                        public void onResponse(JSONObject response) {
                            hideLoading();

                            try {
                                drawData(response.getJSONObject("about"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(VolleyError error) {
                            tapForReloadLayout.setVisibility(View.VISIBLE);
                            hideLoading();
                        }
                    });
        }
    }

    protected void drawData(JSONObject data){
        try {
            if (data.has("about_content")) {
                TextView textView = (TextView)findViewById(R.id.textContent);
                textView.setText(Html.fromHtml(data.getString("about_content")));
            }

            List<String> tempImagePaths = new ArrayList<String>();
            if (data.has("about_images") && !data.isNull("about_images") && data.getJSONArray("about_images").length() > 0) {
                for (int i = 0; i < data.getJSONArray("about_images").length(); i++) {
                    tempImagePaths.add(data.getJSONArray("about_images").getJSONObject(i).getString("large"));
                }
            }

            ViewPager pager         = (ViewPager)findViewById(R.id.viewPager);
            pager.setAdapter(new ImageViewAdapter(getSupportFragmentManager(), this, tempImagePaths));

            CirclePageIndicator indicator = (CirclePageIndicator)findViewById(R.id.viewIndicator);
            indicator.setViewPager(pager);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void initTapForReload() {
        buttonTapForReload  = (Button)findViewById(R.id.buttonTapForReload);
        buttonTapForReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tapForReloadLayout.setVisibility(View.GONE);
                loadData();
            }
        });

        tapForReloadLayout  = (RelativeLayout)findViewById(R.id.tapForReload);
        tapForReloadLayout.setVisibility(View.GONE);
    }
    //</editor-fold>
}
