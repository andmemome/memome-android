package com.triodigitalagency.memome.database;

public class Project {
    //private variables
    int _id;
    int _product_id;
    int _reorder_id = 0;
    String _title;
    String _subtitle;
    String _path_image_crop;
    String _path_image_original;
    String _created_at;
    int _corporate_voucher_id;
    String _corporate_voucher_name;
    int _corporate_voucher_qty;
    String _corporate_voucher_code;

    // Empty constructor
    public Project() {}

    // constructor
    public Project(int id, int productId, String title, String subtitle, String pathImageCrop, String pathImageOriginal, String createdAt, int corporateVoucherId, String corporateVoucherName, int corporateVoucherQty, String corporateVoucherCode, int reorderId) {
        this._id                        = id;
        this._product_id                = productId;
        this._title                     = title;
        this._subtitle                  = subtitle;
        this._path_image_crop           = pathImageCrop;
        this._path_image_original       = pathImageOriginal;
        this._created_at                = createdAt;
        this._corporate_voucher_id      = corporateVoucherId;
        this._corporate_voucher_name    = corporateVoucherName;
        this._corporate_voucher_qty     = corporateVoucherQty;
        this._corporate_voucher_code    = corporateVoucherCode;
        this._reorder_id                = reorderId;
    }

    // constructor
    public Project(int productId, String title, String subtitle, String pathImageCrop, String pathImageOriginal, String createdAt, int corporateVoucherId, String corporateVoucherName, int corporateVoucherQty, String corporateVoucherCode, int reorderId) {
        this._product_id                = productId;
        this._title                     = title;
        this._subtitle                  = subtitle;
        this._path_image_crop           = pathImageCrop;
        this._path_image_original       = pathImageOriginal;
        this._created_at                = createdAt;
        this._corporate_voucher_id      = corporateVoucherId;
        this._corporate_voucher_name    = corporateVoucherName;
        this._corporate_voucher_qty     = corporateVoucherQty;
        this._corporate_voucher_code    = corporateVoucherCode;
        this._reorder_id                = reorderId;
    }

    // getting ID
    public int getID() {
        return this._id;
    }

    // setting id
    public void setID(int id) {
        this._id = id;
    }

    public int getProductId() {
        return this._product_id;
    }

    public void setProductId(int productId) {
        this._product_id = productId;
    }

    public int getReorderId() {
        return this._reorder_id;
    }

    public void setReorderId(int reorderId) {
        this._reorder_id = reorderId;
    }

    public String getTitle() {
        return this._title;
    }

    public void setTitle(String title) {
        this._title = title;
    }

    public String getSubtitle() {
        return this._subtitle;
    }

    public void setSubtitle(String subtitle) {
        this._subtitle = subtitle;
    }

    public String getPathImageCrop() {
        return this._path_image_crop;
    }

    public void setPathImageCrop(String pathImageCrop) {
        this._path_image_crop = pathImageCrop;
    }

    public String getPathImageOriginal() {
        return this._path_image_original;
    }

    public void setPathImageOriginal(String pathImageOriginal) {
        this._path_image_original = pathImageOriginal;
    }

    public String getCreatedAt() {
        return this._created_at;
    }

    public void setCreatedAt(String created_at) {
        this._created_at = created_at;
    }

    public int getCorporateVoucherId() {
        return this._corporate_voucher_id;
    }

    public void setCorporateVoucherId(int corporateVoucherId) {
        this._corporate_voucher_id = corporateVoucherId;
    }

    public String getCorporateVoucherName() {
        return this._corporate_voucher_name;
    }

    public void setCorporateVoucherName(String corporateVoucherName) {
        this._corporate_voucher_name = corporateVoucherName;
    }

    public int getCorporateVoucherQty() {
        return this._corporate_voucher_qty;
    }

    public void setCorporateVoucherQty(int corporateVoucherQty) {
        this._corporate_voucher_qty = corporateVoucherQty;
    }

    public String getCorporateVoucherCode() {
        return this._corporate_voucher_code;
    }

    public void setCorporateVoucherCode(String corporateVoucherCode) {
        this._corporate_voucher_code = corporateVoucherCode;
    }
}
