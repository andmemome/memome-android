package com.triodigitalagency.memome.database;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ProjectDetail {
    //private variables
    int _id;
    int _project_id;
    int _product_detail_id;
    String _path_image_crop;
    String _path_image_original;

    // Empty constructor
    public ProjectDetail() { }

    // constructor
    public ProjectDetail(int id, int projectId, int productDetailId, String pathImageCrop, String pathImageOriginal) {
        this._id                    = id;
        this._project_id            = projectId;
        this._product_detail_id     = productDetailId;
        this._path_image_crop       = pathImageCrop;
        this._path_image_original   = pathImageOriginal;
    }

    // constructor
    public ProjectDetail(int projectId, int productDetailId, String pathImageCrop, String pathImageOriginal) {
        this._project_id            = projectId;
        this._product_detail_id     = productDetailId;
        this._path_image_crop       = pathImageCrop;
        this._path_image_original   = pathImageOriginal;
    }

    // getting ID
    public int getID() {
        return this._id;
    }

    // setting id
    public void setID(int id) {
        this._id = id;
    }

    public int getProjectId() {
        return this._project_id;
    }

    public void setProjectId(int projectId) {
        this._project_id = projectId;
    }

    public int getProductDetailId() {
        return this._product_detail_id;
    }

    public void setProductDetailId(int productDetailId) {
        this._product_detail_id = productDetailId;
    }

    public String getPathImageCrop() {
        return this._path_image_crop;
    }

    public void setPathImageCrop(String pathImageCrop) {
        this._path_image_crop = pathImageCrop;
    }

    public String getPathImageOriginal() {
        return this._path_image_original;
    }

    public void setPathImageOriginal(String pathImageOriginal) {
        this._path_image_original = pathImageOriginal;
    }
}
