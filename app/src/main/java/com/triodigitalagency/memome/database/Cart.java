package com.triodigitalagency.memome.database;

public class Cart {
    //private variables
    int _id;
    int _project_id;
    int _qty;

    // Empty constructor
    public Cart() { }

    // constructor
    public Cart(int id, int projectId, int qty) {
        this._id            = id;
        this._project_id    = projectId;
        this._qty           = qty;
    }

    // constructor
    public Cart(int projectId, int qty) {
        this._project_id    = projectId;
        this._qty           = qty;
    }

    // getting ID
    public int getID() {
        return this._id;
    }

    // setting id
    public void setID(int id) {
        this._id = id;
    }

    public int getProjectId() {
        return this._project_id;
    }

    public void setProjectId(int projectId) {
        this._project_id = projectId;
    }

    public int getQty() {
        return this._qty;
    }

    public void setQty(int qty) {
        this._qty = qty;
    }
}
