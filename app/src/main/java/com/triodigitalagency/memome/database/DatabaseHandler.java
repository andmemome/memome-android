package com.triodigitalagency.memome.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.triodigitalagency.memome.utils.Constant;

public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Projects table name
    private static final String TABLE_PROJECTS          = "projects";
    private static final String TABLE_PROJECT_DETAILS   = "project_details";
    private static final String TABLE_CARTS                 = "carts";

    // Projects Table Columns names
    private static final String KEY_PROJECT_ID                  = "id";
    private static final String KEY_PROJECT_PRODUCT_ID          = "product_id";
    private static final String KEY_PROJECT_REORDER_ID          = "reorder_id";
    private static final String KEY_PROJECT_TITLE               = "title";
    private static final String KEY_PROJECT_SUBTITLE            = "subtitle";
    private static final String KEY_PROJECT_PATH_IMAGE_CROP     = "path_image_crop";
    private static final String KEY_PROJECT_PATH_IMAGE_ORIGINAL = "path_image_original";
    private static final String KEY_PROJECT_CREATED_AT          = "created_at";
    private static final String KEY_PROJECT_CORPORATE_VOUCHER_ID    = "corporate_voucher_id";
    private static final String KEY_PROJECT_CORPORATE_VOUCHER_NAME  = "corporate_voucher_name";
    private static final String KEY_PROJECT_CORPORATE_VOUCHER_QTY   = "corporate_voucher_qty";
    private static final String KEY_PROJECT_CORPORATE_VOUCHER_CODE  = "corporate_voucher_code";

    // ProjectDetails Table Columns names
    private static final String KEY_PROJECT_DETAIL_ID                   = "id";
    private static final String KEY_PROJECT_DETAIL_PROJECT_ID           = "project_id";
    private static final String KEY_PROJECT_DETAIL_PRODUCT_DETAIL_ID    = "product_detail_id";
    private static final String KEY_PROJECT_DETAIL_PATH_IMAGE_CROP      = "path_image_crop";
    private static final String KEY_PROJECT_DETAIL_PATH_IMAGE_ORIGINAL  = "path_image_original";

    // Carts Table Columns names
    private static final String KEY_CART_ID                  = "id";
    private static final String KEY_CART_PROJECT_ID          = "cart_id";
    private static final String KEY_CART_QTY                 = "qty";

    public DatabaseHandler(Context context) {
        super(context, Constant.DATABASE_NAME, null, Constant.DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_PROJECTS_TABLE = "CREATE TABLE " + TABLE_PROJECTS + "("
                + KEY_PROJECT_ID + " INTEGER PRIMARY KEY," + KEY_PROJECT_PRODUCT_ID + " INTEGER,"
                + KEY_PROJECT_TITLE + " TEXT" + ","
                + KEY_PROJECT_SUBTITLE + " TEXT" + ","
                + KEY_PROJECT_PATH_IMAGE_CROP + " TEXT" + ","
                + KEY_PROJECT_PATH_IMAGE_ORIGINAL + " TEXT" + ","
                + KEY_PROJECT_CREATED_AT + " TEXT" + ","
                + KEY_PROJECT_CORPORATE_VOUCHER_ID + " INTEGER" + ","
                + KEY_PROJECT_CORPORATE_VOUCHER_NAME + " TEXT" + ","
                + KEY_PROJECT_CORPORATE_VOUCHER_QTY + " INTEGER" + ","
                + KEY_PROJECT_CORPORATE_VOUCHER_CODE + " TEXT" + ","
                + KEY_PROJECT_REORDER_ID + " INTEGER DEFAULT 0)";
        db.execSQL(CREATE_PROJECTS_TABLE);

        String CREATE_PROJECT_DETAILS_TABLE = "CREATE TABLE " + TABLE_PROJECT_DETAILS + "("
                + KEY_PROJECT_DETAIL_ID + " INTEGER PRIMARY KEY," + KEY_PROJECT_DETAIL_PROJECT_ID + " INTEGER,"
                + KEY_PROJECT_DETAIL_PRODUCT_DETAIL_ID + " INTEGER" + ","
                + KEY_PROJECT_DETAIL_PATH_IMAGE_CROP + " TEXT" + ","
                + KEY_PROJECT_DETAIL_PATH_IMAGE_ORIGINAL + " TEXT)";
        db.execSQL(CREATE_PROJECT_DETAILS_TABLE);

        String CREATE_CARTS_TABLE = "CREATE TABLE " + TABLE_CARTS + "("
                + KEY_CART_ID + " INTEGER PRIMARY KEY," + KEY_CART_PROJECT_ID + " INTEGER,"
                + KEY_CART_QTY + " INTEGER)";
        db.execSQL(CREATE_CARTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROJECTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROJECT_DETAILS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CARTS);

        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new project
    public void addProject(Project project) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PROJECT_PRODUCT_ID, project.getProductId());
        values.put(KEY_PROJECT_REORDER_ID, project.getReorderId());
        values.put(KEY_PROJECT_TITLE, project.getTitle());
        values.put(KEY_PROJECT_SUBTITLE, project.getSubtitle());
        values.put(KEY_PROJECT_PATH_IMAGE_CROP, project.getPathImageCrop());
        values.put(KEY_PROJECT_PATH_IMAGE_ORIGINAL, project.getPathImageOriginal());
        values.put(KEY_PROJECT_CREATED_AT, project.getCreatedAt());
        values.put(KEY_PROJECT_CORPORATE_VOUCHER_ID, project.getCorporateVoucherId());
        values.put(KEY_PROJECT_CORPORATE_VOUCHER_NAME, project.getCorporateVoucherName());
        values.put(KEY_PROJECT_CORPORATE_VOUCHER_QTY, project.getCorporateVoucherQty());
        values.put(KEY_PROJECT_CORPORATE_VOUCHER_CODE, project.getCorporateVoucherCode());

        // Inserting Row
        db.insert(TABLE_PROJECTS, null, values);
        db.close(); // Closing database connection
    }

    // Getting single project
    public Project getProject(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_PROJECTS, new String[] { KEY_PROJECT_ID,
                        KEY_PROJECT_PRODUCT_ID, KEY_PROJECT_TITLE, KEY_PROJECT_SUBTITLE, KEY_PROJECT_PATH_IMAGE_CROP, KEY_PROJECT_PATH_IMAGE_ORIGINAL, KEY_PROJECT_CREATED_AT, KEY_PROJECT_CORPORATE_VOUCHER_ID, KEY_PROJECT_CORPORATE_VOUCHER_NAME, KEY_PROJECT_CORPORATE_VOUCHER_QTY, KEY_PROJECT_CORPORATE_VOUCHER_CODE, KEY_PROJECT_REORDER_ID }, KEY_PROJECT_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Project project = new Project(Integer.parseInt(cursor.getString(0)),
                Integer.parseInt(cursor.getString(1)), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getInt(7), cursor.getString(8), cursor.getInt(9), cursor.getString(10), Integer.parseInt(cursor.getString(11)));

        db.close(); // Closing database connection
        // return project
        return project;
    }

    // Getting last insert id
    public int getLastInsertId() {
        SQLiteDatabase db   = this.getReadableDatabase();
        Cursor c            = db.rawQuery("SELECT " + KEY_PROJECT_ID + " from " + TABLE_PROJECTS + " order by " + KEY_PROJECT_ID + " DESC limit 1", null);

        if (c != null && c.moveToFirst()) {
            return (int)c.getLong(0); //The 0 is the column index, we only have 1 column, so the index is 0
        }

        db.close(); // Closing database connection
        return 0;
    }

    // Getting All Projects
    public List<Project> getAllProjects() {
        List<Project> projectList = new ArrayList<Project>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_PROJECTS + " ORDER BY " + KEY_PROJECT_ID + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Project project = new Project();
                project.setID(Integer.parseInt(cursor.getString(0)));
                project.setProductId(Integer.parseInt(cursor.getString(1)));
                project.setTitle(cursor.getString(2));
                project.setSubtitle(cursor.getString(3));
                project.setPathImageCrop(cursor.getString(4));
                project.setPathImageOriginal(cursor.getString(5));
                project.setCreatedAt(cursor.getString(6));
                project.setCorporateVoucherId(cursor.getInt(7));
                project.setCorporateVoucherName(cursor.getString(8));
                project.setCorporateVoucherQty(cursor.getInt(9));
                project.setCorporateVoucherCode(cursor.getString(10));
                if (cursor.isNull(11)) {
                    project.setReorderId(0);
                }
                else {
                    project.setReorderId(Integer.parseInt(cursor.getString(11)));
                }
                // Adding project to list
                projectList.add(project);
            } while (cursor.moveToNext());
        }

        db.close(); // Closing database connection
        // return project list
        return projectList;
    }

    // Updating single project
    public int updateProject(Project project) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PROJECT_PRODUCT_ID, project.getProductId());
        values.put(KEY_PROJECT_REORDER_ID, project.getReorderId());
        values.put(KEY_PROJECT_TITLE, project.getTitle());
        values.put(KEY_PROJECT_SUBTITLE, project.getSubtitle());
        values.put(KEY_PROJECT_PATH_IMAGE_CROP, project.getPathImageCrop());
        values.put(KEY_PROJECT_PATH_IMAGE_ORIGINAL, project.getPathImageOriginal());
        values.put(KEY_PROJECT_CREATED_AT, project.getCreatedAt());
        values.put(KEY_PROJECT_CORPORATE_VOUCHER_ID, project.getCorporateVoucherId());
        values.put(KEY_PROJECT_CORPORATE_VOUCHER_NAME, project.getCorporateVoucherName());
        values.put(KEY_PROJECT_CORPORATE_VOUCHER_QTY, project.getCorporateVoucherQty());
        values.put(KEY_PROJECT_CORPORATE_VOUCHER_CODE, project.getCorporateVoucherCode());

        // updating row
        int result = db.update(TABLE_PROJECTS, values, KEY_PROJECT_ID + " = ?",
                new String[] { String.valueOf(project.getID()) });
        db.close(); // Closing database connection
        return result;
    }

    // Deleting single project
    public void deleteProject(Project project) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_PROJECTS, KEY_PROJECT_ID + " = ?",
                new String[]{String.valueOf(project.getID())});
        db.delete(TABLE_PROJECT_DETAILS, KEY_PROJECT_DETAIL_PROJECT_ID + " = ?",
                new String[]{String.valueOf(project.getID())});
        db.delete(TABLE_CARTS, KEY_CART_PROJECT_ID + " = ?",
                new String[]{String.valueOf(project.getID())});
        db.close(); // Closing database connection
    }


    // Getting projects Count
    public int getProjectsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_PROJECTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        db.close(); // Closing database connection
        // return count
        return cursor.getCount();
    }

    // Adding new projectDetail
    public void addProjectDetail(ProjectDetail projectDetail) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PROJECT_DETAIL_PROJECT_ID, projectDetail.getProjectId());
        values.put(KEY_PROJECT_DETAIL_PRODUCT_DETAIL_ID, projectDetail.getProductDetailId());
        values.put(KEY_PROJECT_DETAIL_PATH_IMAGE_CROP, projectDetail.getPathImageCrop());
        values.put(KEY_PROJECT_DETAIL_PATH_IMAGE_ORIGINAL, projectDetail.getPathImageOriginal());

        // Inserting Row
        db.insert(TABLE_PROJECT_DETAILS, null, values);
        db.close(); // Closing database connection
    }

    // Getting single projectDetail
    public ProjectDetail getProjectDetail(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_PROJECT_DETAILS, new String[]{KEY_PROJECT_DETAIL_ID,
                        KEY_PROJECT_DETAIL_PROJECT_ID, KEY_PROJECT_DETAIL_PRODUCT_DETAIL_ID, KEY_PROJECT_DETAIL_PATH_IMAGE_CROP, KEY_PROJECT_DETAIL_PATH_IMAGE_ORIGINAL}, KEY_PROJECT_DETAIL_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        ProjectDetail projectDetail = new ProjectDetail(Integer.parseInt(cursor.getString(0)),
                Integer.parseInt(cursor.getString(1)), Integer.parseInt(cursor.getString(2)), cursor.getString(3), cursor.getString(4));

        db.close(); // Closing database connection
        // return projectDetail
        return projectDetail;
    }

    // Getting All ProjectDetails By Project ID
    public List<ProjectDetail> getAllProjectDetailsByProjectId(int projectId) {
        List<ProjectDetail> projectDetailList = new ArrayList<ProjectDetail>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_PROJECT_DETAILS + " WHERE " + KEY_PROJECT_DETAIL_PROJECT_ID + " = " + projectId;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ProjectDetail projectDetail = new ProjectDetail();
                projectDetail.setID(Integer.parseInt(cursor.getString(0)));
                projectDetail.setProjectId(Integer.parseInt(cursor.getString(1)));
                projectDetail.setProductDetailId(Integer.parseInt(cursor.getString(2)));
                projectDetail.setPathImageCrop(cursor.getString(3));
                projectDetail.setPathImageOriginal(cursor.getString(4));
                // Adding projectDetail to list
                projectDetailList.add(projectDetail);
            } while (cursor.moveToNext());
        }

        db.close(); // Closing database connection
        // return projectDetail list
        return projectDetailList;
    }

    // Getting All ProjectDetails
    public List<ProjectDetail> getAllProjectDetails() {
        List<ProjectDetail> projectDetailList = new ArrayList<ProjectDetail>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_PROJECT_DETAILS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ProjectDetail projectDetail = new ProjectDetail();
                projectDetail.setID(Integer.parseInt(cursor.getString(0)));
                projectDetail.setProjectId(Integer.parseInt(cursor.getString(1)));
                projectDetail.setProductDetailId(Integer.parseInt(cursor.getString(2)));
                projectDetail.setPathImageCrop(cursor.getString(3));
                projectDetail.setPathImageOriginal(cursor.getString(4));
                // Adding projectDetail to list
                projectDetailList.add(projectDetail);
            } while (cursor.moveToNext());
        }

        db.close(); // Closing database connection
        // return projectDetail list
        return projectDetailList;
    }

    // Updating single projectDetail
    public int updateProjectDetail(ProjectDetail projectDetail) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PROJECT_DETAIL_PROJECT_ID, projectDetail.getProjectId());
        values.put(KEY_PROJECT_DETAIL_PRODUCT_DETAIL_ID, projectDetail.getProductDetailId());
        values.put(KEY_PROJECT_DETAIL_PATH_IMAGE_CROP, projectDetail.getPathImageCrop());
        values.put(KEY_PROJECT_DETAIL_PATH_IMAGE_ORIGINAL, projectDetail.getPathImageOriginal());

        // updating row
        int result = db.update(TABLE_PROJECT_DETAILS, values, KEY_PROJECT_DETAIL_ID + " = ?",
                new String[] { String.valueOf(projectDetail.getID()) });
        db.close(); // Closing database connection
        return result;
    }

    // Deleting single projectDetail
    public void deleteProjectDetail(ProjectDetail projectDetail) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_PROJECT_DETAILS, KEY_PROJECT_DETAIL_ID + " = ?",
                new String[] { String.valueOf(projectDetail.getID()) });
        db.close(); // Closing database connection
    }


    // Getting projectDetails Count
    public int getProjectDetailsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_PROJECT_DETAILS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        db.close(); // Closing database connection
        // return count
        return cursor.getCount();
    }

    // Adding new cart
    public void addCart(Cart cart) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_CART_PROJECT_ID, cart.getProjectId());
        values.put(KEY_CART_QTY, cart.getQty());

        // Inserting Row
        db.insert(TABLE_CARTS, null, values);
        db.close(); // Closing database connection
    }

    // Getting single cart
    public Cart getCart(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_CARTS, new String[] { KEY_CART_ID,
                        KEY_CART_PROJECT_ID, KEY_CART_QTY }, KEY_CART_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Cart cart = new Cart(Integer.parseInt(cursor.getString(0)),
                Integer.parseInt(cursor.getString(1)), Integer.parseInt(cursor.getString(2)));

        db.close(); // Closing database connection
        // return cart
        return cart;
    }

    // Getting All Carts
    public List<Cart> getAllCarts() {
        List<Cart> cartList = new ArrayList<Cart>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CARTS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Cart cart = new Cart();
                cart.setID(Integer.parseInt(cursor.getString(0)));
                cart.setProjectId(Integer.parseInt(cursor.getString(1)));
                cart.setQty(Integer.parseInt(cursor.getString(2)));
                // Adding cart to list
                cartList.add(cart);
            } while (cursor.moveToNext());
        }

        db.close(); // Closing database connection
        // return cart list
        return cartList;
    }

    // Updating single cart
    public int updateCart(Cart cart) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_CART_PROJECT_ID, cart.getProjectId());
        values.put(KEY_CART_QTY, cart.getQty());

        int update = db.update(TABLE_CARTS, values, KEY_CART_ID + " = ?",
                new String[]{String.valueOf(cart.getID())});

        db.close(); // Closing database connection
        // updating row
        return update;
    }

    // Deleting single cart
    public void deleteCart(Cart cart) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CARTS, KEY_CART_ID + " = ?",
                new String[] { String.valueOf(cart.getID()) });
        db.close(); // Closing database connection
    }


    // Getting carts Count
    public int getCartsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_CARTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        db.close(); // Closing database connection
        // return count
        return cursor.getCount();
    }
}