package com.triodigitalagency.memome.webview;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.webkit.JavascriptInterface;

import com.triodigitalagency.memome.MainActivity;
import com.triodigitalagency.memome.utils.Globals;

public class WebAppInterface {
    Activity activity;

    /**
     * Instantiate the interface and set the context
     */
    WebAppInterface(Activity a) {
        activity    = a;
    }

    /**
     * Show a toast from the web page
     */
    @JavascriptInterface
    public void showToast(String toast) {
        Globals.showToast(toast, (Context)activity);
    }

    @JavascriptInterface
    public void closeWebView() {
        Intent intent = new Intent(this.activity, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.finish();
    }
}
