package com.triodigitalagency.memome.webview;

import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebView;

import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PrivacyPolice extends BaseActivity {

    @BindView(R.id.myWebview)WebView myWebview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_police);
        ButterKnife.bind(this);
        firstInit();
        setupActionBar(getResources().getString(R.string.privacy_and_policy));

        myWebview.loadUrl("file:///android_asset/privacypolice.html");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
