package com.triodigitalagency.memome.webview;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.triodigitalagency.memome.MainActivity;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.BaseActivity;

public class WebViewActivity extends BaseActivity {

    //<editor-fold desc="Variable">
    private Bundle extras;
    private String title;
    //</editor-fold>

    //<editor-fold desc="Override">
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        firstInit();

        extras  = getIntent().getExtras();
        title   = extras.getString("title");
        setupActionBar(title);
        showLoading();

        WebView webView         = (WebView)findViewById(R.id.webView);
        webView.addJavascriptInterface(new WebAppInterface(this), "Android");
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                findViewById(R.id.webView).setVisibility(View.VISIBLE);
                hideLoading();
            }
        });
        webView.loadUrl(extras.getString("url"));
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            back();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //Handle the back button
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            back();
            return true;
        }
        else {
            return super.onKeyDown(keyCode, event);
        }
    }
    //</editor-fold>

    //<editor-fold desc="Procedure & Function">
    protected void back() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
    //</editor-fold>
}
