package com.triodigitalagency.memome.gcm;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.app.AppController;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;

public class GcmIntentService extends IntentService {

    private static final String TAG = GcmIntentService.class.getSimpleName();

    public GcmIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        /*String key      = intent.getStringExtra(Constant.KEY);
        String topic    = intent.getStringExtra(Constant.TOPIC);
        switch (key) {
            case Constant.SUBSCRIBE:
                subscribeToTopic(topic);
                break;
            case Constant.UNSUBSCRIBE:
                unsubscribeFromTopic(topic);
                break;
            case Constant.KEY_REGISTER_GCM:
                // if key is specified, register with GCM
                registerGCM();
        }
*/
    }

    /**
     * Registering with GCM and obtaining the gcm registration id
     */
    private void registerGCM() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        try {
            InstanceID instanceID   = InstanceID.getInstance(this);
            String token            = instanceID.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

            // sending the registration id to our server
            sendRegistrationToServer(token);

            sharedPreferences.edit().putBoolean(Constant.SENT_TOKEN_TO_SERVER, true).apply();
        } catch (Exception e) {

            sharedPreferences.edit().putBoolean(Constant.SENT_TOKEN_TO_SERVER, false).apply();
        }

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Constant.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(final String token) {

        final Context context = this;

        // checking for valid login session
        if (Globals.getLongDataPreference(context, Constant.P_USER_ID) <= 0 || Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN).equalsIgnoreCase("")) {
            // TODO
            // user not found, redirecting him to login screen
            return;
        }

        StringRequest strReq = new StringRequest(Request.Method.POST, Globals.getApiUrl("users/updateGCMRegistrationId"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj      = new JSONObject(response);

                    // check for error
                    if (obj.getBoolean("error") == false) {
                        // broadcasting token sent to server
                        Intent registrationComplete = new Intent(Constant.SENT_TOKEN_TO_SERVER);
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(registrationComplete);
                    }
                    else {
                        Globals.showToast("Unable to send gcm registration id to our sever. " + obj.getJSONObject("error").getString("message"), context);
                    }

                } catch (JSONException e) {
                    Globals.showToast("Json parse error: " + e.getMessage(), context);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Globals.showToast("Volley error: " + error.getMessage(), context);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("gcmRegistrationId", token);
                params.put("clientId", Constant.CLIENT_ID);
                params.put("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
                return params;
            }
        };

        //Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }

    /**
     * Subscribe to a topic
     */
    public static void subscribeToTopic(String topic) {
        GcmPubSub pubSub        = GcmPubSub.getInstance(AppController.getInstance().getApplicationContext());
        InstanceID instanceID   = InstanceID.getInstance(AppController.getInstance().getApplicationContext());
        String token            = null;
        try {
            token = instanceID.getToken(AppController.getInstance().getApplicationContext().getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            if (token != null) {
                pubSub.subscribe(token, "/topics/" + topic, null);
            } else {
                //Log.e(TAG, "error: gcm registration id is null");
            }
        } catch (IOException e) {
            Globals.showToast("Topic subscribe error. Topic: " + topic + ", error: " + e.getMessage(), AppController.getInstance().getApplicationContext());
        }
    }

    public void unsubscribeFromTopic(String topic) {
        GcmPubSub pubSub        = GcmPubSub.getInstance(getApplicationContext());
        InstanceID instanceID   = InstanceID.getInstance(getApplicationContext());
        String token            = null;
        try {
            token = instanceID.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            if (token != null) {
                pubSub.unsubscribe(token, "");
            } else {
                //Log.e(TAG, "error: gcm registration id is null");
            }
        } catch (IOException e) {
            Globals.showToast("Topic subscribe error. Topic: " + topic + ", error: " + e.getMessage(), AppController.getInstance().getApplicationContext());
        }
    }

}
