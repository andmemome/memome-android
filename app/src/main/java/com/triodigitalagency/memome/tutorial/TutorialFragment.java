package com.triodigitalagency.memome.tutorial;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.triodigitalagency.memome.R;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * A simple {@link Fragment} subclass.
 */
public class TutorialFragment extends Fragment {

    private ImageView imageView;
    static JSONArray jsonArray = new JSONArray();

    protected ProgressBar progressBar;

    public static TutorialFragment getInstance(int position,JSONArray jsonArrays) {
        TutorialFragment myFragment = new TutorialFragment();
        Bundle args                 = new Bundle();
        jsonArray                   = jsonArrays;

        args.putInt("position", position);
        myFragment.setArguments(args);

        return myFragment;
    }

    protected void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    protected void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    protected boolean isLoading() {
        return progressBar.getVisibility() == View.VISIBLE;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View layout     = inflater.inflate(R.layout.fragment_tutorial, container, false);
        Log.e("test aplikasi ==> ","fragment_tutorial");

        imageView       = layout.findViewById(R.id.tutorialImage);
        progressBar     = layout.findViewById(R.id.ctrlActivityIndicator);
        Bundle bundle   = getArguments();

//        arrayOfImages   = getResources().obtainTypedArray(R.array.tutorial_images);

        if (bundle != null) {
            try {
                Log.e("==> ",jsonArray.optJSONObject(bundle.getInt("position")).getString("image"));
                showLoading();
                Picasso.with(getContext())
                        .load(jsonArray.optJSONObject(bundle.getInt("position")).getString("image"))
                        .fit()
                        .centerCrop()
                        .into(imageView, new Callback() {
                            @Override
                            public void onSuccess() {
                              hideLoading();
                            }

                            @Override
                            public void onError() {

                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
//            imageView.setImageResource(id);
        }
        return layout;
    }


}
