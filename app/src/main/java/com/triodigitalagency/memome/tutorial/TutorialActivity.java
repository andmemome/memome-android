package com.triodigitalagency.memome.tutorial;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.android.volley.VolleyError;
import com.triodigitalagency.memome.MainActivity;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.adapter.TutorialAdapter;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class TutorialActivity extends BaseActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        setLog("activity_tutorial");

        firstInit();
        setupActionBar("");
        loadData();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void loadData() {
        showLoading();
        Map<String, String> params = new HashMap<String, String>();
        params.put("clientId", Constant.CLIENT_ID);
        request.getJSONFromUrl(Globals.getApiUrl("products//howtouse"),
                params,
                getResources().getString(R.string.text_product_error),
                new RequestCallback() {
                    @Override
                    public void onResponse(JSONObject response) {

                        setLog("==? "+response);

                        try {
                            JSONArray steps = response.getJSONArray("steps");
                            setPager(steps);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(VolleyError error) {

                    }
                });
    }

    private void setPager(final JSONArray steps) {
        //Set the pager with an adapter
        ViewPager pager = findViewById(R.id.tutorialPager);
        pager.setAdapter(new TutorialAdapter(getSupportFragmentManager(), this,steps));
        hideLoading();
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                LinearLayout linearLayoutStartMyProject = (LinearLayout)findViewById(R.id.linearLayoutStartMyProject);
                linearLayoutStartMyProject.setVisibility(View.GONE);
                if (position == steps.length()-1) {
                    linearLayoutStartMyProject.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        //Bind the title indicator to the adapter
        CirclePageIndicator indicator = (CirclePageIndicator)findViewById(R.id.tutorialIndicator);
        indicator.setViewPager(pager);

        findViewById(R.id.buttonStartMyProject).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TutorialActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
    }
}
