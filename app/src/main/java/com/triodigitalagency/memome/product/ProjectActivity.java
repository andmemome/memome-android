package com.triodigitalagency.memome.product;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.triodigitalagency.memome.MainActivity;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.adapter.ProjectAdapter;
import com.triodigitalagency.memome.database.DatabaseHandler;
import com.triodigitalagency.memome.database.Project;
import com.triodigitalagency.memome.database.ProjectDetail;
import com.triodigitalagency.memome.transaction.CartActivity;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.utils.NumberTextWatcher;
import com.triodigitalagency.memome.volley.callback.RequestCallback;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProjectActivity extends BaseActivity {

    private RelativeLayout tapForReloadLayout;
    private LinearLayout linearLayoutCorporate;
    private Button buttonTapForReload;
    private List<Project> projects;
    private List<JSONObject> products;
    private ProjectAdapter projectAdapter;
    private TextView textTitle, textTotalDetail, textCreatedAt;
    private DatabaseHandler db;
    private int currentProjectPosition    = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.showCartIcon   = true;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project);
        setLog("activity_project");

        firstInit();
        setupActionBar(getResources().getString(R.string.title_project));

        textTitle               = (TextView)findViewById(R.id.textTitle);
        textTotalDetail         = (TextView)findViewById(R.id.textTotalDetail);
        textCreatedAt           = (TextView)findViewById(R.id.textCreatedAt);
        linearLayoutCorporate   = (LinearLayout)findViewById(R.id.linearLayoutCorporate);

        db                      = new DatabaseHandler(this);
        projects                = db.getAllProjects();

        findViewById(R.id.buttonAddToCart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentProjectPosition >= 0) {
                    Project p = projects.get(currentProjectPosition);
                    addToCart(p.getID());
                    startActivity(CartActivity.class);
                }
            }
        });

        findViewById(R.id.buttonView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentProjectPosition >= 0) {
                    final Project p = projects.get(currentProjectPosition);
                    startActivity(ProjectDetailActivity.class, new OnSetupIntentExtras() {
                        @Override
                        public void onSetupIntentExtras(Intent intent) {
                            intent.putExtra("projectId", p.getID());
                        }
                    });
                }
            }
        });

        findViewById(R.id.buttonDelete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(context)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(R.string.text_cart_delete_title)
                    .setMessage(R.string.text_cart_delete_message)
                    .setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            deleteProject();
                        }
                    })
                    .setNegativeButton(R.string.button_no, null)
                    .show();
            }
        });

        loadData();
        initTapForReload();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_default, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            back();
            return true;
        }
        else if (id == R.id.actionCart) {
            startActivity(CartActivity.class);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //Handle the back button
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            back();
            return true;
        }
        else {
            return super.onKeyDown(keyCode, event);
        }
    }
    //</editor-fold>

    //<editor-fold desc="Procedure & Function">
    protected void deleteProject() {
        if (currentProjectPosition >= 0) {
            Project project                     = projects.get(currentProjectPosition);
            List<ProjectDetail> projectDetails  = db.getAllProjectDetailsByProjectId(project.getID());

            for (ProjectDetail pd : projectDetails) {
                if (!pd.getPathImageCrop().equals(pd.getPathImageOriginal())) {
                    File file = new File(pd.getPathImageCrop());
                    if (file.exists()) {
                        file.delete();

                        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(pd.getPathImageCrop()))));
                    }
                }
            }

            if (!project.getPathImageCrop().equals(project.getPathImageOriginal())) {
                File file = new File(project.getPathImageCrop());
                if (file.exists()) {
                    file.delete();

                    context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(project.getPathImageCrop()))));
                }
            }
            db.deleteProject(project);

            Intent intent = new Intent(this, ProjectActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    protected void reDrawFragmentContent(int position) {
        currentProjectPosition      = position;
        if (position >= 0) {
            Project project             = projects.get(currentProjectPosition);
            List<ProjectDetail> details = db.getAllProjectDetailsByProjectId(projects.get(position).getID());

            if (project.getCorporateVoucherId() > 0) {
                linearLayoutCorporate.setVisibility(View.VISIBLE);
                TextView textCorporate  = (TextView)findViewById(R.id.textCorporate);
                textCorporate.setText(String.format(getResources().getString(R.string.text_corporate), project.getCorporateVoucherName(), project.getCorporateVoucherQty()));
            }
            else {
                linearLayoutCorporate.setVisibility(View.GONE);
            }

            try {
                for (int i = 0; i < products.size(); i++) {
                    JSONObject product = products.get(i);
                    if (product.getInt("id") == projects.get(position).getProductId()) {
                        textTotalDetail.setText(String.format(getResources().getString(R.string.text_total_detail), details.size() + (product.has("cover_content_width") && product.getLong("cover_content_width") > 0 ? 1 : 0)));
                        textCreatedAt.setText(String.format(getResources().getString(R.string.text_created_at), projects.get(position).getCreatedAt()));

                        if (product.has("title")) {
                            textTitle.setText((!projects.get(position).getTitle().equals("") ? "\"" + projects.get(position).getTitle() + "\" " : "") + product.getString("title"));
                        }

                        TextView textProductSize = (TextView)findViewById(R.id.textProductSize);
                        if (product.has("size")) {
                            textProductSize.setText(String.format(getResources().getString(R.string.text_product_size), product.getString("size")));
                        }

                        TextView textProductSpecialPrice   = (TextView)findViewById(R.id.textProductSpecialPrice);
                        if (product.has("special_price") && product.getLong("special_price") > 0) {
                            EditText tempEditText                       = new EditText(context);
                            tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
                            tempEditText.setText(String.valueOf(product.getLong("special_price")));
                            textProductSpecialPrice.setVisibility(View.VISIBLE);
                            textProductSpecialPrice.setText("@IDR " + tempEditText.getText());
                        }
                        else {
                            textProductSpecialPrice.setVisibility(View.GONE);
                        }

                        TextView textProductPrice           = (TextView)findViewById(R.id.textProductPrice);
                        if (product.has("price")) {
                            EditText tempEditText               = new EditText(context);
                            tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
                            tempEditText.setText(String.valueOf(product.getLong("price")));
                            textProductPrice.setText("@IDR " + tempEditText.getText());

                            if (product.has("special_price") && product.getLong("special_price") > 0) {
                                textProductPrice.setPaintFlags(textProductPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            }
                            else {
                                textProductPrice.setPaintFlags(textProductSize.getPaintFlags());
                            }
                        }

                        if (product.has("info")) {
                            TextView textProductInfo        = (TextView) findViewById(R.id.textProductInfo);
                            textProductInfo.setText(product.getString("info"));
                        }
                        break;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    protected void back() {
        Intent intent = new Intent(ProjectActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    protected void initTapForReload() {
        buttonTapForReload  = (Button)findViewById(R.id.buttonTapForReload);
        buttonTapForReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tapForReloadLayout.setVisibility(View.GONE);
                loadData();
            }
        });

        tapForReloadLayout  = (RelativeLayout)findViewById(R.id.tapForReload);
        tapForReloadLayout.setVisibility(View.GONE);
    }

    protected void loadData() {
        if (products == null) {
            products      = new ArrayList<JSONObject>();
        }
        else {
            products.clear();
        }

        if (projects.size() > 0) {
            String productIds   = "";
            for (int i = 0; i < projects.size(); i++) {
                productIds += projects.get(i).getProductId() + (i < projects.size() - 1 ? "," : "");
            }

            if (!productIds.equals("")) {
                loadDataProduct(productIds);
            }
        }
    }

    protected void loadDataProduct(final String productIds){
        if(!isLoading()){
            showLoading();
            Map<String, String> params = new HashMap<String, String>();
            params.put("clientId", Constant.CLIENT_ID);
            params.put("productIds", productIds);
            request.getJSONFromUrl(Globals.getApiUrl("products/searchByIds"),
                    params,
                    getResources().getString(R.string.text_product_error),
                    new RequestCallback() {
                        @Override
                        public void onResponse(JSONObject response) {
                            hideLoading();
                            setLog(response.toString());
                            try {
                                ViewPager pager         = (ViewPager)findViewById(R.id.projectPager);
                                JSONArray rProducts     = response.getJSONArray("products");

                                for (int i = 0; i < projects.size(); i++) {
                                    for (int j = 0; j < rProducts.length(); j++) {
                                        if (projects.get(i).getProductId() == rProducts.getJSONObject(j).getInt("id")) {
                                            rProducts.getJSONObject(j).put("project_id", projects.get(i).getID());
                                            rProducts.getJSONObject(j).put("cover_title", projects.get(i).getTitle());
                                            rProducts.getJSONObject(j).put("cover_subtitle", projects.get(i).getSubtitle());
                                            rProducts.getJSONObject(j).put("path_image_crop", projects.get(i).getPathImageCrop());
                                            products.add(new JSONObject(rProducts.getJSONObject(j).toString()));
                                            break;
                                        }
                                    }
                                }

                                projectAdapter          = new ProjectAdapter(getSupportFragmentManager(), activity, products);
                                pager.setAdapter(projectAdapter);
                                pager.addOnPageChangeListener(pagerOnPageChangeListener);

                                //Bind the title indicator to the adapter
                                CirclePageIndicator indicator = (CirclePageIndicator)findViewById(R.id.projectIndicator);
                                indicator.setViewPager(pager);

                                reDrawFragmentContent(0);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(VolleyError error) {
                            tapForReloadLayout.setVisibility(View.VISIBLE);
                            hideLoading();
                        }
                    });
        }
    }

    private void byPass() {
        if (currentProjectPosition >= 0) {
            Project p = projects.get(currentProjectPosition);
            addToCart(p.getID());
            startActivity(CartActivity.class);
        }
    }
    //</editor-fold>

    //<editor-fold desc="Listener">
    protected ViewPager.OnPageChangeListener pagerOnPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }

        @Override
        public void onPageSelected(int position) {
            reDrawFragmentContent(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) { }
    };
    //</editor-fold>
}
