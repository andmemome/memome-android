package com.triodigitalagency.memome.product;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RelativeLayout;

import com.android.volley.VolleyError;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.adapter.ProjectReviewArrayAdapter;
import com.triodigitalagency.memome.database.DatabaseHandler;
import com.triodigitalagency.memome.database.Project;
import com.triodigitalagency.memome.database.ProjectDetail;
import com.triodigitalagency.memome.transaction.CartActivity;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProjectDetailActivity extends BaseActivity {

    //<editor-fold desc="Variable">
    private Bundle extras;
    private int projectId, productId;
    private RelativeLayout tapForReloadLayout;
    private Button buttonTapForReload;
    private Project project;
    private List<ProjectDetail> projectDetails;
    private DatabaseHandler db;
    private ProjectReviewArrayAdapter projectReviewArrayAdapter;
    private ArrayList<JSONObject> dataSource;
    private String productCoverTitleActivity = "";
    //</editor-fold>

    //<editor-fold desc="Override">
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_detail);
        setLog("activity_project_detail");

        firstInit();
        setupActionBar(getResources().getString(R.string.title_project_preview));

        extras = getIntent().getExtras();
        if (extras != null){
            db              = new DatabaseHandler(this);
            projectId       = extras.getInt("projectId");
            project         = db.getProject(projectId);
            productId       = project.getProductId();
            projectDetails  = db.getAllProjectDetailsByProjectId(projectId);
        }
        else {
            finish();
        }

        findViewById(R.id.buttonAddToCart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!projectReviewArrayAdapter.isResGood()){

                }
                addToCart(project.getID());
                startActivity(CartActivity.class);
            }
        });

        dataSource                  = new ArrayList<JSONObject>();
        projectReviewArrayAdapter   = new ProjectReviewArrayAdapter(context);
        GridView gridview           = (GridView)findViewById(R.id.gridview);
        gridview.setAdapter(projectReviewArrayAdapter);

        loadData();
        initTapForReload();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_delete, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        else if (id == R.id.actionDelete) {
            new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.text_cart_delete_title)
                .setMessage(R.string.text_cart_delete_message)
                .setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteProject();
                    }

                })
                .setNegativeButton(R.string.button_no, null)
                .show();
            return true;
        }
        else if (id == R.id.actionEdit) {
            startActivity(ProductCoverActivity.class, new OnSetupIntentExtras() {
                @Override
                public void onSetupIntentExtras(Intent intent) {
                    intent.putExtra("productId", (long)productId);
                    intent.putExtra("projectId", projectId);
                    intent.putExtra("title", project.getTitle());
                    intent.putExtra("productCoverTitleActivity", productCoverTitleActivity);
                    intent.putExtra("subtitle", project.getSubtitle());
                    intent.putExtra("pathImageCrop", project.getPathImageCrop());
                    intent.putExtra("pathImageOriginal", project.getPathImageCrop());

                    ArrayList<Uri> selectedImagesPath = new ArrayList<>();
                    for (ProjectDetail pd: projectDetails) {
                        selectedImagesPath.add(Uri.parse(pd.getPathImageCrop()));
                    }

                    intent.putExtra("selectedImagesPath", selectedImagesPath);
                }
            });
        }

        return super.onOptionsItemSelected(item);
    }
    //</editor-fold>

    //<editor-fold desc="Procedure & Function">
    protected void deleteProject() {
        for (ProjectDetail pd : projectDetails) {
            if (!pd.getPathImageCrop().equals(pd.getPathImageOriginal())) {
                File file = new File(pd.getPathImageCrop());
                if (file.exists()) {
                    file.delete();

                    context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(pd.getPathImageCrop()))));
                }
            }
        }

        if (!project.getPathImageCrop().equals(project.getPathImageOriginal())) {
            File file = new File(project.getPathImageCrop());
            if (file.exists()) {
                file.delete();

                context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(project.getPathImageCrop()))));
            }
        }
        db.deleteProject(project);

        Intent intent = new Intent(this, ProjectActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    protected void initTapForReload() {
        buttonTapForReload  = (Button)findViewById(R.id.buttonTapForReload);
        buttonTapForReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tapForReloadLayout.setVisibility(View.GONE);
                loadData();
            }
        });

        tapForReloadLayout  = (RelativeLayout)findViewById(R.id.tapForReload);
        tapForReloadLayout.setVisibility(View.GONE);
    }

    protected void loadData() {
        if (!isLoading()) {
            showLoading();
            Map<String, String> params = new HashMap<String, String>();
            params.put("clientId", Constant.CLIENT_ID);
            params.put("productId", String.format("%d", productId));
            request.getJSONFromUrl(Globals.getApiUrl("products/view"),
                    params,
                    getResources().getString(R.string.text_product_error),
                    new RequestCallback() {
                        @Override
                        public void onResponse(JSONObject response) {
                            hideLoading();

                            try {
                                drawLayout(response.getJSONObject("product"), response.getJSONArray("details"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(VolleyError error) {
                            tapForReloadLayout.setVisibility(View.VISIBLE);
                            hideLoading();
                        }
                    });
        }
    }

    protected void drawLayout(JSONObject product, JSONArray details) {
        try {
            JSONObject tempJSONObject   = new JSONObject();
            if (dataSource == null) {
                dataSource              = new ArrayList<JSONObject>();
            }
            else {
                dataSource.clear();
            }

            if (product.has("custom_cover_title_activity")) {
                productCoverTitleActivity       = product.getString("custom_cover_title_activity");
            }

            tempJSONObject.put("id", product.getLong("id"));
            tempJSONObject.put("type", 0);
            tempJSONObject.put("path_image_original", project.getPathImageOriginal());
            tempJSONObject.put("path_image_crop", project.getPathImageCrop());
            tempJSONObject.put("width", product.getLong("cover_width"));
            tempJSONObject.put("height", product.getLong("cover_height"));
            tempJSONObject.put("content_width", product.getLong("cover_content_width"));
            tempJSONObject.put("content_height", product.getLong("cover_content_height"));
            tempJSONObject.put("border_left", product.getLong("cover_border_left"));
            tempJSONObject.put("border_top", product.getLong("cover_border_top"));
            tempJSONObject.put("title", project.getTitle());
            tempJSONObject.put("subtitle", project.getSubtitle());
            tempJSONObject.put("title_area_width", product.getLong("cover_title_area_width"));
            tempJSONObject.put("title_area_height", product.getLong("cover_title_area_height"));
            tempJSONObject.put("title_area_top", product.getLong("cover_title_area_top"));
            tempJSONObject.put("title_area_left", product.getLong("cover_title_area_left"));
            tempJSONObject.put("title_font_size", product.getLong("cover_title_font_size"));
            tempJSONObject.put("subtitle_font_size", product.getLong("cover_subtitle_font_size"));
            tempJSONObject.put("title_top", product.getLong("cover_title_top"));
            tempJSONObject.put("subtitle_top", product.getLong("cover_subtitle_top"));
            if (product.has("cover_images")  && !product.isNull("cover_images")) {
                tempJSONObject.put("images", product.getJSONObject("cover_images"));
            }


            dataSource.add(tempJSONObject);

            for (int i = 0; i < projectDetails.size(); i++) {
                tempJSONObject  = new JSONObject();
                tempJSONObject.put("id", details.getJSONObject(i).getLong("id"));
                tempJSONObject.put("type", 1);
                tempJSONObject.put("path_image_original", projectDetails.get(i).getPathImageOriginal());
                tempJSONObject.put("path_image_crop", projectDetails.get(i).getPathImageCrop());
                tempJSONObject.put("width", details.getJSONObject(i).getLong("width"));
                tempJSONObject.put("height", details.getJSONObject(i).getLong("height"));
                tempJSONObject.put("content_width", details.getJSONObject(i).getLong("content_width"));
                tempJSONObject.put("content_height", details.getJSONObject(i).getLong("content_height"));
                tempJSONObject.put("border_left", details.getJSONObject(i).getLong("border_left"));
                tempJSONObject.put("border_top", details.getJSONObject(i).getLong("border_top"));
                if (details.getJSONObject(i).has("images")  && !details.getJSONObject(i).isNull("images")) {
                    tempJSONObject.put("images", details.getJSONObject(i).getJSONObject("images"));
                }
                dataSource.add(tempJSONObject);
            }

            projectReviewArrayAdapter.setValues(dataSource);
            projectReviewArrayAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    //</editor-fold>
}
