package com.triodigitalagency.memome.product;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.instagrampicker.InstagramPhoto;
import android.instagrampicker.InstagramPhotoPicker;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageRequest;
import com.naver.android.helloyako.imagecrop.view.ImageCropView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.ExifUtil;
import com.triodigitalagency.memome.utils.Globals;

import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;


import static com.triodigitalagency.memome.utils.Globals.CLIENT_ID;
import static com.triodigitalagency.memome.utils.Globals.REDIRECT_URI;
import static com.triodigitalagency.memome.utils.Globals.REQUEST_CODE_INSTAGRAM_PICKER;

public class CropActivity extends BaseActivity {
    private final String TAG = CropActivity.class.getSimpleName();

    private Bundle extras;
    private ImageCropView imageCropView;
    private Integer ratioX, ratioY;
    private String pathImageOriginal, pathImageCrop = "";
    private final int SELECT_FILE = 1;
    private final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 2;
    private int rotate = 0;
    private boolean isResGood = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop);
        setLog("activity_crop");

        firstInit();
        setupActionBar(getResources().getString(R.string.title_product_crop));

        extras = getIntent().getExtras();
        if (extras != null) {
            pathImageOriginal   = extras.getString("pathImageOriginal");
            if (extras.get("pathImageCrop") != null && !extras.getString("pathImageCrop").equals("")) {
                pathImageCrop = extras.getString("pathImageCrop");
            }
            ratioX              = extras.getInt("ratioX");
            ratioY              = extras.getInt("ratioY");

            ratioY              = (int)ratioY / (ratioX < 100 ? 1 : 10);
            ratioX              = (int)ratioX / (ratioX < 100 ? 1 : 10);
        }
        else {
            finish();
        }

        imageCropView           = (ImageCropView)findViewById(R.id.imageCropView);
        if (pathImageOriginal != null && !pathImageOriginal.equals("")) {
            new ExifTask(pathImageOriginal, imageCropView).execute();
            imageCropView.setAspectRatio(ratioX, ratioY);
        }

        findViewById(R.id.buttonBrowse).setOnClickListener(buttonBrowseClicked);
        findViewById(R.id.buttonRotate).setOnClickListener(buttonRotateClicked);

        Button buttonCropSave       = (Button)findViewById(R.id.buttonCropSave);
        buttonCropSave.setOnClickListener(buttonCropSaveClicked);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == REQUEST_CODE_INSTAGRAM_PICKER){
            if (resultCode == Activity.RESULT_OK) {
                showLoading();
                InstagramPhoto[] instagramPhotos = InstagramPhotoPicker.getResultPhotos(data);
                Log.e("dbotha", "User selected " + instagramPhotos.length + " Instagram photos");
                Log.e("dbotha", "Photo: " + instagramPhotos[0].getFullURL());
                getPhotoFromUrl(instagramPhotos);
            }
        }else {
            if (resultCode == RESULT_OK && data != null) {

                if (requestCode == SELECT_FILE) {
                    setLog("Test Xiaomy");
                    Uri selectedImageUri    = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor           = getContentResolver().query(
                            selectedImageUri, filePathColumn,null, null, null);
                    cursor.moveToFirst();
                    int columnIndex         = cursor.getColumnIndex(filePathColumn[0]);
                    pathImageOriginal       = cursor.getString(columnIndex);
                    cursor.close();

                    new ExifTask(pathImageOriginal, imageCropView).execute();
                    imageCropView.setAspectRatio(ratioX, ratioY);
                }else {
                    setToast("failed get to data image");
                }
            }
        }
    }

    private void getPhotoFromUrl(final InstagramPhoto[] instagramPhotos) {

        if(isNetworkAvailable()){
            DownloadTask downloadTask = new DownloadTask();
            downloadTask.execute(instagramPhotos[0].getFullURL().toString());
        }else{
            Toast.makeText(getBaseContext(), "Network is not Available", Toast.LENGTH_SHORT).show();
        }
//        Picasso.with(CropActivity.this)
//                .load(instagramPhotos[0].getFullURL().toString())
//                .into(new Target() {
//                    @Override
//                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                        hideLoading();
//                        Uri selectedImageUri    = getImageUri(getApplicationContext(), bitmap);
//                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
//                        Cursor cursor           = getContentResolver().query(
//                                selectedImageUri, filePathColumn,null, null, null);
//                        cursor.moveToFirst();
//                        int columnIndex         = cursor.getColumnIndex(filePathColumn[0]);
//                        pathImageOriginal       = cursor.getString(columnIndex);
//                        cursor.close();
//
//                        new ExifTask(pathImageOriginal, imageCropView).execute();
//                        imageCropView.setAspectRatio(ratioX, ratioY);
//                    }
//
//                    @Override
//                    public void onBitmapFailed(Drawable errorDrawable) {
//                        getPhotoFromUrl(instagramPhotos);
//                    }
//
//                    @Override
//                    public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//                    }
//                });

    }

    private boolean isNetworkAvailable(){
        boolean available = false;
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if(networkInfo !=null && networkInfo.isAvailable())
            available = true;
        return available;
    }

    private Bitmap downloadUrl(String strUrl) throws IOException {
        Bitmap bitmap=null;
        InputStream iStream = null;
        try{
            URL url = new URL(strUrl);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();
            bitmap = BitmapFactory.decodeStream(iStream);

        }catch(Exception e){
            Log.e("Exception", e.toString());
        }finally{
            iStream.close();
        }
        return bitmap;
    }

    private class DownloadTask extends AsyncTask<String, Integer, Bitmap>{
        Bitmap bitmap = null;
        @Override
        protected Bitmap doInBackground(String... url) {
            try{
                bitmap = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {

            hideLoading();
            Uri selectedImageUri    = getImageUri(getApplicationContext(), bitmap);
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor           = getContentResolver().query(
                    selectedImageUri, filePathColumn,null, null, null);
            cursor.moveToFirst();
            int columnIndex         = cursor.getColumnIndex(filePathColumn[0]);
            pathImageOriginal       = cursor.getString(columnIndex);
            cursor.close();

            new ExifTask(pathImageOriginal, imageCropView).execute();
            imageCropView.setAspectRatio(ratioX, ratioY);

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // task you need to do.

                    getImage();
                }
                else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

        }
    }

    public File bitmapConvertToFile(Bitmap bitmap) {
        FileOutputStream fileOutputStream   = null;
        File bitmapFile                     = null;
        try {
            File file = new File(Environment.getExternalStoragePublicDirectory(getResources().getString(R.string.app_name)),"");
            if (!file.exists()) {
                file.mkdir();
            }

            bitmapFile          = new File(file, "IMG_" + (new SimpleDateFormat("yyyyMMddHHmmss")).format(Calendar.getInstance().getTime()) + ".jpg");
            fileOutputStream    = new FileOutputStream(bitmapFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);

            MediaScannerConnection.scanFile(this, new String[]{bitmapFile.getAbsolutePath()}, null, new MediaScannerConnection.MediaScannerConnectionClient() {
                @Override
                public void onMediaScannerConnected() { }

                @Override
                public void onScanCompleted(String path, Uri uri) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // alert file saved
                        }
                    });
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.flush();
                    fileOutputStream.close();
                } catch (Exception e) { }
            }
        }

        return bitmapFile;
    }

    protected void chooseImage() {

        Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(
                Intent.createChooser(intent, "Select File"),
                SELECT_FILE);
    }

    private void getImage(){
//        "Take Photo from instagram"
        final CharSequence[] options = {"Choose From Gallery","Cancel"};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity);
        builder.setTitle("Select Option");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                /*if (options[item].equals("Take Photo from instagram")) {
                    getIntagram();
                }*/ if (options[item].equals("Choose From Gallery")) {
                    chooseImage();
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void getIntagram() {
        InstagramPhotoPicker.startPhotoPickerForResult(this, CLIENT_ID, REDIRECT_URI, REQUEST_CODE_INSTAGRAM_PICKER,1,1);
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    protected View.OnClickListener buttonBrowseClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (ContextCompat.checkSelfPermission(CropActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(CropActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            }
            else {
                getImage();
            }
        }
    };

    protected View.OnClickListener buttonRotateClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (pathImageOriginal!= null && !pathImageOriginal.equals("")) {
                new RotateTask(pathImageOriginal, imageCropView).execute();
            }
            else {
                Globals.showToast(getResources().getString(R.string.text_image_empty), context);
            }
        }
    };

    protected View.OnClickListener buttonCropSaveClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(!imageCropView.isChangingScale() && pathImageOriginal!= null && !pathImageOriginal.equals("")) {
                if (pathImageCrop != null && !pathImageCrop.equals("")) {
                    File file           = new File(pathImageCrop);
                    if (file.exists()) {
                        file.delete();
                        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(pathImageCrop))));
                    }
                }


                Bitmap bitmap   = imageCropView.getCroppedImage();

                long fileSize=bitmap.getByteCount()/1024;//call function and convert bytes into Kb

                String value=fileSize+" Kb";

                Log.e(TAG, "file size before : " + value);

                if (fileSize>2048){
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG,40,stream);

                    byte[] BYTE = stream.toByteArray();

                    Bitmap bitmap2 = BitmapFactory.decodeByteArray(BYTE,0,BYTE.length);
                    fileSize = bitmap.getByteCount()/1024;
                    value = fileSize+" Kb";
                }


               /* while (fileSize > 2048){


*//*
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    byte[] byteArray = stream.toByteArray();
                    Bitmap compressedBitmap = BitmapFactory.decodeByteArray(byteArray,0,byteArray.length);
                   *//**//* fileSize = compressedBitmap.getByteCount()/1024;
                    value = fileSize+" Kb";*//**//*
//                    bitmap.compress(Bitmap.CompressFormat.JPEG,50,stream);
                   *//**//**//*
                    *//*bitmap = Globals.codec(bitmap, Bitmap.CompressFormat.JPEG, 99);
                    fileSize = bitmap.getByteCount()/1024;
                    value = fileSize+" Kb";*//*

                }*/

                Log.e(TAG, "file size after : " + value);

                File bitmapFile = bitmapConvertToFile(bitmap);
                pathImageCrop   = bitmapFile.getPath();
                Intent data = new Intent();
                data.putExtra("pathImageOriginal", pathImageOriginal);
                data.putExtra("pathImageCrop", pathImageCrop);
                setResult(RESULT_OK, data);
                finish();
            }
            else {
                Globals.showToast(getResources().getString(R.string.text_image_empty), context);
            }
        }
    };

    public class ExifTask extends AsyncTask<Void, Void, Bitmap> {
        private WeakReference<ImageCropView> imageCropView;
        private String pathImageOriginal;

        public ExifTask(String pathImageOriginal, ImageCropView imageCropView){
            this.pathImageOriginal  = pathImageOriginal;
            this.imageCropView      = new WeakReference<ImageCropView>(imageCropView);
        }

        @Override
        protected void onPreExecute() {
            if (!isLoading()) showLoading();
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            Bitmap myBitmap                 = Globals.decodeScaledBitmapFromFilePath(this.pathImageOriginal, 1000, 1000);
            return ExifUtil.rotateBitmap(this.pathImageOriginal, myBitmap);
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            imageCropView.get().setImageBitmap(result);
            try {
                int height = result.getHeight();
                int width = result.getWidth();
                Log.w(TAG, "onPostExecute height : " + height + "; width : " + width);
                if(height < Constant.MIN_PIXEL && width < Constant.MIN_PIXEL){
                    isResGood = true;
                }else{
                    isResGood = true;
                }
            }catch(Exception e){
                Log.e(TAG, "onPostExecute : " + e.toString());
            }

            hideLoading();
        }
    }

    public class RotateTask extends AsyncTask<Void, Void, Bitmap> {
        private WeakReference<ImageCropView> imageCropView;
        private String pathImageOriginal;

        public RotateTask(String pathImageOriginal, ImageCropView imageCropView){
            this.pathImageOriginal  = pathImageOriginal;
            this.imageCropView      = new WeakReference<ImageCropView>(imageCropView);
        }

        @Override
        protected void onPreExecute() {
            if (!isLoading()) showLoading();
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            Bitmap myBitmap     = Globals.decodeScaledBitmapFromFilePath(this.pathImageOriginal, 1000, 1000);
            Matrix matrix       = new Matrix();
            rotate              += 90;
            if (rotate > 270) {
                rotate          = 0;
            }
            matrix.postRotate(rotate);
            return Bitmap.createBitmap(myBitmap, 0, 0, myBitmap.getWidth(), myBitmap.getHeight(), matrix, true);
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            imageCropView.get().setImageBitmap(result);
            try{
                int height = result.getHeight();
                int width = result.getWidth();
                Log.w(TAG, "onPostExecute height : " + height + "; width : " + width);
                if(height < Constant.MIN_PIXEL && width < Constant.MIN_PIXEL){
                    isResGood = true;
                }else{
                    isResGood = true;
                }
            }catch(Exception e){
                Log.e(TAG, "onPostExecute : " + e.toString());
            }

            hideLoading();

        }
    }
}
