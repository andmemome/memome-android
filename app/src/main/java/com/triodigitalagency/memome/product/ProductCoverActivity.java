package com.triodigitalagency.memome.product;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.instagrampicker.InstagramPhoto;
import android.instagrampicker.InstagramPhotoPicker;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.sangcomz.fishbun.FishBun;
import com.sangcomz.fishbun.adapter.image.impl.PicassoAdapter;
import com.sangcomz.fishbun.define.Define;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.adapter.RecyclerAdapterProdukInprogress;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.triodigitalagency.memome.utils.Globals.CLIENT_ID;
import static com.triodigitalagency.memome.utils.Globals.REDIRECT_URI;
import static com.triodigitalagency.memome.utils.Globals.REQUEST_CODE_INSTAGRAM_PICKER;

public class ProductCoverActivity extends BaseActivity {

    private final String TAG = ProductCoverActivity.class.getSimpleName();

    private JSONObject product;
    private Bundle extras;
    private long productId, corporateVoucherId;
    private RelativeLayout tapForReloadLayout;
    private Button buttonContinue, buttonTapForReload;
    private ImageView imageViewContent;
    private EditText title, subtitle, tempTitle, tempSubtitle;
    private long productCoverWidth, productCoverHeight;
    private Boolean isRequireTitle = true, isRequireCoverImage = true;
    private String pathImageOriginal = "", pathImageCrop = "", corporateVoucherName, corporateVoucherCode, productCoverTitleActivity = "";
    private final int BROWSE = 1;
    private int countDetail = 0, projectId = -1, corporateVoucherQty;
    private ArrayList<Uri> selectedImagesPath = new ArrayList<>();
    private LinearLayout parentWarning;
    private TextView txtWarning;
    private boolean isChanged = false;
    private boolean isResGood = true;
    public JSONObject jsonProduck = new JSONObject();

    @BindView(R.id.buttonChooseCoverImage)TextView choseImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_cover);
        setLog("activity_product_cover");
        ButterKnife.bind(this);
//        choseImage.setText("nsvdijmk".toUpperCase());

        firstInit();

        parentWarning      = findViewById(R.id.parentWarning);
        txtWarning             =  findViewById(R.id.txtWarning);

        buttonContinue          = findViewById(R.id.buttonContinue);
        buttonContinue.setOnClickListener(buttonContinueProjectClicked);
        imageViewContent        = findViewById(R.id.imageViewProductCoverContent);
//        imageViewContent.setOnClickListener(imageViewContentClicked);

        title                   = findViewById(R.id.editTextTitle);
        subtitle                = findViewById(R.id.editTextSubtitle);
        tempTitle               = findViewById(R.id.editTextTempTitle);
        tempSubtitle            = findViewById(R.id.editTextTempSubtitle);

        tempTitle.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                title.setText(tempTitle.getText());
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        tempSubtitle.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                subtitle.setText(tempSubtitle.getText());
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        extras = getIntent().getExtras();
        if (extras != null) {
            title.setText(extras.getString("title"));
            subtitle.setText(extras.getString("subtitle"));
            productId                   = extras.getLong("productId");
            projectId                   = extras.getInt("projectId", -1);
            pathImageCrop               = extras.getString("pathImageCrop", "");
            pathImageOriginal           = extras.getString("pathImageOriginal", "");
            corporateVoucherId          = extras.getLong("corporateVoucherId", -1);
            corporateVoucherName        = extras.getString("corporateVoucherName", "");
            corporateVoucherQty         = extras.getInt("corporateVoucherQty", -1);
            corporateVoucherCode        = extras.getString("corporateVoucherCode", "");
            productCoverTitleActivity   = extras.getString("productCoverTitleActivity", "");

            if (extras.get("selectedImagesPath") != null) {
                selectedImagesPath = extras.getParcelableArrayList("selectedImagesPath");
            }

            title.setText(extras.getString("title", ""));
            tempTitle.setText(extras.getString("title", ""));
            subtitle.setText(extras.getString("subtitle", ""));
            tempSubtitle.setText(extras.getString("subtitle", ""));

            if (!pathImageCrop.equals("")) {
//                Glide.with(context)
//                        .load(pathImageCrop)
//                        .placeholder(R.mipmap.loading_img)
//                        .into(imageViewContent);

                Picasso.with(context)
                        .load(pathImageCrop)
                        .placeholder(R.mipmap.loading_img)
                        .into(imageViewContent);
//

                try{
                    Bitmap bitmap = BitmapFactory.decodeFile(pathImageCrop);
                    int height = bitmap.getHeight();
                    int width = bitmap.getWidth();
                    Log.w(TAG, "height : " + height + "; width : " + width);
                    if(height < Constant.MIN_PIXEL && width < Constant.MIN_PIXEL){
                        this.isResGood = true;
                        parentWarning.setVisibility(View.GONE);
                        txtWarning.setText(getResources().getString(R.string.warn_low_res));
                    }else{
                        this.isResGood = true;
                        parentWarning.setVisibility(View.GONE);

                        //get file size
                        String value;
                        File file = new File(pathImageCrop);
                        long Filesize=Globals.getFolderOrFileSize(file)/1024;//call function and convert bytes into Kb

                        value=Filesize+" Kb";

                        Log.w(TAG, "file size : " + value);

                    }
                }catch(Exception e){
                    Log.e(TAG, "onCreate : " + e.toString());
                }
            }
        }
        else {
            finish();
        }

        setupActionBar(!productCoverTitleActivity.equals("") ? productCoverTitleActivity : getResources().getString(R.string.title_product_crop));



        initTapForReload();
        loadDataProduct();

    }



    public void choseCover(View view) {
        startActivityForResult(CropActivity.class, BROWSE, new OnSetupIntentExtras() {
            @Override
            public void onSetupIntentExtras(Intent intent) {
                intent.putExtra("pathImageCrop", pathImageCrop);
                intent.putExtra("pathImageOriginal", pathImageOriginal);

                int ratioX  = (int)productCoverWidth;
                int ratioY  = (int)productCoverHeight;
                intent.putExtra("ratioX", ratioX);
                intent.putExtra("ratioY", ratioY);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            back();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //Handle the back button
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            back();
            return true;
        }
        else {
            return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == BROWSE) {
                Bundle extra        = data.getExtras();
                pathImageCrop       = extra.getString("pathImageCrop");
                pathImageOriginal   = extra.getString("pathImageOriginal");
//
                Glide.with(context)
                    .load(pathImageCrop)
                    .placeholder(R.mipmap.loading_img)
                    .into(imageViewContent);

//                Picasso.with(context)
//                        .load(pathImageCrop)
//                        .placeholder(R.mipmap.loading_img)
//                        .into(imageViewContent);

                isChanged           = true;

                try{
                    Bitmap bitmap = BitmapFactory.decodeFile(pathImageCrop);
                    int height = bitmap.getHeight();
                    int width = bitmap.getWidth();
                    Log.w(TAG, "height : " + height + "; width : " + width);
                    if(height < Constant.MIN_PIXEL && width < Constant.MIN_PIXEL){
                        this.isResGood = true;
                        parentWarning.setVisibility(View.GONE);
                        txtWarning.setText(getResources().getString(R.string.warn_low_res));
                    }else{
                        this.isResGood = true;
                        parentWarning.setVisibility(View.GONE);

                        //get file size
                        String value;
                        File file = new File(pathImageCrop);
                        long Filesize=Globals.getFolderOrFileSize(file)/1024;//call function and convert bytes into Kb
//                        if(Filesize>=1024)
//                            value=Filesize/1024+" Mb";
//                        else
                            value=Filesize+" Kb";

                        Log.w(TAG, "file size : " + value);
                    }
                }catch(Exception e){
                    Log.e(TAG, "onActivityResult : " + e.toString());
                }
            }


            else if (requestCode == Define.ALBUM_REQUEST_CODE) {
                try {
                    if (!"1".equals(product.getString("category_id"))||!"5".equals(product.getString("category_id"))) {
                        selectedImagesPath = data.getParcelableArrayListExtra(Define.INTENT_PATH);
                        gotoReview();
                        finish();
                    }else {
                        selectedImagesPath = data.getParcelableArrayListExtra(Define.INTENT_PATH);
                        gotoReview();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }

        if (requestCode == REQUEST_CODE_INSTAGRAM_PICKER){
            if (resultCode == Activity.RESULT_OK) {
                showLoading();
                final InstagramPhoto[] instagramPhotos = InstagramPhotoPicker.getResultPhotos(data);
                Log.e("dbotha", "User selected " + instagramPhotos.length + " Instagram photos");
                for (int i = 0; i < instagramPhotos.length; ++i) {
                    Log.e("dbotha", "Photo: " + instagramPhotos[i].getFullURL());
                    if(isNetworkAvailable()){
                        DownloadTask downloadTask = new DownloadTask();
                        downloadTask.execute(instagramPhotos[i].getFullURL().toString());
                    }else{
                        Toast.makeText(getBaseContext(), "Network is not Available", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        }

    }

    private boolean isNetworkAvailable(){
        boolean available = false;
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if(networkInfo !=null && networkInfo.isAvailable())
            available = true;
        return available;
    }

    private Bitmap downloadUrl(String strUrl) throws IOException {
        Bitmap bitmap=null;
        InputStream iStream = null;
        try{
            URL url = new URL(strUrl);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();
            bitmap = BitmapFactory.decodeStream(iStream);

        }catch(Exception e){
            Log.e("Exception", e.toString());
        }finally{
            iStream.close();
        }
        return bitmap;
    }

    private class DownloadTask extends AsyncTask<String, Integer, Bitmap> {
        Bitmap bitmap = null;
        @Override
        protected Bitmap doInBackground(String... url) {
            try{
                bitmap = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            Uri tempUri = getImageUri(getApplicationContext(), bitmap);
            try {
                if (!"1".equals(product.getString("category_id"))||!"5".equals(product.getString("category_id"))) {
                    selectedImagesPath.add(tempUri);
                    gotoReview(selectedImagesPath.size());
                }else {
                    selectedImagesPath.add(tempUri);
                    gotoReview(selectedImagesPath.size());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    //</editor-fold>

    //<editor-fold desc="Listener">
    protected View.OnClickListener buttonContinueProjectClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if(!isResGood){

            }

            boolean isGood = true;
            isGood  = validateVariableStringRequired(isGood);

            if (isGood && projectId <= 0) {
                if ("10".equals(jsonProduck.optString("category_id"))||"20".equals(jsonProduck.optString("id"))||"21".equals(jsonProduck.optString("id"))||"22".equals(jsonProduck.optString("id"))||"23".equals(jsonProduck.optString("id"))||"24".equals(jsonProduck.optString("id"))||"25".equals(jsonProduck.optString("id"))){
                    selectedImagesPath.add(Uri.parse(""));
                    gotoReview();
                }else {
                    getChoiseImage();
                }
            }
            else if (isGood && projectId > 0) {

                startActivity(ProductReviewActivity.class, new OnSetupIntentExtras() {
                    @Override
                    public void onSetupIntentExtras(Intent intent) {
                        intent.putExtra("productId", productId);
                        intent.putExtra("projectId", projectId);
                        intent.putExtra("title", title.getText().toString());
                        intent.putExtra("subtitle", subtitle.getText().toString());
                        intent.putExtra("pathImageCrop", pathImageCrop);
                        intent.putExtra("pathImageOriginal", pathImageOriginal);
                        intent.putExtra("selectedImagesPath", selectedImagesPath);
                    }
                });
            }
        }
    };

    private void getChoiseImage() {
        final CharSequence[] options = {"Choose From Gallery","Cancel"};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity);
        builder.setTitle("Select Option");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                /*if (options[item].equals("Take Photo from instagram")) {
                    getIntagram();
                } else*/ if (options[item].equals("Choose From Gallery")) {
                    getFishBun();
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    }

    private void getIntagram() {

        int minCount = product.optInt("min_image");
        int maxCount = product.optInt("max_photo");
        if (minCount == 0) {
            minCount = maxCount;
        }
        setLog("Tsttttt "+maxCount);
        InstagramPhotoPicker.startPhotoPickerForResult(this, CLIENT_ID, REDIRECT_URI, REQUEST_CODE_INSTAGRAM_PICKER,product.optInt("min_image"),maxCount);
    }


    private void gotoReview() {
        startActivity(ProductReviewActivity.class, new OnSetupIntentExtras() {
            @Override
            public void onSetupIntentExtras(Intent intent) {
                intent.putExtra("productId", productId);
                intent.putExtra("projectId", projectId);
                intent.putExtra("title",title.getText().toString());
                intent.putExtra("subtitle", subtitle.getText().toString());
                intent.putExtra("pathImageCrop", pathImageCrop);
                intent.putExtra("pathImageOriginal", pathImageOriginal);
                intent.putExtra("selectedImagesPath", selectedImagesPath);
                intent.putExtra("corporateVoucherId", corporateVoucherId);
                intent.putExtra("corporateVoucherName", corporateVoucherName);
                intent.putExtra("corporateVoucherQty", corporateVoucherQty);
                intent.putExtra("corporateVoucherCode", corporateVoucherCode);
            }
        });
    }
    int completed = 10000;

    private void gotoReview(final int total) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (total==product.optInt("min_image")){
                    hideLoading();
                    startActivity(ProductReviewActivity.class, new OnSetupIntentExtras() {
                        @Override
                        public void onSetupIntentExtras(Intent intent) {
                            intent.putExtra("productId", productId);
                            intent.putExtra("projectId", projectId);
                            intent.putExtra("title", title.getText().toString());
                            intent.putExtra("subtitle", subtitle.getText().toString());
                            intent.putExtra("pathImageCrop", pathImageCrop);
                            intent.putExtra("pathImageOriginal", pathImageOriginal);
                            intent.putExtra("selectedImagesPath", selectedImagesPath);
                            intent.putExtra("corporateVoucherId", corporateVoucherId);
                            intent.putExtra("corporateVoucherName", corporateVoucherName);
                            intent.putExtra("corporateVoucherQty", corporateVoucherQty);
                            intent.putExtra("corporateVoucherCode", corporateVoucherCode);
                        }
                    });
                }else {
                    gotoReview(total);
                    setLog("show loading");
                    setLog("total "+total+" "+product.optInt("min_image"));
                }

            }
        }, completed);
    }


    private void getFishBun() {
        int minCount = product.optInt("min_image");
        if (minCount == 0) {
            minCount = countDetail;
        }

        FishBun.with(ProductCoverActivity.this)
                .setImageAdapter(new PicassoAdapter())
                .setMinCount(minCount)
                .setMaxCount(countDetail)
                .exceptGif(true)
                .setSelectedImages(selectedImagesPath)
                .setActionBarColor(Color.parseColor("#ffffff"), Color.parseColor("#40A99C"), true)
                .setActionBarTitleColor(Color.parseColor("#000000"))
                .setHomeAsUpIndicatorDrawable(ContextCompat.getDrawable(ProductCoverActivity.this, R.drawable.toolbar_navigation_back_button))
                .setOkButtonDrawable(ContextCompat.getDrawable(ProductCoverActivity.this, R.drawable.baseline_check_black_24))
                .startAlbum();
    }
/*
    protected View.OnClickListener imageViewContentClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };*/
    //</editor-fold>

    //<editor-fold desc="Procedure & Function">
    protected void loadDataProduct() {
        if(!isLoading()){
            showLoading();
            Map<String, String> params = new HashMap<String, String>();
            params.put("clientId", Constant.CLIENT_ID);
            params.put("productId", String.format("%d", productId));
            request.getJSONFromUrl(Globals.getApiUrl("products/view"),
                    params,
                    getResources().getString(R.string.text_product_error),
                    new RequestCallback() {
                        @Override
                        public void onResponse(JSONObject response) {
                            setLog("Tester "+response);

                            try {
                                countDetail = response.getJSONArray("details").length();
                                jsonProduck = response.getJSONObject("product");
                                drawProductData(jsonProduck);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(VolleyError error) {
                            tapForReloadLayout.setVisibility(View.VISIBLE);
                            hideLoading();
                        }
                    });
        }
    }

    protected void drawProductData(JSONObject data) {
        setLog("Test data api "+data.optInt("max_image"));
        setLog(""+data.toString());
        product = data;
        try {
            if (product.getInt("is_require_title") == 0) {
                isRequireTitle = false;
                tempTitle.setHint(getResources().getString(R.string.hint_title) + " (Optional)");
                tempSubtitle.setHint(getResources().getString(R.string.hint_subtitle) + " (Optional)");
            }

            if (product.getLong("cover_content_width") == 0 && product.getLong("cover_content_height") == 0) {
                isRequireCoverImage = false;
            }

            if ("20".equals(product.optString("id"))||"21".equals(product.optString("id"))||"22".equals(product.optString("id"))||"23".equals(product.optString("id"))||"24".equals(product.optString("id"))||"25".equals(product.optString("id"))) {
                if ("9".equals(product.getString("id"))){

                }else {
                    setupActionBar(product.optString("title"));
                    choseImage.setText("choose image".toUpperCase());
                }
            }

        } catch (JSONException e) {}
        prepareImageViewProductCover();
    }

    protected void initTapForReload() {
        buttonTapForReload  = (Button)findViewById(R.id.buttonTapForReload);
        buttonTapForReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tapForReloadLayout.setVisibility(View.GONE);
                loadDataProduct();
            }
        });

        tapForReloadLayout  = (RelativeLayout)findViewById(R.id.tapForReload);
        tapForReloadLayout.setVisibility(View.GONE);
    }

    protected void back() {
        finish();
        /*new AlertDialog.Builder(context)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setTitle(R.string.text_back_title)
            .setMessage(R.string.text_back_message)
            .setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if ((projectId <= 0 && pathImageCrop != null && !pathImageCrop.equals("")) || (projectId > 0 && isChanged)) {
                        File file           = new File(pathImageCrop);
                        if (file.exists()) {
                            file.delete();

                            context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(pathImageCrop))));
                        }
                    }
                    finish();
                }
            })
            .setNegativeButton(R.string.button_no, null)
            .show();*/
    }

    public void prepareImageViewProductCover() {
        try {
            productCoverWidth                           = product.getLong("cover_width");
            productCoverHeight                          = product.getLong("cover_height");
            long productCoverContentWidth               = product.getLong("cover_content_width");
            long productCoverContentHeight              = product.getLong("cover_content_height");
            long productCoverBorderLeft                 = product.getLong("cover_border_left");
            long productCoverBorderTop                  = product.getLong("cover_border_top");
            long productCoverTitleAreaWidth             = product.getLong("cover_title_area_width");
            long productCoverTitleAreaHeight            = product.getLong("cover_title_area_height");
            long productCoverTitleAreaTop               = product.getLong("cover_title_area_top");
            long productCoverTitleAreaLeft              = product.getLong("cover_title_area_left");
            long productCoverTitleFontSize              = product.getLong("cover_title_font_size");
            long productCoverSubtitleFontSize           = product.getLong("cover_subtitle_font_size");
            long productCoverTitleTop                   = product.getLong("cover_title_top");
            long productCoverSubtitleTop                = product.getLong("cover_subtitle_top");

            ImageView imageView                         = findViewById(R.id.imageViewProductCover);
            RelativeLayout relativeLayoutTitleArea      = findViewById(R.id.relativeLayoutTitleArea);
            DisplayMetrics displayMetrics               = context.getResources().getDisplayMetrics();
            final float dpWidth                         = displayMetrics.widthPixels / displayMetrics.density;
            final float dpHeight                        = displayMetrics.heightPixels / displayMetrics.density;
            imageView.getLayoutParams().width           = (int) Globals.convertDpToPixel(dpWidth - 22, context); // -22 for padding outside

            Globals.drawProductWithTitleLayout(context, productCoverWidth, productCoverHeight, imageView, imageViewContent, productCoverContentWidth,
                    productCoverContentHeight, productCoverBorderLeft, productCoverBorderTop,
                    relativeLayoutTitleArea, productCoverTitleAreaWidth, productCoverTitleAreaHeight, productCoverTitleAreaTop, productCoverTitleAreaLeft,
                    productCoverTitleFontSize, productCoverSubtitleFontSize, productCoverTitleTop, productCoverSubtitleTop, title, subtitle);

            Typeface font = Typeface.createFromAsset(activity.getAssets(), "fonts/trajan.ttf");
            Typeface fontSub = Typeface.createFromAsset(activity.getAssets(),"fonts/trajansmall.ttf");
            title.setTypeface(font);
            subtitle.setTypeface(fontSub);

            if (product.has("cover_images")  && !product.isNull("cover_images")) {
                JSONObject images   = product.getJSONObject("cover_images");

                Glide.with(context)
                        .load(images.getString("thumb"))
                        .placeholder(R.mipmap.loading_img)
                        .into(imageView);
//                Picasso.with(context)
//                        .load(images.getString("thumb"))
//                        .placeholder(R.mipmap.loading_img)
//                        .fit()
//                        .into(imageView);

            }

            if (productCoverContentWidth > 0) {
                findViewById(R.id.buttonChooseCoverImage).setVisibility(View.VISIBLE);
            }

            if (productCoverTitleFontSize > 0) {
                tempTitle.setVisibility(View.GONE);
                findViewById(R.id.viewTempTitle).setVisibility(View.GONE);
            }

            if (productCoverSubtitleFontSize > 0) {
                tempSubtitle.setVisibility(View.GONE);
                findViewById(R.id.viewTempSubtitle).setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        //

        hideLoading();
    }


    protected Boolean validateEditTextStringRequired(Boolean isGood) {
        if (isGood && isRequireTitle) {
            EditText[] requiredFields       = new EditText[]{tempTitle, tempSubtitle};
            String[] requiredFieldNames     = new String[]{
                    getResources().getString(R.string.hint_title),
                    getResources().getString(R.string.hint_subtitle)
            };

            int i = 0;
            for (EditText field : requiredFields) {
                if (field.getText().toString().trim().equals("")) {
                    field.requestFocus();
                    Globals.showAlert(
                            getResources().getString(R.string.text_product_error),
                            String.format(getResources().getString(R.string.text_add_error_required), requiredFieldNames[i]),
                            activity
                    );
                    isGood = false;
                    break;
                }
                i++;
            }
        }
        return isGood;
    }

    protected Boolean validateVariableStringRequired(Boolean isGood) {
        if (isGood && isRequireCoverImage) {
            String[] requiredValues         = new String[]{pathImageCrop};
            String[] requiredFieldNames     = new String[]{
                    getResources().getString(R.string.hint_product_cover),
            };

            int i = 0;
            for (String field : requiredValues) {
                if (field.equals("")) {
                    Globals.showAlert(
                            getResources().getString(R.string.text_product_error),
                            String.format(getResources().getString(R.string.text_add_error_required), requiredFieldNames[i]),
                            activity
                    );
                    isGood = false;
                    break;
                }
                i++;
            }
        }
        return isGood;
    }
    //</editor-fold>

}
