package com.triodigitalagency.memome.product;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.BaseActivity;

import java.io.File;

public class ThetitleAvtivity extends BaseActivity {

    private long productId, corporateVoucherId;
    private int countDetail = 0, projectId = -1, corporateVoucherQty;
    private String pathImageOriginal = "", pathImageCrop = "", corporateVoucherName, corporateVoucherCode, productCoverTitleActivity = "";
    private boolean isChanged = false;
    private Bundle extras;
    EditText titleProduct,titleSubProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thetitle_avtivity);

        titleProduct = (EditText)findViewById(R.id.eTtitleProduct);
        titleSubProduct = (EditText)findViewById(R.id.eTSubtitleProduct);

        Typeface type = Typeface.createFromAsset(getAssets(), "fonts/loraregular.ttf");
        titleProduct.setTypeface(type);
        titleSubProduct.setTypeface(type);

        setupActionBar(!productCoverTitleActivity.equals("") ? productCoverTitleActivity : getResources().getString(R.string.title_product_title));

        extras = getIntent().getExtras();
        if (extras != null) {
            projectId                   = extras.getInt("projectId", -1);
            pathImageCrop               = extras.getString("pathImageCrop", "");
            pathImageOriginal           = extras.getString("pathImageOriginal", "");
            corporateVoucherName        = extras.getString("corporateVoucherName", "");
            corporateVoucherQty         = extras.getInt("corporateVoucherQty", -1);
            corporateVoucherCode        = extras.getString("corporateVoucherCode", "");
            productCoverTitleActivity   = extras.getString("productCoverTitleActivity", "");

        }
        else {
            finish();
        }

        productId = extras.getLong("productId");
        corporateVoucherId = extras.getLong("corporateVoucherId");


//                titleSubProduct
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            back();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void back() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.text_back_title)
                .setMessage(R.string.text_back_message)
                .setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if ((projectId <= 0 && pathImageCrop != null && !pathImageCrop.equals("")) || (projectId > 0 && isChanged)) {
                            File file           = new File(pathImageCrop);
                            if (file.exists()) {
                                file.delete();

                                context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(pathImageCrop))));
                            }
                        }
                        finish();
                    }

                })
                .setNegativeButton(R.string.button_no, null)
                .show();

        /*
        try {
            new AlertDialog.Builder(context)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(R.string.text_back_title)
                    .setMessage(R.string.text_back_message)
                    .setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if ((projectId <= 0 && pathImageCrop != null && !pathImageCrop.equals("")) || (projectId > 0 && isChanged)) {
                                File file           = new File(pathImageCrop);
                                if (file.exists()) {
                                    file.delete();

                                    context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(pathImageCrop))));
                                }
                            }
                            finish();
                        }

                    })
                    .setNegativeButton(R.string.button_no, null)
                    .show();
        }catch (Exception e){
            startActivity(new Intent(this,ProductViewActivity.class));
        }*/
    }

    public void saveListener(View view){
        startActivity(ProductCoverActivity.class, new OnSetupIntentExtras() {
            @Override
            public void onSetupIntentExtras(Intent intent) {
                if (!titleProduct.getText().toString().isEmpty()){
                    intent.putExtra("productId", productId);
                    intent.putExtra("productCoverTitleActivity", productCoverTitleActivity);
                    intent.putExtra("corporateVoucherId", corporateVoucherId);
                    intent.putExtra("corporateVoucherName", corporateVoucherName);
                    intent.putExtra("corporateVoucherQty", corporateVoucherQty);
                    intent.putExtra("corporateVoucherCode", corporateVoucherCode);
                    intent.putExtra("title",titleProduct.getText().toString());
                    intent.putExtra("subtitle",titleSubProduct.getText().toString());
                }else {
                    setToast("Please insert the title");
                }
            }
        });
       /* if (!titleProduct.getText().toString().isEmpty()&&!titleSubProduct.getText().toString().isEmpty()){

        }else {
            setToast("Please input title And Subtitle");
        }*/

    }
}
