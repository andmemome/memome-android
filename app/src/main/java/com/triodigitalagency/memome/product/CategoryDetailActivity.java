package com.triodigitalagency.memome.product;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.instagrampicker.InstagramPhoto;
import android.instagrampicker.InstagramPhotoPicker;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.sangcomz.fishbun.FishBun;
import com.sangcomz.fishbun.adapter.image.impl.PicassoAdapter;
import com.sangcomz.fishbun.define.Define;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.adapter.ProductArrayAdapterr;
import com.triodigitalagency.memome.transaction.CartActivity;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.triodigitalagency.memome.utils.Globals.CLIENT_ID;
import static com.triodigitalagency.memome.utils.Globals.REDIRECT_URI;
import static com.triodigitalagency.memome.utils.Globals.REQUEST_CODE_INSTAGRAM_PICKER;

public class CategoryDetailActivity extends BaseActivity {

    private Bundle extras;
    private long categoryId, corporateVoucherId;
    private String pathImageOriginal = "", pathImageCrop = "",categoryTitle, corporateVoucherName, corporateVoucherCode, productCoverTitleActivity = "";
    private int corporateVoucherQty;
    private ProductArrayAdapterr productArrayAdapter;
    private RecyclerView productListView;
    private ArrayList<JSONObject> productDataSource;
    private Boolean isLastProduct;
    private long lastProductIndex, limitProduct;
    private RelativeLayout tapForReloadLayout;
    private Button buttonTapForReload;
    JSONObject product = new JSONObject();
    int projectId = -1,positiion;
    private ArrayList<Uri> selectedImagesPath = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_detail);
        setLog("activity_category_detail");

        firstInit();

        extras = getIntent().getExtras();
        if (extras != null){
            categoryId              = extras.getLong("categoryId");
            categoryTitle           = extras.getString("categoryTitle");
            corporateVoucherId      = extras.getLong("corporateVoucherId", -1);
            corporateVoucherName    = extras.getString("corporateVoucherName", "");
            corporateVoucherQty     = extras.getInt("corporateVoucherQty", -1);
            corporateVoucherCode    = extras.getString("corporateVoucherCode", "");
        }
        else {
            finish();
        }

        setupActionBar(categoryTitle);

        isLastProduct               = false;
        lastProductIndex            = 0;
        limitProduct                = Constant.SEARCH_LIMIT;
        productDataSource           = new ArrayList<JSONObject>();
        productListView             = findViewById(R.id.listView);
        productArrayAdapter         = new ProductArrayAdapterr(context);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        productListView.setLayoutManager(mLayoutManager);
        productListView.setItemAnimator(new DefaultItemAnimator());
        productListView.setNestedScrollingEnabled(false);
        productListView.setAdapter(productArrayAdapter);

//        productListView.setAdapter(categoryArrayAdapter);
//        productListView.setOnItemClickListener(onProductClickListener);
        productArrayAdapter.setOnClickAdapter(onClickAdapter);
        productListView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
//                loadDataProduct();
            }
        });
        /*productListView.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                super.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
            }

            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                loadDataProduct();
            }
        });
*/
//        if (isLogin()) {
//            loadDataOrdersNotYetPaid();
//        }

        initTapForReload();
        resetAndLoadDataProduct();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_default, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }
        else if (id == R.id.actionCart) {
            startActivity(CartActivity.class);
        }

        return super.onOptionsItemSelected(item);
    }

    protected ProductArrayAdapterr.onClickAdapter onClickAdapter =  new ProductArrayAdapterr.onClickAdapter() {
        @Override
        public void onClick(int holder, final int position) {
            positiion = position;
            if (holder==R.id.btn_check||holder==-1){
                if (position >= 0 && position <= productArrayAdapter.getCount()) {
//                startActivity(ProductViewActivity.class, new BaseActivity.OnSetupIntentExtras() {
//                    @Override
//                    public void onSetupIntentExtras(Intent intent) {
//                        intent.putExtra("productId", productArrayAdapter.getItemId(position));
//                    }
//                });


                    product = (JSONObject) productArrayAdapter.getItem(position);
                    Log.d("PRODUCT ===>", "onItemClick: "+product.toString());
                    if (product.optInt("is_require_title") == 0) {
                        try {
                            if ("9".equals(product.getString("category_id"))||"1".equals(product.getString("category_id"))||"5".equals(product.getString("category_id"))||"10".equals(product.getString("category_id"))||
                                    "0".equals(product.getString("min_image"))||"0".equals(product.getString("max_image"))) {
                                if ("9".equals(product.getString("id"))){
                                    getImage();
                                }else {
                                    startActivity(ProductCoverActivity.class, new OnSetupIntentExtras() {
                                        @Override
                                        public void onSetupIntentExtras(Intent intent) {
                                            intent.putExtra("productId", productArrayAdapter.getItemId(position));
                                            intent.putExtra("productCoverTitleActivity", productCoverTitleActivity);
                                            intent.putExtra("corporateVoucherId", corporateVoucherId);
                                            intent.putExtra("corporateVoucherName", corporateVoucherName);
                                            intent.putExtra("corporateVoucherQty", corporateVoucherQty);
                                            intent.putExtra("corporateVoucherCode", corporateVoucherCode);
                                            intent.putExtra("title","");
                                            intent.putExtra("subtitle","");
                                            intent.putExtra("product",product.toString());
                                        }
                                    });
                                }
                            }else{
                               getImage();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                    else {
                        startActivity(ThetitleAvtivity.class, new OnSetupIntentExtras() {
                            @Override
                            public void onSetupIntentExtras(Intent intent) {
                                intent.putExtra("productId", productArrayAdapter.getItemId(position));
                                intent.putExtra("productCoverTitleActivity", product.optString("custom_cover_title_activity"));
                                intent.putExtra("corporateVoucherId", corporateVoucherId);
                                intent.putExtra("corporateVoucherName", corporateVoucherName);
                                intent.putExtra("corporateVoucherQty", corporateVoucherQty);
                                intent.putExtra("corporateVoucherCode", corporateVoucherCode);
                                intent.putExtra("product",product.toString());
                            }
                        });
                    }
                }
            }
            Log.d("PRODUCT", "onItemClick: "+position);

        }
    };

//    protected AdapterView.OnItemClickListener onProductClickListener = new AdapterView.OnItemClickListener() {
//        @Override
//        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
//            positiion = position;
//            Log.d("PRODUCT", "onItemClick: "+position);
//            if (position >= 0 && position <= productArrayAdapter.getCount()) {
////                startActivity(ProductViewActivity.class, new BaseActivity.OnSetupIntentExtras() {
////                    @Override
////                    public void onSetupIntentExtras(Intent intent) {
////                        intent.putExtra("productId", productArrayAdapter.getItemId(position));
////                    }
////                });
//
//
//                product = (JSONObject) productArrayAdapter.getItem(position);
//                Log.d("PRODUCT ===>", "onItemClick: "+product.toString());
//                if (product.optInt("is_require_title") == 0) {
//
//                    try {
//                        if ("1".equals(product.getString("category_id"))||"5".equals(product.getString("category_id"))||"10".equals(product.getString("category_id"))) {
//                            startActivity(ProductCoverActivity.class, new OnSetupIntentExtras() {
//                                @Override
//                                public void onSetupIntentExtras(Intent intent) {
//                                    intent.putExtra("productId", productArrayAdapter.getItemId(position));
//                                    intent.putExtra("productCoverTitleActivity", productCoverTitleActivity);
//                                    intent.putExtra("corporateVoucherId", corporateVoucherId);
//                                    intent.putExtra("corporateVoucherName", corporateVoucherName);
//                                    intent.putExtra("corporateVoucherQty", corporateVoucherQty);
//                                    intent.putExtra("corporateVoucherCode", corporateVoucherCode);
//                                    intent.putExtra("title","");
//                                    intent.putExtra("subtitle","");
//                                    intent.putExtra("product",product.toString());
//                                }
//                            });
//                        }else{
//                            getFishBun();
//                            setLog("----------- ------------"+product.toString());
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
//
//                }
//                else {
//                    startActivity(ThetitleAvtivity.class, new OnSetupIntentExtras() {
//                        @Override
//                        public void onSetupIntentExtras(Intent intent) {
//                            intent.putExtra("productId", productArrayAdapter.getItemId(position));
//                            intent.putExtra("productCoverTitleActivity", product.optString("custom_cover_title_activity"));
//                            intent.putExtra("corporateVoucherId", corporateVoucherId);
//                            intent.putExtra("corporateVoucherName", corporateVoucherName);
//                            intent.putExtra("corporateVoucherQty", corporateVoucherQty);
//                            intent.putExtra("corporateVoucherCode", corporateVoucherCode);
//                            intent.putExtra("product",product.toString());
//                        }
//                    });
//                }
//            }
//        }
//    };

    private void getImage(){
        final CharSequence[] options = {"Choose From Gallery","Cancel"};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity);
        builder.setTitle("Select Option");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                /*if (options[item].equals("Take Photo from instagram")) {
                    getIntagram();
                } else */if (options[item].equals("Choose From Gallery")) {
                    getFishBun();
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void getIntagram() {
        int minCount = product.optInt("min_image");
        int maxCount = product.optInt("max_image");
        if (minCount == 0) {
            minCount = maxCount;
        }
        InstagramPhotoPicker.startPhotoPickerForResult(this, CLIENT_ID, REDIRECT_URI, REQUEST_CODE_INSTAGRAM_PICKER,product.optInt("min_image"),maxCount);
    }


    private void getFishBun() {

//        selectedImagesPath.add(Uri.parse("gambar-kosong"));
        int minCount = product.optInt("min_image");
        if (minCount == 0) {
            minCount = product.optInt("max_image");
        }

        FishBun.with(this)
                .setImageAdapter(new PicassoAdapter())
                .setMinCount(minCount)
                .setMaxCount(product.optInt("max_image"))
                .exceptGif(true)
                .setSelectedImages(selectedImagesPath)
                .setActionBarColor(Color.parseColor("#ffffff"), Color.parseColor("#40A99C"), true)
                .setActionBarTitleColor(Color.parseColor("#000000"))
                .setHomeAsUpIndicatorDrawable(ContextCompat.getDrawable(this, R.drawable.toolbar_navigation_back_button))
                .setOkButtonDrawable(ContextCompat.getDrawable(this, R.drawable.baseline_check_black_24))
                .startAlbum();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            if (requestCode == Define.ALBUM_REQUEST_CODE) {
                try {
                    if (!"1".equals(product.getString("category_id"))||!"5".equals(product.getString("category_id"))) {
                        selectedImagesPath = data.getParcelableArrayListExtra(Define.INTENT_PATH);
                        productReview();
                        finish();
                    }else {
                        selectedImagesPath = data.getParcelableArrayListExtra(Define.INTENT_PATH);
                        productReview();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        if (requestCode == REQUEST_CODE_INSTAGRAM_PICKER){
            if (resultCode == Activity.RESULT_OK) {
                showLoading();
                InstagramPhoto[] instagramPhotos = InstagramPhotoPicker.getResultPhotos(data);
                Log.e("dbotha", "User selected " + instagramPhotos.length + " Instagram photos");
                for (int i = 0; i < instagramPhotos.length; ++i) {
                    Log.e("dbotha", "Photo: " + instagramPhotos[i].getFullURL());
                    if(isNetworkAvailable()){
                        DownloadTask downloadTask = new DownloadTask();
                        downloadTask.execute(instagramPhotos[i].getFullURL().toString());
                    }else{
                        Toast.makeText(getBaseContext(), "Network is not Available", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        }

    }

    private boolean isNetworkAvailable(){
        boolean available = false;
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if(networkInfo !=null && networkInfo.isAvailable())
            available = true;
        return available;
    }

    private Bitmap downloadUrl(String strUrl) throws IOException {
        Bitmap bitmap=null;
        InputStream iStream = null;
        try{
            URL url = new URL(strUrl);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();
            bitmap = BitmapFactory.decodeStream(iStream);

        }catch(Exception e){
            Log.e("Exception", e.toString());
        }finally{
            iStream.close();
        }
        return bitmap;
    }

    private class DownloadTask extends AsyncTask<String, Integer, Bitmap> {
        Bitmap bitmap = null;
        @Override
        protected Bitmap doInBackground(String... url) {
            try{
                bitmap = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            Uri tempUri = getImageUri(getApplicationContext(), bitmap);
            try {
                if (!"1".equals(product.getString("category_id"))||!"5".equals(product.getString("category_id"))) {
                    selectedImagesPath.add(tempUri);
                    productReview(selectedImagesPath.size());
//                    finish();
                }else {
                    selectedImagesPath.add(tempUri);
                    productReview(selectedImagesPath.size());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    int completed = 5000;


    void productReview(final int total){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (total==product.optInt("min_image")){
                    hideLoading();
                    startActivity(ProductReviewActivity.class, new OnSetupIntentExtras() {
                        @Override
                        public void onSetupIntentExtras(Intent intent) {
                            intent.putExtra("productId", productArrayAdapter.getItemId(positiion));
                            intent.putExtra("projectId", projectId);
                            intent.putExtra("title", "");
                            intent.putExtra("subtitle", "");
                            intent.putExtra("pathImageCrop", pathImageCrop);
                            intent.putExtra("pathImageOriginal", pathImageOriginal);
                            intent.putExtra("selectedImagesPath", selectedImagesPath);
                            intent.putExtra("corporateVoucherId", corporateVoucherId);
                            intent.putExtra("corporateVoucherName", corporateVoucherName);
                            intent.putExtra("corporateVoucherQty", corporateVoucherQty);
                            intent.putExtra("corporateVoucherCode", corporateVoucherCode);
                        }
                    });
                }else {
                    productReview(total);
                }

            }
        }, completed);
    }

    void productReview(){
        startActivity(ProductReviewActivity.class, new OnSetupIntentExtras() {
            @Override
            public void onSetupIntentExtras(Intent intent) {
                intent.putExtra("productId", productArrayAdapter.getItemId(positiion));
                intent.putExtra("projectId", projectId);
                intent.putExtra("title", "");
                intent.putExtra("subtitle", "");
                intent.putExtra("pathImageCrop", pathImageCrop);
                intent.putExtra("pathImageOriginal", pathImageOriginal);
                intent.putExtra("selectedImagesPath", selectedImagesPath);
                intent.putExtra("corporateVoucherId", corporateVoucherId);
                intent.putExtra("corporateVoucherName", corporateVoucherName);
                intent.putExtra("corporateVoucherQty", corporateVoucherQty);
                intent.putExtra("corporateVoucherCode", corporateVoucherCode);
            }
        });
    }

    protected void resetAndLoadDataProduct() {
        isLastProduct      = false;
        lastProductIndex   = 0;
        if (productDataSource == null) {
            productDataSource = new ArrayList<JSONObject>();
        }
        else {
            productDataSource.clear();
        }
        productArrayAdapter.setValues(productDataSource);
        productArrayAdapter.notifyDataSetChanged();
        loadDataProduct(true);
    }

    protected void loadDataProduct() {
        this.loadDataProduct(false);
    }

    protected void loadDataProduct(boolean isForce) {
        if ((!isLoading() || isForce) && !isLastProduct) {
            showLoading();

            Map<String, String> params = new HashMap<String, String>();
            params.put("clientId", Constant.CLIENT_ID);
            params.put("limit", String.format("%d", limitProduct));
            params.put("start", String.format("%d", lastProductIndex));
            params.put("categoryId", String.valueOf(categoryId));

            request.getJSONFromUrl(Globals.getApiUrl("products/searchByCategory"), params, getResources().getString(R.string.text_product_error), new RequestCallback() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        setLog("-- "+response);
                        JSONArray products = response.getJSONArray("products");

                        lastProductIndex += products.length();
                        if (products.length() < limitProduct) {
                            isLastProduct = true;
                        }

                        for (int i = 0; i < products.length(); i++) {
                            productDataSource.add(products.getJSONObject(i));
                        }

                        productArrayAdapter.setValues(productDataSource);
                        productArrayAdapter.notifyDataSetChanged();


//                        JSONArray banners = response.getJSONArray("banners");
//                        List<String> tempImagePaths = new ArrayList<String>();
//
//                        for (int i = 0; i < banners.length(); i++) {
//                            if (banners.getJSONObject(i).has("images") && !banners.getJSONObject(i).isNull("images")) {
//                                JSONObject images = banners.getJSONObject(i).getJSONObject("images");
//                                tempImagePaths.add(images.getString("original"));
//                            }
//                        }
//
//                        if(tempImagePaths.size() > 0) {
//                            if(listHeaderView == null){
//                                LayoutInflater inflater         = activity.getLayoutInflater();
//                                listHeaderView                  = (RelativeLayout)inflater.inflate(R.layout.layout_product_banner, null);
//                                if(productListView.getAdapter() == null) {
//                                    productListView.addHeaderView(listHeaderView);
//
//                                    productListView.setAdapter(categoryArrayAdapter);
//                                }
//                            }
//
//                            ViewPager pager = (ViewPager) listHeaderView.findViewById(R.id.viewPagerProductBanner);
//                            pager.setAdapter(new ImageViewAdapter(getSupportFragmentManager(), MainActivity.this, tempImagePaths));
//
//                            CirclePageIndicator indicator = (CirclePageIndicator) listHeaderView.findViewById(R.id.viewIndicatorProductBanner);
//                            indicator.setViewPager(pager);
//                        }
//                        else
                        if(productListView.getAdapter() == null) {
                            productListView.setAdapter(productArrayAdapter);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    hideLoading();
                }

                @Override
                public void onError(VolleyError error) {
                    tapForReloadLayout.setVisibility(View.VISIBLE);
                    hideLoading();
                }
            });
        }
    }

    protected void initTapForReload() {
        buttonTapForReload  = (Button)findViewById(R.id.buttonTapForReload);
        buttonTapForReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tapForReloadLayout.setVisibility(View.GONE);
                resetAndLoadDataProduct();
            }
        });

        tapForReloadLayout  = (RelativeLayout)findViewById(R.id.tapForReload);
        tapForReloadLayout.setVisibility(View.GONE);
    }
}
