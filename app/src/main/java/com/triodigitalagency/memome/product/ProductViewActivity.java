package com.triodigitalagency.memome.product;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.adapter.ImageViewAdapter;
import com.triodigitalagency.memome.transaction.CartActivity;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.utils.NumberTextWatcher;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductViewActivity extends BaseActivity {

    private Bundle extras;
    private long productId, corporateVoucherId;
    private String productTitle, corporateVoucherName, corporateVoucherCode, productCoverTitleActivity = "";
    private RelativeLayout tapForReloadLayout;
    private TextView titleMenu;
    private Button buttonMakeProject, buttonTapForReload;
    private int corporateVoucherQty;
    private LinearLayout linearLayoutCorporate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.showCartIcon   = true;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_vieww);
        setLog("activity_product_view");

        titleMenu = (TextView)findViewById(R.id.titleMenu);

        firstInit();

        extras = getIntent().getExtras();
        if (extras != null){
            productId               = extras.getLong("productId");
            corporateVoucherId      = extras.getLong("corporateVoucherId", -1);
            corporateVoucherName    = extras.getString("corporateVoucherName", "");
            corporateVoucherQty     = extras.getInt("corporateVoucherQty", -1);
            corporateVoucherCode    = extras.getString("corporateVoucherCode", "");
        }
        else {
            finish();
        }

        linearLayoutCorporate   = (LinearLayout)findViewById(R.id.linearLayoutCorporate);
        buttonMakeProject       = (Button)findViewById(R.id.buttonMakeProject);
        buttonMakeProject.setOnClickListener(buttonMakeProjectClicked);

        initTapForReload();
        loadDataProduct();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_default, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        else if (id == R.id.actionCart) {
            startActivity(CartActivity.class);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();

        // make slider height same with width deviceTake Photo from instagram"
        findViewById(R.id.viewPager).getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override public void onGlobalLayout() {
                View squareView                 = findViewById(R.id.viewPager);
                ViewGroup.LayoutParams layout   = squareView.getLayoutParams();
                layout.height                   = squareView.getWidth() - 55;
                squareView.setLayoutParams(layout);
                squareView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }
    //</editor-fold>

    //<editor-fold desc="Listener">
    protected View.OnClickListener buttonMakeProjectClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(ThetitleAvtivity.class, new OnSetupIntentExtras() {
                @Override
                public void onSetupIntentExtras(Intent intent) {
                    intent.putExtra("productId", productId);
                    intent.putExtra("productCoverTitleActivity", productCoverTitleActivity);
                    intent.putExtra("corporateVoucherId", corporateVoucherId);
                    intent.putExtra("corporateVoucherName", corporateVoucherName);
                    intent.putExtra("corporateVoucherQty", corporateVoucherQty);
                    intent.putExtra("corporateVoucherCode", corporateVoucherCode);
                }
            });
        }
    };
    //</editor-fold>

    //<editor-fold desc="Procedure & Function">
    protected void loadDataProduct(){
        if(!isLoading()){
            showLoading();
            Map<String, String> params = new HashMap<String, String>();
            params.put("clientId", Constant.CLIENT_ID);
            params.put("productId", String.format("%d", productId));
            request.getJSONFromUrl(Globals.getApiUrl("products/view"),
                    params,
                    getResources().getString(R.string.text_product_error),
                    new RequestCallback() {
                        @Override
                        public void onResponse(JSONObject response) {
                            hideLoading();

                            try {
                                drawProductData(response.getJSONObject("product"), response.getJSONArray("details"));
                                setupActionBar(productTitle.toUpperCase());
                                titleMenu.setText(productTitle);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(VolleyError error) {
                            tapForReloadLayout.setVisibility(View.VISIBLE);
                            hideLoading();
                        }
                    });
        }
    }

    protected void drawProductData(JSONObject product, JSONArray details){
        if (corporateVoucherId > 0) {
            linearLayoutCorporate.setVisibility(View.VISIBLE);
            TextView textCorporate  = (TextView)findViewById(R.id.textCorporate);
            textCorporate.setText(String.format(getResources().getString(R.string.text_corporate), corporateVoucherName, corporateVoucherQty));
        }
        else {
            linearLayoutCorporate.setVisibility(View.GONE);
        }

        try {
            if (product.has("title")) {
                productTitle = product.getString("title");
            }

            TextView textProductSize = (TextView)findViewById(R.id.textProductSize);
            if (product.has("size")) {
                textProductSize.setText(String.format(getResources().getString(R.string.text_product_size), product.getString("size")));
            }

            TextView textProductSpecialPrice   = (TextView)findViewById(R.id.textProductSpecialPrice);
            if (product.has("special_price") && product.getLong("special_price") > 0) {
                EditText tempEditText                       = new EditText(context);
                tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
                tempEditText.setText(String.valueOf(product.getLong("special_price")));
                textProductSpecialPrice.setVisibility(View.VISIBLE);
                textProductSpecialPrice.setText("Rp " + tempEditText.getText());
            }
            else {
                textProductSpecialPrice.setVisibility(View.GONE);
            }

            TextView textProductPrice           = (TextView)findViewById(R.id.textProductPrice);
            if (product.has("price")) {
                EditText tempEditText               = new EditText(context);
                tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
                tempEditText.setText(String.valueOf(product.getLong("price")));
                textProductPrice.setText("Rp " + tempEditText.getText());

                if (product.has("special_price") && product.getLong("special_price") > 0) {
                    textProductPrice.setPaintFlags(textProductPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                }
                else {
                    textProductPrice.setPaintFlags(textProductSize.getPaintFlags());
                }
            }

            TextView textProductTotalPhotos     = (TextView)findViewById(R.id.textProductTotalPhotos);
            textProductTotalPhotos.setText(String.format(getResources().getString(R.string.text_total_detail), String.valueOf(details.length() + (product.has("cover_content_width") && product.getLong("cover_content_width") > 0 ? 1 : 0))));

            if (product.has("info")) {
                TextView textProductInfo        = (TextView) findViewById(R.id.textProductInfo);
                textProductInfo.setText(product.getString("info"));
            }

            if (product.has("custom_cover_title_activity")) {
                productCoverTitleActivity       = product.getString("custom_cover_title_activity");
            }

            List<String> tempImagePaths = new ArrayList<String>();
            if (product.has("gallery_images") && !product.isNull("gallery_images") && product.getJSONArray("gallery_images").length() > 0) {
                for (int i = 0; i < product.getJSONArray("gallery_images").length(); i++) {
                    tempImagePaths.add(product.getJSONArray("gallery_images").getJSONObject(i).getString("large"));
                }
            }

            ViewPager pager             = (ViewPager)findViewById(R.id.viewPager);
            pager.setAdapter(new ImageViewAdapter(getSupportFragmentManager(), this, tempImagePaths));

            /*CirclePageIndicator indicator = (CirclePageIndicator)findViewById(R.id.viewIndicator);
            indicator.setViewPager(pager);*/
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void initTapForReload() {
        buttonTapForReload  = (Button)findViewById(R.id.buttonTapForReload);
        buttonTapForReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tapForReloadLayout.setVisibility(View.GONE);
                loadDataProduct();
            }
        });

        tapForReloadLayout  = (RelativeLayout)findViewById(R.id.tapForReload);
        tapForReloadLayout.setVisibility(View.GONE);
    }
    //</editor-fold>
}
