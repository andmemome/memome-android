package com.triodigitalagency.memome.product;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.adapter.RecyclerAdapterRiview;
import com.triodigitalagency.memome.database.Project;
import com.triodigitalagency.memome.database.ProjectDetail;
import com.triodigitalagency.memome.database.DatabaseHandler;
import com.triodigitalagency.memome.transaction.CartActivity;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.FileUtils;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Intent.FLAG_ACTIVITY_NO_HISTORY;

public class ProductReviewActivity extends BaseActivity implements RecyclerAdapterRiview.onClickCheckoutAdapter {

    private ArrayList<JSONObject> dataSource;
    private ArrayList<JSONObject> dataSource2 = new ArrayList<>();
    @BindView(R.id.myrecyclerview)RecyclerView listView;
    @BindView(R.id.line1)RelativeLayout line1;
    @BindView(R.id.imageViewProductReview)ImageView image1;
    @BindView(R.id.imageViewProductReviewContent)ImageView imageViewContent;
    @BindView(R.id.relativeLayoutTitleArea)RelativeLayout relativeLayoutTitleArea;
    @BindView(R.id.editTextTitle)EditText tvTitle;
    @BindView(R.id.editTextSubtitle)EditText tvSubtitlee;

    private RecyclerAdapterRiview reviewArrayAdapter;
    private RelativeLayout tapForReloadLayout;
    private Button buttonTapForReload;
    private JSONObject product;
    private JSONArray details;
    private Bundle extras;
    private long productId, corporateVoucherId;
    private int listViewPosition = -1, projectId = -1, corporateVoucherQty;
    private String title, subtitle, pathImageOriginal, pathImageCrop, corporateVoucherName, corporateVoucherCode;
    private ArrayList<Uri> selectedImagesPath = new ArrayList<>();
    private final int BROWSE = 1;
    JSONObject jsonObject  = new JSONObject();
    @BindView(R.id.buttonEdit)ImageButton buttonEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_review);
        setLog("activity_product_review");
        ButterKnife.bind(this);

        firstInit();
        setupActionBar(getResources().getString(R.string.title_product_review));

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        dataSource                      = new ArrayList<>();
        reviewArrayAdapter              = new RecyclerAdapterRiview(context,dataSource,this);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 3);
        listView.setLayoutManager(mLayoutManager);
        listView.addItemDecoration(new GridSpacingItemDecoration(3, dpToPx(5), true));
        listView.setItemAnimator(new DefaultItemAnimator());
        listView.setAdapter(reviewArrayAdapter);
        listView.setNestedScrollingEnabled(false);


        Button buttonAddToMyProject = findViewById(R.id.buttonAddToMyProject);

        buttonAddToMyProject.setOnClickListener(buttonAddToMyProjectClicked);


        if ("edit".equals(getIntent().getStringExtra("edit"))){
            DatabaseHandler db = new DatabaseHandler(this);
            Project project = db.getProject(getIntent().getIntExtra("idp",0));
            List<ProjectDetail> projectDetails = db.getAllProjectDetailsByProjectId(db.getProject(getIntent().getIntExtra("idp",0)).getID());

            productId               = project.getProductId();
            projectId               = project.getID();
            title                   = project.getTitle();
            subtitle                = project.getSubtitle();
            pathImageCrop           = project.getPathImageCrop();
            pathImageOriginal       = project.getPathImageOriginal();
            setLog("==>- "+project.getPathImageCrop());
            setLog("==>- "+project.getPathImageOriginal());

            for (int i =0;i<projectDetails.size();i++){
                selectedImagesPath.add(Uri.parse(projectDetails.get(i).getPathImageOriginal()));
            }

            corporateVoucherId      = project.getCorporateVoucherId();
            corporateVoucherName    = project.getCorporateVoucherName();
            corporateVoucherQty     = project.getCorporateVoucherQty();
            corporateVoucherCode    = project.getCorporateVoucherCode();

        }else {
            extras = getIntent().getExtras();
            if (extras != null){

                productId               = extras.getLong("productId");
                projectId               = extras.getInt("projectId", -1);
                title                   = extras.getString("title");
                subtitle                = extras.getString("subtitle");


                pathImageCrop           = extras.getString("pathImageCrop");
                pathImageOriginal       = extras.getString("pathImageOriginal");
                selectedImagesPath      = extras.getParcelableArrayList("selectedImagesPath");
                setLog("selectedImagesPath ===> "+gson.toJson(extras.getParcelableArrayList("selectedImagesPath")));
                corporateVoucherId      = extras.getLong("corporateVoucherId", -1);
                corporateVoucherName    = extras.getString("corporateVoucherName", "");
                corporateVoucherQty     = extras.getInt("corporateVoucherQty", -1);
                corporateVoucherCode    = extras.getString("corporateVoucherCode", "");

            }
            else {
                finish();
            }

        }

        if (projectId > 0) {
            buttonAddToMyProject.setText(getResources().getString(R.string.button_save_to_my_project));
        }


        loadDataProduct();
        initTapForReload();

        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listViewPosition    = -2;
                startActivityForResult(CropActivity.class, BROWSE, new OnSetupIntentExtras() {
                    @Override
                    public void onSetupIntentExtras(Intent intent) {
                        try {
                            intent.putExtra("pathImageOriginal", dataSource2.get(0).getString("path_image_original"));
                            int ratioX  = (int)dataSource2.get(0).getLong("content_width");
                            int ratioY  = (int)dataSource2.get(0).getLong("content_height");
                            intent.putExtra("ratioX", ratioX);
                            intent.putExtra("ratioY", ratioY);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            back();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            back();
            return true;
        }
        else {
            return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        && listViewPosition - 1 > -1
        if (resultCode == RESULT_OK && requestCode == BROWSE && listViewPosition - 1 > -1) {
            Bundle extra        = data.getExtras();

            if (!reviewArrayAdapter.getItemPathImageCrop(listViewPosition).equals(extra.getString("pathImageCrop"))) {
                reviewArrayAdapter.setIsChanged(listViewPosition, true);
            }

            selectedImagesPath.set(listViewPosition - 1, Uri.parse(extra.getString("pathImageOriginal")));
            reviewArrayAdapter.setPathImage(listViewPosition, extra.getString("pathImageCrop"), extra.getString("pathImageOriginal"));
            reviewArrayAdapter.notifyDataSetChanged();
            listViewPosition    = -1;
        }else if(listViewPosition == -2){
            setLog("edited -->"+listViewPosition);
            try{

                if("20".equals(jsonObject.optString("id"))||"22".equals(jsonObject.optString("id"))||"24".equals(jsonObject.optString("id"))||"10".equals(jsonObject.optString("category_id"))){
                    Bundle extra = data.getExtras();
                    if (!reviewArrayAdapter.getItemPathImageCrop(listViewPosition).equals(extra.getString("pathImageCrop"))) {
                        reviewArrayAdapter.setIsChanged(listViewPosition, true);
                    }
                    selectedImagesPath.set(listViewPosition , Uri.parse(extra.getString("pathImageOriginal")));
                    reviewArrayAdapter.setPathImage(listViewPosition, extra.getString("pathImageCrop"), extra.getString("pathImageOriginal"));
                    reviewArrayAdapter.notifyDataSetChanged();
                    listViewPosition = -1;
                }else {
                    Bundle extra = data.getExtras();
                    String path = extra.getString("pathImageCrop");
                    File imgFile = new  File(path);
                    pathImageCrop           = path;
                    pathImageOriginal       = path;

                    if(imgFile.exists()){
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
//                        image1.setImageBitmap(myBitmap);
                        imageViewContent.setImageBitmap(myBitmap);
                    }
                    listViewPosition = -1;
                    setLog("edited -->"+" edit");
                }
            }catch (Exception e){
                e.printStackTrace();
                try {
                    setLog("edited -->"+" gagal");
                    Bundle extra = data.getExtras();
                    String path = extra.getString("pathImageCrop");
                    File imgFile = new  File(path);
                    pathImageCrop           = path;
                    pathImageOriginal       = path;

                    if(imgFile.exists()){
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
//                        image1.setImageBitmap(myBitmap);
                        imageViewContent.setImageBitmap(myBitmap);
                    }
                }catch (Exception ee){
                    ee.printStackTrace();
                }
                listViewPosition = -1;
            }
        }else {
            try{
                Bundle extra        = data.getExtras();
                if (!reviewArrayAdapter.getItemPathImageCrop(listViewPosition).equals(extra.getString("pathImageCrop"))) {
                    reviewArrayAdapter.setIsChanged(listViewPosition, true);
                }

                selectedImagesPath.set(listViewPosition, Uri.parse(extra.getString("pathImageOriginal")));
                reviewArrayAdapter.setPathImage(listViewPosition, extra.getString("pathImageCrop"), extra.getString("pathImageOriginal"));
                reviewArrayAdapter.notifyDataSetChanged();
                listViewPosition    = -1;
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    protected View.OnClickListener buttonAddToMyProjectClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(!reviewArrayAdapter.isResGood.isEmpty()) {

            }else{
                saveProjects();
            }
        }
    };

    protected void back() {
        new AlertDialog.Builder(context)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setTitle(R.string.text_back_title)
            .setMessage(R.string.text_back_message)
            .setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    deleteCurrentImages();
                    finish();
                }

            })
            .setNegativeButton(R.string.button_no, null)
            .show();
    }

    protected void deleteCurrentImages() {
        for (int i = 1; i < reviewArrayAdapter.getItemCount(); i++) { // position 0 is cover
            if ((projectId <= 0 && !reviewArrayAdapter.getItemPathImageOriginal(i).equals(reviewArrayAdapter.getItemPathImageCrop(i))) || (projectId > 0 && reviewArrayAdapter.getIsChanged(i))) {
                File file           = new File(reviewArrayAdapter.getItemPathImageCrop(i));
                if (file.exists()) {
                    file.delete();

                    context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(reviewArrayAdapter.getItemPathImageCrop(i)))));
                }
            }
        }
    }

    protected void initTapForReload() {
        buttonTapForReload  = findViewById(R.id.buttonTapForReload);
        buttonTapForReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tapForReloadLayout.setVisibility(View.GONE);
                loadDataProduct();
            }
        });

        tapForReloadLayout  = findViewById(R.id.tapForReload);
        tapForReloadLayout.setVisibility(View.GONE);
    }

    protected void loadDataProduct() {
        if (!isLoading()) {
            showLoading();
            Map<String, String> params = new HashMap<String, String>();
            params.put("clientId", Constant.CLIENT_ID);
            params.put("productId", String.format("%d", productId));
            request.getJSONFromUrl(Globals.getApiUrl("products/view"),
                params,
                getResources().getString(R.string.text_product_error),
                new RequestCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideLoading();

                        try {
                            deleteCurrentImages();
                            setLog("=== > > "+response.toString());
                            jsonObject = response.getJSONObject("product");
                            buttonEdit.setVisibility(View.VISIBLE);

                            drawLayout(jsonObject, response.getJSONArray("details"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(VolleyError error) {
                        tapForReloadLayout.setVisibility(View.VISIBLE);
                        hideLoading();
                    }
                });
        }
    }

    protected void drawLayout(JSONObject product, JSONArray details) {
        this.product    = product;
        this.details    = details;

        try {
            JSONObject tempJSONObject   = new JSONObject();
            if (dataSource == null) {
                dataSource              = new ArrayList<JSONObject>();
            }
            else {
                dataSource.clear();
            }

            tempJSONObject.put("id", product.getLong("id"));
            tempJSONObject.put("type", 0);
            tempJSONObject.put("path_image_original", pathImageOriginal);
            tempJSONObject.put("path_image_crop", pathImageCrop);
            tempJSONObject.put("width", product.getLong("cover_width"));
            tempJSONObject.put("height", product.getLong("cover_height"));
            tempJSONObject.put("content_width", product.getLong("cover_content_width"));
            tempJSONObject.put("content_height", product.getLong("cover_content_height"));
            tempJSONObject.put("border_left", product.getLong("cover_border_left"));
            tempJSONObject.put("border_top", product.getLong("cover_border_top"));
            tempJSONObject.put("title", title);
            tempJSONObject.put("subtitle", subtitle);
            tempJSONObject.put("title_area_width", product.getLong("cover_title_area_width"));
            tempJSONObject.put("title_area_height", product.getLong("cover_title_area_height"));
            tempJSONObject.put("title_area_top", product.getLong("cover_title_area_top"));
            tempJSONObject.put("title_area_left", product.getLong("cover_title_area_left"));
            tempJSONObject.put("title_font_size", product.getLong("cover_title_font_size"));
            tempJSONObject.put("subtitle_font_size", product.getLong("cover_subtitle_font_size"));
            tempJSONObject.put("title_top", product.getLong("cover_title_top"));
            tempJSONObject.put("subtitle_top", product.getLong("cover_subtitle_top"));
            if (product.has("cover_images")  && !product.isNull("cover_images")) {
                tempJSONObject.put("images", product.getJSONObject("cover_images"));
            }

            dataSource2.add(tempJSONObject);
            setImageViewCover();

            if ("20".equals(jsonObject.optString("id"))||"22".equals(jsonObject.optString("id"))||"24".equals(jsonObject.optString("id"))||"10".equals(jsonObject.optString("category_id"))){
                line1.setVisibility(View.VISIBLE);
            }else {
                for (int i = 0; i < selectedImagesPath.size(); i++) {
                    setLog("--> image "+selectedImagesPath.get(i));
                    String image = "";
                    File file = FileUtils.getFile(this, selectedImagesPath.get(i));
                    if (file != null) {
                        image = file.getPath();
                    }
                    else {
                        image = selectedImagesPath.get(i).toString();
                    }
                    tempJSONObject  = new JSONObject();
                    tempJSONObject.put("id", details.getJSONObject(i).getLong("id"));
                    tempJSONObject.put("type", 1);
                    tempJSONObject.put("path_image_original", image);
                    tempJSONObject.put("path_image_crop", image);
                    tempJSONObject.put("width", details.getJSONObject(i).getLong("width"));
                    tempJSONObject.put("height", details.getJSONObject(i).getLong("height"));
                    tempJSONObject.put("content_width", details.getJSONObject(i).getLong("content_width"));
                    tempJSONObject.put("content_height", details.getJSONObject(i).getLong("content_height"));
                    tempJSONObject.put("border_left", details.getJSONObject(i).getLong("border_left"));
                    tempJSONObject.put("border_top", details.getJSONObject(i).getLong("border_top"));
                    if (details.getJSONObject(i).has("images")  && !details.getJSONObject(i).isNull("images")) {
                        tempJSONObject.put("images", details.getJSONObject(i).getJSONObject("images"));
                    }
                    dataSource.add(tempJSONObject);
                }
            }




        } catch (JSONException e) {
            e.printStackTrace();
        }

        reviewArrayAdapter.notifyDataSetChanged();
    }

    private void setImageViewCover() {
        JSONObject item            = dataSource2.get(0);
        try {
            long type               = item.getLong("type");
            long CoverWidth         = item.getLong("width");
            long CoverHeight        = item.getLong("height");
            long CoverContentWidth  = item.getLong("content_width");
            long CoverContentHeight = item.getLong("content_height");
            long CoverBorderLeft    = item.getLong("border_left");
            long CoverBorderTop     = item.getLong("border_top");

            if (type == 0) {

                setLog("type " + type + " CoverWidth " + CoverWidth + " CoverHeight " + CoverHeight + " CoverContentWidth " + CoverContentWidth + " CoverContentHeight " + CoverContentHeight);
                long CoverTitleAreaWidth = item.getLong("title_area_width");
                long CoverTitleAreaHeight = item.getLong("title_area_height");
                long CoverTitleAreaTop = item.getLong("title_area_top");
                long CoverTitleAreaLeft = item.getLong("title_area_left");
                long CoverTitleFontSize = item.getLong("title_font_size");
                long CoverSubtitleFontSize = item.getLong("subtitle_font_size");
                long CoverTitleTop = item.getLong("title_top");
                long CoverSubtitleTop = item.getLong("subtitle_top");

                String pathImageCrop            = item.getString("path_image_crop");
                String pathImageOriginal        = item.getString("path_image_original");

                if ("5".equals(jsonObject.optString("category_id"))||"1".equals(jsonObject.optString("category_id"))){
                    if ("9".equals(jsonObject.optString("id"))){
                        line1.setVisibility(View.GONE);
                    }else {
                        line1.setVisibility(View.VISIBLE);
                    }
                }else {
                    line1.setVisibility(View.GONE);
                }

                tvTitle.setText(item.getString("title"));
                tvSubtitlee.setText(item.getString("subtitle"));


                DisplayMetrics displayMetrics               = context.getResources().getDisplayMetrics();
                final float dpWidth                         = displayMetrics.widthPixels / displayMetrics.density;
                final float dpHeight                        = displayMetrics.heightPixels / displayMetrics.density;
                image1.getLayoutParams().width           = (int) Globals.convertDpToPixel(dpWidth - 22, context); // -22 for padding outside

                Globals.drawProductWithTitleLayout(context, CoverWidth, CoverHeight, image1, imageViewContent, CoverContentWidth,
                        CoverContentHeight, CoverBorderLeft, CoverBorderTop,
                        relativeLayoutTitleArea, CoverTitleAreaWidth, CoverTitleAreaHeight, CoverTitleAreaTop, CoverTitleAreaLeft,
                        CoverTitleFontSize, CoverSubtitleFontSize, CoverTitleTop, CoverSubtitleTop, tvTitle, tvSubtitlee);


                Typeface font = Typeface.createFromAsset(activity.getAssets(), "fonts/trajan.ttf");
                Typeface fontSub = Typeface.createFromAsset(activity.getAssets(),"fonts/trajansmall.ttf");
                tvTitle.setTypeface(font);
                tvSubtitlee.setTypeface(fontSub);


                if (item.has("images")  && !item.isNull("images")) {
                    JSONObject images   = item.getJSONObject("images");
                    if ("https".equals(images.getString("thumb").substring(0,5))){
//                        Glide.with(this)
//                                .load("http"+images.getString("thumb").substring(5,images.getString("thumb").length()))
//                                .placeholder(R.mipmap.loading_img)
//                                .into(image1);

                        Picasso.with(this)
                                .load("http"+images.getString("thumb").substring(5,images.getString("thumb").length()))
                                .placeholder(R.mipmap.loading_img)
                                .fit()
                                .into(image1);


                        setLog("--"+"http"+images.getString("thumb").substring(5,images.getString("thumb").length()));
                    }else {
//                        Glide.with(this)
//                                .load(images.getString("thumb"))
//                                .placeholder(R.mipmap.loading_img)
//                                .into(image1);

                        Picasso.with(this)
                                .load(images.getString("thumb"))
                                .placeholder(R.mipmap.loading_img)
                                .fit()
                                .into(image1);


                        setLog("thumb "+images.getString("thumb"));
                    }
                }

                File imgFile = new  File(pathImageCrop);
                if(imgFile.exists()){
                    setLog("image corp "+pathImageCrop);
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    imageViewContent.setImageBitmap(myBitmap);
                }

                image1.post(new Runnable(){
                    public void run(){
                        int height = image1.getWidth();
                    }
                });


            }
        }catch (Exception e){

        }
    }


    private void setImageViewCover(String imageViewCover) {
        JSONObject item            = dataSource2.get(0);
        try {
            long type               = item.getLong("type");
            long CoverWidth         = item.getLong("width");
            long CoverHeight        = item.getLong("height");
            long CoverContentWidth  = item.getLong("content_width");
            long CoverContentHeight = item.getLong("content_height");
            long CoverBorderLeft    = item.getLong("border_left");
            long CoverBorderTop     = item.getLong("border_top");

            if (type == 0) {

                setLog("type " + type + " CoverWidth " + CoverWidth + " CoverHeight " + CoverHeight + " CoverContentWidth " + CoverContentWidth + " CoverContentHeight " + CoverContentHeight);
                long CoverTitleAreaWidth = item.getLong("title_area_width");
                long CoverTitleAreaHeight = item.getLong("title_area_height");
                long CoverTitleAreaTop = item.getLong("title_area_top");
                long CoverTitleAreaLeft = item.getLong("title_area_left");
                long CoverTitleFontSize = item.getLong("title_font_size");
                long CoverSubtitleFontSize = item.getLong("subtitle_font_size");
                long CoverTitleTop = item.getLong("title_top");
                long CoverSubtitleTop = item.getLong("subtitle_top");

                String pathImageCrop            = imageViewCover;
                String pathImageOriginal        = item.getString("path_image_original");

                if ("5".equals(jsonObject.optString("category_id"))||"1".equals(jsonObject.optString("category_id"))){
                    line1.setVisibility(View.VISIBLE);
                }else {
                    line1.setVisibility(View.GONE);
                }

                tvTitle.setText(item.getString("title"));
                tvSubtitlee.setText(item.getString("subtitle"));


                DisplayMetrics displayMetrics               = context.getResources().getDisplayMetrics();
                final float dpWidth                         = displayMetrics.widthPixels / displayMetrics.density;
                final float dpHeight                        = displayMetrics.heightPixels / displayMetrics.density;
                image1.getLayoutParams().width           = (int) Globals.convertDpToPixel(dpWidth - 22, context); // -22 for padding outside

                Globals.drawProductWithTitleLayout(context, CoverWidth, CoverHeight, image1, imageViewContent, CoverContentWidth,
                        CoverContentHeight, CoverBorderLeft, CoverBorderTop,
                        relativeLayoutTitleArea, CoverTitleAreaWidth, CoverTitleAreaHeight, CoverTitleAreaTop, CoverTitleAreaLeft,
                        CoverTitleFontSize, CoverSubtitleFontSize, CoverTitleTop, CoverSubtitleTop, tvTitle, tvSubtitlee);


                Typeface font = Typeface.createFromAsset(activity.getAssets(), "fonts/trajan.ttf");
                Typeface fontSub = Typeface.createFromAsset(activity.getAssets(),"fonts/trajansmall.ttf");
                tvTitle.setTypeface(font);
                tvSubtitlee.setTypeface(fontSub);


                if (item.has("images")  && !item.isNull("images")) {
                    JSONObject images   = item.getJSONObject("images");
                    if ("https".equals(images.getString("thumb").substring(0,5))){
                        Glide.with(this)
                                .load("http"+images.getString("thumb").substring(5,images.getString("thumb").length()))
                                .placeholder(R.mipmap.loading_img)
                                .into(image1);

//                        Picasso.with(this)
//                                .load("http"+images.getString("thumb").substring(5,images.getString("thumb").length()))
//                                .placeholder(R.mipmap.loading_img)
//                                .fit()
//                                .into(image1);


                        setLog("--"+"http"+images.getString("thumb").substring(5,images.getString("thumb").length()));
                    }else {
                        Glide.with(this)
                                .load(images.getString("thumb"))
                                .placeholder(R.mipmap.loading_img)
                                .into(image1);
//                        Picasso.with(this)
//                                .load(images.getString("thumb"))
//                                .placeholder(R.mipmap.loading_img)
//                                .fit()
//                                .into(image1);


                        setLog("thumb "+images.getString("thumb"));
                    }
                }


                File imgFile = new  File(pathImageCrop);
                if(imgFile.exists()){
                    setLog("image corp "+pathImageCrop);
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    imageViewContent.setImageBitmap(myBitmap);
                }

                image1.post(new Runnable(){
                    public void run(){
                        int height = image1.getWidth();
                        setLog(height+" ===== ");
                    }
                });


            }
        }catch (Exception e){

        }
    }

    protected void drawLayout(String pathImageCrop) {

        try {
            JSONObject tempJSONObject   = new JSONObject();
            if (dataSource == null) {
                dataSource              = new ArrayList<JSONObject>();
            }
            else {
                dataSource.clear();
            }

            tempJSONObject.put("id", product.getLong("id"));
            tempJSONObject.put("type", 0);
            tempJSONObject.put("path_image_original", pathImageCrop);
            tempJSONObject.put("path_image_crop", pathImageCrop);
            tempJSONObject.put("width", product.getLong("cover_width"));
            tempJSONObject.put("height", product.getLong("cover_height"));
            tempJSONObject.put("content_width", product.getLong("cover_content_width"));
            tempJSONObject.put("content_height", product.getLong("cover_content_height"));
            tempJSONObject.put("border_left", product.getLong("cover_border_left"));
            tempJSONObject.put("border_top", product.getLong("cover_border_top"));
            tempJSONObject.put("title", title);
            tempJSONObject.put("subtitle", subtitle);
            tempJSONObject.put("title_area_width", product.getLong("cover_title_area_width"));
            tempJSONObject.put("title_area_height", product.getLong("cover_title_area_height"));
            tempJSONObject.put("title_area_top", product.getLong("cover_title_area_top"));
            tempJSONObject.put("title_area_left", product.getLong("cover_title_area_left"));
            tempJSONObject.put("title_font_size", product.getLong("cover_title_font_size"));
            tempJSONObject.put("subtitle_font_size", product.getLong("cover_subtitle_font_size"));
            tempJSONObject.put("title_top", product.getLong("cover_title_top"));
            tempJSONObject.put("subtitle_top", product.getLong("cover_subtitle_top"));
            if (product.has("cover_images")  && !product.isNull("cover_images")) {
                tempJSONObject.put("images", product.getJSONObject("cover_images"));
            }

            dataSource2.add(tempJSONObject);
            setImageViewCover();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        reviewArrayAdapter.notifyDataSetChanged();
    }


    protected void saveProjects() {

        DatabaseHandler db = new DatabaseHandler(this);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
        String formattedDate = df.format(c.getTime());

        if (projectId <= 0) {
            Project project = new Project((int) productId, title, subtitle, pathImageCrop, pathImageOriginal, formattedDate, (int) corporateVoucherId, corporateVoucherName, corporateVoucherQty, corporateVoucherCode, 0);
            db.addProject(project);
            int lastInsertId = db.getLastInsertId();
            try {
                for (int i = 0; i < dataSource.size(); i++) {
                   db.addProjectDetail(new ProjectDetail(lastInsertId, (int) details.getJSONObject(i).getLong("id"), reviewArrayAdapter.getItemPathImageCrop(i), reviewArrayAdapter.getItemPathImageOriginal(i ))); // + 1 cz index 0 is cover
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            addToCart(lastInsertId);
            Intent intent = new Intent(ProductReviewActivity.this,CartActivity.class);
            intent.setFlags(FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
            finish();

        } else {

            Project project = db.getProject(projectId);

            if (!project.getPathImageCrop().equals(project.getPathImageOriginal()) && !project.getPathImageCrop().equals(pathImageCrop)) {
                File file = new File(project.getPathImageCrop());
                if (file.exists()) {
                    file.delete();
                    context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(project.getPathImageCrop()))));
                }
            }

            project.setTitle(title);
            project.setSubtitle(subtitle);
            project.setPathImageOriginal(pathImageOriginal);
            project.setPathImageCrop(pathImageCrop);
            db.updateProject(project);

            int i = 0;
            for (ProjectDetail pd : db.getAllProjectDetailsByProjectId(projectId)) {

                if (!pd.getPathImageCrop().equals(pd.getPathImageOriginal()) && !pd.getPathImageCrop().equals(reviewArrayAdapter.getItemPathImageCrop(i + 0))) {
                    File file = new File(pd.getPathImageCrop());
                    if (file.exists()) {
                        file.delete();
                        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(pd.getPathImageCrop()))));
                    }
                }

                pd.setPathImageCrop(reviewArrayAdapter.getItemPathImageCrop(i ));
                pd.setPathImageOriginal(reviewArrayAdapter.getItemPathImageOriginal(i )); // + 1 cz index 0 is cover
                db.updateProjectDetail(pd);

                i++;
            }

//            addToCart(projectId);
            startActivity(CartActivity.class);
            finish();
        }

    }

    @Override
    public void onClick(int holder, int position) {
        int viewId = holder;
        if (position >= 0 && viewId == R.id.buttonEdit) {
            listViewPosition    = position;
            startActivityForResult(CropActivity.class, BROWSE, new OnSetupIntentExtras() {
                @Override
                public void onSetupIntentExtras(Intent intent) {
                    if (!reviewArrayAdapter.getItemPathImageOriginal(listViewPosition).equals(reviewArrayAdapter.getItemPathImageCrop(listViewPosition))) {
                        intent.putExtra("pathImageCrop", reviewArrayAdapter.getItemPathImageCrop(listViewPosition));
                    }
                    intent.putExtra("pathImageOriginal", reviewArrayAdapter.getItemPathImageOriginal(listViewPosition));

                    int ratioX  = (int)reviewArrayAdapter.getItemWidth(listViewPosition);
                    int ratioY  = (int)reviewArrayAdapter.getItemHeight(listViewPosition);
                    intent.putExtra("ratioX", ratioX);
                    intent.putExtra("ratioY", ratioY);
                }
            });
        }
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
