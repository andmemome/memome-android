package com.triodigitalagency.memome.product;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.Globals;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProjectFragment extends Fragment {
    protected final int PREVIEW = 1;

    public static ProjectFragment getInstance(int position, JSONObject product) {
        ProjectFragment myFragment = new ProjectFragment();
        Bundle args                = new Bundle();

        args.putInt("position", position);
        try {
            args.putInt("project_id", product.getInt("project_id"));
            args.putString("cover_title", product.getString("cover_title"));
            args.putString("cover_subtitle", product.getString("cover_subtitle"));
            args.putString("path_image_crop", product.getString("path_image_crop"));
            args.putLong("cover_width", product.getLong("cover_width"));
            args.putLong("cover_height", product.getLong("cover_height"));
            args.putLong("cover_content_width", product.getLong("cover_content_width"));
            args.putLong("cover_content_height", product.getLong("cover_content_height"));
            args.putLong("cover_border_left", product.getLong("cover_border_left"));
            args.putLong("cover_border_top", product.getLong("cover_border_top"));
            args.putLong("cover_title_area_width", product.getLong("cover_title_area_width"));
            args.putLong("cover_title_area_height", product.getLong("cover_title_area_height"));
            args.putLong("cover_title_area_top", product.getLong("cover_title_area_top"));
            args.putLong("cover_title_area_left", product.getLong("cover_title_area_left"));
            args.putLong("cover_title_font_size", product.getLong("cover_title_font_size"));
            args.putLong("cover_subtitle_font_size", product.getLong("cover_subtitle_font_size"));
            args.putLong("cover_title_top", product.getLong("cover_title_top"));
            args.putLong("cover_subtitle_top", product.getLong("cover_subtitle_top"));

            if (product.has("cover_images")  && !product.isNull("cover_images")) {
                JSONObject images   = product.getJSONObject("cover_images");
                args.putString("cover_images", images.getString("thumb"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        myFragment.setArguments(args);

        return myFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View layout                                 = inflater.inflate(R.layout.fragment_project, container, false);
        final Bundle bundle                         = getArguments();

        if (bundle != null) {
            String coverTitle                   = bundle.getString("cover_title");
            String coverSubtitle                = bundle.getString("cover_subtitle");
            String pathImageCrop                = bundle.getString("path_image_crop");
            long productCoverWidth              = bundle.getLong("cover_width");
            long productCoverHeight             = bundle.getLong("cover_height");
            long productCoverContentWidth       = bundle.getLong("cover_content_width");
            long productCoverContentHeight      = bundle.getLong("cover_content_height");
            long productCoverBorderLeft         = bundle.getLong("cover_border_left");
            long productCoverBorderTop          = bundle.getLong("cover_border_top");
            long productCoverTitleAreaWidth     = bundle.getLong("cover_title_area_width");
            long productCoverTitleAreaHeight    = bundle.getLong("cover_title_area_height");
            long productCoverTitleAreaTop       = bundle.getLong("cover_title_area_top");
            long productCoverTitleAreaLeft      = bundle.getLong("cover_title_area_left");
            long productCoverTitleFontSize      = bundle.getLong("cover_title_font_size");
            long productCoverSubtitleFontSize   = bundle.getLong("cover_subtitle_font_size");
            long productCoverTitleTop           = bundle.getLong("cover_title_top");
            long productCoverSubtitleTop        = bundle.getLong("cover_subtitle_top");

            EditText title                      = (EditText) layout.findViewById(R.id.editTextTitle);
            title.setText(coverTitle);
            EditText subtitle                   = (EditText) layout.findViewById(R.id.editTextSubtitle);
            subtitle.setText(coverSubtitle);
            DisplayMetrics displayMetrics       = getActivity().getResources().getDisplayMetrics();
            final float dpWidth                 = displayMetrics.widthPixels / displayMetrics.density;
            ImageView imageView                 = (ImageView) layout.findViewById(R.id.imageViewProductReview);
            imageView.getLayoutParams().width   = (int) Globals.convertDpToPixel(dpWidth - 120, layout.getContext()); // -120 for padding outside
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), ProjectDetailActivity.class);
                    intent.putExtra("projectId", bundle.getInt("project_id"));
                    startActivity(intent);
                }
            });
            ImageView imageViewContent          = (ImageView) layout.findViewById(R.id.imageViewProductReviewContent);
            RelativeLayout relativeLayoutTitleArea = (RelativeLayout) layout.findViewById(R.id.relativeLayoutTitleArea);
            relativeLayoutTitleArea.setVisibility(View.VISIBLE);

            Globals.drawProductWithTitleLayout(layout.getContext(), productCoverWidth, productCoverHeight, imageView, imageViewContent, productCoverContentWidth,
                    productCoverContentHeight, productCoverBorderLeft, productCoverBorderTop,
                    relativeLayoutTitleArea, productCoverTitleAreaWidth, productCoverTitleAreaHeight, productCoverTitleAreaTop, productCoverTitleAreaLeft,
                    productCoverTitleFontSize, productCoverSubtitleFontSize, productCoverTitleTop, productCoverSubtitleTop, title, subtitle);

            Glide.with(layout.getContext())
                    .load(pathImageCrop)
                    .placeholder(R.mipmap.loading_img)
                    .into(imageViewContent);
//            Picasso.with(layout.getContext())
//                    .load(pathImageCrop)
//                    .placeholder(R.mipmap.loading_img)
//                    .fit()
//                    .into(imageViewContent);


            if (bundle.get("cover_images") != null && !bundle.getString("cover_images").equals("")) {
                Picasso.with(layout.getContext())
                        .load(bundle.getString("cover_images"))
                        .placeholder(R.mipmap.loading_img)
                        .fit()
                        .into(imageView);
//
//                Glide.with(layout.getContext())
//                        .load(bundle.getString("cover_images"))
//                        .placeholder(R.mipmap.loading_img)
//                        .into(imageView);
            }
        }

        return layout;
    }
}
