package com.triodigitalagency.memome.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.utils.NumberTextWatcher;
import com.triodigitalagency.memome.volley.utils.VolleyRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class ConfirmOrderArrayAdapter extends BaseAdapter {
    private final Context context;
    private ArrayList<JSONObject> values;
    private VolleyRequest request;
    private Long selectedId;

    public ConfirmOrderArrayAdapter(Context context){
        this.context    = context;
        this.values     = new ArrayList<JSONObject>();
        this.request    = new VolleyRequest((Activity)context);
    }

    @Override
    public int getCount() {
        return this.values.size();
    }

    @Override
    public Object getItem(int position) {
        return this.values.get(position);
    }

    @Override
    public long getItemId(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getLong("id");
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public String getItemNumber(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getString("number");
        } catch (JSONException e) {
            e.printStackTrace();
            return String.valueOf(this.getItemId(position));
        }
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view               = inflater.inflate(R.layout.adapter_confirm_order, parent, false);
        TextView textView1      = (TextView)view.findViewById(R.id.textView1);
        TextView textView2      = (TextView)view.findViewById(R.id.textView2);
        JSONObject item         = values.get(position);

        try {
            String orderId              = String.format(view.getResources().getString(R.string.text_order_id), String.valueOf(item.getString("number")));
            String createdAt            = item.getString("created_at");
            SimpleDateFormat format     = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
            try {
                Date date   = format.parse(createdAt);
                createdAt   = dateFormat.format(date);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            EditText tempEditText   = new EditText(context);
            tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
            tempEditText.setText(String.valueOf((int)item.getDouble("grand_total")));
            String grandTotal       = String.format(view.getResources().getString(R.string.text_total_idr), tempEditText.getText().toString());

            textView1.setText(orderId);
            textView2.setText(createdAt + "\n" + grandTotal);

            ImageView imageViewCheck  = (ImageView)view.findViewById(R.id.imageViewCheck);
            if (this.selectedId != null && this.selectedId > 0 && this.selectedId == item.getLong("id")) {
                imageViewCheck.setVisibility(View.VISIBLE);
            }
            else {
                imageViewCheck.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }

    public Double getGrandTotal(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getDouble("grand_total");
        } catch (JSONException e) {
            e.printStackTrace();
            return Double.valueOf(0);
        }
    }

    public void setValues(ArrayList<JSONObject> values){
        this.values = values;
    }

    public void setSelectedId(Long id) { this.selectedId = id; }
}
