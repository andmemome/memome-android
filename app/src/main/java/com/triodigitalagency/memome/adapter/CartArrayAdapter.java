package com.triodigitalagency.memome.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.database.Cart;
import com.triodigitalagency.memome.database.DatabaseHandler;
import com.triodigitalagency.memome.database.ProjectDetail;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.utils.NumberTextWatcher;
import com.triodigitalagency.memome.utils.OnclikList;
import com.triodigitalagency.memome.volley.utils.VolleyRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CartArrayAdapter extends BaseAdapter {
    private final Context context;
    private ArrayList<JSONObject> values;
    private VolleyRequest request;
    private Boolean showButton = true;

    public OnclikList onclikList;

    public void setOnclikList(OnclikList onclikList) {
        this.onclikList = onclikList;
    }

    public CartArrayAdapter(Context context){
        this.context    = context;
        this.values     = new ArrayList<JSONObject>();
        this.request    = new VolleyRequest((Activity)context);
    }


    @Override
    public int getCount() {
        return this.values.size();
    }

    @Override
    public Object getItem(int position) {
        return this.values.get(position);
    }

    @Override
    public long getItemId(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getLong("id");
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        LayoutInflater inflater                     = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view                                   = inflater.inflate(R.layout.adapter_cart, parent, false);
        JSONObject item                             = values.get(position);
        final View cartView                         = view.findViewById(R.id.productCart);
        final ImageView imageView                   = (ImageView)view.findViewById(R.id.imageViewProductReview);
        imageView.getLayoutParams().width           = (int) Globals.convertDpToPixel(130, context); // 130dp, manual from layout
        ImageView imageViewContent                  = (ImageView)view.findViewById(R.id.imageViewProductReviewContent);
        final Button buttonMinusCart                = (Button)view.findViewById(R.id.buttonMinusCart);
        final Button buttonAddCart                  = (Button)view.findViewById(R.id.buttonAddCart);
        final ImageView buttonDeleteCart            = (ImageView)view.findViewById(R.id.buttonDeleteCart);

        RelativeLayout relativeLayoutTitleArea      = (RelativeLayout)view.findViewById(R.id.relativeLayoutTitleArea);
        relativeLayoutTitleArea.setVisibility(View.VISIBLE);

        if (this.showButton) {
            view.findViewById(R.id.buttonTextQtyCart).setVisibility(View.VISIBLE);
            view.findViewById(R.id.buttonMinusCart).setVisibility(View.VISIBLE);
            view.findViewById(R.id.buttonAddCart).setVisibility(View.VISIBLE);
            view.findViewById(R.id.buttonDeleteCart).setVisibility(View.VISIBLE);
        }

        try {
            long CoverWidth             = item.getLong("cover_width");
            long CoverHeight            = item.getLong("cover_height");
            long CoverContentWidth      = item.getLong("cover_content_width");
            long CoverContentHeight     = item.getLong("cover_content_height");
            long CoverBorderLeft        = item.getLong("cover_border_left");
            long CoverBorderTop         = item.getLong("cover_border_top");
            long CoverTitleAreaWidth    = item.getLong("cover_title_area_width");
            long CoverTitleAreaHeight   = item.getLong("cover_title_area_height");
            long CoverTitleAreaTop      = item.getLong("cover_title_area_top");
            long CoverTitleAreaLeft     = item.getLong("cover_title_area_left");
            long CoverTitleFontSize     = item.getLong("cover_title_font_size");
            long CoverSubtitleFontSize  = item.getLong("cover_subtitle_font_size");
            long CoverTitleTop          = item.getLong("cover_title_top");
            long CoverSubtitleTop       = item.getLong("cover_subtitle_top");

            EditText title              = (EditText)view.findViewById(R.id.editTextTitle);
            title.setText(item.getString("cover_title"));
            EditText subtitle           = (EditText)view.findViewById(R.id.editTextSubtitle);
            subtitle.setText(item.getString("cover_subtitle"));

            TextView textPrice          = (TextView)view.findViewById(R.id.textCartPrice);
            EditText tempTextPrice      = new EditText(view.getContext());
            tempTextPrice.addTextChangedListener(new NumberTextWatcher(tempTextPrice));
            tempTextPrice.setText(String.valueOf((int) item.getLong("special_price") > 0 ? item.getLong("special_price") : item.getLong("price")));
            textPrice.setText(tempTextPrice.getText().toString());

            TextView textProductTitle   = (TextView)view.findViewById(R.id.textProductTitle);
            textProductTitle.setText(!item.getString("cover_title").equals("") ? "\"" + item.getString("cover_title").trim() + "\"" : item.getString("title"));

            TextView textTotalPhoto     = (TextView)view.findViewById(R.id.textTotalPhoto);
            textTotalPhoto.setText(String.format(view.getResources().getString(R.string.text_total_detail), item.getInt("total_detail") + (CoverContentWidth > 0 ? 1 : 0)));

            if (item.getInt("corporate_voucher_qty") > 0) {
                textTotalPhoto.setText(textTotalPhoto.getText().toString() + "\n" + String.format(view.getResources().getString(R.string.text_corporate_cart), item.getInt("corporate_voucher_qty")));
            }

            Button buttonTextQtyCart    = (Button)view.findViewById(R.id.buttonTextQtyCart);
            buttonTextQtyCart.setText(String.valueOf(item.getInt("cart_qty")));



            view.findViewById(R.id.buttonMinusCart).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ListView) parent).performItemClick(v, position, 0); // Let the event be handled in onItemClick()
                }
            });

            view.findViewById(R.id.buttonAddCart).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ListView)parent).performItemClick(v, position, 0); // Let the event be handled in onItemClick()
                }
            });

            view.findViewById(R.id.buttonDeleteCart).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ListView)parent).performItemClick(v, position, 0); // Let the event be handled in onItemClick()
                }
            });

            Globals.drawProductWithTitleLayout(context, CoverWidth, CoverHeight, imageView, imageViewContent, CoverContentWidth,
                    CoverContentHeight, CoverBorderLeft, CoverBorderTop,
                    relativeLayoutTitleArea, CoverTitleAreaWidth, CoverTitleAreaHeight, CoverTitleAreaTop, CoverTitleAreaLeft,
                    CoverTitleFontSize, CoverSubtitleFontSize, CoverTitleTop, CoverSubtitleTop, title, subtitle);

            String pathImageCrop            = item.getString("path_image_crop");
            Globals.setLog("--> "+pathImageCrop );
            String pathImageOriginal        = item.getString("path_image_original");
            Glide.with(context)
                    .load(pathImageCrop)
                    .placeholder(R.mipmap.loading_img)
                    .into(imageViewContent);
//            Picasso.with(context)
//                    .load(pathImageCrop)
//                    .placeholder(R.mipmap.loading_img)
//                    .into(imageViewContent);

            /*try {
                String pathImage = item.optJSONArray("cover_image").getJSONObject(0).optJSONObject("image").optString("large");
                Glide.with(context)
                        .load(pathImage)
                        .placeholder(R.mipmap.loading_img)
                        .into(imageView);
            }catch (Exception e){
                e.printStackTrace();
                try {
                    String pathImage = item.optJSONArray("order_product_detail").getJSONObject(0).optJSONObject("image").optString("large");
                    Glide.with(context)
                            .load(pathImage)
                            .placeholder(R.mipmap.loading_img)
                            .into(imageView);
                }catch (Exception e1){
                    e1.printStackTrace();

                    Globals.setLog(item.toString());
                    Globals.setLog("image null");
                    try {
                        viewImage(position,imageView);
                    }catch (Exception e2){
                        e2.printStackTrace();
                    }

                }
            }*/


            if (item.has("images")  && !item.isNull("images")) {
                JSONObject images   = item.getJSONObject("images");
                Globals.setLog("image not null"+images.toString());
                try {
                    Picasso.with(context)
                            .load(images.getString("thumb"))
                            .placeholder(R.mipmap.loading_img)
                            .into(imageView);
//                    Glide.with(context)
//                            .load(images.getString("thumb"))
//                            .placeholder(R.mipmap.loading_img)
//                            .into(imageView);
                }catch (Exception e){
                    e.printStackTrace();
                    viewImage(position,imageView,imageViewContent);
                }
            }else {
                Globals.setLog(item.toString());
                Globals.setLog("image null");
                try {
                    viewImage(position,imageView,imageViewContent);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        cartView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("test","test");
                onclikList.clik(position,cartView.getId());
            }
        });

        buttonAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("test","test");
                onclikList.clik(position,buttonAddCart.getId());
            }
        });

        buttonMinusCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("test","test");
                onclikList.clik(position,buttonMinusCart.getId());
            }
        });

        buttonDeleteCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onclikList.clik(position,buttonDeleteCart.getId());
            }
        });

        return view;
    }

    protected void viewImage(int projectId, ImageView imageView,ImageView imageViewContent) {
        DatabaseHandler db = new DatabaseHandler(context);
        List<Cart> carts   = db.getAllCarts();
        List<ProjectDetail> projectDetails = db.getAllProjectDetailsByProjectId(carts.get(projectId).getProjectId());

        if (projectDetails.get(0).getPathImageOriginal()!=null&&(projectDetails.get(0).getPathImageOriginal()!="")){

            if (projectDetails.get(0).getPathImageOriginal().substring(0,5).equals("https")){
                Globals.setLog(projectDetails.get(0).getPathImageOriginal().substring(0,5));

                Picasso.with(context)
                        .load(projectDetails.get(0).getPathImageOriginal())
                        .placeholder(R.mipmap.loading_img)
                        .fit()
                        .into(imageView);

                Picasso.with(context)
                        .load(projectDetails.get(0).getPathImageOriginal())
                        .placeholder(R.mipmap.loading_img)
                        .into(imageViewContent);

//                Glide.with(context)
//                        .load(projectDetails.get(0).getPathImageOriginal())
//                        .placeholder(R.mipmap.loading_img)
//                        .centerCrop()
//                        .into(imageView);
//                Glide.with(context)
//                        .load(projectDetails.get(0).getPathImageOriginal())
//                        .placeholder(R.mipmap.loading_img)
//                        .into(imageViewContent);
            }else {
                File file = new File(projectDetails.get(0).getPathImageOriginal());
                Uri imageUri = Uri.fromFile(file);

                Picasso.with(context)
                        .load(imageUri)
                        .placeholder(R.mipmap.loading_img)
//                        .centerCrop()
                        .fit()
                        .into(imageView);
//                Glide.with(context)
//                        .load(imageUri)
//                        .placeholder(R.mipmap.loading_img)
//                        .centerCrop()
//                        .into(imageView);
            }

        }

    }



    public void setValues(ArrayList<JSONObject> values){
        this.values = values;
    }

    public void setShowButton(Boolean value) { this.showButton = value; }
}
