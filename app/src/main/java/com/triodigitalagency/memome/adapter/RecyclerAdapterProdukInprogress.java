package com.triodigitalagency.memome.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.NumberTextWatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by khoiron on 02/04/18.
 */

public class RecyclerAdapterProdukInprogress extends RecyclerView.Adapter<RecyclerAdapterProdukInprogress.MyViewHolder>{

    private Context context;
    public ArrayList<JSONObject> values = new ArrayList<>();
    public setOnClikLikAdapter setOnClikLikAdapter;

    boolean completed = false;

    public void setSetOnClikLikAdapter(RecyclerAdapterProdukInprogress.setOnClikLikAdapter setOnClikLikAdapter) {
        this.setOnClikLikAdapter = setOnClikLikAdapter;
    }

    public RecyclerAdapterProdukInprogress(Context mContext) {
        this.context = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_purchase_product, parent, false);

        return new MyViewHolder(itemView);
    }

    public void setCompleted(boolean completed){
        this.completed = completed;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final JSONObject item         = values.get(position);
        setLog("nnn "+item.toString());


        if (completed){
            holder.addToCart.setVisibility(View.VISIBLE);
            holder.qty.setVisibility(View.GONE);
        }

        holder.addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setOnClikLikAdapter.onclik(item,position);
            }
        });


        Typeface custom_font;
        custom_font = Typeface.createFromAsset(context.getAssets(), "fonts/myriad.otf");
        holder.title.setTypeface(custom_font);

        if (position==values.size()){
            holder.lineGrone.setVisibility(View.GONE);
        }

        try {

            holder.productTitle.setText(item.getString("product_title").toUpperCase());
//            if (item.has("title")) {
//                holder.title.setText(item.getString("title"));
//            }

            if ("15".equals(item.getString("product_id"))){
                holder.title.setText(Integer.valueOf(item.getInt("count_photo")+1)+"");
            }else {
                holder.title.setText(item.getString("count_photo"));
            }


            if (item.has("total_price")) {
                Long total = item.getLong("total_price");
                if (total == 0){
                    holder.price.setText(R.string.text_free);
                }
                else{
                    EditText tempEditText               = new EditText(context);
                    tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
                    tempEditText.setText(String.valueOf(total));
                    holder.price.setText("IDR " + tempEditText.getText());
                }

            }
            if (item.has("qty")) {
                Long total = item.getLong("qty");
                EditText tempEditText               = new EditText(context);
                tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
                tempEditText.setText(String.valueOf(total));
                holder.qty.setText(tempEditText.getText() + " X");
            }

            try {
                String pathImage = item.optJSONArray("cover_image").getJSONObject(0).optJSONObject("image").optString("large");
                Glide.with(context)
                        .load(pathImage)
                        .placeholder(R.mipmap.loading_img)
                        .into(holder.photoDetail);

//                Picasso.with(context)
//                        .load(pathImage)
//                        .placeholder(R.mipmap.loading_img)
////                        .centerCrop()
//                        .fit()
//                        .into(holder.photoDetail);
            }catch (Exception e){
                e.printStackTrace();
                try {
                    String pathImage = item.optJSONArray("order_product_detail").getJSONObject(0).optJSONObject("image").optString("large");

                    Glide.with(context)
                            .load(pathImage)
                            .placeholder(R.mipmap.loading_img)
                            .into(holder.photoDetail);

                }catch (Exception ee){
                    ee.printStackTrace();
                }
//                Picasso.with(context)
//                        .load(pathImage)
//                        .placeholder(R.mipmap.loading_img)
//                        .fit()
//                        .into(holder.photoDetail);

            }



        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void setToast(String test) {
        Toast.makeText(context, test, Toast.LENGTH_SHORT).show();
    }

    private void setLog(String message) {
        Log.e("TEST APLIKASI",message);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public void setValues(ArrayList<JSONObject> values) {
        this.values = values;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.productTitle)TextView productTitle;
        @BindView(R.id.title)TextView title;
        @BindView(R.id.productPrice)TextView price;
        @BindView(R.id.productQty)TextView qty;
        @BindView(R.id.lineGrone)LinearLayout lineGrone;
        @BindView(R.id.photoDetail)ImageView photoDetail;
        @BindView(R.id.qtyPhoto)TextView getQty;
        @BindView(R.id.addToCart)Button addToCart;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface setOnClikLikAdapter{
        void onclik(JSONObject jsonObject,int position);
    }

}
