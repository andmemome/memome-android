package com.triodigitalagency.memome.adapter;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.utils.ImageViewFragment;

import java.util.List;

public class ImageViewAdapter extends FragmentPagerAdapter {

    private Activity activity;
    private List<String> imagePaths;
    private int total;

    public ImageViewAdapter(FragmentManager fm, Activity activity, List<String> imagePaths) {
        super(fm);
        this.activity   = activity;
        this.imagePaths = imagePaths;
        this.total      = imagePaths.size();
    }

    @Override
    public Fragment getItem(int position) {
        ImageViewFragment fragment = ImageViewFragment.getInstance(position, imagePaths.get(position));
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

    @Override
    public int getCount() {
        return total;
    }
}
