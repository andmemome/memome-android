package com.triodigitalagency.memome.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.triodigitalagency.memome.R;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by khoiron on 02/04/18.
 */

public class RecyclerAdapterDialogKurir extends RecyclerView.Adapter<RecyclerAdapterDialogKurir.MyViewHolder> {

    private Context mContext;
    Typeface type;

    public void setValues(ArrayList<JSONObject> values) {
        this.values = values;
        notifyDataSetChanged();
    }

    private ArrayList<JSONObject> values = new ArrayList<>();
    public onClickCheckoutAdapter onClickCheckoutAdapter;

    public RecyclerAdapterDialogKurir(Context mContext) {
        this.mContext = mContext;
    }

    public void setOnClickCheckoutAdapter(RecyclerAdapterDialogKurir.onClickCheckoutAdapter onClickCheckoutAdapter) {
        this.onClickCheckoutAdapter = onClickCheckoutAdapter;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_dialog_kurir, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        JSONObject item                             = values.get(position);
        holder.lokasi.setText(item.optString("manifest_description").toLowerCase());
        holder.jam.setText(item.optString("manifest_time").toUpperCase());
        holder.tanggal.setText(item.optString("manifest_date").toUpperCase());

    }

    private void setLog(String s) {
        Log.e("Test App",s);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.lokasi)TextView lokasi;
        @BindView(R.id.tanggal)TextView tanggal;
        @BindView(R.id.jam)TextView jam;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface onClickCheckoutAdapter{
        void onClick(int holder, int position);
    }
}
