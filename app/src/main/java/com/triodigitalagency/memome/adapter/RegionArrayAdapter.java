package com.triodigitalagency.memome.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.volley.utils.VolleyRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RegionArrayAdapter extends BaseAdapter {
    private final Context context;
    private ArrayList<JSONObject> values;
    private VolleyRequest request;
    private Long selectedId;

    public RegionArrayAdapter(Context context){
        this.context    = context;
        this.values     = new ArrayList<JSONObject>();
        this.request    = new VolleyRequest((Activity)context);
    }

    @Override
    public int getCount() {
        return this.values.size();
    }

    @Override
    public Object getItem(int position) {
        return this.values.get(position);
    }

    @Override
    public long getItemId(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getLong("id");
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public String getItemName(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getString("name");
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view               = inflater.inflate(R.layout.adapter_region, parent, false);
        TextView name           = (TextView)view.findViewById(R.id.textViewName);
        JSONObject item         = values.get(position);

        try {
            name.setText(item.getString("name").toUpperCase());
            ImageView imageViewCheck  = (ImageView)view.findViewById(R.id.imageViewCheck);
            if (this.selectedId != null && this.selectedId > 0 && this.selectedId == item.getLong("id")) {
                imageViewCheck.setVisibility(View.VISIBLE);
            }
            else {
                imageViewCheck.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }

    public void setValues(ArrayList<JSONObject> values){
        this.values = values;
    }

    public void setSelectedId(Long id) { this.selectedId = id; }
}
