package com.triodigitalagency.memome.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.utils.VolleyRequest;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class UserAddressArrayAdapter extends BaseAdapter {
    private final Context context;
    private ArrayList<JSONObject> values;
    private VolleyRequest request;
    private Boolean canEdit = true;
    private Long selectedId;

    public UserAddressArrayAdapter(Context context){
        this.context    = context;
        this.values     = new ArrayList<JSONObject>();
        this.request    = new VolleyRequest((Activity)context);
    }

    @Override
    public int getCount() {
        return this.values.size();
    }

    @Override
    public Object getItem(int position) {
        return this.values.get(position);
    }

    @Override
    public long getItemId(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getLong("id");
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view               = inflater.inflate(R.layout.adapter_user_address, parent, false);
        TextView textView1      = (TextView)view.findViewById(R.id.textView1);
        TextView textView2      = (TextView)view.findViewById(R.id.textView2);
        ImageView imageViewCheck  = (ImageView)view.findViewById(R.id.imageViewCheck);
        ImageView imageViewCheck2 = (ImageView)view.findViewById(R.id.imageViewCheck2);
        JSONObject item         = values.get(position);


        if (position== Globals.getIntDataPreference(context,"posisi")){
            if (selectedId==null){
                imageViewCheck2.setVisibility(View.VISIBLE);
            }else {
                imageViewCheck2.setVisibility(View.GONE);
            }
        }

        if (position >= 0) {
            view.findViewById(R.id.textViewEdit).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ListView)parent).performItemClick(v, position, 0); // Let the event be handled in onItemClick()
                }
            });
        }

        if (this.canEdit) {
            view.findViewById(R.id.textViewEdit).setVisibility(View.VISIBLE);
        }

        try {
            String firstName    = item.getString("first_name");
            String lastName     = item.getString("last_name");
            String address      = item.getString("address");
            String province     = item.getString("provinceName");
            String city         = item.getString("cityName");
            String postalCode   = item.getString("postal_code");
            String phone        = item.getString("phone");
            String email        = item.getString("email");

            String name         = firstName;
            if (!lastName.equals("")) {
                name            += " " + lastName;
            }

            textView1.setText(name);
            textView2.setText(address + "\n" + city + ", " + province + " - " + postalCode + "\n" + phone + " / " + email);


            if (this.selectedId != null && this.selectedId > 0 && this.selectedId == item.getLong("id")) {
                imageViewCheck.setVisibility(View.VISIBLE);

            }
            else {
                imageViewCheck.setVisibility(View.GONE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }

    public String getItemFirstName(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getString("first_name");
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getItemLastName(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getString("last_name");
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getItemAddress(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getString("address");
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getItemProvinceName(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getString("provinceName");
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getItemCityName(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getString("cityName");
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public long getItemProvinceId(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getLong("province_id");
        } catch (JSONException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public long getItemCityId(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getLong("city_id");
        } catch (JSONException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public String getItemPostalCode(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getString("postal_code");
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getItemPhone(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getString("phone");
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getItemEmail(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getString("email");
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public void setValues(ArrayList<JSONObject> values){
        this.values = values;
    }

    public void setCanEdit(Boolean value) { this.canEdit = value; }

    public void setSelectedId(Long id) { this.selectedId = id; }
}
