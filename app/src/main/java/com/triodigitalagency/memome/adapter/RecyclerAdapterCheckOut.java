package com.triodigitalagency.memome.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.database.Cart;
import com.triodigitalagency.memome.database.DatabaseHandler;
import com.triodigitalagency.memome.database.ProjectDetail;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.utils.NumberTextWatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by khoiron on 02/04/18.
 */

public class RecyclerAdapterCheckOut extends RecyclerView.Adapter<RecyclerAdapterCheckOut.MyViewHolder> {

    private Context mContext;
    Typeface type;
    private ArrayList<JSONObject> values = new ArrayList<>();
    onClickCheckoutAdapter onClickCheckoutAdapter;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_cart2, parent, false);

        return new MyViewHolder(itemView);
    }

    public RecyclerAdapterCheckOut(Context mContext, ArrayList<JSONObject> values,onClickCheckoutAdapter onClickCheckoutAdapter) {
        this.mContext = mContext;
        this.values = values;
        this.onClickCheckoutAdapter = onClickCheckoutAdapter;
    }

    @SuppressLint("StringFormatMatches")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        JSONObject item                             = values.get(position);
        holder.imageView.getLayoutParams().width    = (int) Globals.convertDpToPixel(130, mContext); // 130dp, manual from layout
        int positionn = position +1;
        if (positionn==values.size()){
            holder.lineView.setVisibility(View.GONE);
        }

        type = Typeface.createFromAsset(mContext.getAssets(), "fonts/montserrat-light.otf");
        holder.title.setTypeface(type);
        holder.subtitle.setTypeface(type);
        holder.textPrice.setTypeface(type);
        holder.textProductTitle.setTypeface(type);
        holder.textTotalPhoto.setTypeface(type);

        holder.relativeLayoutTitleArea.setVisibility(View.VISIBLE);
        holder.buttonTextQtyCart.setVisibility(View.VISIBLE);
        holder.buttonMinusCart.setVisibility(View.VISIBLE);
        holder.buttonAddCart.setVisibility(View.VISIBLE);
        holder.buttonDeleteCart.setVisibility(View.GONE);

        holder.buttonMinusCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickCheckoutAdapter.onClick(holder.buttonMinusCart.getId(),position);
            }
        });
        holder.buttonAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickCheckoutAdapter.onClick(holder.buttonAddCart.getId(),position);
            }
        });


        try {
            long CoverWidth             = item.getLong("cover_width");
            long CoverHeight            = item.getLong("cover_height");
            long CoverContentWidth      = item.getLong("cover_content_width");
            long CoverContentHeight     = item.getLong("cover_content_height");
            long CoverBorderLeft        = item.getLong("cover_border_left");
            long CoverBorderTop         = item.getLong("cover_border_top");
            long CoverTitleAreaWidth    = item.getLong("cover_title_area_width");
            long CoverTitleAreaHeight   = item.getLong("cover_title_area_height");
            long CoverTitleAreaTop      = item.getLong("cover_title_area_top");
            long CoverTitleAreaLeft     = item.getLong("cover_title_area_left");
            long CoverTitleFontSize     = item.getLong("cover_title_font_size");
            long CoverSubtitleFontSize  = item.getLong("cover_subtitle_font_size");
            long CoverTitleTop          = item.getLong("cover_title_top");
            long CoverSubtitleTop       = item.getLong("cover_subtitle_top");



            holder.title.setText(item.getString("cover_title"));
            holder.subtitle.setText(item.getString("cover_subtitle"));


            EditText tempTextPrice      = new EditText(mContext);
            tempTextPrice.addTextChangedListener(new NumberTextWatcher(tempTextPrice));
            tempTextPrice.setText(String.valueOf((int) item.getLong("special_price") > 0 ? item.getLong("special_price") : item.getLong("price")));
            holder.textPrice.setText(tempTextPrice.getText().toString());

            holder.textProductTitle.setText(!item.getString("cover_title").equals("") ? "\"" + item.getString("cover_title").trim() + "\"" : item.getString("title"));

//            if ("14".equals(item.getString("product_id"))){
//                holder.textTotalPhoto.setText(String.format(mContext.getResources().getString(R.string.text_total_detail), item.getInt("total_detail") + (CoverContentWidth > 0 ? 0 : 0)));
//            }else {
                holder.textTotalPhoto.setText(String.format(mContext.getResources().getString(R.string.text_total_detail), item.getInt("total_detail") + (CoverContentWidth > 0 ? 1 : 0)));
//            }
//

            /*setLog("==============+++++++ "+item.optString("count_photo"));
            setLog("==============+++++-- "+item.optString("total_detail"));*/


            if (item.getInt("corporate_voucher_qty") > 0) {
                holder.textTotalPhoto.setText(holder.textTotalPhoto.getText().toString() + "\n" + String.format(mContext.getResources().getString(R.string.text_corporate_cart), item.getInt("corporate_voucher_qty")));
            }

            holder.buttonTextQtyCart.setText(String.valueOf(item.getInt("cart_qty")));


/*
            view.findViewById(R.id.buttonDeleteCart).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ListView)parent).performItemClick(v, position, 0); // Let the event be handled in onItemClick()
                }
            });
*/
            Globals.drawProductWithTitleLayout(mContext, CoverWidth, CoverHeight, holder.imageView, holder.imageViewContent, CoverContentWidth,
                    CoverContentHeight, CoverBorderLeft, CoverBorderTop,
                    holder.relativeLayoutTitleArea, CoverTitleAreaWidth, CoverTitleAreaHeight, CoverTitleAreaTop, CoverTitleAreaLeft,
                    CoverTitleFontSize, CoverSubtitleFontSize, CoverTitleTop, CoverSubtitleTop, holder.title, holder.subtitle);

            String pathImageCrop            = item.getString("path_image_crop");
            String pathImageOriginal        = item.getString("path_image_original");

            Glide.with(mContext)
                    .load(pathImageCrop)
                    .placeholder(R.mipmap.loading_img)
                    .into(holder.imageViewContent);

//            Picasso.with(mContext)
//                    .load(pathImageCrop)
//                    .placeholder(R.mipmap.loading_img)
//                    .fit()
//                    .into(holder.imageViewContent);


            setLog("pathImageCrop "+pathImageCrop);

//            if (item.has("images")  && !item.isNull("images")) {
//                JSONObject images   = item.getJSONObject("images");
//                Glide.with(mContext)
//                        .load(images.getString("thumb"))
//                        .placeholder(R.mipmap.loading_img)
//                        .into(holder.imageView);
//                setLog("image "+images.getString("thumb"));
//            }else {
//                try {
//                    viewImage(position,holder.imageView);
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//
//            }

            if (item.has("images")  && !item.isNull("images")) {
                JSONObject images   = item.getJSONObject("images");
                Globals.setLog("image ==>"+images.toString());
                try {
                    Glide.with(mContext)
                            .load(images.getString("thumb"))
//                            .placeholder(R.mipmap.loading_img)
                            .into(holder.imageView);
                }catch (Exception e){
                    e.printStackTrace();
                    viewImage(position,holder.imageView,holder.imageViewContent);
                }
            }else {
                Globals.setLog("image null");
                try {
                    viewImage(position,holder.imageView,holder.imageViewContent);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setLog(String s) {
        Log.e("Test App",s);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.imageViewProductReview)ImageView imageView;
        @BindView(R.id.imageViewProductReviewContent)ImageView imageViewContent;
        @BindView(R.id.relativeLayoutTitleArea)RelativeLayout relativeLayoutTitleArea;

        @BindView(R.id.buttonTextQtyCart)Button buttonTextQtyCart;
        @BindView(R.id.buttonMinusCart)Button buttonMinusCart;
        @BindView(R.id.buttonAddCart)Button buttonAddCart;
        @BindView(R.id.buttonDeleteCart)ImageView buttonDeleteCart;

        @BindView(R.id.editTextTitle)EditText title;
        @BindView(R.id.editTextSubtitle)EditText subtitle;
        @BindView(R.id.textCartPrice)TextView textPrice;
        @BindView(R.id.textProductTitle)TextView textProductTitle;
        @BindView(R.id.textTotalPhoto)TextView textTotalPhoto;
        @BindView(R.id.lineView)LinearLayout lineView;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface onClickCheckoutAdapter{
        void onClick(int holder, int position);
    }

    protected void viewImage(int projectId, ImageView imageView,ImageView imageViewContent) {
        DatabaseHandler db = new DatabaseHandler(mContext);
        List<Cart> carts   = db.getAllCarts();
        List<ProjectDetail> projectDetails = db.getAllProjectDetailsByProjectId(carts.get(projectId).getProjectId());

        if (projectDetails.get(0).getPathImageOriginal().substring(0,5).equals("https")){
            Globals.setLog("image file ==>"+projectDetails.get(0).getPathImageOriginal());
            Globals.setLog(projectDetails.get(0).getPathImageOriginal().substring(0,5));

            Glide.with(mContext)
                    .load(projectDetails.get(0).getPathImageOriginal())
                    .placeholder(R.mipmap.loading_img)
                    .centerCrop()
                    .into(imageView);

//            Picasso.with(mContext)
//                    .load(projectDetails.get(0).getPathImageOriginal())
//                    .placeholder(R.mipmap.loading_img)
////                    .centerCrop()
//                    .fit()
//                    .into(imageView);

//            Picasso.with(mContext)
//                    .load(projectDetails.get(0).getPathImageOriginal())
//                    .placeholder(R.mipmap.loading_img)
//                    .fit()
//                    .into(imageViewContent);

            Glide.with(mContext)
                    .load(projectDetails.get(0).getPathImageOriginal())
                    .placeholder(R.mipmap.loading_img)
                    .into(imageViewContent);
        }else {
            File file = new File(projectDetails.get(0).getPathImageOriginal());
            Uri imageUri = Uri.fromFile(file);

//            Picasso.with(mContext)
//                    .load(imageUri)
//                    .placeholder(R.mipmap.loading_img)
////                    .centerCrop()
//                    .fit()
//                    .into(imageView);
//
            Glide.with(mContext)
                    .load(imageUri)
                    .placeholder(R.mipmap.loading_img)
                    .centerCrop()
                    .into(imageView);
        }


       /* if (projectDetails.get(0).getPathImageOriginal()!=null&&(projectDetails.get(0).getPathImageOriginal()!="")){


            File file = new File(projectDetails.get(0).getPathImageOriginal());
            Uri imageUri = Uri.fromFile(file);

            Glide.with(mContext)
                    .load(imageUri)
                    .placeholder(R.mipmap.loading_img)
                    .centerCrop()
                    .into(imageView);

        }*/

    }
}
