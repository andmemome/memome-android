package com.triodigitalagency.memome.adapter;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.user.CompletedFragment;
import com.triodigitalagency.memome.user.InProgressFragment;

public class MyPurchasesTabAdapter extends FragmentPagerAdapter {

    String[] tabs;
    Activity activity;

    public MyPurchasesTabAdapter(FragmentManager fm, Activity activity) {
        super(fm);
        this.activity   = activity;
        tabs            = this.activity.getResources().getStringArray(R.array.tab_strings_my_purchases);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new InProgressFragment();
        }
        return new CompletedFragment();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabs[position];
    }

    @Override
    public int getCount() {
        return 2;
    }
}
