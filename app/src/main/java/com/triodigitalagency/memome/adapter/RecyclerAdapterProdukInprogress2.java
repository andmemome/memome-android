package com.triodigitalagency.memome.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.utils.NumberTextWatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by khoiron on 02/04/18.
 */

public class RecyclerAdapterProdukInprogress2 extends RecyclerView.Adapter<RecyclerAdapterProdukInprogress2.MyViewHolder>{

    private Context context;
    public ArrayList<JSONObject> values = new ArrayList<>();

    public RecyclerAdapterProdukInprogress2(Context mContext) {
        this.context = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_purchase_product_inprogress2, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        JSONObject item         = values.get(position);
        holder.namaProduk.setText(item.optString("product_title"));
        holder.titleProduk.setText(item.optString("title"));
        holder.qtyProduk.setText(item.optString("qty"));
        holder.totalProduck.setText(Globals.currencyFormatIDR(Double.parseDouble(item.optString("total_price"))));
        setLog("## "+item.toString());

        Typeface custom_font;
        custom_font = Typeface.createFromAsset(context.getAssets(), "fonts/myriad.otf");
        holder.titleProduk.setTypeface(custom_font);
    }


    private void setToast(String test) {
        Toast.makeText(context, test, Toast.LENGTH_SHORT).show();
    }

    private void setLog(String message) {
        Log.e("TEST APLIKASI",message);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public void setValues(ArrayList<JSONObject> values) {
        this.values = values;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.namaProduk)TextView namaProduk;
        @BindView(R.id.titleProduk)TextView titleProduk;
        @BindView(R.id.qtyProduk)TextView qtyProduk;
        @BindView(R.id.totalProduck)TextView totalProduck;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}
