package com.triodigitalagency.memome.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.model.KurirModel;
import com.triodigitalagency.memome.transaction.ProgressActivity;
import com.triodigitalagency.memome.user.DialoKurirTransit;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.utils.NumberTextWatcher;
import com.triodigitalagency.memome.volley.utils.VolleyRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class MyPurchasesArrayAdapter extends BaseAdapter {
    private final Context context;
    private ArrayList<Bundle> values;
    private VolleyRequest request;
    private JSONObject transitKurir;
    private Long selectedId;

    public MyPurchasesArrayAdapter(Context context){
        this.context    = context;
        this.values     = new ArrayList<Bundle>();
        this.request    = new VolleyRequest((Activity)context);
    }

    @Override
    public int getCount() {
        return this.values.size();
    }

    @Override
    public Object getItem(int position) {
        return this.values.get(position);
    }

    @Override
    public long getItemId(int position) {
        try {
            Bundle data = (Bundle) this.getItem(position);
            JSONObject item = new JSONObject(data.getString("order"));
            return item.getLong("id");
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        LayoutInflater inflater     = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view                   = inflater.inflate(R.layout.adapter_my_purchases, parent, false);
        TextView textViewOrderId    = view.findViewById(R.id.textViewOrderId);
        LinearLayout ltitem2        = view.findViewById(R.id.ltitem2);
        LinearLayout ltitem1        = view.findViewById(R.id.ltitem1);
        TextView textViewCreatedAt  = view.findViewById(R.id.textViewCreatedAt);
        TextView total              = view.findViewById(R.id.total);
        Button btnReorder           = view.findViewById(R.id.btnReorder);
        TextView textViewStatus     = view.findViewById(R.id.textViewStatus);
        TextView textViewTotal      = view.findViewById(R.id.textViewTotal);
        ImageView imageExpand       = view.findViewById(R.id.image_expand);
        View separatorTop           = view.findViewById(R.id.separatorTop);
        LinearLayout listItem       = view.findViewById(R.id.list_item);
        RelativeLayout lineButton   = view.findViewById(R.id.lineButton);
        TextView textViewFirstTitle = view.findViewById(R.id.textViewFirstTitle);
        TextView textViewFirstValue = view.findViewById(R.id.textViewFirstValue);
        TextView textViewSecondTitle= view.findViewById(R.id.textViewSecondTitle);
        TextView textViewSecondValue= view.findViewById(R.id.textViewSecondValue);
        Button btnCheck             = view.findViewById(R.id.btn_check);
        View btnExpand              = view.findViewById(R.id.btn_expand);
        Button tryupload            = view.findViewById(R.id.tryupload);


        final Bundle data                 = values.get(position);
        JSONObject item             = null;
        final Boolean isExpanded          = data.getBoolean("isExpanded", false);
        try {
            item = new JSONObject(data.getString("order"));
            Log.e("--- ",item.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (item == null) {
            return view;
        }

            total.setText("Total ".toUpperCase()+item.optString("jumlah_order")+" Products".toUpperCase());
            if (data.get("expandData") != null) {
                ArrayList<Bundle> expandData = (ArrayList<Bundle>) data.getSerializable("expandData");

                if (expandData != null) {
                    for (Bundle eData: expandData) {
                        View eLayout = inflater.inflate(R.layout.layout_my_purchases_list, parent, false);
                        TextView title = eLayout.findViewById(R.id.textViewTitle);
                        TextView value = eLayout.findViewById(R.id.textViewValue);
                        TextView quantity = eLayout.findViewById(R.id.textViewQuantity);
                        TextView name = eLayout.findViewById(R.id.textViewName);

                        EditText tempEditText   = new EditText(context);
                        tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
                        tempEditText.setText(eData.getString("value", "0"));

                        title.setText(eData.getString("title", ""));
                        value.setText(String.format(view.getResources().getString(R.string.text_total_amount), tempEditText.getText().toString()));
                        quantity.setText(eData.get("quantity") != null ? String.format("x%s", eData.getString("quantity", "")) : "");
                        name.setText(eData.getString("name", ""));
                        name.setVisibility(View.GONE);

                        listItem.addView(eLayout);
                    }
                }
            }

            if (isExpanded) {
                separatorTop.setVisibility(View.VISIBLE);
                listItem.setVisibility(View.VISIBLE);
                imageExpand.setImageResource(R.drawable.icon_up);
            }
            else {
                separatorTop.setVisibility(View.GONE);
                listItem.setVisibility(View.GONE);
                imageExpand.setImageResource(R.drawable.icon_down);
            }

            btnExpand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    data.putBoolean("isExpanded", !isExpanded);
                    values.set(position, data);
                    notifyDataSetChanged();
                }
            });

//        try {
            SimpleDateFormat format     = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
            SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
            String orderId              = String.format(view.getResources().getString(R.string.text_order_id), String.valueOf(item.optString("number", "")));
            String createdAt            = item.optString("created_at", format.format(new Date()));
            try {
                Date date   = format.parse(createdAt);
                createdAt   = dateFormat.format(date);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            EditText tempEditText   = new EditText(context);
            tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
            tempEditText.setText(String.valueOf((int) item.optDouble("grand_total", 0)));

            textViewOrderId.setText(orderId);
            textViewCreatedAt.setText(createdAt);
            textViewStatus.setText(item.optString("status", "").toUpperCase());

            textViewFirstTitle.setTextColor(context.getResources().getColor(R.color.grey));
            textViewSecondTitle.setTextColor(context.getResources().getColor(R.color.grey));
            textViewFirstValue.setTextColor(context.getResources().getColor(R.color.black));
            textViewSecondValue.setTextColor(context.getResources().getColor(R.color.black));

            textViewFirstTitle.setVisibility(View.GONE);
            textViewSecondTitle.setVisibility(View.GONE);
            textViewFirstValue.setVisibility(View.GONE);
            textViewSecondValue.setVisibility(View.GONE);
            lineButton.setVisibility(View.GONE);
            btnReorder.setVisibility(View.GONE);

            if ("pending".equalsIgnoreCase(item.optString("status", ""))) {
                String dateString = item.optString("created_at", format.format(new Date()));
                Date date;
                try {
                    date   = format.parse(dateString);
                    dateString   = dateFormat2.format(date);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    date = new Date();
                }

                textViewStatus.setText("on progress".toUpperCase());
                textViewStatus.setTextColor(context.getResources().getColor(R.color.grey2));
                textViewFirstTitle.setText(R.string.est_production);
                textViewSecondTitle.setText(R.string.text_est_delivery_date);
                textViewFirstValue.setText(Globals.calculateShipping(date, 12, 16));
                textViewSecondValue.setText(Globals.calculateShipping(date, 13, 17));

                textViewFirstTitle.setVisibility(View.VISIBLE);
                textViewSecondTitle.setVisibility(View.GONE);
                textViewFirstValue.setVisibility(View.VISIBLE);
                textViewSecondValue.setVisibility(View.GONE);

            } else if ("process".equalsIgnoreCase(item.optString("status", ""))) {
                String dateString = item.optString("process_date", format.format(new Date()));
                Date date;
                try {
                    date   = format.parse(dateString);
                    dateString   = dateFormat2.format(date);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    date = new Date();
                }

                textViewStatus.setTextColor(context.getResources().getColor(R.color.kuning));
                textViewStatus.setText("in production".toUpperCase());
                textViewFirstTitle.setText(R.string.text_production_date);
                textViewSecondTitle.setText(R.string.text_est_delivery_date);
                textViewFirstValue.setText(dateString);
                textViewSecondValue.setText(Globals.calculateShipping(date, 13, 17));

                textViewFirstTitle.setVisibility(View.VISIBLE);
                textViewSecondTitle.setVisibility(View.VISIBLE);
                textViewFirstValue.setVisibility(View.VISIBLE);
                textViewSecondValue.setVisibility(View.VISIBLE);
            } else if ("shipping".equalsIgnoreCase(item.optString("status", ""))) {
                String shipping_number = item.optString("shipping_number", "");
                String dateString = item.optString("ship_date", format.format(new Date()));
                try {
                    Date date   = format.parse(dateString);
                    dateString   = dateFormat2.format(date);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                textViewStatus.setTextColor(context.getResources().getColor(R.color.green));
                textViewStatus.setText("Shipping".toUpperCase());
                textViewFirstTitle.setText(R.string.text_delivery_date);
                textViewSecondTitle.setText(R.string.text_tracking_code);
                textViewFirstValue.setText(dateString);
                textViewSecondValue.setText(shipping_number);

                textViewFirstTitle.setVisibility(View.VISIBLE);
                textViewSecondTitle.setVisibility(View.VISIBLE);
                textViewFirstValue.setVisibility(View.VISIBLE);
                textViewSecondValue.setVisibility(View.VISIBLE);
                lineButton.setVisibility(View.VISIBLE);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onClikItemListen.onClikItem(position,view.getId());
                    }
                });

                final JSONObject finalItem1 = item;
                btnCheck.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, DialoKurirTransit.class);
                        intent.putExtra("shipping",finalItem1.toString());
                        context.startActivity(intent);
                    }
                });

            } else if ("complete".equalsIgnoreCase(item.optString("status", ""))) {
                String dateString = item.optString("complete_date", format.format(new Date()));
                try {
                    Date date   = format.parse(dateString);
                    dateString   = dateFormat2.format(date);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                textViewStatus.setTextColor(context.getResources().getColor(R.color.blue));
                textViewFirstTitle.setText(R.string.text_received_on);
                textViewFirstValue.setText(dateString);

                textViewFirstTitle.setTextColor(context.getResources().getColor(R.color.blue));
                textViewFirstValue.setTextColor(context.getResources().getColor(R.color.blueDark));

                textViewFirstTitle.setVisibility(View.VISIBLE);
                textViewFirstValue.setVisibility(View.VISIBLE);
                btnReorder.setVisibility(View.VISIBLE);


                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onClikItemGJ.onClikItem(position);
                    }
                });

                if("1".equals(item.optString("keterangan_expired"))){
                    btnReorder.setVisibility(View.GONE);
                }

                final JSONObject finalItem = item;
                btnReorder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onClikItemGJ.onClikReorder(finalItem.optString("id"));
                    }
                });

            } else if ("failed".equalsIgnoreCase(item.optString("status", ""))) {
                String dateString = item.optString("canceled_at", format.format(new Date()));


                try {
                    Date date   = format.parse(dateString);
                    dateString   = dateFormat2.format(date);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                textViewStatus.setText("canceled".toUpperCase());
                textViewStatus.setTextColor(context.getResources().getColor(R.color.redDark));
                textViewFirstTitle.setText(R.string.text_canceled_on);

                textViewFirstTitle.setTextColor(context.getResources().getColor(R.color.red));
                textViewFirstValue.setTextColor(context.getResources().getColor(R.color.redDark));

                textViewFirstTitle.setVisibility(View.VISIBLE);
                textViewFirstValue.setVisibility(View.VISIBLE);
                btnCheck.setVisibility(View.GONE);


            }else if("upload failed".equalsIgnoreCase(item.optString("status", ""))){

                Log.e("Test ","okeeeee");
                tryupload.setVisibility(View.VISIBLE);
                textViewStatus.setTextColor(context.getResources().getColor(R.color.red));
                lineButton.setVisibility(View.VISIBLE);
                btnCheck.setVisibility(View.GONE);


/*
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onClikItemListen.onClikItem(position,view.getId());
                    }
                });*/

                tryupload.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(Globals.getDataPreference(context,"uploadfile")){
                            context.startActivity(new Intent(context,ProgressActivity.class));
                        }
                    }
                });

            }else if("canceled".equals(item.optString("status", ""))) {

                String dateString =  item.optString("canceled_date", format.format(new Date()));

                try {
                    Date date   = format.parse(dateString);
                    dateString   = dateFormat2.format(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                textViewStatus.setText("cancelled".toUpperCase());
                textViewStatus.setTextColor(context.getResources().getColor(R.color.redDark));
                textViewFirstTitle.setText(R.string.text_canceled_on);

                textViewSecondTitle.setTextColor(context.getResources().getColor(R.color.redDark));
                textViewSecondValue.setTextColor(context.getResources().getColor(R.color.redDark));

                ltitem2.setVisibility(View.VISIBLE);
                lineButton.setVisibility(View.VISIBLE);
                textViewFirstTitle.setVisibility(View.VISIBLE);
                textViewSecondTitle.setVisibility(View.VISIBLE);
                textViewFirstTitle.setText("REASONS");
                textViewSecondTitle.setText("CANCELLED ON");
                textViewFirstValue.setText(item.optString("reason_for_cancel"));
                textViewSecondValue.setText(dateString);


                textViewSecondValue.setVisibility(View.VISIBLE);
                textViewFirstTitle.setVisibility(View.VISIBLE);
                textViewFirstValue.setVisibility(View.VISIBLE);
                lineButton.setVisibility(View.GONE);

            }else if("cancelled".equals(item.optString("status", ""))) {

                String dateString =  item.optString("canceled_date", format.format(new Date()));

                try {
                    Date date   = format.parse(dateString);
                    dateString   = dateFormat2.format(date);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                textViewStatus.setText("cancelled".toUpperCase());
                textViewStatus.setTextColor(context.getResources().getColor(R.color.redDark));
                textViewFirstTitle.setText(R.string.text_canceled_on);

                textViewSecondTitle.setTextColor(context.getResources().getColor(R.color.redDark));
                textViewSecondValue.setTextColor(context.getResources().getColor(R.color.redDark));

                ltitem2.setVisibility(View.VISIBLE);
                lineButton.setVisibility(View.VISIBLE);
                textViewFirstTitle.setVisibility(View.VISIBLE);
                textViewSecondTitle.setVisibility(View.VISIBLE);
                textViewFirstTitle.setText("REASONS");
                textViewSecondTitle.setText("CANCELLED ON");
                textViewFirstValue.setText(item.optString("reason_for_cancel"));
                textViewSecondValue.setText(dateString);


                textViewSecondValue.setVisibility(View.VISIBLE);
                textViewFirstTitle.setVisibility(View.VISIBLE);
                textViewFirstValue.setVisibility(View.VISIBLE);
                lineButton.setVisibility(View.GONE);

            }else {
                textViewStatus.setTextColor(context.getResources().getColor(R.color.blue));
            }

            textViewTotal.setText(String.format(view.getResources().getString(R.string.text_total_amount), tempEditText.getText().toString()));


            ImageView imageViewCheck  = (ImageView)view.findViewById(R.id.imageViewCheck);
            if (this.selectedId != null && this.selectedId > 0 && this.selectedId == item.optLong("id", 0)) {
                imageViewCheck.setVisibility(View.VISIBLE);
            }
            else {
                imageViewCheck.setVisibility(View.GONE);
            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

        try {
            transitKurir = item.getJSONObject("waybill");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }

    private void setLog(String test) {
        Log.e("TAG",test);
    }

    public onClikItemGJ onClikItemGJ;
    public onClikItemListen onClikItemListen;

    public void setOnClikItemListen(MyPurchasesArrayAdapter.onClikItemListen onClikItemGJ) {
        this.onClikItemListen = onClikItemGJ;
    }

    public void setOnClikItemGJ(MyPurchasesArrayAdapter.onClikItemGJ onClikItemGJ) {
        this.onClikItemGJ = onClikItemGJ;
    }

    public interface onClikItemGJ{
        void onClikItem(int position);
        void onClikReorder(String id);
    }

    public interface onClikItemListen{
        void onClikItem(int position,int id_item);
    }


    public Double getGrandTotal(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getDouble("grand_total");
        } catch (JSONException e) {
            e.printStackTrace();
            return Double.valueOf(0);
        }
    }

    public void setValues(ArrayList<Bundle> values){
        this.values = values;
    }

    public void setSelectedId(Long id) { this.selectedId = id; }
}
