package com.triodigitalagency.memome.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.Point;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.NumberTextWatcher;
import com.triodigitalagency.memome.volley.utils.VolleyRequest;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by khoiron on 02/04/18.
 */

public class ProductArrayAdapterr extends RecyclerView.Adapter<ProductArrayAdapterr.MyViewHolder> {

    private final Context mContext;



    private ArrayList<JSONObject> values;
    private VolleyRequest request;
    public void setValues(ArrayList<JSONObject> values) {
        this.values = values;
        notifyDataSetChanged();
    }

    public ProductArrayAdapterr(Context mContext) {
        this.mContext = mContext;
    }

    public onClickAdapter onClickAdapter;


    public void setOnClickAdapter(ProductArrayAdapterr.onClickAdapter onClickCheckoutAdapter) {
        this.onClickAdapter = onClickCheckoutAdapter;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_product, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        JSONObject item                             = values.get(position);
        setLog("-->>>"+item.toString());

        try {
            holder.title.setText(item.getString("title").toUpperCase());
            holder.size.setText(item.getString("size")+" cm");
            holder.description.setText(item.getString("info"));
//            request.getImageFromUrl(item.getString("front_images"), imageView, 0);
            if (item.has("max_image")) {
                if (item.has("min_image")) {
                    if (item.getString("min_image").equals(item.getString("max_image"))){
                        if (item.getString("min_image").equals("0")&&item.getString("max_image").equals("0")){
                            holder.photos.setText("1 Photos");
                        }else {
                            holder.photos.setText(item.getString("max_image")+" Photos");
                        }
                    }else {
                        holder.photos.setText(item.getString("min_image") + " - " +item.getString("max_image")+" Photos");
                    }
                }else {
                    holder.photos.setText(item.getString("max_image")+" Photos");
                }
            }
            else {
                holder.photos.setText("");
            }
            if (item.has("price")) {
                EditText tempEditText               = new EditText(mContext);
                tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
                tempEditText.setText(String.valueOf(item.getLong("price")));
                holder.salePrice.setText("IDR " + tempEditText.getText());
                holder.price.setText("IDR " + tempEditText.getText());
            }

            if (item.has("special_price") && item.getLong("special_price") > 0) {
                EditText tempEditText                       = new EditText(mContext);
                tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
                tempEditText.setText(String.valueOf(item.getLong("special_price")));
                holder.salePrice.setVisibility(View.VISIBLE);
                holder.salePrice.setPaintFlags(holder.salePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.price.setText("IDR " + tempEditText.getText());
            }
            else {
                holder.salePrice.setVisibility(View.GONE);
            }

            holder.btn_check.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickAdapter.onClick(holder.btn_check.getId(),position);
                }
            });



            /*Glide.with(mContext)
                    .load(item.getString("front_images"))
                    .placeholder(R.mipmap.loading_img)
                    .centerCrop()
                    .into(holder.imageView);*/

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    setLog(holder.itemView.getId()+" ---- ");
                    onClickAdapter.onClick(holder.itemView.getId(),position);
                }
            });


            ArrayList<String> ImagesArray = new ArrayList<>();
            ImagesArray.add(item.optString("front_images"));
            try {
                for (int i=0;i<item.optJSONArray("gallery_images").length();i++){
                    ImagesArray.add(item.optJSONArray("gallery_images").optJSONObject(i).optString("large"));
                }
            }catch (Exception e){
                e.printStackTrace();
            }

            SlidingImage_Adapter adapter = new SlidingImage_Adapter(mContext,ImagesArray);
            holder.mPager.setAdapter(adapter);
            holder.circle.setViewPager(holder.mPager);
            adapter.notifyDataSetChanged();

            Display display = ((Activity)mContext).getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;
            int height = width;

            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(width,height);
            holder.mPager.setLayoutParams(lp);
//            LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(width,holder.;
//            holder.relativeLayout.setLayoutParams(lp1);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setLog(String s) {
        Log.e("Test App",s);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }


    public int getCount() {
        return this.values.size();
    }

    public Object getItem(int position) {
        return this.values.get(position);
    }

    @Override
    public long getItemId(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getLong("id");
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.productTitle)TextView title;
        @BindView(R.id.productTotalPhotos)TextView photos;
        @BindView(R.id.productSize)TextView size;
        @BindView(R.id.productDescription)TextView description;
        @BindView(R.id.productSalePrice)TextView salePrice;
        @BindView(R.id.productPrice)TextView price;
        @BindView(R.id.viewIndicatorProductBanner)CirclePageIndicator circle;
//        @BindView(R.id.imageViewProduct)ImageView imageView;
        @BindView(R.id.btn_check)TextView btn_check;
        @BindView(R.id.line1)RelativeLayout relativeLayout;
        @BindView(R.id.pager)ViewPager mPager;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface onClickAdapter{
        void onClick(int holder, int position);
    }

}
