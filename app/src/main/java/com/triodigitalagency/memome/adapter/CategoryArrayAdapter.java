package com.triodigitalagency.memome.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.utils.VolleyRequest;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class CategoryArrayAdapter extends BaseAdapter {
    private final Context context;
    private ArrayList<JSONObject> values;
    private VolleyRequest request;
    private int height = 0;

    public CategoryArrayAdapter(Context context){
        this.context    = context;
        this.values     = new ArrayList<JSONObject>();
        this.request    = new VolleyRequest((Activity)context);
    }

    @Override
    public int getCount() {
        return this.values.size();
    }

    @Override
    public Object getItem(int position) {
        return this.values.get(position);
    }

    @Override
    public long getItemId(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getLong("id");
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view               = inflater.inflate(R.layout.adapter_category, parent, false);
        TextView title          = view.findViewById(R.id.productTitle);
        final ImageView imageView     = view.findViewById(R.id.imageViewProduct);
//        ViewPager viewPager     = view.findViewById(R.id.pager);
        JSONObject item         = values.get(position);
        setLog(""+item.toString());

        try {
            title.setText(item.getString("name").toUpperCase());
//            request.getImageFromUrl(item.getString("front_images"), imageView, 0);

            Glide.with(context)
                    .load(item.getString("photo"))
                    .placeholder(R.mipmap.loading_img)
                    .into(imageView);

//            Picasso.with(context)
//                    .load(item.getString("photo"))
//                    .placeholder(R.mipmap.loading_img)
//                    .fit()
//                    .into(imageView);


            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, height);
            setLog(height+"--");
            imageView.setLayoutParams(layoutParams);
//            imageView.getLayoutParams().height = ;


            /*
            ArrayList<String> ImagesArray = new ArrayList<>();
            for (int i=0;i<item.optJSONArray("gallery_images").length();i++){
                ImagesArray.add(item.optJSONArray("gallery_images").optJSONObject(i).optString("large"));
            }

            SlidingImage_Adapter adapter = new SlidingImage_Adapter(context,ImagesArray);
            viewPager.setAdapter(adapter);
            adapter.notifyDataSetChanged();*/

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }

    private void setLog(String s) {
        Log.e("--> ",s);
    }
    public void setValues(ArrayList<JSONObject> values){
        this.values = values;
//        this.height = height;
    }

}
