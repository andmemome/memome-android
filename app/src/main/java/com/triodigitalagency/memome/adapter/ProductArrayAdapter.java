package com.triodigitalagency.memome.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.NumberTextWatcher;
import com.triodigitalagency.memome.volley.utils.VolleyRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ProductArrayAdapter extends BaseAdapter {
    private final Context context;
    private ArrayList<JSONObject> values;
    private VolleyRequest request;

    public ProductArrayAdapter(Context context){
        this.context    = context;
        this.values     = new ArrayList<JSONObject>();
        this.request    = new VolleyRequest((Activity)context);
    }

    @Override
    public int getCount() {
        return this.values.size();
    }

    @Override
    public Object getItem(int position) {
        return this.values.get(position);
    }

    @Override
    public long getItemId(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getLong("id");
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view               = inflater.inflate(R.layout.adapter_product, parent, false);
        TextView title          = view.findViewById(R.id.productTitle);
        TextView photos         = view.findViewById(R.id.productTotalPhotos);
        TextView size           = view.findViewById(R.id.productSize);
        TextView description    = view.findViewById(R.id.productDescription);
        TextView salePrice      = view.findViewById(R.id.productSalePrice);
        TextView price          = view.findViewById(R.id.productPrice);
        ImageView imageView     = view.findViewById(R.id.imageViewProduct);
        JSONObject item         = values.get(position);
        setLog(""+item.toString());

        try {
            title.setText(item.getString("title").toUpperCase());
            size.setText(item.getString("size")+" cm");
            description.setText(item.getString("info"));
//            request.getImageFromUrl(item.getString("front_images"), imageView, 0);
            if (item.has("max_image")) {
                if (item.has("min_image")) {
                    if (item.getString("min_image").equals(item.getString("max_image"))){
                        photos.setText(item.getString("min_image")+" Photos");
                    }else {
                        photos.setText(item.getString("min_image") + " - " +item.getString("max_image")+" Photos");
                    }
                }
                else {
                    photos.setText(item.getString("max_image")+" Photos");
                }
            }
            else {
                photos.setText("");
            }
            if (item.has("price")) {
                EditText tempEditText               = new EditText(context);
                tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
                tempEditText.setText(String.valueOf(item.getLong("price")));
                salePrice.setText("IDR " + tempEditText.getText());
                price.setText("IDR " + tempEditText.getText());
            }

            if (item.has("special_price") && item.getLong("special_price") > 0) {
                EditText tempEditText                       = new EditText(context);
                tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
                tempEditText.setText(String.valueOf(item.getLong("special_price")));
                salePrice.setVisibility(View.VISIBLE);
                salePrice.setPaintFlags(salePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                price.setText("IDR " + tempEditText.getText());
            }
            else {
                salePrice.setVisibility(View.GONE);
            }

            Glide.with(context)
                    .load(item.getString("front_images"))
                    .placeholder(R.mipmap.loading_img)
                    .into(imageView);

//            Picasso.with(context)
//                    .load(item.getString("front_images"))
//                    .placeholder(R.mipmap.loading_img)
//                    .fit()
//                    .into(imageView);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }

    private void setLog(String s) {
        Log.e("===> ",s);
    }

    public void setValues(ArrayList<JSONObject> values){
        this.values = values;
    }

}

