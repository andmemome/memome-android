package com.triodigitalagency.memome.adapter;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.triodigitalagency.memome.product.ProjectFragment;

import org.json.JSONObject;

import java.util.List;

public class ProjectAdapter extends FragmentPagerAdapter {

    Activity activity;
    protected int total;
    protected List<JSONObject> products;

    public ProjectAdapter(FragmentManager fm, Activity activity, List<JSONObject> products) {
        super(fm);
        this.activity   = activity;
        this.total      = products.size();
        this.products   = products;
    }

    @Override
    public Fragment getItem(int position) {
        ProjectFragment pFragment = ProjectFragment.getInstance(position, this.products.get(position));
        return pFragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

    @Override
    public int getCount() {
        return total;
    }
}
