package com.triodigitalagency.memome.adapter;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.triodigitalagency.memome.tutorial.TutorialFragment;
import org.json.JSONArray;

public class TutorialAdapter extends FragmentPagerAdapter {

    Activity activity;
    JSONArray jsonArray;

    public TutorialAdapter(FragmentManager fm, Activity activity,JSONArray jsonArrays) {
        super(fm);
        this.activity   = activity;
        jsonArray = jsonArrays;
    }

    @Override
    public Fragment getItem(int position) {

        TutorialFragment tFragment = TutorialFragment.getInstance(position,jsonArray);
        return tFragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

    @Override
    public int getCount() {
        return jsonArray.length();
    }

}
