package com.triodigitalagency.memome.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.utils.VolleyRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ProjectReviewArrayAdapter extends BaseAdapter {
    private final String TAG = ProjectReviewArrayAdapter.class.getSimpleName();
    private final Context context;
    private ArrayList<JSONObject> values;
    private ArrayList<Boolean> isResGood;
    private VolleyRequest request;

    public ProjectReviewArrayAdapter(Context context){
        this.context    = context;
        this.values     = new ArrayList<JSONObject>();
        this.isResGood  = new ArrayList<>();
        this.request    = new VolleyRequest((Activity)context);
    }

    @Override
    public int getCount() {
        return this.values.size();
    }

    @Override
    public Object getItem(int position) {
        return this.values.get(position);
    }

    @Override
    public long getItemId(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getLong("id");
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater                     = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view                                   = inflater.inflate(R.layout.adapter_project_detail, parent, false);
        DisplayMetrics displayMetrics               = context.getResources().getDisplayMetrics();
        final float dpWidth                         = displayMetrics.widthPixels / displayMetrics.density;
        JSONObject item                             = values.get(position);
        ImageView imageView                         = (ImageView)view.findViewById(R.id.imageViewProductReview);
        imageView.getLayoutParams().width           = (int) Globals.convertDpToPixel((dpWidth - 32) / 2, context); // -32 for padding outside
        ImageView imageViewContent                  = (ImageView)view.findViewById(R.id.imageViewProductReviewContent);
        RelativeLayout relativeLayoutTitleArea      = (RelativeLayout)view.findViewById(R.id.relativeLayoutTitleArea);

        LinearLayout parentWarning                  = (LinearLayout) view.findViewById(R.id.parentWarning);
        TextView txtWarning                         = (TextView) view.findViewById(R.id.txtWarning);

        try {
            long type               = item.getLong("type");
            long CoverWidth         = item.getLong("width");
            long CoverHeight        = item.getLong("height");
            long CoverContentWidth  = item.getLong("content_width");
            long CoverContentHeight = item.getLong("content_height");
            long CoverBorderLeft    = item.getLong("border_left");
            long CoverBorderTop     = item.getLong("border_top");

            if (type == 0) {
                long CoverTitleAreaWidth    = item.getLong("title_area_width");
                long CoverTitleAreaHeight   = item.getLong("title_area_height");
                long CoverTitleAreaTop      = item.getLong("title_area_top");
                long CoverTitleAreaLeft     = item.getLong("title_area_left");
                long CoverTitleFontSize     = item.getLong("title_font_size");
                long CoverSubtitleFontSize  = item.getLong("subtitle_font_size");
                long CoverTitleTop          = item.getLong("title_top");
                long CoverSubtitleTop       = item.getLong("subtitle_top");

                EditText title              = (EditText)view.findViewById(R.id.editTextTitle);
                title.setText(item.getString("title"));
                EditText subtitle           = (EditText)view.findViewById(R.id.editTextSubtitle);
                subtitle.setText(item.getString("subtitle"));
                
                relativeLayoutTitleArea.setVisibility(View.VISIBLE);
                Globals.drawProductWithTitleLayout(context, CoverWidth, CoverHeight, imageView, imageViewContent, CoverContentWidth,
                        CoverContentHeight, CoverBorderLeft, CoverBorderTop,
                        relativeLayoutTitleArea, CoverTitleAreaWidth, CoverTitleAreaHeight, CoverTitleAreaTop, CoverTitleAreaLeft,
                        CoverTitleFontSize, CoverSubtitleFontSize, CoverTitleTop, CoverSubtitleTop, title, subtitle);
            }
            else {
                Globals.drawProductLayout(CoverWidth, CoverHeight, imageView, imageViewContent, CoverContentWidth, CoverContentHeight, CoverBorderLeft, CoverBorderTop);
            }

            String pathImageCrop            = item.getString("path_image_crop");
            String pathImageOriginal        = item.getString("path_image_original");

            Glide.with(context)
                    .load(pathImageCrop)
                    .placeholder(R.mipmap.loading_img)
                    .into(imageViewContent);
//            Picasso.with(context)
//                    .load(pathImageCrop)
//                    .placeholder(R.mipmap.loading_img)
//                    .fit()
//                    .into(imageViewContent);

            try{
                Bitmap bitmap = BitmapFactory.decodeFile(pathImageCrop);
                int height = bitmap.getHeight();
                int width = bitmap.getWidth();
                Log.w(TAG, "height : " + height + "; width : " + width);
                if(height < Constant.MIN_PIXEL && width < Constant.MIN_PIXEL){
                    parentWarning.setVisibility(View.GONE);
                    txtWarning.setText(context.getResources().getString(R.string.warn_low_res));
                    this.isResGood.set(position, true);
                }else{
                    parentWarning.setVisibility(View.GONE);
                    this.isResGood.set(position, true);
                }
            }catch(Exception e){
                Log.e(TAG, "getView : " + e.toString());
            }

            if (item.has("images")  && !item.isNull("images")) {
                JSONObject images   = item.getJSONObject("images");
                Glide.with(context)
                        .load(images.getString("thumb"))
                        .placeholder(R.mipmap.loading_img)
                        .into(imageView);

//                Picasso.with(context)
//                        .load(images.getString("thumb"))
//                        .placeholder(R.mipmap.loading_img)
//                        .fit()
//                        .into(imageView);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }

    public void setValues(ArrayList<JSONObject> values){
        this.values = values;
        this.isResGood = new ArrayList<>();
        for(int i = 0; i < this.values.size(); i++){
            this.isResGood.add(true);
        }
    }

    public boolean isResGood(){
        return isResGood(0);
    }

    private boolean isResGood(int idx){
        return this.isResGood.get(idx) && (idx < this.isResGood.size() - 1 ? isResGood(idx + 1) : true);
    }
}
