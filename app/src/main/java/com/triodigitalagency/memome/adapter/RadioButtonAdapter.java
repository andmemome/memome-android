package com.triodigitalagency.memome.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.model.KurirModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by khoiron on 07/05/18.
 */

public class RadioButtonAdapter extends RecyclerView.Adapter<RadioButtonAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<KurirModel> kurirList = new ArrayList<>();
    private int lastSelectedPosition = -1;
    private onClikRadioButton onClikRadioButton;

    public RadioButtonAdapter(Context context, ArrayList<KurirModel> kurirList) {
        this.context = context;
        this.kurirList = kurirList;
    }

    public void onClikRadioButtonListener(onClikRadioButton onClikRadioButton){
        this.onClikRadioButton = onClikRadioButton;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.radio1)RadioButton radio1;
        @BindView(R.id.titleKurir)TextView titleKurir;
        @BindView(R.id.hargaKurir)TextView hargaKurir;
        @BindView(R.id.estimasiKurir)TextView estimasiKurir;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }

    @Override
    public RadioButtonAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layoutkurir, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RadioButtonAdapter.MyViewHolder holder, final int position) {
        final KurirModel kurirModel = kurirList.get(position);

        holder.estimasiKurir.setText(kurirModel.getEstimasi());
        holder.hargaKurir.setText(kurirModel.getHarga());
        holder.titleKurir.setText(kurirModel.getJenis());
        holder.radio1.setChecked(lastSelectedPosition == position);
        if ("CTC".equals(kurirModel.getJenis())||"REG".equals(kurirModel.getJenis())){
            holder.titleKurir.setText("regular".toUpperCase());
        }else if("CTCYES".equals(kurirModel.getJenis())||"YES".equals(kurirModel.getJenis())){
            holder.titleKurir.setText("express".toUpperCase());
        }

        if ("1-1".equals(holder.estimasiKurir.getText())){
            holder.estimasiKurir.setText("1-2");
        }

        holder.radio1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRadioButton(kurirModel,position);
            }
        });
        holder.titleKurir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRadioButton(kurirModel,position);
            }
        });
        holder.hargaKurir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRadioButton(kurirModel,position);
            }
        });
        holder.estimasiKurir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRadioButton(kurirModel,position);
            }
        });

    }

    private void setRadioButton(KurirModel kurirModel, int position) {
        lastSelectedPosition = position;
        if (onClikRadioButton!=null){onClikRadioButton.onClik(kurirModel.getHarga());}
        notifyDataSetChanged();
        estimated.estimated(kurirModel.getEstimasi(),kurirModel.getJenis(),kurirModel.getCode());
    }

    @Override
    public int getItemCount() {
        return kurirList.size();
    }

    public estimated estimated;

    public void setEstimated(RadioButtonAdapter.estimated estimated) {
        this.estimated = estimated;
    }

    public interface estimated{
        void estimated(String string,String jenis,String code);
    }
}
