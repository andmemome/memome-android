package com.triodigitalagency.memome.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.NumberTextWatcher;
import com.triodigitalagency.memome.volley.utils.VolleyRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class NotPaidArrayAdapter extends BaseAdapter {
    private final Context context;
    private ArrayList<JSONObject> values;
    private VolleyRequest request;
    private Long selectedId;

    public NotPaidArrayAdapter(Context context){
        this.context    = context;
        this.values     = new ArrayList<JSONObject>();
        this.request    = new VolleyRequest((Activity)context);
    }

    @Override
    public int getCount() {
        return this.values.size();
    }

    @Override
    public Object getItem(int position) {
        return this.values.get(position);
    }

    @Override
    public long getItemId(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getLong("id");
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public String getPaymentType(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getString("payment_type");
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public double getItemGrandTotal(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getDouble("grand_total");
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public String getItemNumber(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getString("number");
        } catch (JSONException e) {
            e.printStackTrace();
            return String.valueOf(this.getItemId(position));
        }
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        LayoutInflater inflater     = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view                   = inflater.inflate(R.layout.adapter_not_paid, parent, false);
        TextView textViewOrderId    = (TextView)view.findViewById(R.id.textViewOrderId);
        TextView textViewCreatedAt  = (TextView)view.findViewById(R.id.textViewCreatedAt);
        TextView textViewPayment    = (TextView)view.findViewById(R.id.textViewPayment);
        TextView textViewTotal      = (TextView)view.findViewById(R.id.textViewTotal);
        JSONObject item             = values.get(position);

        try {
            String orderId              = String.format(view.getResources().getString(R.string.text_order_id), String.valueOf(item.getString("number")));
            String createdAt            = item.getString("created_at");
            SimpleDateFormat format     = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
            try {
                Date date   = format.parse(createdAt);
                createdAt   = dateFormat.format(date);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            EditText tempEditText   = new EditText(context);
            tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
            tempEditText.setText(String.valueOf((int) item.getDouble("grand_total")));

            textViewOrderId.setText(orderId);
            textViewCreatedAt.setText(createdAt);
            textViewPayment.setText((item.getString("payment_type").equals(Constant.PAYMENT_CREDIT_CARD) ? Constant.PAYMENT_CREDIT_CARD_NAME : (item.getString("payment_type").equals(Constant.PAYMENT_TRANSFER) ? Constant.PAYMENT_TRANSFER_NAME : item.getString("payment_type"))).toUpperCase());
            textViewTotal.setText(String.format(view.getResources().getString(R.string.text_total_amount), tempEditText.getText().toString()));

            ImageView imageViewCheck  = (ImageView)view.findViewById(R.id.imageViewCheck);
            if (this.selectedId != null && this.selectedId > 0 && this.selectedId == item.getLong("id")) {
                imageViewCheck.setVisibility(View.VISIBLE);
            }
            else {
                imageViewCheck.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }

    public Double getGrandTotal(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getDouble("grand_total");
        } catch (JSONException e) {
            e.printStackTrace();
            return Double.valueOf(0);
        }
    }

    public void setValues(ArrayList<JSONObject> values){
        this.values = values;
    }

    public void setSelectedId(Long id) { this.selectedId = id; }
}
