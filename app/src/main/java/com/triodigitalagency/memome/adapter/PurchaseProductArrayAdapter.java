package com.triodigitalagency.memome.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.NumberTextWatcher;
import com.triodigitalagency.memome.volley.utils.VolleyRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Trio-1602 on 5/4/16.
 */
public class PurchaseProductArrayAdapter extends BaseAdapter {
    private final Context context;
    private ArrayList<JSONObject> values;
    private VolleyRequest request;

    public PurchaseProductArrayAdapter(Context context){
        this.context    = context;
        this.values     = new ArrayList<JSONObject>();
        this.request    = new VolleyRequest((Activity)context);
    }

    @Override
    public int getCount() {
        return this.values.size();
    }

    @Override
    public Object getItem(int position) {
        return this.values.get(position);
    }

    @Override
    public long getItemId(int position) {
        try {
            return ((JSONObject)this.getItem(position)).getLong("id");
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view               = inflater.inflate(R.layout.adapter_purchase_product, parent, false);
        TextView productTitle   = (TextView)view.findViewById(R.id.productTitle);
        TextView title          = (TextView)view.findViewById(R.id.title);
        TextView price          = (TextView)view.findViewById(R.id.productPrice);
        TextView qty          = (TextView)view.findViewById(R.id.productQty);
        JSONObject item         = values.get(position);

        try {
            productTitle.setText(item.getString("product_title").toUpperCase());
            if (item.has("title")) {
                title.setText(item.getString("title").toUpperCase());
            }

            if (item.has("total_price")) {
                Long total = item.getLong("total_price");
                if (total == 0){
                    price.setText(R.string.text_free);
                }
                else{
                    EditText tempEditText               = new EditText(context);
                    tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
                    tempEditText.setText(String.valueOf(total));
                    price.setText("IDR " + tempEditText.getText());
                }

            }
            if (item.has("qty")) {
                Long total = item.getLong("qty");
                EditText tempEditText               = new EditText(context);
                tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
                tempEditText.setText(String.valueOf(total));
                qty.setText(tempEditText.getText() + " X");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }

    public void setValues(ArrayList<JSONObject> values){
        this.values = values;
    }
}
