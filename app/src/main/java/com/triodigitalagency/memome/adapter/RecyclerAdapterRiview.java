package com.triodigitalagency.memome.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.Globals;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by khoiron on 02/04/18.
 */

public class RecyclerAdapterRiview extends RecyclerView.Adapter<RecyclerAdapterRiview.MyViewHolder>{

    private Context mContext;
    private float lat;
    public ArrayList<JSONObject> values;
    public ArrayList<Boolean> isResGood;
    onClickCheckoutAdapter onClickCheckoutAdapter;

    public int getID(int position) {
        try {
            return (int) values.get(position).getLong("id");
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public String getItemPathImageCrop(int position) {
        try {
            return values.get(position).getString("path_image_crop");
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getItemPathImageOriginal(int position) {
        try {

            return values.get(position).getString("path_image_original");
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public long getItemWidth(int position) {
        try {

            return (long) values.get(position).getLong("content_width");
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public long getItemHeight(int position) {
        try {

            return (long) values.get(position).getLong("content_height");
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void setPathImage(int position, String pathCrop, String pathOriginal) {
        JSONObject item = this.values.get(position);
        try {
            item.put("path_image_crop", pathCrop);
            item.put("path_image_original", pathOriginal);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.values.set(position, item);
    }

    public boolean getIsChanged(int position) {
        try {
            return (Boolean) values.get(position).getBoolean("is_changed");
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void setIsChanged(int position, boolean isChanged) {
        JSONObject item = this.values.get(position);
        try {
            item.put("is_changed", isChanged);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.values.set(position, item);
    }


    public RecyclerAdapterRiview(Context mContext, ArrayList<JSONObject> values, onClickCheckoutAdapter onClickCheckoutAdapter) {
        this.mContext = mContext;
        this.values = values;
        this.onClickCheckoutAdapter = onClickCheckoutAdapter;
        this.isResGood = new ArrayList<>();
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_rivew, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        JSONObject item                             = values.get(position);
        if (position >= 0) {
            holder.buttonEdit.setVisibility(View.VISIBLE);
            holder.buttonEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickCheckoutAdapter.onClick(holder.buttonEdit.getId(),position);
                }
            });

        }else {
            holder.buttonEdit.setVisibility(View.GONE);
        }

        try {

            long type               = item.getLong("type");
            long CoverWidth         = item.getLong("width");
            long CoverHeight        = item.getLong("height");
            long CoverContentWidth  = item.getLong("content_width");
            long CoverContentHeight = item.getLong("content_height");
            long CoverBorderLeft    = item.getLong("border_left");
            long CoverBorderTop     = item.getLong("border_top");

            setLog("type "+type+" CoverWidth "+CoverWidth+" CoverHeight "+CoverHeight+" CoverContentWidth "+CoverContentWidth+" CoverContentHeight "+CoverContentHeight+" CoverBorderLeft "+CoverBorderLeft+" CoverBorderTop "+CoverBorderTop);
            if (type == 0) {
                setLog("test");
                long CoverTitleAreaWidth    = item.getLong("title_area_width");
                long CoverTitleAreaHeight   = item.getLong("title_area_height");
                long CoverTitleAreaTop      = item.getLong("title_area_top");
                long CoverTitleAreaLeft     = item.getLong("title_area_left");
                long CoverTitleFontSize     = item.getLong("title_font_size");
                long CoverSubtitleFontSize  = item.getLong("subtitle_font_size");
                long CoverTitleTop          = item.getLong("title_top");
                long CoverSubtitleTop       = item.getLong("subtitle_top");

                holder.title.setText(item.getString("title"));
                holder.subtitle.setText(item.getString("subtitle"));
                holder.relativeLayoutTitleArea.setVisibility(View.VISIBLE);

                holder.imageViewContent.getLayoutParams().height = (int) CoverHeight;
                holder.imageViewContent.getLayoutParams().width =  (int) CoverWidth;
//                holder.relativeLayoutTitleArea.getLayoutParams().width = (int) CoverTitleAreaWidth;
//                holder.relativeLayoutTitleArea.getLayoutParams().height = (int) CoverTitleAreaHeight;
                setLog("=== "+(int) CoverTitleAreaWidth);
                setLog("=== "+(int) CoverTitleAreaHeight);

                holder.relativeLayoutTitleArea.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setToast("alhamdulillah");
                    }
                });

//                Globals.drawProductLayout(CoverWidth, CoverHeight, holder.imageView, holder.imageViewContent, CoverContentWidth, CoverContentHeight, CoverBorderLeft, CoverBorderTop);
//                float ratio                                 = (float)CoverWidth / (float)holder.imageView.getLayoutParams().width;
//                RelativeLayout.LayoutParams layoutParams    = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//                layoutParams.width                          = (int)((float)CoverTitleAreaWidth / ratio);
//                layoutParams.height                         = (int)((float)CoverTitleAreaHeight / ratio);
//                layoutParams.setMargins((int) ((float) CoverTitleAreaLeft / ratio), (int) ((float) CoverTitleAreaTop / ratio), 0, 0);
//                holder.relativeLayoutTitleArea.setLayoutParams(layoutParams);
//
//                holder.title.setTextSize(TypedValue.COMPLEX_UNIT_SP, Globals.convertPixelsToSp((float) CoverTitleFontSize / ratio, mContext));
//                layoutParams                        = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//                layoutParams.width = (int) ((float) CoverTitleAreaWidth / ratio);
//                layoutParams.setMargins(0, (int) ((float) CoverTitleTop / ratio), 0, 0);
//                holder.title.setLayoutParams(layoutParams);
//
//                holder.subtitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, Globals.convertPixelsToSp((float) CoverSubtitleFontSize / ratio, mContext));
//                layoutParams                        = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//                layoutParams.width                  = (int)((float)CoverTitleAreaWidth / ratio);
//                layoutParams.setMargins(0, (int) ((float) CoverSubtitleTop / ratio), 0, 0);
//                holder.subtitle.setLayoutParams(layoutParams);
//
//                holder.imageView.requestLayout();
//                holder.imageViewContent.requestLayout();
//                RelativeLayout.LayoutParams relativeParams = (RelativeLayout.LayoutParams)holder.relativeLayoutTitleArea.getLayoutParams();
//                relativeParams.setMargins((int) CoverTitleAreaLeft, (int) CoverTitleAreaTop, 0, 0);  // left, top, right, bottom
//                holder.relativeLayoutTitleArea.setLayoutParams(relativeParams);
//                holder.relativeLayoutTitleArea.requestLayout();
//
//                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//                params.setMargins(0, (int) CoverTitleTop, 0, 0);
//                holder.title.setLayoutParams(params);
//                holder.title.requestLayout();
//
//                RelativeLayout.LayoutParams paramss = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//                paramss.setMargins(0, (int) CoverSubtitleTop, 0, 0);
//                holder.subtitle.setLayoutParams(paramss);
//                holder.subtitle.requestLayout();
//                holder.title.setTextSize(TypedValue.COMPLEX_UNIT_SP, CoverTitleFontSize);
//                holder.subtitle.setTextSize(TypedValue.COMPLEX_UNIT_SP,CoverSubtitleFontSize);
//                holder.imageViewContent.requestLayout();
//                Globals.drawProductWithTitleLayout(mContext, CoverWidth, CoverHeight, holder.imageView, holder.imageViewContent, CoverContentWidth,
//                        CoverContentHeight, CoverBorderLeft, CoverBorderTop,
//                        holder.relativeLayoutTitleArea, CoverTitleAreaWidth, CoverTitleAreaHeight, CoverTitleAreaTop, CoverTitleAreaLeft,
//                        CoverTitleFontSize, CoverSubtitleFontSize, CoverTitleTop, CoverSubtitleTop, holder.title, holder.subtitle);

            }
            else {
                holder.imageViewContent.getLayoutParams().height = (int) Globals.convertDpToPixel(CoverHeight,mContext);
                holder.imageViewContent.getLayoutParams().width =  (int) Globals.convertDpToPixel(CoverWidth,mContext);
                holder.imageViewContent.requestLayout();
/*

                DisplayMetrics displayMetrics               = mContext.getResources().getDisplayMetrics();
                final float dpWidth                         = displayMetrics.widthPixels / displayMetrics.density;
                final float dpHeight                        = displayMetrics.heightPixels / displayMetrics.density;
                holder.imageView.getLayoutParams().width    = (int) Globals.convertDpToPixel(dpWidth, mContext); // -22 for padding outside
                holder.imageView.getLayoutParams().height   = (int) Globals.convertDpToPixel(dpHeight,mContext);

                Globals.drawProductLayout(CoverWidth, CoverHeight, holder.imageView, holder.imageViewContent, CoverContentWidth, CoverContentHeight, CoverBorderLeft, CoverBorderTop);
                setLog("CoverHeight = "+CoverHeight+"CoverWidthh "+holder.imageView.getLayoutParams().width );
*/

//                Globals.drawProductLayout2(lat,CoverWidth, CoverHeight, holder.imageView, holder.imageViewContent, CoverContentWidth, CoverContentHeight, CoverBorderLeft, CoverBorderTop);
            }


            /*if (position >= 1) {
                holder.buttonEdit.setVisibility(View.VISIBLE);
                holder.buttonEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onClickCheckoutAdapter.onClick(holder.buttonEdit.getId(),position);
                    }
                });

            }else {
                holder.buttonEdit.setVisibility(View.GONE);
            }*/

            try {

                String pathImageCrop            = item.getString("path_image_crop");
                setLog("image  "+pathImageCrop);
                String pathImageOriginal        = item.getString("path_image_original");

                File file = new File(pathImageCrop);
//                setLog("image file  "+file.getAbsolutePath());
//
                Picasso.with(mContext)
                        .load(file)
                        .placeholder(R.mipmap.loading_img)
                        .fit()
                        .centerCrop()
                        .into(holder.imageViewContent);

                /*File imgFile = new  File(pathImageCrop);
                if(imgFile.exists()){
                    setLog("image corp "+pathImageCrop);
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    holder.imageViewContent.setImageBitmap(myBitmap);
                }*/


                /*try{
                    Bitmap bitmap = BitmapFactory.decodeFile(pathImageCrop);
                    int height = bitmap.getHeight();
                    int width = bitmap.getWidth();
                    this.isResGood.set(position, true);
                    Log.w("TAG", "height : " + height + "; width : " + width);
                    if(height < Constant.MIN_PIXEL && width < Constant.MIN_PIXEL){
                        holder.parentWarning.setVisibility(View.GONE);
                        holder.txtWarning.setText(mContext.getResources().getString(R.string.warn_low_res));
                        this.isResGood.set(position, true);
                    }else{
                        holder.parentWarning.setVisibility(View.GONE);
                        this.isResGood.set(position, true);
                    }
                }catch(Exception e){
                    Log.e("TAG", "getView : " + e.toString());
                }
*/
                if (item.has("images")  && !item.isNull("images")) {
                    JSONObject images   = item.getJSONObject("images");
                    if ("https".equals(images.getString("thumb").substring(0,5))){
                        Picasso.with(mContext)
                                .load("http"+images.getString("thumb").substring(5,images.getString("thumb").length()))
                                .placeholder(R.mipmap.loading_img)
                                .into(holder.imageView);
                        setLog("--"+"http"+images.getString("thumb").substring(5,images.getString("thumb").length()));
                     }else {
                        Picasso.with(mContext)
                                .load(images.getString("thumb"))
                                .placeholder(R.mipmap.loading_img)
                                .into(holder.imageView);
                        setLog("thumb "+images.getString("thumb"));
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    private void setToast(String test) {
        Toast.makeText(mContext, test, Toast.LENGTH_SHORT).show();
    }

    private void setLog(String message) {
        Log.e("TEST REVIEW",message);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public void setValues(ArrayList<JSONObject> values) {
        this.values = values;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.terakhir)RelativeLayout relativeLayout1;
        @BindView(R.id.firstImage)RelativeLayout relativeLayout;
        @BindView(R.id.imageViewProductReview)ImageView imageView;
        @BindView(R.id.imageViewProductReviewContent)ImageView imageViewContent;
        @BindView(R.id.relativeLayoutTitleArea)RelativeLayout relativeLayoutTitleArea;
        @BindView(R.id.editTextTitle)EditText title;
        @BindView(R.id.editTextSubtitle)EditText subtitle;
        @BindView(R.id.parentWarning)LinearLayout parentWarning;
        @BindView(R.id.txtWarning)TextView txtWarning;
        @BindView(R.id.buttonEdit)ImageButton buttonEdit;


        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface onClickCheckoutAdapter{
        void onClick(int holder, int position);
    }

    public boolean isResGood(){
        return isResGood(0);
    }

    private boolean isResGood(int idx){
        return this.isResGood.get(idx) && (idx < this.isResGood.size() - 1 ? isResGood(idx + 1) : true);
    }

}
