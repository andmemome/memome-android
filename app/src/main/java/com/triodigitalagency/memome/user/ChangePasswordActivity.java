package com.triodigitalagency.memome.user;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.android.volley.VolleyError;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ChangePasswordActivity extends BaseActivity {

    EditText editTextOldPassword, editTextNewPassword, editTextConfirmNewPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        setLog("activity_change_password");

        firstInit();
        setupActionBar(getResources().getString(R.string.title_change_password));

        editTextOldPassword            = (EditText)findViewById(R.id.editTextOldPassword);
        editTextNewPassword            = (EditText)findViewById(R.id.editTextNewPassword);
        editTextConfirmNewPassword     = (EditText)findViewById(R.id.editTextConfirmNewPassword);

        findViewById(R.id.buttonSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isLoading()) {
                    showLoading();
                }

                if (editTextNewPassword.getText().toString().equals(editTextConfirmNewPassword.getText().toString())) {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("clientId", Constant.CLIENT_ID);
                    params.put("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
                    params.put("oldPassword", editTextOldPassword.getText().toString());
                    params.put("newPassword", editTextNewPassword.getText().toString());
                    request.getJSONFromUrl(Globals.getApiUrl("users/changePassword"),
                            params,
                            getResources().getString(R.string.text_change_password_error),
                            new RequestCallback() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Globals.showToast(getResources().getString(R.string.text_change_password_success), context);
                                    hideLoading();
                                    finish();
                                }

                                @Override
                                public void onError(VolleyError error) {
                                    hideLoading();
                                }
                            });
                } else {
                    Globals.showAlert(getResources().getString(R.string.text_reset_password_error), getResources().getString(R.string.text_password_mismatch_error), activity);
                    hideLoading();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
