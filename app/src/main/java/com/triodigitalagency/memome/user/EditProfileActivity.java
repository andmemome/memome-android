package com.triodigitalagency.memome.user;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.android.volley.VolleyError;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class EditProfileActivity extends BaseActivity {

    private RelativeLayout tapForReloadLayout;
    private Button buttonTapForReload;
    private EditText editTextName, editTextBirthDate, editTextEmail;
    private DatePickerDialog birthDateDialog;
    private DateFormat longDateFormatter, dbDateFormatter;
    private String birthDate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        setLog("activity_edit_profile");

        firstInit();
        setupActionBar(getResources().getString(R.string.title_my_profile));

        findViewById(R.id.buttonSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });

        longDateFormatter       = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        dbDateFormatter         = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        editTextName            = (EditText)findViewById(R.id.editTextName);
        editTextBirthDate       = (EditText)findViewById(R.id.editTextBirthDate);
        editTextEmail           = (EditText)findViewById(R.id.editTextEmail);
        editTextBirthDate.setInputType(InputType.TYPE_NULL);
        editTextBirthDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    hideSoftInputFromWindow();
                    birthDateDialog.show();
                }
            }
        });

        Calendar newCalendar    = Calendar.getInstance();
        birthDateDialog         = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate    = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                editTextBirthDate.setText(longDateFormatter.format(newDate.getTime()));
                birthDate           = dbDateFormatter.format(newDate.getTime());
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        findViewById(R.id.buttonChangePassword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(ChangePasswordActivity.class);
            }
        });

        loadDataUser();
        initTapForReload();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //</editor-fold>

    //<editor-fold desc="Procedure & Function">
    private void submit() {
        Boolean isGood = true;
        if (isGood && !Globals.isValidEmail(editTextEmail.getText().toString())){
            Globals.showAlert(getResources().getString(R.string.text_my_profile_error),
                    getResources().getString(R.string.text_invalid_email),
                    (Activity) context);
            isGood = false;
        }
        isGood  = validateEditTextStringRequired(isGood);
        isGood  = validateVariableStringRequired(isGood);

        if (isGood) {
            if (!isLoading()) {
                showLoading();
            }

            Map<String, String> params = new HashMap<String, String>();
            params.put("clientId", Constant.CLIENT_ID);
            params.put("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
            params.put("name", editTextName.getText().toString());
            params.put("email", editTextEmail.getText().toString());
            params.put("birthdate", String.format("%s", birthDate));

            request.getJSONFromUrl(Globals.getApiUrl("users/update"), params, getResources().getString(R.string.text_my_profile_error),
                new RequestCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject user = response.getJSONObject("user");
                            Globals.setDataPreference(context, Constant.P_USER_NAME, user.getString("name"));
                            Globals.showToast(getResources().getString(R.string.text_update_my_profile_success), context);
                            Globals.setDataPreference(context,"emailuser",user.getString("email"));

                            Intent intent = new Intent(EditProfileActivity.this, MyProfileActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        hideLoading();
                    }

                    @Override
                    public void onError(VolleyError error) {
                        hideLoading();
                    }
                });
        }
    }

    protected void loadDataUser() {
        if (!isLoading()) {
            showLoading();
        }

        Map<String, String> params = new HashMap<String, String>();
        params.put("clientId", Constant.CLIENT_ID);
        params.put("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
        request.getJSONFromUrl(Globals.getApiUrl("users/view"), params, getResources().getString(R.string.text_my_profile_error), new RequestCallback() {
            @Override
            public void onResponse(JSONObject response) {
                hideLoading();
                try {
                    JSONObject user = response.getJSONObject("user");
                    editTextName.setText(user.getString("name"));
                    editTextEmail.setText(user.getString("email"));
                    Globals.setDataPreference(EditProfileActivity.this,"emailuser",user.getString("email"));


                    try {
                        Date tempDate       = dbDateFormatter.parse(user.getString("birthdate"));
                        editTextBirthDate.setText(longDateFormatter.format(tempDate.getTime()));
                        birthDate           = dbDateFormatter.format(tempDate.getTime());
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeInMillis (tempDate.getTime());
                        birthDateDialog.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    hideLoading();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(VolleyError error) {
                tapForReloadLayout.setVisibility(View.VISIBLE);
                hideLoading();
            }
        });
    }

    protected void initTapForReload() {
        buttonTapForReload  = (Button)findViewById(R.id.buttonTapForReload);
        buttonTapForReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tapForReloadLayout.setVisibility(View.GONE);
                loadDataUser();
            }
        });

        tapForReloadLayout  = (RelativeLayout)findViewById(R.id.tapForReload);
        tapForReloadLayout.setVisibility(View.GONE);
    }

    protected Boolean validateEditTextStringRequired(Boolean isGood) {
        if (isGood) {
            EditText[] requiredFields       = new EditText[]{editTextName, editTextEmail};
            String[] requiredFieldNames     = new String[]{
                    getResources().getString(R.string.hint_name),
                    getResources().getString(R.string.hint_email),
            };

            int i = 0;
            for (EditText field : requiredFields) {
                if (field.getText().toString().trim().equals("")) {
                    Globals.showAlert(
                            getResources().getString(R.string.text_my_profile_error),
                            String.format(getResources().getString(R.string.text_add_error_required), requiredFieldNames[i]),
                            activity
                    );
                    isGood = false;
                    break;
                }
                i++;
            }
        }
        return isGood;
    }

    protected Boolean validateVariableStringRequired(Boolean isGood) {
        if (isGood) {
            String[] requiredValues         = new String[]{birthDate};
            String[] requiredFieldNames     = new String[]{
                    getResources().getString(R.string.hint_birth_date),
            };

            int i = 0;
            for (String field : requiredValues) {
                if (field.equals("")) {
                    Globals.showAlert(
                            getResources().getString(R.string.text_my_profile_error),
                            String.format(getResources().getString(R.string.text_add_error_required), requiredFieldNames[i]),
                            activity
                    );
                    isGood = false;
                    break;
                }
                i++;
            }
        }
        return isGood;
    }
    //</editor-fold>
}
