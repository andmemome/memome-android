package com.triodigitalagency.memome.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import com.triodigitalagency.memome.MainActivity;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.adapter.MyPurchasesTabAdapter;
import com.triodigitalagency.memome.transaction.CartActivity;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.viewpagerindicator.TabPageIndicator;

public class MyPurchasesActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.showCartIcon   = true;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_purchases);
        setLog("activity_my_purchases");

        firstInit();
        setupActionBar(getResources().getString(R.string.title_my_purchases));

        if (!isLogin()) {
            back();
        }

        ViewPager pager = findViewById(R.id.viewPager);
        pager.setAdapter(new MyPurchasesTabAdapter(getSupportFragmentManager(), activity));

        TabPageIndicator indicator = findViewById(R.id.tabIndicator);
        indicator.setViewPager(pager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_default, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            startActivity(MainActivity.class);
            finish();
            return true;
        }
        else if (id == R.id.actionCart) {
            startActivity(CartActivity.class);
        }

        return super.onOptionsItemSelected(item);
    }

    private void back() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
    //</editor-fold>
}
