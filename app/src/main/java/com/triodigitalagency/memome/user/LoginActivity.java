package com.triodigitalagency.memome.user;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.VolleyError;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.triodigitalagency.memome.BuildConfig;
import com.triodigitalagency.memome.MainActivity;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Chiper;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class LoginActivity extends BaseActivity {

    protected final int REGISTER = 1;
    protected final int FORGOT = 2;
    protected final int SUCCESS = 2;
    protected final int RESET = 3;
    protected final int RC_SIGN_IN = 4;
    protected Bundle extras;

    private LoginButton buttonLoginFacebook;
    List<String> fbReadPermissions;
    CallbackManager callbackManager;
    AccessTokenTracker accessTokenTracker;

    GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult connectionResult) {
                    }
                } /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        setContentView(R.layout.activity_login);
        setLog("activity_login");

        firstInit();
        setupActionBar(getResources().getString(R.string.title_log_in));

        extras = getIntent().getExtras();
        if (extras != null){
            if (extras.getInt("reset") == RESET) {
                Globals.showAlert(getResources().getString(R.string.text_reset_password_success), getResources().getString(R.string.text_reset_password_success_info), activity);
            }
        }

        if (isLogin()) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }

        findViewById(R.id.textForgotPassword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(ForgotPasswordActivity.class, FORGOT);
            }
        });

        findViewById(R.id.textRegister).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(RegisterActivity.class, REGISTER);
            }
        });

        findViewById(R.id.buttonLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isLoading()){
                    showLoading();

                    EditText editTextEmail      = (EditText)findViewById(R.id.editTextEmail);
                    EditText editTextPassword   = (EditText)findViewById(R.id.editTextPassword);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("clientId", Constant.CLIENT_ID);
                    params.put("email", editTextEmail.getText().toString());
                    params.put("password", editTextPassword.getText().toString());
                    params.put("deviceId", Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));

                    if (BuildConfig.DEBUG) {
                        params.put("isSandbox", "1");
                    }

                    request.getJSONFromUrl(
                        Globals.getApiUrl("users/login"),
                        params,
                        activity.getResources().getString(R.string.text_login_error),
                        new RequestCallback() {
                            @Override
                            public void onResponse(JSONObject response) {
                                hideLoading();
                                afterLogin(response);
                            }

                            @Override
                            public void onError(VolleyError error) {
                                hideLoading();
                            }
                        });
                }
            }
        });

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                // Set the access token using
                // currentAccessToken when it's loaded or set.
            }
        };

        buttonLoginFacebook = (LoginButton)findViewById(R.id.buttonLoginFacebook);
        fbReadPermissions   = new ArrayList<String>();
        fbReadPermissions.add("public_profile");
        fbReadPermissions.add("email");
        buttonLoginFacebook.setReadPermissions(fbReadPermissions);
        buttonLoginFacebook.setLoginBehavior(LoginBehavior.WEB_ONLY);
        callbackManager = CallbackManager.Factory.create();
        buttonLoginFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if (AccessToken.getCurrentAccessToken() != null) {
                    if (!isLoading()) {
                        showLoading();
                    }

                    GraphRequest graphRequest = GraphRequest.newMeRequest(
                            AccessToken.getCurrentAccessToken(),
                            new GraphRequest.GraphJSONObjectCallback() {
                                @Override
                                public void onCompleted(
                                        JSONObject object,
                                        GraphResponse response) {
                                    // Application code
                                    facebookLogin(object);
                                    Log.d("debug", "object : " + object.toString());
                                }
                            });

                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "id,name,email,first_name,last_name,cover");
                    graphRequest.setParameters(parameters);
                    graphRequest.executeAsync();
                }
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                Globals.showAlert(getResources().getString(R.string.text_login_error), getResources().getString(R.string.text_login_error_fb), activity);
            }
        });

        // Customize sign-in button. The sign-in button can be displayed in
        // multiple sizes and color schemes. It can also be contextually
        // rendered based on the requested scopes. For example. a red button may
        // be displayed when Google+ scopes are requested, but a white button
        // may be displayed when only basic profile is requested. Try adding the
        // Scopes.PLUS_LOGIN scope to the GoogleSignInOptions to see the
        // difference.
        Button signInButton = (Button)findViewById(R.id.buttonLoginGoogle);
        //signInButton.setSize(SignInButton.SIZE_STANDARD);
        //signInButton.setScopes(gso.getScopeArray());
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

            googleLogin(result);
        }

        if (requestCode == REGISTER && resultCode == SUCCESS){
            Globals.showAlert(getResources().getString(R.string.text_register_success), getResources().getString(R.string.text_register_success_message), activity);
        }
        else if (requestCode == FORGOT && resultCode == SUCCESS){
            Globals.showAlert(getResources().getString(R.string.text_forget_password_success), getResources().getString(R.string.text_forget_password_success_message), activity);
        }
    }
    //</editor-fold>

    //<editor-fold desc="Procedure & Function">
    protected void facebookLogin(JSONObject object){
        Map<String, String> params = new HashMap<String, String>();
        params.put("clientId", Constant.CLIENT_ID);
        params.put("deviceId", Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));

        try{
            String email    = "";
            if(object.has("email")){
                email = object.getString("email");
            }
            params.put("facebookId", Chiper.encrypt(object.getString("id"), email));
            params.put("email", email);

            String name     = "";
            if(object.has("first_name")) {
                name = " " + object.getString("first_name");
            }
            else {
                name = " " + object.getString("name");
            }

            if(object.has("last_name")) {
                name += " " + object.getString("last_name");
            }

            params.put("name", name);
        }
        catch (Exception e){
        }

        if (BuildConfig.DEBUG) {
            params.put("isSandbox", "1");
        }

        request.getJSONFromUrl(
            Globals.getApiUrl("users/facebookLogin"),
            params,
            activity.getResources().getString(R.string.text_login_error),
            new RequestCallback() {
                @Override
                public void onResponse(JSONObject response) {
                    hideLoading();
                    afterLogin(response);
                }

                @Override
                public void onError(VolleyError error) {
                    hideLoading();
                }
            });
    }

    private void googleLogin(GoogleSignInResult result) {
        if (result.isSuccess()) {
            if (!isLoading()) {
                showLoading();
            }

            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct    = result.getSignInAccount();
            String personName           = acct.getDisplayName();
            String personEmail          = acct.getEmail();
            String personId             = acct.getId();
            Uri personPhoto             = acct.getPhotoUrl();

            Map<String, String> params  = new HashMap<String, String>();
            params.put("clientId", Constant.CLIENT_ID);
            params.put("deviceId", Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
            params.put("googleId", Chiper.encrypt(personId, personEmail));
            params.put("email", personEmail);
            params.put("name", personName);

            if (BuildConfig.DEBUG) {
                params.put("isSandbox", "1");
            }

            request.getJSONFromUrl(
                Globals.getApiUrl("users/googleLogin"),
                params,
                activity.getResources().getString(R.string.text_login_error),
                new RequestCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideLoading();
                        afterLogin(response);
                    }

                    @Override
                    public void onError(VolleyError error) {
                        hideLoading();
                    }
                });
        } else {
            // Signed out, show unauthenticated UI.
            //Globals.showToast(activity.getResources().getString(R.string.text_login_error), context);
            //Log.d("DEBUG", "GOOGLE SIGN IN RESULT: " + result.toString() + ". STATUS: " + result.getStatus().toString());
        }
    }


    protected void afterLogin(JSONObject response){
        try {
            JSONObject user = response.getJSONObject("user");
            Globals.setDataPreference(LoginActivity.this,"emailuser",user.optString("email"));
            Globals.setDataPreference(context, Constant.P_ACCESS_TOKEN, response.getString("accessToken"));
            Globals.setDataPreference(context, Constant.P_USER_ID, user.getLong("id"));
            Globals.setDataPreference(context, Constant.P_USER_NAME, user.getString("name"));
            if (!user.get("images").equals(null) && !user.get("images").equals("") && !user.getJSONObject("images").getString("original").equals(null) && !user.getJSONObject("images").getString("original").equals("")) {
                Globals.setDataPreference(context, Constant.P_USER_IMAGE_PATH, user.getJSONObject("images").getString("original"));
            }
            Globals.showToast(
                    String.format(getResources().getString(R.string.text_profile_name), user.getString("name")),
                    context
            );

            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } catch (Exception e) {
            Log.d("Error", e.getMessage());
            Globals.showAlert(
                    activity.getResources().getString(R.string.text_login_error),
                    activity.getResources().getString(R.string.text_unknown_error),
                    activity
            );
        }
    }
    //</editor-fold>
}
