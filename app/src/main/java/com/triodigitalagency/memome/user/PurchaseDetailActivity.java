package com.triodigitalagency.memome.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.adapter.RecyclerAdapterProdukInprogress;
import com.triodigitalagency.memome.adapter.RecyclerAdapterProdukInprogress2;
import com.triodigitalagency.memome.database.DatabaseHandler;
import com.triodigitalagency.memome.database.Project;
import com.triodigitalagency.memome.database.ProjectDetail;
import com.triodigitalagency.memome.transaction.CartActivity;
import com.triodigitalagency.memome.transaction.CheckoutActivity;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.utils.NumberTextWatcher;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PurchaseDetailActivity extends BaseActivity {

    private Bundle extras = null;
    private TextView lblOrderDate, lblOrderStatus, lblOrderTotal, lblOrderDiscount ,lblOrderGrandTotal, lblOrderShipping;
    private RecyclerView productListView;
    private Button buttonTapForReload;
    private RelativeLayout tapForReloadLayout;

    private RecyclerAdapterProdukInprogress purchaseProductArrayAdapter;
    private RecyclerAdapterProdukInprogress2 recyclerAdapterProdukInprogress2;
    ArrayList<JSONObject> productDataSource;
    private JSONObject order;
    private long orderId;
    @BindView(R.id.lineButton)LinearLayout lineButton;
    @BindView(R.id.btnReorder)TextView btnReorder;
    @BindView(R.id.trackinglbl)TextView trackinglbl;
    @BindView(R.id.titleBottom)TextView titleBottom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_purchase_detail);
        ButterKnife.bind(this);
        setLog("activity_purchase_detail");

        firstInit();

        extras = getIntent().getExtras();
        if (extras != null){
            try {

                order = new JSONObject(extras.getString("order"));
                setLog("--- "+order.toString());
                orderId = order.getLong("id");

                String number = order.getString("number");
                this.setupActionBar(number);
            }
            catch (JSONException e){
                finish();
            }

        }
        else {
            finish();
        }

        lblOrderDate = findViewById(R.id.lblOrderDate);
        lblOrderStatus = findViewById(R.id.lblOrderStatus);
        lblOrderTotal = findViewById(R.id.lblOrderTotal);
        lblOrderDiscount = findViewById(R.id.lblOrderDiscount);
        lblOrderGrandTotal = findViewById(R.id.lblOrderGrandTotal);
        lblOrderShipping = findViewById(R.id.lblOrderShipping);

        productListView = findViewById(R.id.productListView);
        purchaseProductArrayAdapter = new RecyclerAdapterProdukInprogress(context);
        productDataSource = new ArrayList<>();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        productListView.setLayoutManager(mLayoutManager);
        productListView.setItemAnimator(new DefaultItemAnimator());
        productListView.setAdapter(purchaseProductArrayAdapter);
        productListView.setHasFixedSize(true);
        productListView.setNestedScrollingEnabled(false);

        recyclerAdapterProdukInprogress2 = new RecyclerAdapterProdukInprogress2(this);
        RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(getApplicationContext());
        recyclerviewProduk2.setLayoutManager(mLayoutManager2);
        recyclerviewProduk2.setItemAnimator(new DefaultItemAnimator());
        recyclerviewProduk2.setAdapter(recyclerAdapterProdukInprogress2);
        recyclerviewProduk2.setHasFixedSize(true);
        recyclerviewProduk2.setNestedScrollingEnabled(false);

        initTapForReload();
        renderDataOrder();
        loadDataProducts();

        trackingCode.setSelected(true);
        deleveryDate.setSelected(true);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @BindView(R.id.recyclerviewProduk2)RecyclerView recyclerviewProduk2;
    @BindView(R.id.shippingMethod)TextView shippingMethod;
    @BindView(R.id.shippingCost)TextView shippingCost;
    @BindView(R.id.deleveryDate)TextView deleveryDate;
    @BindView(R.id.trackingCode)TextView trackingCode;
    @BindView(R.id.totalProduk)TextView totalProduk;
    @BindView(R.id.orderNumber)TextView orderNumber;
    @BindView(R.id.lineHide1)LinearLayout lineHide1;
    @BindView(R.id.trackShip)Button trackShip;


    // actions
    protected void renderDataOrder() {
        try{

            setDateDelivery();
            shippingCost.setText(Globals.currencyFormatIDR(Double.parseDouble(order.optString("shipping_price"))));
            totalProduk.setText("TOTAL "+order.optString("jumlah_order")+" PRODUCTS");
            orderNumber.setText("#"+order.optString("number"));
            String createdAt            = order.getString("created_at");
            SimpleDateFormat format     = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.US);


            if (!order.optString("shipping_number").isEmpty()){
                trackingCode.setText(order.optString("shipping_number"));
                setLog("notempety");
                lineHide1.setVisibility(View.VISIBLE);
                lineButton.setVisibility(View.VISIBLE);
                trackShip.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, DialoKurirTransit.class);
                        intent.putExtra("shipping",order.toString());
                        context.startActivity(intent);
                    }
                });
            }else {

            }

            try {
                Date date   = format.parse(createdAt);
                createdAt   = dateFormat.format(date);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            lblOrderDate.setText( createdAt );

            if (order.has("status")) {
                lblOrderStatus.setText(order.getString("status"));

            }
            else{
                lblOrderStatus.setText("");
            }

            if (order.has("subtotal")) {
                Long total = order.getLong("subtotal");
                if (order.has("shipping_price")) {
                    total += order.getLong("shipping_price");
                }
                EditText tempEditText               = new EditText(context);
                tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
                tempEditText.setText(String.valueOf( total ));
                lblOrderTotal.setText("IDR " + tempEditText.getText());
            }
            else {
                lblOrderTotal.setText("");
            }

            if (order.has("voucher_total")){
                EditText tempEditText               = new EditText(context);
                tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
                tempEditText.setText(String.valueOf( order.getLong("voucher_total") ));
                lblOrderDiscount.setText("IDR " + tempEditText.getText());
            }
            else{
                lblOrderDiscount.setText("");
            }

            if (order.has("grand_total")){
                EditText tempEditText               = new EditText(context);
                tempEditText.addTextChangedListener(new NumberTextWatcher(tempEditText));
                tempEditText.setText(String.valueOf( order.getLong("grand_total") ));
                lblOrderGrandTotal.setText("IDR " + tempEditText.getText());
            }
            else{
                lblOrderGrandTotal.setText("");
            }

            String shipping = "";
            if (order.has("shipping_first_name")){
                shipping = shipping + order.getString("shipping_first_name");
            }
            if (order.has("shipping_last_name")){
                shipping = shipping + order.getString("shipping_last_name");
            }
            if (order.has("shipping_address")){
                if (!shipping.equals("")){
                    shipping += "\n";
                }
                shipping += order.getString("shipping_address") + "\n";
            }
            if (order.has("shipping_city")){
                shipping += order.getString("shipping_city") + ", ";
            }
            if (order.has("shipping_province")){
                shipping += order.getString("shipping_province") + " - ";
            }
            if (order.has("shipping_postal_code")){
                shipping += order.getString("shipping_postal_code");
            }
            if (order.has("shipping_phone")){
                if(!shipping.equals("")){
                    shipping += "\n";
                }
                shipping += order.getString("shipping_phone");
            }
            if (order.has("shipping_type")){
                if(!shipping.equals("")){
                    shipping += "\n";
                }
                shipping += order.getString("shipping_type");

                if (order.has("shipping_number")){
                    String shippingNumber = order.getString("shipping_number");
                    if(!shippingNumber.equals("")){
                        shipping += "(" + shippingNumber + ")";
                    }
                }
            }

            lblOrderShipping.setText(shipping);
        }
        catch (JSONException e){

        }

    }

    private void setDateDelivery() {
        lineButton.setVisibility(View.GONE);
        SimpleDateFormat format2     = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd MMM yyyy", Locale.US);

        if ("pending".equalsIgnoreCase(order.optString("status", ""))) {
            lblOrderStatus.setText("on progress".toUpperCase());
            trackinglbl.setVisibility(View.GONE);
            String dateString = order.optString("created_at", format2.format(new Date()));
            Date date;
            try {
                date   = format2.parse(dateString);
                dateString   = dateFormat2.format(date);
            } catch (ParseException e) {

                e.printStackTrace();
                date = new Date();
            }

            deleveryDate.setText(Globals.calculateShipping(date, 12, 16));
            titleBottom.setText(R.string.est_production);
            lineButton.setVisibility(View.GONE);
            trackShip.setVisibility(View.GONE);


        } else if ("process".equalsIgnoreCase(order.optString("status", ""))) {
            String dateString = order.optString("process_date", format2.format(new Date()));
            Date date;
            try {
                date   = format2.parse(dateString);
                dateString   = dateFormat2.format(date);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                date = new Date();
            }
            deleveryDate.setText(dateString);
            trackinglbl.setVisibility(View.VISIBLE);
            lblOrderStatus.setText("In Production".toUpperCase());

            titleBottom.setText(R.string.text_production_date);
            trackinglbl.setText(R.string.text_est_delivery_date);
            titleBottom.setText(dateString);
            trackingCode.setText(Globals.calculateShipping(date, 3, 4));

        } else if ("shipping".equalsIgnoreCase(order.optString("status", ""))) {
            String shipping_number = order.optString("shipping_number", "");
            String dateString = order.optString("ship_date", format2.format(new Date()));
            try {
                Date date   = format2.parse(dateString);
                dateString   = dateFormat2.format(date);
            } catch (ParseException e){
                e.printStackTrace();
            }

            deleveryDate.setText(dateString);

        } else if ("complete".equalsIgnoreCase(order.optString("status", ""))) {
            purchaseProductArrayAdapter.setCompleted(true);
            lineHide1.setVisibility(View.GONE);
            String dateString = order.optString("complete_date", format2.format(new Date()));
            try {
                Date date   = format2.parse(dateString);
                dateString   = dateFormat2.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            deleveryDate.setText(dateString);
            btnReorder.setVisibility(View.VISIBLE);
            titleBottom.setText(R.string.text_received_on);
            trackShip.setVisibility(View.GONE);
            lineButton.setVisibility(View.GONE);

            if("1".equals(order.optString("keterangan_expired"))){
                btnReorder.setVisibility(View.GONE);
            }

            purchaseProductArrayAdapter.setSetOnClikLikAdapter(new RecyclerAdapterProdukInprogress.setOnClikLikAdapter() {
                @Override
                public void onclik(JSONObject jsonObject, int position) {
                    setLog("ok"+jsonObject);
                    saveProjects(jsonObject,position);

                }
            });

        } else if ("failed".equalsIgnoreCase(order.optString("status", ""))) {
            String dateString = order.optString("canceled_at", format2.format(new Date()));
            lineHide1.setVisibility(View.GONE);
            lblOrderStatus.setTextColor(ContextCompat.getColor(context, R.color.red));
            try {
                Date date   = format2.parse(dateString);
                dateString   = dateFormat2.format(date);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            deleveryDate.setText(dateString);
            titleBottom.setText(R.string.text_canceled_on);
            trackShip.setVisibility(View.GONE);
            lineButton.setVisibility(View.GONE);

        }else if("upload failed".equalsIgnoreCase(order.optString("status", ""))){

            String dateString = order.optString("canceled_at", format2.format(new Date()));
            lineHide1.setVisibility(View.GONE);
            lblOrderStatus.setTextColor(ContextCompat.getColor(context, R.color.red));
            try {
                Date date   = format2.parse(dateString);
                dateString   = dateFormat2.format(date);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            deleveryDate.setText(dateString);
            titleBottom.setText(R.string.text_canceled_on);
            Log.e("Test ","okeeeee");
            titleBottom.setTextColor(context.getResources().getColor(R.color.red));
            trackShip.setVisibility(View.GONE);
            lineButton.setVisibility(View.GONE);

        }else if("canceled".equals(order.optString("status", ""))) {

            String dateString =  order.optString("canceled_date", format2.format(new Date()));

            try {
                Date date   = format2.parse(dateString);
                dateString   = dateFormat2.format(date);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            titleBottom.setTextColor(context.getResources().getColor(R.color.redDark));
            deleveryDate.setText(dateString);

            trackinglbl.setText("REASONS");
            titleBottom.setText("CANCELLED ON");
            trackingCode.setText(order.optString("reason_for_cancel"));
            trackShip.setVisibility(View.GONE);
            lineButton.setVisibility(View.GONE);



        }else if ("cancelled".equals(order.optString("status",""))){
            String dateString =  order.optString("canceled_date", format2.format(new Date()));

            try {
                Date date   = format2.parse(dateString);
                dateString   = dateFormat2.format(date);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            titleBottom.setTextColor(context.getResources().getColor(R.color.redDark));
            deleveryDate.setText(dateString);

            trackinglbl.setText("REASONS");
            titleBottom.setText("CANCELLED ON");
            trackingCode.setText(order.optString("reason_for_cancel"));
            trackShip.setVisibility(View.GONE);
            lineButton.setVisibility(View.GONE);
        }
    }

    protected void loadDataProducts() {
        if(!isLoading()){
            showLoading();
            Map<String, String> params = new HashMap<String, String>();
            params.put("clientId", Constant.CLIENT_ID);
            params.put("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
            params.put("orderId", String.format("%d", orderId));
            request.getJSONFromUrl(Globals.getApiUrl("orders/getById"),
                    params,
                    getResources().getString(R.string.text_product_error),
                    new RequestCallback() {
                        @Override
                        public void onResponse(JSONObject response) {
                            hideLoading();
                            setLog("Response "+response.toString());

                            try {

                                JSONArray products = response.getJSONObject("order").getJSONArray("order_product");

                                for (int i = 0; i < products.length(); i++) {
                                    productDataSource.add(products.getJSONObject(i));
                                    setLog("00000>>>>> "+products.length());
                                }

                                order = response.getJSONObject("order");
                                purchaseProductArrayAdapter.setValues(productDataSource);
                                recyclerAdapterProdukInprogress2.setValues(productDataSource);
                                purchaseProductArrayAdapter.notifyDataSetChanged();
                                recyclerAdapterProdukInprogress2.notifyDataSetChanged();
                                renderDataOrder();

                                /*purchaseProductArrayAdapter.registerDataSetObserver(new DataSetObserver() {
                                    @Override
                                    public void onChanged() {
                                        super.onChanged();
                                        int totalHeight = 0;
                                        for (int i = 0; i < productDataSource.size(); i++) {
                                            totalHeight += productListView.getChildAt(0).getHeight();
                                        }
                                        productListView.setMinimumHeight(totalHeight);
                                    }
                                });

                                productListView.setMinimumHeight(productDataSource.size() * 100);*/

                                Log.d("debug", "Get measured height : " + productListView.getMeasuredHeight());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(VolleyError error) {
                            tapForReloadLayout.setVisibility(View.VISIBLE);
                            hideLoading();
                        }
                    });
        }
    }

    protected void initTapForReload() {
        buttonTapForReload  = (Button)findViewById(R.id.buttonTapForReload);
        buttonTapForReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tapForReloadLayout.setVisibility(View.GONE);
                loadDataProducts();
            }
        });

        tapForReloadLayout  = (RelativeLayout)findViewById(R.id.tapForReload);
        tapForReloadLayout.setVisibility(View.GONE);
    }

    @OnClick(R.id.btnReorder)void btnReorderListener(){
        getReorder(String.valueOf(orderId));
    }

    protected void getReorder(String id){
        showLoading();
        Map<String, String> params = new HashMap<String, String>();
        params.put("clientId", Constant.CLIENT_ID);
        params.put("accessToken", Globals.getStringDataPreference(getApplicationContext(), Constant.P_ACCESS_TOKEN));
        params.put("orderId",id);
        request.getJSONFromUrl(Globals.getApiUrl("orders/getById"), params, getResources().getString(R.string.text_my_reorder_error), new RequestCallback() {
            @Override
            public void onResponse(final JSONObject response) {
                hideLoading();

                /*Gson gson = new Gson();
                String total =  response.optJSONObject("order").optString("subtotal");
                Intent intent = new Intent(PurchaseDetailActivity.this,CheckoutActivity.class);
                intent.putExtra("totalPrice",Double.valueOf(total));
                intent.putExtra("id",response.optJSONObject("order").optJSONArray("order_product").optJSONObject(0).optString("product_id"));
                intent.putExtra("order",gson.toJson(response.optJSONObject("order").optJSONArray("order_product")));
                intent.putExtra("reorder",true);
                startActivity(intent);*/


                if ("0".equals(response.optJSONObject("order").optString("orderid_reorder"))){
                    Gson gson = new Gson();
                    String total =  response.optJSONObject("order").optString("subtotal");
                    Intent intent = new Intent(PurchaseDetailActivity.this,CheckoutActivity.class);
                    intent.putExtra("totalPrice",Double.valueOf(total));
                    intent.putExtra("id",response.optJSONObject("order").optJSONArray("order_product").optJSONObject(0).optString("product_id"));
                    intent.putExtra("order",gson.toJson(response.optJSONObject("order").optJSONArray("order_product")));
                    intent.putExtra("order1",gson.toJson(response.optJSONObject("order")));
                    intent.putExtra("id_order",response.optJSONObject("order").optString("id"));
                    intent.putExtra("reorder",true);
                    startActivity(intent);
                }else {
                    Gson gson = new Gson();
                    String total =  response.optJSONObject("order").optString("subtotal");
                    Intent intent = new Intent(PurchaseDetailActivity.this,CheckoutActivity.class);
                    intent.putExtra("totalPrice",Double.valueOf(total));
                    intent.putExtra("id",response.optJSONObject("order").optJSONArray("order_product").optJSONObject(0).optString("product_id"));
                    intent.putExtra("order",gson.toJson(response.optJSONObject("order").optJSONArray("order_product")));
                    intent.putExtra("order1",gson.toJson(response.optJSONObject("order")));
                    intent.putExtra("id_order",response.optJSONObject("order").optString("orderid_reorder"));
                    intent.putExtra("reorder",true);
                    startActivity(intent);
                }

            }

            @Override
            public void onError(VolleyError error) {
                hideLoading();
            }
        });
    }

    protected void saveProjects(JSONObject jsonObject,int position) {
        setLog("mk-- "+jsonObject);

        DatabaseHandler db = new DatabaseHandler(this);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
        String formattedDate = df.format(c.getTime());

        Project project;

        try {
            project = new Project((int) Integer.valueOf(jsonObject.optString("product_id")), jsonObject.optString("title"), jsonObject.optString("subtitle"), jsonObject.optJSONObject("image").optString("large"), jsonObject.optJSONObject("image").optString("large"), formattedDate, (int) jsonObject.optInt("corporate_voucher_id", -1),"", jsonObject.optInt("discount_qty"), jsonObject.opt("corporate_voucher_code") == null ? "" : jsonObject.optString("corporate_voucher_code", ""), Integer.valueOf(jsonObject.optString("id")));
        }catch (Exception e){
            e.printStackTrace();
            setLog("mkm "+jsonObject.optString("product_id")+" -"+ jsonObject.optString("title")+" -"+ jsonObject.optString("subtitle")+" -"+ "https://s2.bukalapak.com/img/729700873/w-1000/Background_Foto_Putih_Polos_ukuran_2_25_x_6_meter.jpg"+" -"+jsonObject.optString("discount_qty"));
            project = new Project((int) Integer.valueOf(jsonObject.optString("product_id")), jsonObject.optString("title"), jsonObject.optString("subtitle"), "https://s2.bukalapak.com/img/729700873/w-1000/Background_Foto_Putih_Polos_ukuran_2_25_x_6_meter.jpg", "", formattedDate, -1,"", Integer.valueOf(jsonObject.optString("discount_qty")), "", Integer.valueOf(jsonObject.optString("id")));

        }

        project.setReorderId(Integer.valueOf(jsonObject.optString("id")));

        db.addProject(project);
        int lastInsertId = db.getLastInsertId();
        try {

            int count =jsonObject.optInt("count_photo");
            if ("14".equals(jsonObject.optString("product_id"))||"9".equals(jsonObject.optString("product_id"))||"3".equals(jsonObject.optString("product_id"))){
                count = count-1;
            }

//            if ("14".equals(prdId)||"9".equals(prdId)||"3".equals(prdId)){
//                product.put("total_detail", String.valueOf(jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optInt("count_photo")-1));
//            }else {
//                product.put("total_detail", jsonObject.optJSONArray("values").optJSONObject(i).optJSONObject("nameValuePairs").optInt("count_photo"));
//            }


            for (int i = 0; i < count; i++) {
                String image = "";
                try {
                    image = jsonObject.optJSONArray("cover_image").optJSONObject(0).optJSONObject("image").optString("large");
                }catch (Exception e){
                    e.printStackTrace();
                    image = jsonObject.optJSONArray("order_product_detail").optJSONObject(0).optJSONObject("image").optString("large");
                }
                db.addProjectDetail(new ProjectDetail(lastInsertId, (int) jsonObject.optInt("id"),image, image)); // + 1 cz index 0 is cover
            }

            /*if ("15".equals(jsonObject.getString("product_id"))){
                db.addProjectDetail(new ProjectDetail(lastInsertId, (int) jsonObject.optInt("id"),image, image)); // + 1 cz index 0 is cover
            }*/

        } catch (Exception e) {
            e.printStackTrace();
        }

        addToCart(lastInsertId);
        startActivity(CartActivity.class);
        finish();


        /*if (projectId <= 0) {

        } else {
            Project project = db.getProject(projectId);

            if (!project.getPathImageCrop().equals(project.getPathImageOriginal()) && !project.getPathImageCrop().equals(pathImageCrop)) {
                File file = new File(project.getPathImageCrop());
                if (file.exists()) {
                    file.delete();
                    context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(project.getPathImageCrop()))));
                }
            }

            project.setTitle(title);
            project.setSubtitle(subtitle);
            project.setPathImageOriginal(pathImageOriginal);
            project.setPathImageCrop(pathImageCrop);
            db.updateProject(project);

            int i = 0;
            for (ProjectDetail pd : db.getAllProjectDetailsByProjectId(projectId)) {

                if (!pd.getPathImageCrop().equals(pd.getPathImageOriginal()) && !pd.getPathImageCrop().equals(reviewArrayAdapter.getItemPathImageCrop(i + 1))) {
                    File file = new File(pd.getPathImageCrop());
                    if (file.exists()) {
                        file.delete();
                        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(pd.getPathImageCrop()))));
                    }
                }

                pd.setPathImageCrop(reviewArrayAdapter.getItemPathImageCrop(i + 1));
                pd.setPathImageOriginal(reviewArrayAdapter.getItemPathImageOriginal(i + 1)); // + 1 cz index 0 is cover
                db.updateProjectDetail(pd);

                i++;
            }

            addToCart(projectId);
            startActivity(CartActivity.class);
        }*/

/*
        Intent intent = new Intent(ProductReviewActivity.this, ProjectActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);*/


    }

}
