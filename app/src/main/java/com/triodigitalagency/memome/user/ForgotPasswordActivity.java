package com.triodigitalagency.memome.user;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.android.volley.VolleyError;
import com.triodigitalagency.memome.MainActivity;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotPasswordActivity extends BaseActivity {

    //<editor-fold desc="Variable">
    protected final int SUCCESS = 2;
    //</editor-fold>

    //<editor-fold desc="Override">
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        setLog("activity_forgot_password");

        firstInit();
        setupActionBar(getResources().getString(R.string.title_forgot_password));

        if (isLogin()) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }

        findViewById(R.id.buttonSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isValid             = true;
                EditText editTextEmail      = (EditText)findViewById(R.id.editTextEmail);

                if (!Globals.isValidEmail(editTextEmail.getText().toString())){
                    Globals.showAlert(getResources().getString(R.string.text_register_error),
                            getResources().getString(R.string.text_invalid_email),
                            (Activity) context);
                    isValid = false;
                }

                if (isValid && !isLoading()) {
                    showLoading();

                    Map<String, String> params = new HashMap<String, String>();
                    params.put("clientId", Constant.CLIENT_ID);
                    params.put("email", editTextEmail.getText().toString());

                    request.getJSONFromUrl(Globals.getApiUrl("users/forgetPassword"),
                        params,
                        getResources().getString(R.string.text_forget_password_error),
                        new RequestCallback() {
                            @Override
                            public void onResponse(JSONObject response) {
                                hideLoading();
                                setResult(SUCCESS);
                                finish();
                            }

                            @Override
                            public void onError(VolleyError error) {
                                hideLoading();
                            }
                        });
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //</editor-fold>
}
