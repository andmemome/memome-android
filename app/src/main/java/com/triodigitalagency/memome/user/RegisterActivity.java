package com.triodigitalagency.memome.user;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.android.volley.VolleyError;
import com.triodigitalagency.memome.MainActivity;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends BaseActivity {

    //<editor-fold desc="Variable">
    protected EditText editTextEmail, editTextPassword, editTextConfirm, editTextName;
    protected final int SUCCESS = 2;
    //</editor-fold>

    //<editor-fold desc="Override">
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setLog("activity_register");

        firstInit();
        setupActionBar(getResources().getString(R.string.title_register));

        if (isLogin()) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }

        editTextEmail       = (EditText)findViewById(R.id.editTextEmail);
        editTextPassword    = (EditText)findViewById(R.id.editTextPassword);
        editTextConfirm     = (EditText)findViewById(R.id.editTextConfirmPassword);
        editTextName        = (EditText)findViewById(R.id.editTextName);

        findViewById(R.id.buttonRegister).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isLoading()){
                    showLoading();

                    if(validateForm()){
                        submitForm();
                    }
                    else{
                        hideLoading();
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //</editor-fold>

    //<editor-fold desc="Procedure & Function">
    protected boolean validateForm(){
        boolean isValid             = true;

        if (!Globals.isValidEmail(editTextEmail.getText().toString())){
            Globals.showAlert(getResources().getString(R.string.text_register_error),
                    getResources().getString(R.string.text_invalid_email),
                    (Activity) context);
            isValid = false;
        }
        else if (editTextPassword.getText().toString().length() == 0 && !editTextPassword.getText().toString().equals(editTextConfirm.getText().toString() )){
            Globals.showAlert(getResources().getString(R.string.text_register_error),
                    getResources().getString(R.string.text_password_mismatch_error),
                    (Activity)context);
            isValid = false;
        }
        else if (editTextName.getText().toString().length() == 0) {
            Globals.showAlert(getResources().getString(R.string.text_register_error),
                    getResources().getString(R.string.text_name_required),
                    (Activity)context);
            isValid = false;
        }
        return isValid;
    }

    protected void submitForm(){
        Map<String, String> params = new HashMap<String, String>();
        params.put("clientId", Constant.CLIENT_ID);
        params.put("email", editTextEmail.getText().toString());
        params.put("password", editTextPassword.getText().toString());
        params.put("name", editTextName.getText().toString());
        request.getJSONFromUrl(Globals.getApiUrl("users/register"), params, getResources().getString(R.string.text_register_error), new RequestCallback() {
            @Override
            public void onResponse(JSONObject response) {
                hideLoading();
                setResult(SUCCESS);
                finish();
            }

            @Override
            public void onError(VolleyError error) {
                hideLoading();
            }
        });
    }
    //</editor-fold>
}
