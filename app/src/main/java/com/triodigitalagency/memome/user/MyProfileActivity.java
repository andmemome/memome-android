package com.triodigitalagency.memome.user;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.instagrampicker.InstagramPhoto;
import android.instagrampicker.InstagramPhotoPicker;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alexbbb.uploadservice.AbstractUploadServiceReceiver;
import com.alexbbb.uploadservice.ContentType;
import com.alexbbb.uploadservice.MultipartUploadRequest;
import com.android.volley.VolleyError;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.sangcomz.fishbun.FishBun;
import com.sangcomz.fishbun.adapter.image.impl.PicassoAdapter;
import com.sangcomz.fishbun.define.Define;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.triodigitalagency.memome.MainActivity;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.adapter.UserAddressArrayAdapter;
import com.triodigitalagency.memome.transaction.CartActivity;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


import static com.triodigitalagency.memome.utils.Globals.CLIENT_ID;
import static com.triodigitalagency.memome.utils.Globals.REDIRECT_URI;
import static com.triodigitalagency.memome.utils.Globals.REQUEST_CODE_INSTAGRAM_PICKER;

public class MyProfileActivity extends BaseActivity {

    private RelativeLayout tapForReloadLayout;
    private Button buttonTapForReload;
    private RelativeLayout listHeaderView;
    private ImageView imageViewProfilePicture;
    private GoogleApiClient mGoogleApiClient;
    private UserAddressArrayAdapter arrayAdapter;
    private ListView listView;
    private ArrayList<JSONObject> dataSource;
    private final int SELECT_FILE = 1;
    private final int ADDRESS = 2;
    private final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 99;
    private static final String TAG = "MyProfileActivity";
    private ArrayList<Uri> selectedImagesPath = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.showCartIcon   = true;

        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult connectionResult) {
                    }
                } /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        setContentView(R.layout.activity_my_profile);
        setLog("activity_my_profile");

        firstInit();
        setupActionBar(getResources().getString(R.string.title_my_profile));

        if (!isLogin()) {
            back();
        }

        findViewById(R.id.buttonLogout).setOnClickListener(buttonLogoutClicked);

        dataSource                      = new ArrayList<JSONObject>();
        arrayAdapter                    = new UserAddressArrayAdapter(context);
        LayoutInflater inflater         = activity.getLayoutInflater();
        listHeaderView                  = (RelativeLayout)inflater.inflate(R.layout.layout_my_profile_header, null);
        listView                        = (ListView) findViewById(R.id.listViewAddress);
        listView.addHeaderView(listHeaderView);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(onListViewClicked);

        listHeaderView.findViewById(R.id.buttonAddAddress).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(AddressActivity.class, ADDRESS, new OnSetupIntentExtras() {
                    @Override
                    public void onSetupIntentExtras(Intent intent) {
                        intent.putExtra("isCreate", true);
                    }
                });
            }
        });
        TextView textViewProfileName        = (TextView)listHeaderView.findViewById(R.id.textViewProfileName);
        imageViewProfilePicture             = (ImageView)listHeaderView.findViewById(R.id.imageViewProfilePicture);
        
        String name                         = Globals.getStringDataPreference(context, Constant.P_USER_NAME);
        textViewProfileName.setText(name + " - " + getResources().getString(R.string.text_edit));
        textViewProfileName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(EditProfileActivity.class);
            }
        });

        listHeaderView.findViewById(R.id.relativeLayoutProfilePicture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ActivityCompat.requestPermissions(MyProfileActivity.this,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED){
//                    getImage();
//                    getFishBun();
                    getChoiseImage();
                }else {
                    setLog("test");
                    Snackbar s = Snackbar.make(findViewById(android.R.id.content),"We require a write permission. Please allow it in Settings.",Snackbar.LENGTH_INDEFINITE)
                            .setAction("SETTINGS", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                                    intent.setData(uri);
                                    startActivityForResult(intent, 1000);
                                }
                            });
                    View snackbarView = s.getView();
                    TextView textView = snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setMaxLines(3);
                    s.show();

                }

            }
        });

        loadDataUser();
        initTapForReload();
    }

    private void getFishBun() {
        selectedImagesPath.clear();
        int countDetail = 1;
        FishBun.with(this)
                .setImageAdapter(new PicassoAdapter())
                .setMaxCount(countDetail)
                .exceptGif(true)
                .setSelectedImages(selectedImagesPath)
                .setActionBarColor(Color.parseColor("#ffffff"), Color.parseColor("#40A99C"), true)
                .setActionBarTitleColor(Color.parseColor("#000000"))
                .setHomeAsUpIndicatorDrawable(ContextCompat.getDrawable(this, R.drawable.toolbar_navigation_back_button))
                .setOkButtonDrawable(ContextCompat.getDrawable(this, R.drawable.baseline_check_black_24))
                .startAlbum();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_default, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            back();
            return true;
        }
        else if (id == R.id.actionCart) {
            startActivity(CartActivity.class);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //Handle the back button
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            back();
            return true;
        }
        else {
            return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        uploadReceiver.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        uploadReceiver.unregister(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                if (0 == PackageManager.PERMISSION_GRANTED) {   // Comment 1.
//                    getImage();
//                    getFishBun();
                    getChoiseImage();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {    // Comment 2.
                        Snackbar s = Snackbar.make(findViewById(android.R.id.content),"We require a write permission. Please allow it in Settings.",Snackbar.LENGTH_INDEFINITE)
                                .setAction("SETTINGS", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                                        intent.setData(uri);
                                        startActivityForResult(intent, 1000);     // Comment 3.
                                    }
                                });
                        View snackbarView = s.getView();
                        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                        textView.setMaxLines(3);    // Comment 4.
                        s.show();
                    }
                }
            }
        }

        if (requestCode== REQUEST_CODE_INSTAGRAM_PICKER){
            if (resultCode == Activity.RESULT_OK) {
                InstagramPhoto[] instagramPhotos = InstagramPhotoPicker.getResultPhotos(data);
                Log.e("dbotha", "User selected " + instagramPhotos.length + " Instagram photos");
                for (int i = 0; i < instagramPhotos.length; ++i) {
                    Log.e("dbotha", "Photo: " + instagramPhotos[i].getFullURL());
                }

                Picasso.with(MyProfileActivity.this)
                        .load(instagramPhotos[0].getFullURL().toString())
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                Uri tempUri = getImageUri(getApplicationContext(), bitmap);

                                // CALL THIS METHOD TO GET THE ACTUAL PATH
                                File finalFile = new File(getRealPathFromURI(tempUri));

                                uploadProfilePicture(finalFile.getPath());
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {

                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        });
            }
        }
        /* if (resultCode == RESULT_OK && requestCode == SELECT_FILE) {
            Uri selectedImageUri        = data.getData();
            String[] filePathColumn     = {MediaStore.Images.Media.DATA};
            Cursor cursor               = getContentResolver().query(
                    selectedImageUri, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex             = cursor.getColumnIndex(filePathColumn[0]);
            String pathImageOriginal    = cursor.getString(columnIndex);
            cursor.close();

            setLog("---->> "+pathImageOriginal);

//       uploadProfilePicture(pathImageOriginal);
        /storage/emulated/0/Memome/IMG_20180718141451.jpg
        }
        */

        if (resultCode == RESULT_OK && requestCode == ADDRESS) {
            loadDataUser();
        }


        if (requestCode == Define.ALBUM_REQUEST_CODE) {
            try {
                selectedImagesPath = data.getParcelableArrayListExtra(Define.INTENT_PATH);
                Uri selectedImageUri        = selectedImagesPath.get(0);
                String[] filePathColumn     = {MediaStore.Images.Media.DATA};
                Cursor cursor               = getContentResolver().query(
                        selectedImageUri, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex             = cursor.getColumnIndex(filePathColumn[0]);
                String pathImageOriginal    = cursor.getString(columnIndex);
                cursor.close();

                uploadProfilePicture(pathImageOriginal);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);

    }



    private void getChoiseImage() {
        final CharSequence[] options = {"Choose From Gallery","Take Photo from instagram","Cancel"};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity);
        builder.setTitle("Select Option");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo from instagram")) {
                    getIntagram();
                } else if (options[item].equals("Choose From Gallery")) {
                    getFishBun();
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();


    }

    private void getIntagram() {
        InstagramPhotoPicker.startPhotoPickerForResult(this, CLIENT_ID, REDIRECT_URI, REQUEST_CODE_INSTAGRAM_PICKER,1);
    }

    private void getImage() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(
                Intent.createChooser(intent, "Select File"),
                SELECT_FILE);
    }


    private View.OnClickListener buttonLogoutClicked = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (!isLoading()) {
                showLoading();
                Map<String, String> params = new HashMap<String, String>();
                params.put("clientId", Constant.CLIENT_ID);
                params.put("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
                request.getJSONFromUrl(Globals.getApiUrl("users/logout"), params, getResources().getString(R.string.text_logout_error), new RequestCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideLoading();
                        Globals.setDataPreference(context, Constant.P_USER_ID, (long) 0);
                        Globals.setDataPreference(context, Constant.P_USER_NAME, "");
                        Globals.setDataPreference(context, Constant.P_USER_IMAGE_PATH, "");
                        Globals.setDataPreference(context, Constant.P_ACCESS_TOKEN, "");
                        Globals.showToast(getResources().getString(R.string.text_logout_success), context);

                        LoginManager.getInstance().logOut();

                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {

                                    }
                                });

                        Intent intent = new Intent(activity, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onError(VolleyError error) { hideLoading(); }
                });
            }
        }
    };

    protected AdapterView.OnItemClickListener onListViewClicked = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
            long viewId = view.getId();
            if (position >= 0 && viewId == R.id.textViewEdit) {
                startActivityForResult(AddressActivity.class, ADDRESS, new OnSetupIntentExtras() {
                    @Override
                    public void onSetupIntentExtras(Intent intent) {
                        setLog("-- "+arrayAdapter.getItemId(position));
                        intent.putExtra("isCreate", false);
                        intent.putExtra("userAddressId", arrayAdapter.getItemId(position));
                    }
                });
            }
        }
    };

    protected void initTapForReload() {
        buttonTapForReload  = findViewById(R.id.buttonTapForReload);
        buttonTapForReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tapForReloadLayout.setVisibility(View.GONE);
                loadDataUser();
            }
        });

        tapForReloadLayout  = findViewById(R.id.tapForReload);
        tapForReloadLayout.setVisibility(View.GONE);
    }

    protected void loadDataUser() {
        if (!isLoading()) {
            showLoading();
        }

        Map<String, String> params = new HashMap<String, String>();
        params.put("clientId", Constant.CLIENT_ID);
        params.put("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
        request.getJSONFromUrl(Globals.getApiUrl("users/view"), params, getResources().getString(R.string.text_my_profile_error), new RequestCallback() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray userAddresses = response.getJSONArray("userAddresses");

                    if (dataSource == null) {
                        dataSource = new ArrayList<JSONObject>();
                    } else {
                        dataSource.clear();
                    }

                    for (int i = 0; i < userAddresses.length(); i++) {
                        dataSource.add(userAddresses.getJSONObject(i));
                    }

                    arrayAdapter.setValues(dataSource);
                    arrayAdapter.notifyDataSetChanged();

                    JSONObject user = response.getJSONObject("user");
                    if (!user.get("images").equals(null) && !user.get("images").equals("") && !user.getJSONObject("images").getString("original").equals(null) && !user.getJSONObject("images").getString("original").equals("")) {
                        Globals.setDataPreference(context, Constant.P_USER_IMAGE_PATH, user.getJSONObject("images").getString("original"));
                    }
                    updateImageViewProfilePicture();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                hideLoading();
            }

            @Override
            public void onError(VolleyError error) {
                tapForReloadLayout.setVisibility(View.VISIBLE);
                hideLoading();
            }
        });
    }

    protected void uploadProfilePicture(String pathImageOriginal) {
        if (!isLoading()) {
            showLoading();
        }

        String serverUrlString  = Globals.getApiUrl("users/uploadProfilePicture");

        final MultipartUploadRequest request =
                new MultipartUploadRequest(this, UUID.randomUUID().toString(), serverUrlString);

        request.addParameter("clientId", Constant.CLIENT_ID);
        request.addParameter("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));

        String paramNameString              = "profile_picture";
        request.addFileToUpload(pathImageOriginal, paramNameString,
                "memome", ContentType.APPLICATION_OCTET_STREAM);

        request.setNotificationConfig(R.mipmap.ic_launch,
                getString(R.string.app_name),
                getString(R.string.uploading),
                getString(R.string.upload_success),
                getString(R.string.upload_error),
                false, true);

        // if you comment the following line, the system default user-agent will be used
        request.setCustomUserAgent("UploadServiceDemo/1.0");

        // set the intent to perform when the user taps on the upload notification.
        // currently tested only with intents that launches an activity
        // if you comment this line, no action will be performed when the user taps
        // on the notification
        Intent intent = new Intent(context, MyProfileActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        request.setNotificationClickIntent(intent);

        // set the maximum number of automatic upload retries on error
        request.setMaxRetries(2);

        try {
            request.startUpload();
        } catch (Exception exc) {
            Globals.showToast("Malformed upload request. " + exc.getLocalizedMessage(), context);
        }
    }

    private final AbstractUploadServiceReceiver uploadReceiver = new AbstractUploadServiceReceiver() {

        @Override
        public void onProgress(String uploadId, int progress) {
            if (!isLoading()) {
                showLoading();
            }
            Log.i(TAG, "The progress of the upload with ID " + uploadId + " is: " + progress);
        }

        @Override
        public void onError(String uploadId, Exception exception) {
            hideLoading();
            //Log.e(TAG, "Error in upload with ID: " + uploadId + ". " + exception.getLocalizedMessage(), exception);
        }

        @Override
        public void onCompleted(String uploadId, int serverResponseCode, String serverResponseMessage) {
            //Log.i(TAG, "Upload with ID " + uploadId + " is completed: " + serverResponseCode + ", " + serverResponseMessage);

            try {
                JSONObject user = new JSONObject(serverResponseMessage).getJSONObject("user");
                if (!user.get("images").equals(null) && !user.get("images").equals("") && !user.getJSONObject("images").getString("original").equals(null) && !user.getJSONObject("images").getString("original").equals("")) {
                    Globals.setDataPreference(context, Constant.P_USER_IMAGE_PATH, user.getJSONObject("images").getString("original"));
                }
                updateImageViewProfilePicture();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            hideLoading();
        }
    };

    private void updateImageViewProfilePicture() {
        String imagePath                    = Globals.getStringDataPreference(context, Constant.P_USER_IMAGE_PATH);
        if (!imagePath.equals("")) {
            request.getImageFromUrl(imagePath, imageViewProfilePicture, 0);
        }
        else {
            imageViewProfilePicture.setImageResource(0);
        }
    }

    private void back() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
    //</editor-fold>
}
