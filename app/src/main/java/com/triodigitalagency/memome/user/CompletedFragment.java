package com.triodigitalagency.memome.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.adapter.MyPurchasesArrayAdapter;
import com.triodigitalagency.memome.transaction.CheckoutActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;
import com.triodigitalagency.memome.volley.utils.VolleyRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class CompletedFragment extends Fragment {

    private View layout;
    private ProgressBar progressBar;
    private VolleyRequest request;
    private MyPurchasesArrayAdapter arrayAdapter;
    private ListView listView3;
    private ArrayList<Bundle> dataSource;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layout          = inflater.inflate(R.layout.fragment_completed, container, false);
        progressBar     = (ProgressBar)layout.findViewById(R.id.ctrlActivityIndicator);
        request         = new VolleyRequest(getActivity());
        dataSource      = new ArrayList<>();
        listView3       = (ListView)layout.findViewById(R.id.myListView);
        arrayAdapter    = new MyPurchasesArrayAdapter(getContext());
        listView3.setAdapter(arrayAdapter);

        //I do not understand why this did not work
        /*listView3.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setLog("Test");

            }
        });*/

        arrayAdapter.setOnClikItemGJ(new MyPurchasesArrayAdapter.onClikItemGJ() {
            @Override
            public void onClikItem(int position) {
                if (position < dataSource.size()) {
                    Bundle data = dataSource.get(position);
                    String itemString = data.getString("order");
                    Intent intent = new Intent(getActivity(), PurchaseDetailActivity.class);
                    intent.putExtra("order", itemString);
                    startActivity(intent);
                }
            }

            @Override
            public void onClikReorder(String id) {
                setLog("Get Data Reorder"+id);
                getReorder(id);
            }
        });

        loadData();

        return layout;
    }

    private void setLog(String test) {
        Log.e("TAG",test);
    }

    protected void getReorder(String id){
        showLoading();
        Map<String, String> params = new HashMap<String, String>();
        params.put("clientId", Constant.CLIENT_ID);
        params.put("accessToken", Globals.getStringDataPreference(getContext(), Constant.P_ACCESS_TOKEN));
        params.put("orderId",id);
        request.getJSONFromUrl(Globals.getApiUrl("orders/getById"), params, getResources().getString(R.string.text_my_reorder_error), new RequestCallback() {
            @Override
            public void onResponse(final JSONObject response) {
                hideLoading();
                setLog("---------------- "+response);
                if ("0".equals(response.optJSONObject("order").optString("orderid_reorder"))){
                    Gson gson = new Gson();
                    String total =  response.optJSONObject("order").optString("subtotal");
                    Intent intent = new Intent(getActivity(),CheckoutActivity.class);
                    intent.putExtra("totalPrice",Double.valueOf(total));
                    intent.putExtra("id",response.optJSONObject("order").optJSONArray("order_product").optJSONObject(0).optString("product_id"));
                    intent.putExtra("order",gson.toJson(response.optJSONObject("order").optJSONArray("order_product")));
                    intent.putExtra("order1",gson.toJson(response.optJSONObject("order")));
                    intent.putExtra("id_order",response.optJSONObject("order").optString("id"));
                    intent.putExtra("reorder",true);
                    startActivity(intent);
                }else {
                    Gson gson = new Gson();
                    String total =  response.optJSONObject("order").optString("subtotal");
                    Intent intent = new Intent(getActivity(),CheckoutActivity.class);
                    intent.putExtra("totalPrice",Double.valueOf(total));
                    intent.putExtra("id",response.optJSONObject("order").optJSONArray("order_product").optJSONObject(0).optString("product_id"));
                    intent.putExtra("order",gson.toJson(response.optJSONObject("order").optJSONArray("order_product")));
                    intent.putExtra("order1",gson.toJson(response.optJSONObject("order")));
                    intent.putExtra("id_order",response.optJSONObject("order").optString("orderid_reorder"));
                    intent.putExtra("reorder",true);
                    startActivity(intent);
                }

            }

            @Override
            public void onError(VolleyError error) {
                hideLoading();
            }
        });
    }


    protected void loadData() {
        showLoading();

        Map<String, String> params = new HashMap<String, String>();
        params.put("clientId", Constant.CLIENT_ID);
        params.put("accessToken", Globals.getStringDataPreference(getContext(), Constant.P_ACCESS_TOKEN));
        request.getJSONFromUrl(Globals.getApiUrl("orders/getCompletedOrders"), params, getResources().getString(R.string.text_my_profile_error), new RequestCallback() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray orders = response.getJSONArray("orders");
                    setLog("completedd "+response);
                    hideLoading();

                    for (int i = 0; i < orders.length(); i++) {
                        Bundle data = new Bundle();
                        JSONObject order = orders.getJSONObject(i);
                        data.putString("order", order.toString());
                        data.putBoolean("isExpanded", false);

                        ArrayList<Bundle> arrayList = new ArrayList<>();

                        JSONArray arrayProduct = order.getJSONArray("order_product");

                        for (int j = 0; j < arrayProduct.length(); j++) {

                            Bundle dataProduct = new Bundle();
                            JSONObject product = arrayProduct.getJSONObject(j);

                            if (product.has("product_title")) {
                                dataProduct.putString("title", product.getString("product_title"));
                            }
                            if (product.has("total_price")) {
                                dataProduct.putString("value", String.valueOf((int) product.optDouble("total_price", 0)));
                            }
                            if (product.has("qty")) {
                                dataProduct.putString("quantity", product.getString("qty"));
                            }
                            String name = "";
                            if (product.has("title")) {
                                name += product.getString("title");
                            }
                            if (product.has("subtitle")) {
                                name += " "+product.getString("subtitle");
                            }

                            if (!"".equals(name.trim())) {
                                dataProduct.putString("name", name);
                            }

                            arrayList.add(dataProduct);
                        }
                        if (order.has("voucher_total")) {
                            Bundle dataProduct = new Bundle();
                            dataProduct.putString("title", "Discount");
                            dataProduct.putString("value", String.valueOf((int) order.optDouble("voucher_total", 0)));

                            arrayList.add(dataProduct);
                        }
                        if (order.has("shipping_price")) {
                            Bundle dataProduct = new Bundle();
                            if (order.has("shipping_type")) {
                                dataProduct.putString("title", "Shipping ("+order.getString("shipping_type")+")");
                            }
                            else {
                                dataProduct.putString("title", "Shipping");
                            }
                            dataProduct.putString("value", String.valueOf((int) order.optDouble("shipping_price", 0)));

                            arrayList.add(dataProduct);
                        }

                        data.putSerializable("expandData", arrayList);

                        dataSource.add(data);
                    }

                    layout.findViewById(R.id.textViewDataNotFound).setVisibility(orders.length() <= 0 ? View.VISIBLE : View.GONE);

                    arrayAdapter.setValues(dataSource);
                    arrayAdapter.notifyDataSetChanged();

                    hideLoading();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(VolleyError error) {
                hideLoading();
            }
        });
    }

    protected void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    protected void hideLoading() {
        request.setAlreadyShowErrorAlert(false);
        progressBar.setVisibility(View.GONE);
    }

    protected boolean isLoading() {
        return progressBar.getVisibility() == View.VISIBLE;
    }
}