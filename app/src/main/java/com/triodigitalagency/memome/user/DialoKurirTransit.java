package com.triodigitalagency.memome.user;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.adapter.RecyclerAdapterDialogKurir;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;
import com.triodigitalagency.memome.volley.utils.VolleyRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by khoiron on 23/05/18.
 */

public class DialoKurirTransit extends BaseActivity {

    @BindView(R.id.noResi)TextView noResi;
    @BindView(R.id.resiLogistik)TextView resiLogistik;
    @BindView(R.id.namaPengirim)TextView namaPengirim;
    @BindView(R.id.alamatPengirim)TextView alamatPengirim;
    @BindView(R.id.alamatTujuan)TextView alamatTujuan;
    @BindView(R.id.namaTujuan)TextView namaTujuan;
    @BindView(R.id.statusTracking)TextView statusTracking;
    @BindView(R.id.dateTracking)TextView dateTracking;
    @BindView(R.id.myrecyclerview)RecyclerView myrecyclerview;
    @BindView(R.id.status)TextView status;
    @OnClick(R.id.btnRiwayatStatus)void btnRiwayatStatus(){
        if (lineRiwayat.getVisibility()==View.VISIBLE){
            lineRiwayat.setVisibility(View.GONE);
            imageIcon.setImageResource(R.drawable.icon_up);
        }else {
            lineRiwayat.setVisibility(View.VISIBLE);
            imageIcon.setImageResource(R.drawable.icon_down);
        }

    }
    @BindView(R.id.imageIcon) ImageView imageIcon;
    @OnClick(R.id.imageIcon)void imageIcon(){
        if (lineRiwayat.getVisibility()==View.VISIBLE){
            imageIcon.setImageResource(R.drawable.icon_up);
            lineRiwayat.setVisibility(View.GONE);
        }else {
            lineRiwayat.setVisibility(View.VISIBLE);
            imageIcon.setImageResource(R.drawable.icon_down);
        }
    }
    @BindView(R.id.lineRiwayat)LinearLayout lineRiwayat;
    RecyclerAdapterDialogKurir recyclerAdapterDialogKurir;
    private JSONObject jsonObject;
    ArrayList<JSONObject> values = new ArrayList<>();
    @BindView(R.id.close)ImageView closeListner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_kurir_transit);
        ButterKnife.bind(this);
        imageIcon.setImageResource(R.drawable.icon_up);
        recyclerAdapterDialogKurir = new RecyclerAdapterDialogKurir(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        myrecyclerview.setLayoutManager(mLayoutManager);
        myrecyclerview.setItemAnimator(new DefaultItemAnimator());
        myrecyclerview.setNestedScrollingEnabled(false);
        myrecyclerview.setAdapter(recyclerAdapterDialogKurir);

        firstInit();

        try {
            jsonObject = new JSONObject(getIntent().getStringExtra("shipping"));
            setLog("json "+jsonObject.toString());
            setLog("id "+jsonObject.optString("id"));

            getWayBill(jsonObject.optString("id"));


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }

        closeListner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void getWayBill(String id) {

//        showLoading();
        Map<String, String> params = new HashMap<String, String>();
        params.put("clientId", Constant.CLIENT_ID);
        params.put("orderId",id);
        request.getJSONFromUrl(Globals.getApiUrl("orders/getWaybill"), params, "get waybill error", new RequestCallback() {
            @Override
            public void onResponse(JSONObject response) {
                setLog("------- "+response.toString());


                JSONObject wayBill = response.optJSONObject("order").optJSONObject("waybill").optJSONObject("details");
                JSONArray jsonArray = response.optJSONObject("order").optJSONObject("waybill").optJSONArray("manifest");

                noResi.setText(jsonObject.optString("shipping_number"));
                status.setText(response.optJSONObject("order").optJSONObject("waybill").optJSONObject("summary").optString("status"));
                resiLogistik.setText(response.optJSONObject("order").optJSONObject("waybill").optJSONObject("summary").optString("waybill_number"));
                namaPengirim.setText(wayBill.optString("shippper_name"));

                namaTujuan.setText(jsonObject.optString("shipping_first_name")+" "+jsonObject.optString("shipping_last_name"));
                alamatTujuan.setText(jsonObject.optString("shipping_address")+","+jsonObject.optString("shipping_city")+" "+jsonObject.optString("shipping_province"));
                alamatPengirim.setText(wayBill.optString("shipper_address1")+wayBill.optString("shipper_address2")+wayBill.optString("shipper_address3")+wayBill.optString("origin"));


                if (jsonArray.length()>0){
                    setLog("====== waybill" +jsonArray.toString());
                    for (int i =0;i<jsonArray.length();i++){
                        try {
                            JSONObject data = new JSONObject(jsonArray.get(i).toString());
                            values.add(data);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    recyclerAdapterDialogKurir.setValues(values);
                    recyclerAdapterDialogKurir.notifyDataSetChanged();
                }
            }

            @Override
            public void onError(VolleyError error) {
                hideLoading();
            }
        });
    }


}
