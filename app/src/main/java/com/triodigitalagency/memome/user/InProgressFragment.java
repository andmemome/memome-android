package com.triodigitalagency.memome.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.android.volley.VolleyError;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.adapter.MyPurchasesArrayAdapter;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;
import com.triodigitalagency.memome.volley.utils.VolleyRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class InProgressFragment extends Fragment {

    private View layout;
    private ProgressBar progressBar;
    private VolleyRequest request;
    private MyPurchasesArrayAdapter arrayAdapter;
    private ListView listView;
    private ArrayList<Bundle> dataSource;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layout          = inflater.inflate(R.layout.fragment_in_progress, container, false);
        progressBar     = layout.findViewById(R.id.ctrlActivityIndicator);
        request         = new VolleyRequest(getActivity());
        dataSource      = new ArrayList<>();
        listView        = layout.findViewById(R.id.listView);
        arrayAdapter    = new MyPurchasesArrayAdapter(getContext());
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("CLICK", "onItemClick: ");
                if(position < dataSource.size()){
                    Bundle data = dataSource.get(position);
                    String itemString = data.getString("order");
                    Intent intent = new Intent(getActivity(), PurchaseDetailActivity.class);
                    intent.putExtra("order", itemString);
                    startActivity(intent);
                }
            }
        });

        arrayAdapter.setOnClikItemListen(new MyPurchasesArrayAdapter.onClikItemListen() {
            @Override
            public void onClikItem(int position, int id_item) {


                if(position < dataSource.size()){
                    Bundle data = dataSource.get(position);
                    String itemString = data.getString("order");
                    Intent intent = new Intent(getActivity(), PurchaseDetailActivity.class);
                    intent.putExtra("order", itemString);
                    startActivity(intent);
                }
            }
        });

        loadData();

        return layout;
    }

    protected void loadData() {
        showLoading();
        Map<String, String> params = new HashMap<String, String>();
        params.put("clientId", Constant.CLIENT_ID);
        params.put("accessToken", Globals.getStringDataPreference(getContext(), Constant.P_ACCESS_TOKEN));
        request.getJSONFromUrl(Globals.getApiUrl("orders/getInProgressOrders"), params, getResources().getString(R.string.text_my_profile_error), new RequestCallback() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.e("Test Aplikasi ",response.toString());
                    JSONArray orders = response.getJSONArray("orders");
                    hideLoading();

                    for (int i = 0; i < orders.length(); i++) {
                        Bundle data = new Bundle();
                        JSONObject order = orders.getJSONObject(i);
                        data.putString("order", order.toString());
                        data.putBoolean("isExpanded", false);

                        ArrayList<Bundle> arrayList = new ArrayList<>();

                        JSONArray arrayProduct = order.getJSONArray("order_product");

                        for (int j = 0; j < arrayProduct.length(); j++) {

                            Bundle dataProduct = new Bundle();
                            JSONObject product = arrayProduct.getJSONObject(j);

                            if (product.has("product_title")) {
                                dataProduct.putString("title", product.getString("product_title"));
                            }
                            if (product.has("total_price")) {
                                dataProduct.putString("value", String.valueOf((int) product.optDouble("total_price", 0)));
                            }
                            if (product.has("qty")) {
                                dataProduct.putString("quantity", product.getString("qty"));
                            }
                            String name = "";
                            if (product.has("title")) {
                                name += product.getString("title");
                            }
                            if (product.has("subtitle")) {
                                name += " "+product.getString("subtitle");
                            }

                            if (!"".equals(name.trim())) {
                                dataProduct.putString("name", name);
                            }

                            arrayList.add(dataProduct);
                        }
                        if (order.has("voucher_total")) {
                            Bundle dataProduct = new Bundle();
                            dataProduct.putString("title", "Discount");
                            dataProduct.putString("value", String.valueOf((int) order.optDouble("voucher_total", 0)));

                            arrayList.add(dataProduct);
                        }
                        if (order.has("shipping_price")) {
                            Bundle dataProduct = new Bundle();
                            if (order.has("shipping_type")) {
                                dataProduct.putString("title", "Shipping ("+order.getString("shipping_type")+")");
                            }
                            else {
                                dataProduct.putString("title", "Shipping");
                            }
                            dataProduct.putString("value", String.valueOf((int) order.optDouble("shipping_price", 0)));

                            arrayList.add(dataProduct);
                        }

                        data.putSerializable("expandData", arrayList);

                        dataSource.add(data);
                    }

                    layout.findViewById(R.id.textViewDataNotFound).setVisibility(orders.length() <= 0 ? View.VISIBLE : View.GONE);

                    arrayAdapter.setValues(dataSource);
                    arrayAdapter.notifyDataSetChanged();

                    hideLoading();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(VolleyError error) {
                hideLoading();
            }
        });
    }

    protected void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    protected void hideLoading() {
        request.setAlreadyShowErrorAlert(false);
        progressBar.setVisibility(View.GONE);
    }

    protected boolean isLoading() {
        return progressBar.getVisibility() == View.VISIBLE;
    }

}
