package com.triodigitalagency.memome.user;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.android.volley.VolleyError;
import com.triodigitalagency.memome.MainActivity;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ResetPasswordActivity extends BaseActivity {

    //<editor-fold desc="Variable">
    protected EditText editTextPassword, editTextConfirm;
    private String activationKey = null;
    protected final int RESET = 3;
    protected Bundle extras;
    //</editor-fold>

    //<editor-fold desc="Override">
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        setLog("activity_reset_password");

        firstInit();
        setupActionBar(getResources().getString(R.string.title_reset_password));

        extras = getIntent().getExtras();
        if (extras != null){
            activationKey = extras.getString("activationKey");
        }
        else {
            finish();
        }

        if (isLogin()) {
            back();
        }

        editTextPassword    = (EditText)findViewById(R.id.editTextPassword);
        editTextConfirm     = (EditText)findViewById(R.id.editTextConfirmPassword);
        findViewById(R.id.buttonSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (activationKey != null) {
                    if (!isLoading()) {
                        showLoading();
                    }

                    if (editTextPassword.getText().toString().equals(editTextConfirm.getText().toString())) {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("clientId", Constant.CLIENT_ID);
                        params.put("newPassword", editTextPassword.getText().toString());
                        params.put("activationKey", activationKey);
                        request.getJSONFromUrl(Globals.getApiUrl("users/resetPassword"),
                                params,
                                getResources().getString(R.string.text_reset_password_error),
                                new RequestCallback() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        hideLoading();
                                        startActivity(LoginActivity.class, new OnSetupIntentExtras() {
                                            @Override
                                            public void onSetupIntentExtras(Intent intent) {
                                                intent.putExtra("reset", RESET);
                                            }
                                        });
                                    }

                                    @Override
                                    public void onError(VolleyError error) {
                                        hideLoading();
                                    }
                                });
                    } else {
                        Globals.showAlert(getResources().getString(R.string.text_reset_password_error), getResources().getString(R.string.text_password_mismatch_error), activity);
                        hideLoading();
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            back();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //Handle the back button
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            back();
            return true;
        }
        else {
            return super.onKeyDown(keyCode, event);
        }
    }
    //</editor-fold>

    //<editor-fold desc="Procedure & Function">
    public void back() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
    //</editor-fold>
}
