package com.triodigitalagency.memome.user;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.triodigitalagency.memome.R;
import com.triodigitalagency.memome.region.CityActivity;
import com.triodigitalagency.memome.region.ProvinceActivity;
import com.triodigitalagency.memome.utils.BaseActivity;
import com.triodigitalagency.memome.utils.Constant;
import com.triodigitalagency.memome.utils.Globals;
import com.triodigitalagency.memome.volley.callback.RequestCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AddressActivity extends BaseActivity {

    private Bundle extras;
    private Boolean isCreate;
    private Long userAddressId, provinceId, cityId;
    private EditText editTextFirstName, editTextLastName, editTextAddress, editTextPostalCode, editTextPhone, editTextEmail;
    private TextView textViewProvince, textViewCity;

    private final int BROWSE_PROVINCE = 1;
    private final int BROWSE_CITY = 2;

    private RelativeLayout tapForReloadLayout;
    private Button buttonTapForReload,buttonSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address2);
        setLog("activity_address");

        editTextFirstName   = findViewById(R.id.editTextFirstName);
        editTextLastName    = findViewById(R.id.editTextLastName);
        editTextAddress     = findViewById(R.id.editTextAddress);
        editTextPostalCode  = findViewById(R.id.editTextPostalCode);
        editTextPhone       = findViewById(R.id.editTextPhone);
        editTextEmail       = findViewById(R.id.editTextEmail);
        textViewProvince    = findViewById(R.id.textViewProvince);
        textViewCity        = findViewById(R.id.textViewCity);
        buttonSubmit        = findViewById(R.id.buttonSubmit);

        firstInit();
        extras = getIntent().getExtras();
        if (extras != null){
            isCreate = extras.getBoolean("isCreate");
            if (!isCreate && extras.get("userAddressId") != null && extras.getLong("userAddressId") > 0) {
                userAddressId   = extras.getLong("userAddressId");
            }
        }else {
            finish();
        }


        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });

        textViewProvince.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(ProvinceActivity.class, BROWSE_PROVINCE, new OnSetupIntentExtras() {
                    @Override
                    public void onSetupIntentExtras(Intent intent) {
                        if (provinceId != null && provinceId > 0) {
                            intent.putExtra("selectedId", provinceId);
                        }
                    }
                });
            }
        });

        textViewCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (provinceId != null && provinceId > 0) {
                    startActivityForResult(CityActivity.class, BROWSE_CITY, new OnSetupIntentExtras() {
                        @Override
                        public void onSetupIntentExtras(Intent intent) {
                            intent.putExtra("provinceId", provinceId);
                            if (cityId != null && cityId > 0) {
                                intent.putExtra("selectedId", cityId);
                            }
                        }
                    });
                }
                else {
                    Globals.showAlert(getResources().getString(R.string.text_city_error), getResources().getString(R.string.text_province_required), activity);
                }
            }
        });

        if (isCreate) {
            setupActionBar(getResources().getString(R.string.title_new_address));
        }else {
            setupActionBar(getResources().getString(R.string.title_edit_address));
            buttonSubmit.setText("save address".toUpperCase());
            loadDataUserAddress();
        }

        initTapForReload();

        setLog(isCreate+" ==== ");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!isCreate) {
            getMenuInflater().inflate(R.menu.menu_delete, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }
        else if (id == R.id.actionDelete) {
            new AlertDialog.Builder(context)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(R.string.text_cart_delete_title)
                    .setMessage(R.string.text_cart_delete_message)
                    .setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            deleteAddress();
                        }

                    })
                    .setNegativeButton(R.string.button_no, null)
                    .show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == BROWSE_PROVINCE && resultCode == RESULT_OK){
            Bundle temp     = data.getExtras();
            provinceId      = temp.getLong("selectedId");
            cityId          = Long.valueOf(0);
            textViewProvince.setText(temp.getString("selectedName"));
            textViewCity.setText("");
        }
        else if (requestCode == BROWSE_CITY && resultCode == RESULT_OK) {
            Bundle temp     = data.getExtras();
            cityId          = temp.getLong("selectedId");
            textViewCity.setText(temp.getString("selectedName"));
        }
    }

    protected void submit() {
        Boolean isGood  = true;
        isGood          = validateEditTextStringRequired(isGood);
        isGood          = validateSelectRequired(isGood);
        /*
        if (isGood && !Globals.isValidEmail(editTextEmail.getText().toString())){
            Globals.showAlert(getResources().getString(R.string.text_address_error),
                    getResources().getString(R.string.text_invalid_email),
                    activity);
            isGood = false;
        }*/

        if (isGood) {
            if (!isLoading()) {
                showLoading();
            }

            Map<String, String> params = new HashMap<String, String>();
            params.put("clientId", Constant.CLIENT_ID);
            params.put("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
            params.put("firstName", editTextFirstName.getText().toString());
            params.put("lastName", editTextLastName.getText().toString());
            params.put("address", editTextAddress.getText().toString());
            params.put("provinceId", String.valueOf(provinceId));
            params.put("cityId", String.valueOf(cityId));
            params.put("postalCode", editTextPostalCode.getText().toString());
            params.put("phone", editTextPhone.getText().toString());
            params.put("email", Globals.getStringDataPreference(AddressActivity.this,"emailuser"));

            if (!isCreate && userAddressId != null && userAddressId > 0) {
                params.put("userAddressId", String.valueOf(userAddressId));
            }

            String url  = "users/updateAddress";
            if (isCreate) {
                url  = "users/addAddress";
            }

            setLog(url);

            request.getJSONFromUrl(
                Globals.getApiUrl(url), params, getResources().getString(R.string.text_address_error),
                new RequestCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        long id = 0;
                        try {
                            JSONObject userAddress  = response.getJSONObject("userAddress");
                            id                      = userAddress.getLong("id");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (id == 0) {
                            //error
                            Globals.showAlert(
                                    getResources().getString(R.string.text_address_error),
                                    getResources().getString(isCreate ? R.string.text_add_user_address_error_unknown : R.string.text_update_user_address_error_unknown),
                                    activity
                            );
                            hideLoading();
                        } else {
                            Globals.showToast(getResources().getString(isCreate ? R.string.text_add_user_address_success : R.string.text_update_user_address_success), context);
                            hideLoading();

                            Intent data = new Intent();
                            data.putExtra("provinceId", String.valueOf(provinceId));
                            data.putExtra("cityId", String.valueOf(cityId));
                            data.putExtra("firstName", editTextFirstName.getText().toString());
                            data.putExtra("lastName", editTextLastName.getText().toString());
                            data.putExtra("address", editTextAddress.getText().toString());
                            data.putExtra("provinceName", textViewProvince.getText().toString());
                            data.putExtra("cityName", textViewCity.getText().toString());
                            data.putExtra("postalCode",editTextPostalCode.getText().toString() );
                            data.putExtra("phone", editTextPhone.getText().toString());
                            data.putExtra("email", editTextEmail.getText().toString());
                            setResult(RESULT_OK,data);
                            finish();
                        }

                    }

                    @Override
                    public void onError(VolleyError error) {
                        hideLoading();
                    }
                }
            );
        }
    }

    protected void deleteAddress() {
        if (!isLoading()) {
            showLoading();
        }

        Map<String, String> params = new HashMap<String, String>();
        params.put("clientId", Constant.CLIENT_ID);
        params.put("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
        params.put("userAddressId", String.valueOf(userAddressId));

        request.getJSONFromUrl(
                Globals.getApiUrl("users/deleteAddress"), params, getResources().getString(R.string.text_address_error),
                new RequestCallback() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Boolean flag    = false;
                        try {
                            flag  = response.getBoolean("flag");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (!flag) {
                            //error
                            Globals.showAlert(
                                    getResources().getString(R.string.text_address_error),
                                    getResources().getString(R.string.text_delete_user_address_error_unknown),
                                    activity
                            );
                            hideLoading();
                        } else {
                            Globals.showToast(getResources().getString(R.string.text_delete_user_address_success), context);
                            hideLoading();

                            if (Globals.getIntDataPreference(AddressActivity.this,"posisi")>0){
                                Globals.setDataPreference(AddressActivity.this,"posisi",Globals.getIntDataPreference(AddressActivity.this,"posisi")-1);
                            }

                            setResult(RESULT_OK);
                            finish();
                        }

                    }

                    @Override
                    public void onError(VolleyError error) {
                        hideLoading();
                    }
                }
        );
    }

    protected Boolean validateEditTextStringRequired(Boolean isGood) {
        if (isGood) {
            EditText[] requiredFields       = new EditText[]{editTextFirstName, editTextLastName, editTextAddress, editTextPostalCode, editTextPhone};
            String[] requiredFieldNames     = new String[]{
                    getResources().getString(R.string.hint_first_name),
                    getResources().getString(R.string.hint_last_name),
                    getResources().getString(R.string.hint_address),
                    getResources().getString(R.string.hint_postal_code),
                    getResources().getString(R.string.hint_phone),
            };

            int i = 0;
            for (EditText field : requiredFields) {
                if (field.getText().toString().trim().equals("")) {
                    Globals.showAlert(
                            getResources().getString(R.string.text_address_error),
                            String.format(getResources().getString(R.string.text_add_error_required), requiredFieldNames[i]),
                            activity
                    );
                    isGood = false;
                    break;
                }
                i++;
            }
        }
        return isGood;
    }

    protected Boolean validateSelectRequired(Boolean isGood) {
        if (isGood) {
            Long[] requiredFields           = new Long[]{provinceId, cityId};
            String[] requiredFieldNames     = new String[]{
                    getResources().getString(R.string.hint_province),
                    getResources().getString(R.string.hint_city),
            };

            int i = 0;
            for (Long field : requiredFields) {
                if (field == null || field <= 0) {
                    Globals.showAlert(
                            getResources().getString(R.string.text_address_error),
                            String.format(getResources().getString(R.string.text_add_error_required), requiredFieldNames[i]),
                            activity
                    );
                    isGood = false;
                    break;
                }
                i++;
            }
        }
        return isGood;
    }

    protected void loadDataUserAddress() {
        if (!isLoading()) {
            showLoading();
        }

        Map<String, String> params = new HashMap<String, String>();
        params.put("clientId", Constant.CLIENT_ID);
        params.put("accessToken", Globals.getStringDataPreference(context, Constant.P_ACCESS_TOKEN));
        request.getJSONFromUrl(Globals.getApiUrl("users/view"), params, getResources().getString(R.string.text_my_profile_error), new RequestCallback() {
            @Override
            public void onResponse(JSONObject response) {
                hideLoading();
                try {
                    JSONArray userAddresses = response.getJSONArray("userAddresses");
                    setLog( "== "+response.toString());

                    for (int i = 0; i < userAddresses.length(); i++) {
                        JSONObject userAddress  = userAddresses.getJSONObject(i);
                        if (userAddressId != null && userAddressId > 0 && userAddressId == userAddress.getLong("id")) {
                            editTextFirstName. setText(userAddress.getString("first_name"));
                            editTextLastName.setText(userAddress.getString("last_name"));
                            editTextAddress.setText(userAddress.getString("address"));
                            editTextPostalCode.setText(userAddress.getString("postal_code"));
                            editTextPhone.setText(userAddress.getString("phone"));
                            editTextEmail.setText(userAddress.getString("email"));

                            provinceId  = userAddress.getLong("province_id");
                            cityId      = userAddress.getLong("city_id");
                            textViewProvince.setText(userAddress.getString("provinceName"));
                            textViewCity.setText(userAddress.getString("cityName"));
                            break;
                        }
                    }

                    hideLoading();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(VolleyError error) {
                tapForReloadLayout.setVisibility(View.VISIBLE);
                hideLoading();
            }
        });
    }

    protected void initTapForReload() {
        buttonTapForReload  = (Button)findViewById(R.id.buttonTapForReload);
        buttonTapForReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tapForReloadLayout.setVisibility(View.GONE);
                loadDataUserAddress();
            }
        });

        tapForReloadLayout  = (RelativeLayout)findViewById(R.id.tapForReload);
        tapForReloadLayout.setVisibility(View.GONE);
    }
    //</editor-fold>
}
